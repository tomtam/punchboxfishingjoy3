//
//  FF3SimulatorTouch.m
//  Fish3
//
//  Created by RayMi on 15/3/16.
//
//

#import "FF3SimulatorTouch.h"
#import "SimulateTouch.h"
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>
@implementation FF3SimulatorTouch
+ (instancetype)shared{
    static FF3SimulatorTouch *share;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        share = [[FF3SimulatorTouch alloc]init];
    });
    return share;
}

- (void)simulatorTouch{
    CGSize size = [[UIScreen mainScreen]bounds].size;
    NSLog(@"自动点击屏幕");
    [[self class]simulateTouch:CGPointMake(240, 320-100)];
}

- (NSTimer *)timer{
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:.8 target:self selector:@selector(simulatorTouch) userInfo:nil repeats:YES];
        [self end];
    }
    return _timer;
}

- (void)start{
    NSLog(@"开始点击屏幕");
    [self.timer setFireDate:[NSDate date]];
}
- (void)end{
    NSLog(@"停止点击屏幕");

    [self.timer setFireDate:[NSDate distantFuture]];
}

+ (void)simulateTouch:(CGPoint)p{
    int r = [SimulateTouch simulateTouch:0 atPoint:p withType:STTouchDown];
    if (r == 0) printf("FF3 Error: Simutale touch down failed at (%d, %d).\n", (int)p.x, (int)p.y);
    r = [SimulateTouch simulateTouch:r atPoint:p withType:STTouchUp];
    if (r == 0) printf("FF3 Error: Simutale touch up failed at (%d, %d).\n", (int)p.x, (int)p.y);
}


@end
