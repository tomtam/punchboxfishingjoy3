
#import <UIKit/UIKit.h>
#include <notify.h>

#import "AppController.h"
#import "FF3SimulatorTouch.h"



%hook AppController
static void ApplicationWillEnterForeground(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo);
static void ApplicationDidEnterbackground(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo);
static void StartAutoClick(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo);
static void StopAutoClick(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo);

-(BOOL) application:(id)application didFinishLaunchingWithOptions:(id)options{
    // Override point for customization after application launch.
    
    CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, ApplicationWillEnterForeground, CFSTR("com.wg.ff3.willEnterForground"), NULL, CFNotificationSuspensionBehaviorDeliverImmediately);
    
    CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, ApplicationDidEnterbackground, CFSTR("com.wg.ff3.DidEnterbackground"), NULL, CFNotificationSuspensionBehaviorDeliverImmediately);

    CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, StartAutoClick, CFSTR("com.wg.ff3.startAutoClick"), NULL, CFNotificationSuspensionBehaviorDeliverImmediately);

    CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, StopAutoClick, CFSTR("com.wg.ff3.stopAutoClick"), NULL, CFNotificationSuspensionBehaviorDeliverImmediately);

    return %orig;
}



%new
static void ApplicationWillEnterForeground(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo){
    
    //   %log(@"接收到  willEnterForground 通知");
    AppController *app = [[UIApplication sharedApplication]delegate];
    [app applicationWillEnterForeground:[UIApplication sharedApplication]];
    
    
}
%new
static void ApplicationDidEnterbackground(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo){
    
    //   %log(@"接收到  willEnterForground 通知");
    AppController *app = [[UIApplication sharedApplication]delegate];
    [app applicationDidEnterBackground:[UIApplication sharedApplication]];
    
    
}
%new
static void StartAutoClick(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo){
    [[FF3SimulatorTouch shared] start];
}
%new
static void StopAutoClick(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo){
    [[FF3SimulatorTouch shared] end];

}

%end




