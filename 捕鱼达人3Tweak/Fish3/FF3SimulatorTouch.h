//
//  FF3SimulatorTouch.h
//  Fish3
//
//  Created by RayMi on 15/3/16.
//
//

#import <Foundation/Foundation.h>

@interface FF3SimulatorTouch : NSObject
@property (nonatomic,strong) NSTimer *timer;

+ (instancetype)shared;
- (void)start;
- (void)end;
@end



