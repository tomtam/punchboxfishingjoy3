#line 1 "/Users/raymi/Documents/Tweak/FF3/捕鱼达人3Tweak/Fish3/Fish3.xm"

#import <UIKit/UIKit.h>
#include <notify.h>

#import "AppController.h"
#import "FF3SimulatorTouch.h"



#include <logos/logos.h>
#include <substrate.h>
@class AppController; 
static BOOL (*_logos_orig$_ungrouped$AppController$application$didFinishLaunchingWithOptions$)(AppController*, SEL, id, id); static BOOL _logos_method$_ungrouped$AppController$application$didFinishLaunchingWithOptions$(AppController*, SEL, id, id); 

#line 10 "/Users/raymi/Documents/Tweak/FF3/捕鱼达人3Tweak/Fish3/Fish3.xm"

static void ApplicationWillEnterForeground(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo);
static void ApplicationDidEnterbackground(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo);
static void StartAutoClick(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo);
static void StopAutoClick(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo);

static BOOL _logos_method$_ungrouped$AppController$application$didFinishLaunchingWithOptions$(AppController* self, SEL _cmd, id application, id options){
    
    
    CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, ApplicationWillEnterForeground, CFSTR("com.wg.ff3.willEnterForground"), NULL, CFNotificationSuspensionBehaviorDeliverImmediately);
    
    CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, ApplicationDidEnterbackground, CFSTR("com.wg.ff3.DidEnterbackground"), NULL, CFNotificationSuspensionBehaviorDeliverImmediately);

    CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, StartAutoClick, CFSTR("com.wg.ff3.startAutoClick"), NULL, CFNotificationSuspensionBehaviorDeliverImmediately);

    CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, StopAutoClick, CFSTR("com.wg.ff3.stopAutoClick"), NULL, CFNotificationSuspensionBehaviorDeliverImmediately);

    return _logos_orig$_ungrouped$AppController$application$didFinishLaunchingWithOptions$(self, _cmd, application, options);
}




static void ApplicationWillEnterForeground(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo){
    
    
    AppController *app = [[UIApplication sharedApplication]delegate];
    [app applicationWillEnterForeground:[UIApplication sharedApplication]];
    
    
}

static void ApplicationDidEnterbackground(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo){
    
    
    AppController *app = [[UIApplication sharedApplication]delegate];
    [app applicationDidEnterBackground:[UIApplication sharedApplication]];
    
    
}

static void StartAutoClick(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo){
    [[FF3SimulatorTouch shared] start];
}

static void StopAutoClick(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo){
    [[FF3SimulatorTouch shared] end];

}






static __attribute__((constructor)) void _logosLocalInit() {
{Class _logos_class$_ungrouped$AppController = objc_getClass("AppController"); MSHookMessageEx(_logos_class$_ungrouped$AppController, @selector(application:didFinishLaunchingWithOptions:), (IMP)&_logos_method$_ungrouped$AppController$application$didFinishLaunchingWithOptions$, (IMP*)&_logos_orig$_ungrouped$AppController$application$didFinishLaunchingWithOptions$);} }
#line 65 "/Users/raymi/Documents/Tweak/FF3/捕鱼达人3Tweak/Fish3/Fish3.xm"
