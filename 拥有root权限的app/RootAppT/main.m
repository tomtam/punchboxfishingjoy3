//
//  main.m
//  RootAppT
//
//  Created by RayMi on 15/3/11.
//  Copyright (c) 2015年 RayMi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    setgid(0);
    setuid(0);
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
    
}
