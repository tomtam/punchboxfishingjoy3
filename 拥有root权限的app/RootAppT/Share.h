//
//  Share.h
//  WGTimeSet
//
//  Created by RayMi on 15/3/10.
//
//

#import <Foundation/Foundation.h>
#include <sys/time.h>
#include <unistd.h>

struct Time{
    struct timeval tv;
    struct timezone tz;
};

@interface Share : NSObject
+ (instancetype)share;

+ (struct Time)getTimeWithDate:(NSDate *)date;
+ (int )resetTime:(struct Time)time;

+ (void)resetTimeZero;

@end
