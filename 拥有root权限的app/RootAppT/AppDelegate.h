//
//  AppDelegate.h
//  RootAppT
//
//  Created by RayMi on 15/3/11.
//  Copyright (c) 2015年 RayMi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

