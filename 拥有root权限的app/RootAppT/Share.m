//
//  Share.m
//  WGTimeSet
//
//  Created by RayMi on 15/3/10.
//
//

#import "Share.h"

@interface Share()

@end

@implementation Share
+ (instancetype)share{
    static Share *share_;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        share_ = [[Share alloc]init];
    });
    return share_;
}

#pragma mark -
+ (struct Time)getTimeWithDate:(NSDate *)date{
    NSLog(@"-----------%s-----------",__FUNCTION__);
    
    struct timeval tv;
    struct timezone tz;
    
    int scuess = gettimeofday(&tv, &tz);
    NSLog(@"获取系统时间成功值：%d,tv:%ld,%d,tz:%d,%d",scuess,tv.tv_sec,tv.tv_usec,tz.tz_minuteswest,tz.tz_dsttime);
    
    //将tv的秒值修改为date的数值
    NSTimeInterval interval = [date timeIntervalSince1970];
    tv.tv_sec = (long)interval;
    
    NSLog(@"根据NSDate修改tv.tv_usec：tv:%ld,%d,tz:%d,%d",tv.tv_sec,tv.tv_usec,tz.tz_minuteswest,tz.tz_dsttime);

    struct Time time;
    time.tv = tv;
    time.tz = tz;
    
    return time;
    
}

+ (int )resetTime:(struct Time)time{
    NSLog(@"-----------%s-----------",__FUNCTION__);

    int scuess = settimeofday(&time.tv, &time.tz);
    NSLog(@"设置系统时间成功值：%d,tv:%ld,%d,tz:%d,%d",scuess,time.tv.tv_sec,time.tv.tv_usec,time.tz.tz_minuteswest,time.tz.tz_dsttime);
    return scuess;
}

+ (void)resetTimeZero{
    struct timeval tv;
    struct timezone tz;
    
    int scuess = gettimeofday(&tv, &tz);
    
    NSLog(@"------------------------");
    NSLog(@"获取成功失败：%d",scuess);
    
    NSLog(@"%ld,%d,%d,%d",tv.tv_sec,tv.tv_usec,tz.tz_dsttime,tz.tz_minuteswest   );
    
    tv.tv_sec = 3600;
    
    scuess = settimeofday(&tv, &tz);
    
    NSLog(@"设置成功失败：%d",scuess);
    
    //置0，检测日期时间
    NSLog(@"置0后---------");
    scuess = gettimeofday(&tv, &tz);
    NSLog(@"%ld,%d,%d,%d",tv.tv_sec,tv.tv_usec,tz.tz_dsttime,tz.tz_minuteswest   );
    NSDate *date = [NSDate date];
    NSLog(@"%@",date);
    NSLog(@"interval:%lf",date.timeIntervalSince1970);
    
    
}






@end
