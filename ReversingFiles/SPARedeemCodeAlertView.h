/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>
#import "UIAlertViewDelegate.h"

@class NSString;


@interface SPARedeemCodeAlertView : NSObject <UIAlertViewDelegate> {
	NSString* redeemCode;
}
@property(readonly, copy) NSString* debugDescription;
@property(readonly, copy) NSString* description;
@property(readonly, assign) Class superclass;
@property(readonly, assign) unsigned hash;
@property(retain, nonatomic) NSString* redeemCode;	// @synthesize
 -(void)setRedeemCode:(id)code;
 -(id)redeemCode;
-(void)alertView:(id)view clickedButtonAtIndex:(int)index;
-(void)showAlertView:(id)view cancelButton:(id)button;
-(void)resultMessage:(id)message content:(id)content cancel:(id)cancel;
-(void)exchange:(id)exchange ok:(id)ok cancel:(id)cancel;
@end

