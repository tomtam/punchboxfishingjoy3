/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>

@class NSString, NSData;

@interface CMHTTPPostedFile : NSObject {
	NSString* _fileName;
	NSString* _contentType;
	NSString* _transferEncoding;
	NSData* _fileData;
}
@property(readonly, assign, nonatomic) NSString* transferEncoding;	// @synthesize=_transferEncoding
@property(readonly, assign, nonatomic) NSData* fileData;	// @synthesize=_fileData
@property(readonly, assign, nonatomic) NSString* contentType;	// @synthesize=_contentType
@property(readonly, assign, nonatomic) NSString* fileName;	// @synthesize=_fileName
 -(id)transferEncoding;
 -(id)contentType;
 -(id)fileData;
 -(id)fileName;
-(void)dealloc;
-(id)initWithFilePath:(id)filePath contentType:(id)type;
-(id)initWithFileName:(id)fileName data:(id)data contentType:(id)type transferEncoding:(id)encoding;
-(id)initWithFileName:(id)fileName data:(id)data contentType:(id)type;
@end

