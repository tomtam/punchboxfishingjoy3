/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>


@interface SSCQZoneApiProxy : NSObject {
	id _apiObject;
	id _apiDelegate;
}
@property(assign, nonatomic) id apiDelegate;	// @synthesize=_apiDelegate
 -(void)setApiDelegate:(id)delegate;
 -(id)apiDelegate;
-(void)sendStoryResponse:(id)response;
-(void)tencentDidNotNetWork;
-(void)tencentDidNotLogin:(BOOL)tencent;
-(void)tencentDidLogin;
-(void)setOpenId:(id)anId;
-(void)setExpirationDate:(id)date;
-(void)setAccessToken:(id)token;
-(id)openId;
-(id)expirationDate;
-(id)accessToken;
-(BOOL)addShareWithParams:(id)params;
-(BOOL)sendStory:(id)story friendList:(id)list;
-(BOOL)authorize:(id)authorize;
-(void)dealloc;
-(id)initWithAppId:(id)appId;
@end

