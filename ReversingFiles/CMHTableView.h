/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "捕鱼达人3-Structs.h"
#import <UIKit/UIView.h>
#import "UIScrollViewDelegate.h"

@class NSMutableDictionary, UIScrollView, NSMutableArray;
@protocol CMHTableViewDataSource, CMHTableViewDelegate;

@interface CMHTableView : UIView <UIScrollViewDelegate> {
	NSMutableDictionary* _reuseItemDictionary;
	NSMutableArray* _visibleItemArray;
	id<CMHTableViewDataSource> _dataSource;
	id<CMHTableViewDelegate> _delegate;
	NSMutableArray* _itemsMeasureArray;
	BOOL _needLayout;
	int _itemCount;
	float _itemWidth;
	float _currX;
	UIScrollView* _contentView;
	BOOL showsHorizontalScrollIndicator;
}
@property(assign, nonatomic) BOOL showsHorizontalScrollIndicator;	// @synthesize
@property(assign, nonatomic) float itemWidth;	// @synthesize=_itemWidth
@property(assign, nonatomic) id<CMHTableViewDelegate> delegate;	// @synthesize=_delegate
@property(assign, nonatomic) id<CMHTableViewDataSource> dataSource;	// @synthesize=_dataSource
 -(void)setItemWidth:(float)width;
 -(float)itemWidth;
 -(void)setDelegate:(id)delegate;
 -(id)delegate;
 -(void)setDataSource:(id)source;
 -(id)dataSource;
-(void)scrollViewDidScroll:(id)scrollView;
-(void)recoverItem:(id)item;
-(void)recoverAllVisibleItems;
-(id)dequeueReusableItemWithIdentifier:(id)identifier;
-(void)reloadData;
-(void)setFrame:(CGRect)frame;
-(void)layoutSubviews;
 -(BOOL)showsHorizontalScrollIndicator;
 -(void)setShowsHorizontalScrollIndicator:(BOOL)indicator;
-(void)dealloc;
-(id)initWithFrame:(CGRect)frame;
@end

