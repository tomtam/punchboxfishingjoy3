/*! \mainpage Strings analysis
Analysis of Strings found in the executable
\section SQL SQL Strings
1 14482 @_NSSelectorFromString

2 18021 DELETE FROM %@ WHERE id =%d

3 18022 DELETE FROM %@ WHERE id between %d and %d

4 18023 DELETE FROM configcache WHERE "product"="%@"

5 18024 DELETE FROM levelparams

6 23071 INSERT INTO "%@"("eventid","type","sid","ts","sts","am") VALUES (%d,%d,'%@',%.0f,%.0f,'%@')

7 23072 INSERT INTO "%@"("eventname") VALUES ('%@')

8 23073 INSERT INTO "%@"("levelname") VALUES ('%@')

9 23074 INSERT INTO "%@"("levelname","levelstatus","attemptcount","initialts","lbegints","lendts","totaltime") VALUES ("%@",%d,%d,%.0f,%.0f,%.0f,%d)

10 23075 INSERT INTO "%@"("levelname","levelstatus","timetaken","attemptcount","attempttime") VALUES ("%@",%d,%ld,%d,%ld)

11 23076 INSERT INTO "%@"("name", "value") VALUES ('%@', '%@')

12 23077 INSERT INTO "%@"("title","description","itemtype","price","countrycode","currencycode","productid","transactiondate","transactionid","quantity","transactiontype") VALUES ("%@","%@",%d,%f,"%@","%@","%@",%lld,"%@",%ld,%ld)

13 27356 SELECT "attemptcount","lbegints","levelfinished" FROM %@ WHERE "levelname" LIKE "%@"

14 27357 SELECT "attemptcount","lbegints","totaltime" FROM %@ WHERE "levelname" LIKE "%@"

15 27358 SELECT "title","description","itemtype","price","countrycode","currencycode","productid","transactiondate","transactionid","quantity","transactiontype" FROM %@ WHERE id =%d

16 27359 SELECT * FROM  configcache WHERE "product"="%@"

17 27360 SELECT count(*) FROM %@

18 27361 SELECT count(*) FROM customevent

19 27362 SELECT count(*) FROM eventlist

20 27363 SELECT count(*) FROM levelbegin

21 27364 SELECT count(*) FROM levelend

22 27365 SELECT eventid,type FROM %@ WHERE id BETWEEN %d AND %d

23 27366 SELECT eventname FROM %@ WHERE id =%d

24 27367 SELECT id FROM %@ ORDER BY id DESC LIMIT 1

25 27368 SELECT id,eventid,type FROM %@

26 27369 SELECT id,eventid,type,ts,am,sid,sts FROM %@ LIMIT %d

27 27370 SELECT levelname FROM %@ WHERE id =%d

28 27371 SELECT levelname,levelstatus,timetaken,attemptcount,attempttime FROM %@ WHERE id =%d

29 27372 SELECT name, value FROM %@ WHERE id =%d

30 30592 UPDATE "%@" SET "attemptcount" = %d,"lbegints" = %.0f,"levelfinished" = 0 WHERE "levelname" LIKE "%@"

31 30593 UPDATE "%@" SET "levelstatus" = %d,"attemptcount" = %d,"lendts" = %.0f,"totaltime"= %d,"levelfinished" = 1 WHERE "levelname" LIKE "%@"

32 33303 _NSSelectorFromString

33 40205 delete from %@

34 40206 delete from %@ where %@ = "%@"

35 40207 delete from %@ where %@ = %lu

36 40208 delete from %@ where %@ in (select %@ from %@ order by %@ ASC limit %u)

37 40209 delete from %@ where %@ in (select %@ from %@ where %@ + %lf <= strftime('%%s','now')*1000  and %@='%@') 

38 47362 insert into %@ values (NULL,"%@","%@",%llu,"%@")

39 47363 insert into configcache(product, binarydata) VALUES(?,?);

40 47366 insertIntoEventList:withEventId:

41 5322 %@problem occured in select from levelparams command,IMLevelEnd%@

42 5323 %@problem occured while preparing insert into levelparams command,IMLevelBegin%@

43 5324 %@problem occured while preparing select from levelparams command%@

44 53817 select %@,%@ from %@ where %@="%@" and %@="%@" order by %@ ASC limit 1

45 53818 select * from %@

46 53819 select count(%@) from %@

47 53820 select count(%@) from %@ where %@="%@"

48 53821 select count(%@) from %@ where %@="%@" and %@="%@"


\section URI URI strings
\code1 10             xmlns:exif="http://ns.adobe.com/exif/1.0/">

2 11             xmlns:tiff="http://ns.adobe.com/tiff/1.0/"

3 12             xmlns:xmp="http://ns.adobe.com/xap/1.0/"

4 12361 ://itunes.apple.com/

5 12362 ://phobos.apple.com/

6 12431 :]://%[^

7 12579 <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">

8 17688 Couldn't read a file:// file

9 30127 This is a device which is not listed in this category. Please visit https://github.com/inderkumarrathore/UIDevice-Hardware and add a comment there.

10 32041 You need to read the OpenSSL FAQ, http://www.openssl.org/support/faq.html

11 32680 \b((ftp|https?)://[-\w]+(\.\w[-\w]*)+|(?i:[a-z0-9](?:[-a-z0-9]*[a-z0-9])?\.)+(?-i:com\b|edu\b|biz\b|gov\b|in(?:t|fo)\b|mil\b|net\b|org\b|[a-z][a-z]\b))(:\d+)?(/[^.!,?;"'<>()\[\]{}\s\x7F-\xFF]*(?:[.!,?]+[^.!,?;"'<>()\[\]{}\s\x7F-\xFF]+)*)?

12 32730 \w://.*

13 38924 coco://

14 38925 coco://click?

15 38926 coco://close

16 38927 coco://onload?

17 38928 coco://openh5?redirect=

18 38929 coco://openmoregame?

19 38930 coco://refresh

20 38931 coco://showmore?

21 3939 # http://curl.haxx.se/rfc/cookie_spec.html

22 41453 failed to resume file:// transfer

23 41839 ftp://

24 41840 ftp://%s:%s@%s

25 44    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">

26 45771 http://%@:%@

27 45772 http://192.168.21.84:18000

28 45773 http://3.fishingjoy.com

29 45774 http://a.ai.inmobi.com/v2/ad.html

30 45775 http://a.appcpa.net

31 45776 http://adlog.flurry.com/postAdLog.do

32 45777 http://ads.flurry.com/v10/getAds.do

33 45778 http://api2.sharesdk.cn:5566

34 45779 http://data.bi.chukong-inc.com/sdk

35 45780 http://data.flurry.com/aas.do

36 45781 http://dock.inmobi.com/carb/v1/i

37 45782 http://dock.inmobi.com/carb/v1/o

38 45783 http://excode.appget.cn/?exchangecode2/codeVerifi/

39 45784 http://fishingjoy3.coco.cn:

40 45785 http://fishingjoy3.coco.cn:30011/

41 45786 http://fishingjoy3.coco.cn:30012/appleMakeAuthCode

42 45787 http://fishingjoy3.coco.cn:30012/appleVerify

43 45788 http://fishingjoy3.coco.cn:30013/checkTime

44 45789 http://i.w.inmobi.com/showad.asm?

45 45790 http://itunes.apple.com

46 45791 http://itunes.apple.com/cn/app/id414478124?mt=8

47 45792 http://pubads.g.doubleclick.net/activity;

48 45793 http://s.sharesdk.cn

49 45794 http://service.cocounion.com/core/cached

50 45795 http://service.cocounion.com/core/url

51 45796 http://service.cocounion.com/push/collect

52 45797 http://stats.cocounion.com

53 45798 http://stats.cocounion.com/event/chukong/

54 45799 http://up.sharesdk.cn

55 45800 http://weibo.com/%@

56 45801 http://wsq.qq.com/reflow/257417332

57 45802 http://www.googleadservices.com/pagead/conversion

58 45803 http://www.googleadservices.com/pagead/conversion/%@/?

59 45804 http://www.sharesdk.cn

60 45831 https://adlog.flurry.com/postAdLog.do

61 45832 https://ads.flurry.com/v10/getAds.do

62 45833 https://api.weibo.com/2/comments/create.json

63 45834 https://api.weibo.com/2/comments/reply.json

64 45835 https://api.weibo.com/2/comments/show.json

65 45836 https://api.weibo.com/2/favorites.json

66 45837 https://api.weibo.com/2/favorites/create.json

67 45838 https://api.weibo.com/2/friendships/create.json

68 45839 https://api.weibo.com/2/friendships/followers.json

69 45840 https://api.weibo.com/2/friendships/friends.json

70 45841 https://api.weibo.com/2/statuses/repost.json

71 45842 https://api.weibo.com/2/statuses/update.json

72 45843 https://api.weibo.com/2/statuses/upload_url_text.json

73 45844 https://api.weibo.com/2/statuses/user_timeline.json

74 45845 https://api.weibo.com/2/users/show.json

75 45846 https://api.weibo.com/oauth2/access_token

76 45847 https://api.weibo.com/oauth2/authorize

77 45848 https://api.weixin.qq.com/sns/oauth2/access_token

78 45849 https://api.weixin.qq.com/sns/userinfo

79 45850 https://appcloud-node-stage.corp.flurry.com/

80 45851 https://appcloud.flurry.com/

81 45852 https://buy.itunes.apple.com/verifyReceipt

82 45853 https://d.appsdt.com/download/tracker

83 45854 https://d.appsdt.com/sdkdwnldbeacon.html

84 45855 https://data.flurry.com/aas.do

85 45856 https://e-ltvp.inmobi.com/storm/v1/event

86 45857 https://inmobisdk-a.akamaihd.net/sdk/configs/400/rootConfig.json

87 45858 https://itunes.apple.com

88 45859 https://itunes.apple.com/cn/app/id886104377

89 45860 https://open.weibo.cn/oauth2/authorize

90 45861 https://rules-ltvp.inmobi.com/v2/rules.json

91 45862 https://upload.api.weibo.com/2/statuses/upload.json

92 45863 https://www.apple.com/appleca/0

93 4897 %15[^?&/:]://%c

94 4966 %@%@://

95 4967 %@%@://%@

96 5013 %@://

97 5014 %@://%@

98 5015 %@://%@%@

99 5016 %@://oauth

100 5017 %@://pay/

101 52670 radr://5614542

102 5553 %http://www.apple.com/appleca/root.crl0

103 55818 sinaweibohdsso://login

104 55820 sinaweibosso://login

105 55893 sms://

106 5684 %s://%s

107 5685 %s://%s%s%s:%hu%s%s%s

108 56940 tel://

109 56944 telprompt://%@

110 59066 wechat://

111 59074 weixin://app/%@/

112 6262 'http://www.apple.com/appleca/iphone.crl0

113 7209 (https?://){1}[A-Za-z0-9_\.\-/:\?&%=,;'\[\]\{\}`~!@#\$\^\*\(\)\+\\\|]+

114 7210 (https?://){1}[A-Za-z0-9_\.\-/:\?&%=,;\[\]\{\}`~!@#\$\^\*\(\)\+\\\|]+

\endcode*/
/*!\page Application-Views ViewControllers
 \section appASIAuthenticationDialogasd ASIAuthenticationDialog
ASIAuthenticationDialog click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[ASIAuthenticationDialog alloc]init]');">load</a></div></div>
\section appCMGridViewasd CMGridView
CMGridView click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CMGridView alloc]init]');">load</a></div></div>
\section appCMGridViewDelegateasd CMGridViewDelegate
CMGridViewDelegate click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CMGridViewDelegate alloc]init]');">load</a></div></div>
\section appCMHTableViewasd CMHTableView
CMHTableView click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CMHTableView alloc]init]');">load</a></div></div>
\section appCMImageHeaderTableViewasd CMImageHeaderTableView
CMImageHeaderTableView click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CMImageHeaderTableView alloc]init]');">load</a></div></div>
\section appCMPageViewasd CMPageView
CMPageView click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CMPageView alloc]init]');">load</a></div></div>
\section appCMPageViewDelegateasd CMPageViewDelegate
CMPageViewDelegate click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CMPageViewDelegate alloc]init]');">load</a></div></div>
\section appCMQuiltViewasd CMQuiltView
CMQuiltView click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CMQuiltView alloc]init]');">load</a></div></div>
\section appCMRefreshTableHeaderViewasd CMRefreshTableHeaderView
CMRefreshTableHeaderView click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CMRefreshTableHeaderView alloc]init]');">load</a></div></div>
\section appCSAppSectionCellasd CSAppSectionCell
CSAppSectionCell click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CSAppSectionCell alloc]init]');">load</a></div></div>
\section appCSBannerViewasd CSBannerView
CSBannerView click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CSBannerView alloc]init]');">load</a></div></div>
\section appCSFocusViewasd CSFocusView
CSFocusView click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CSFocusView alloc]init]');">load</a></div></div>
\section appCSInterstitialViewasd CSInterstitialView
CSInterstitialView click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CSInterstitialView alloc]init]');">load</a></div></div>
\section appCSMoreGameViewasd CSMoreGameView
CSMoreGameView click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CSMoreGameView alloc]init]');">load</a></div></div>
\section appCSNMGAppInfoVCasd CSNMGAppInfoVC
CSNMGAppInfoVC click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CSNMGAppInfoVC alloc]init]');">load</a></div></div>
\section appCSNMGCatePageVCasd CSNMGCatePageVC
CSNMGCatePageVC click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CSNMGCatePageVC alloc]init]');">load</a></div></div>
\section appCSNMGMainPageVCasd CSNMGMainPageVC
CSNMGMainPageVC click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CSNMGMainPageVC alloc]init]');">load</a></div></div>
\section appCSNMGPicsVCasd CSNMGPicsVC
CSNMGPicsVC click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CSNMGPicsVC alloc]init]');">load</a></div></div>
\section appCSWebSiteViewasd CSWebSiteView
CSWebSiteView click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[CSWebSiteView alloc]init]');">load</a></div></div>
\section appIMActivityViewControllerasd IMActivityViewController
IMActivityViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[IMActivityViewController alloc]init]');">load</a></div></div>
\section appIMAdDelegateasd IMAdDelegate
IMAdDelegate click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[IMAdDelegate alloc]init]');">load</a></div></div>
\section appIMAdTrackerasd IMAdTracker
IMAdTracker click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[IMAdTracker alloc]init]');">load</a></div></div>
\section appIMAdWebViewControllerasd IMAdWebViewController
IMAdWebViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[IMAdWebViewController alloc]init]');">load</a></div></div>
\section appIMAsyncPingWebViewRequestasd IMAsyncPingWebViewRequest
IMAsyncPingWebViewRequest click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[IMAsyncPingWebViewRequest alloc]init]');">load</a></div></div>
\section appIMEmbeddedWebViewControllerasd IMEmbeddedWebViewController
IMEmbeddedWebViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[IMEmbeddedWebViewController alloc]init]');">load</a></div></div>
\section appIMEventEditViewControllerasd IMEventEditViewController
IMEventEditViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[IMEventEditViewController alloc]init]');">load</a></div></div>
\section appIMMailViewControllerasd IMMailViewController
IMMailViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[IMMailViewController alloc]init]');">load</a></div></div>
\section appIMManagerBannerActionasd IMManagerBannerAction
IMManagerBannerAction click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[IMManagerBannerAction alloc]init]');">load</a></div></div>
\section appIMManagerBannerNetworkasd IMManagerBannerNetwork
IMManagerBannerNetwork click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[IMManagerBannerNetwork alloc]init]');">load</a></div></div>
\section appIMSKStoreProductViewControllerasd IMSKStoreProductViewController
IMSKStoreProductViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[IMSKStoreProductViewController alloc]init]');">load</a></div></div>
\section appIMSMSViewControllerasd IMSMSViewController
IMSMSViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[IMSMSViewController alloc]init]');">load</a></div></div>
\section appIMWebViewasd IMWebView
IMWebView click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[IMWebView alloc]init]');">load</a></div></div>
\section appIMWebViewControllerasd IMWebViewController
IMWebViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[IMWebViewController alloc]init]');">load</a></div></div>
\section appQuiltViewDelegateasd QuiltViewDelegate
QuiltViewDelegate click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[QuiltViewDelegate alloc]init]');">load</a></div></div>
\section appRootViewControllerasd RootViewController
RootViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[RootViewController alloc]init]');">load</a></div></div>
\section appSSFlatShareActionSheetViewControllerasd SSFlatShareActionSheetViewController
SSFlatShareActionSheetViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[SSFlatShareActionSheetViewController alloc]init]');">load</a></div></div>
\section appSSFlatShareContentViewControllerasd SSFlatShareContentViewController
SSFlatShareContentViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[SSFlatShareContentViewController alloc]init]');">load</a></div></div>
\section appSSFullScreenPopupAuthViewControllerasd SSFullScreenPopupAuthViewController
SSFullScreenPopupAuthViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[SSFullScreenPopupAuthViewController alloc]init]');">load</a></div></div>
\section appSSFullScreenPopupAuthViewNavigationControllerasd SSFullScreenPopupAuthViewNavigationController
SSFullScreenPopupAuthViewNavigationController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[SSFullScreenPopupAuthViewNavigationController alloc]init]');">load</a></div></div>
\section appSSModalAuthViewControllerasd SSModalAuthViewController
SSModalAuthViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[SSModalAuthViewController alloc]init]');">load</a></div></div>
\section appSSPopupAuthViewControllerasd SSPopupAuthViewController
SSPopupAuthViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[SSPopupAuthViewController alloc]init]');">load</a></div></div>
\section appSSPowerByViewControllerasd SSPowerByViewController
SSPowerByViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[SSPowerByViewController alloc]init]');">load</a></div></div>
\section appSSShareListViewControllerasd SSShareListViewController
SSShareListViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[SSShareListViewController alloc]init]');">load</a></div></div>
\section appSSSinaWeiboAuthViewasd SSSinaWeiboAuthView
SSSinaWeiboAuthView click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[SSSinaWeiboAuthView alloc]init]');">load</a></div></div>
\section appSSiPadFlatShareViewControllerasd SSiPadFlatShareViewController
SSiPadFlatShareViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[SSiPadFlatShareViewController alloc]init]');">load</a></div></div>
\section appShareSDKFlatShareViewControllerasd ShareSDKFlatShareViewController
ShareSDKFlatShareViewController click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[ShareSDKFlatShareViewController alloc]init]');">load</a></div></div>
\section appUIScrollViewDelegateasd UIScrollViewDelegate
UIScrollViewDelegate click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[UIScrollViewDelegate alloc]init]');">load</a></div></div>
\section appUIWebLoaderasd UIWebLoader
UIWebLoader click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[UIWebLoader alloc]init]');">load</a></div></div>
\section appUIWebViewDelegateasd UIWebViewDelegate
UIWebViewDelegate click to <a href="javascript:command('UIApp.keyWindow.rootViewController=[[UIWebViewDelegate alloc]init]');">load</a></div></div>
*/
