/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>
#import "ISSPlatformShareContentEntity.h"
#import "NSCoding.h"

@class NSMutableDictionary, SSCLocationCoordinate2D, NSString;
@protocol ISSCAttachment;

@interface SSSinaWeiboShareContentEntity : NSObject <ISSPlatformShareContentEntity, NSCoding> {
	NSMutableDictionary* _dict;
	NSString* content;
	id<ISSCAttachment> image;
	SSCLocationCoordinate2D* locationCoordinate;
}
@property(retain, nonatomic) SSCLocationCoordinate2D* locationCoordinate;	// @synthesize
@property(retain, nonatomic) id<ISSCAttachment> image;	// @synthesize
@property(copy, nonatomic) NSString* content;	// @synthesize
-(id)initWithCoder:(id)coder;
-(void)encodeWithCoder:(id)coder;
-(void)setField:(id)field forName:(id)name;
-(id)fieldForName:(id)name;
-(void)parseWithContent:(id)content;
 -(void)setLocationCoordinate:(id)coordinate;
 -(id)locationCoordinate;
 -(void)setImage:(id)image;
 -(id)image;
 -(void)setContent:(id)content;
 -(id)content;
-(void)dealloc;
-(id)init;
@end

