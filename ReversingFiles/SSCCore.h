/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "CMEventDispatcher.h"

@class SSCUrlCachePool, NSArray, NSMutableDictionary, NSString, CMCacheManager;

@interface SSCCore : CMEventDispatcher {
	NSMutableDictionary* _accounts;
	NSMutableDictionary* _commonObjects;
	NSString* _deviceKey;
	CMCacheManager* _cacheManager;
	SSCUrlCachePool* _urlCachePool;
	int _urlCachePoolRefCount;
	NSArray* registerAccountKeys;
}
@property(readonly, assign, nonatomic) CMCacheManager* cacheManager;	// @synthesize=_cacheManager
@property(readonly, assign, nonatomic) SSCUrlCachePool* urlCachePool;	// @synthesize=_urlCachePool
@property(readonly, assign, nonatomic) NSString* deviceKey;	// @synthesize=_deviceKey
@property(readonly, assign, nonatomic) NSArray* registerAccountKeys;	// @synthesize
+(id)getInstance;
 -(id)urlCachePool;
 -(id)deviceKey;
-(id)sdkVer;
-(id)getCommonObject:(id)object;
-(void)setCommonObject:(id)object forKey:(id)key;
-(void)releaseUrlCachePoolRef;
-(void)addUrlCachePoolRef;
-(id)cachePath;
-(BOOL)hasAuthorized:(id)authorized;
-(void)loginWithAccount:(id)account;
-(id)getAccountWithAppKey:(id)appKey;
 -(id)registerAccountKeys;
 -(id)cacheManager;
-(void)dealloc;
-(id)init;
@end

