/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>
#import "CSHTTPConnectionDelegate.h"

@class CSHTTPConnection, NSMutableDictionary, NSString;

@interface UIImageViewManager : NSObject <CSHTTPConnectionDelegate> {
	CSHTTPConnection* _httpDownload;
	NSMutableDictionary* _mdicURLKey;
}
@property(readonly, copy) NSString* debugDescription;
@property(readonly, copy) NSString* description;
@property(readonly, assign) Class superclass;
@property(readonly, assign) unsigned hash;
+(id)sharedInstance;
-(void)httpConnect:(id)connect finish:(id)finish with:(id)with;
-(void)httpConnect:(id)connect error:(id)error with:(id)with;
-(void)cancelDownload:(id)download;
-(void)downloadFile:(id)file from:(id)from showOn:(id)on withResult:(id)result;
-(void)dealloc;
-(id)init;
@end

