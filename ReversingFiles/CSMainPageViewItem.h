/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>

@class UITableView, CSMenuItem, NSArray;

@interface CSMainPageViewItem : NSObject {
	CSMenuItem* _menuItem;
	UITableView* _tableView;
	NSArray* _items;
}
@property(retain, nonatomic) NSArray* items;	// @synthesize=_items
@property(retain, nonatomic) UITableView* tableView;	// @synthesize=_tableView
@property(retain, nonatomic) CSMenuItem* menuItem;	// @synthesize=_menuItem
 -(void)setItems:(id)items;
 -(id)items;
 -(void)setTableView:(id)view;
 -(id)tableView;
 -(void)setMenuItem:(id)item;
 -(id)menuItem;
-(void)dealloc;
@end

