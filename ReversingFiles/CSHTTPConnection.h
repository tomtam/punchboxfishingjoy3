/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>

@class NSMutableArray;
@protocol CSHTTPConnectionDelegate;

@interface CSHTTPConnection : NSObject {
	int _numberOfURLConnection;
	NSMutableArray* _marrayTaskDic;
	int _maxNumberOfURLConnection;
	id<CSHTTPConnectionDelegate> _delegate;
}
@property(assign, nonatomic) id<CSHTTPConnectionDelegate> delegate;	// @synthesize=_delegate
@property(assign, nonatomic) int maxNumberOfURLConnection;	// @synthesize=_maxNumberOfURLConnection
 -(void)setDelegate:(id)delegate;
 -(id)delegate;
 -(void)setMaxNumberOfURLConnection:(int)urlconnection;
 -(int)maxNumberOfURLConnection;
-(void)startURLConnection;
-(void)connectionDidFinishLoading:(id)connection;
-(void)connection:(id)connection didReceiveData:(id)data;
-(void)connection:(id)connection didReceiveResponse:(id)response;
-(void)connection:(id)connection didFailWithError:(id)error;
-(void)clearRequest;
-(BOOL)cancelRequest:(id)request;
-(BOOL)requestWebDataWithRequest:(id)request andParam:(id)param cache:(BOOL)cache priority:(BOOL)priority;
-(BOOL)requestWebDataWithURL:(id)url andParam:(id)param cache:(BOOL)cache priority:(BOOL)priority;
-(BOOL)urlIsRequesting:(id)requesting;
-(BOOL)requestIsExist:(id)exist;
-(void)dealloc;
-(id)init;
@end

