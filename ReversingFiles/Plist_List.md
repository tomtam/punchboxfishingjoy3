Plist Files
=======
\section Plist1 achievement_light.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/achievement_light.plist
[ ->] {
    frames =     {
        "achievement_light_cell-4.png" =         {
            frame = "{{2,2},{18,18}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{1,1},{18,18}}";
            sourceSize = "{20,20}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "achievement_light.png";
        size = "{32,32}";
        smartupdate = "$TexturePacker:SmartUpdate:b969f1b417a817e6fde532cee7329cda$";
        textureFileName = "achievement_light.png";
    };
}
~~~~~~~~~~~~~
\section Plist2 achievement_max.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/achievement_max.plist
[ ->] {
    frames =     {
        "achievement_max_cell-01.png" =         {
            frame = "{{39,55},{55,11}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{55,11}}";
            sourceSize = "{55,11}";
        };
        "achievement_max_cell-02.png" =         {
            frame = "{{94,2},{51,23}}";
            offset = "{1,0}";
            rotated = 1;
            sourceColorRect = "{{3,1},{51,23}}";
            sourceSize = "{55,25}";
        };
        "achievement_max_cell-03.png" =         {
            frame = "{{52,39},{17,15}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{0,2},{17,15}}";
            sourceSize = "{17,17}";
        };
        "achievement_max_cell-s01.png" =         {
            frame = "{{2,2},{90,35}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{90,35}}";
            sourceSize = "{90,35}";
        };
        "achievement_max_cell-s02.png" =         {
            frame = "{{2,39},{80,35}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{80,35}}";
            sourceSize = "{80,35}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "achievement_max.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:b22980a3065fe2e5685295628768f9db$";
        textureFileName = "achievement_max.png";
    };
}
~~~~~~~~~~~~~
\section Plist3 achievement_star.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/achievement_star.plist
[ ->] {
    frames =     {
        "achievement_star_cell-1_00020.png" =         {
            frame = "{{2,80},{19,17}}";
            offset = "{1,-1}";
            rotated = 1;
            sourceColorRect = "{{2,3},{19,17}}";
            sourceSize = "{21,21}";
        };
        "achievement_star_cell-sg_00004.png" =         {
            frame = "{{28,54},{24,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{24,24}}";
            sourceSize = "{24,24}";
        };
        "achievement_star_cell-sg_00005.png" =         {
            frame = "{{2,54},{24,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{24,24}}";
            sourceSize = "{24,24}";
        };
        "achievement_star_cell-sg_00006.png" =         {
            frame = "{{28,28},{24,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{24,24}}";
            sourceSize = "{24,24}";
        };
        "achievement_star_cell-sg_00007.png" =         {
            frame = "{{2,28},{24,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{24,24}}";
            sourceSize = "{24,24}";
        };
        "achievement_star_cell-sg_00008.png" =         {
            frame = "{{28,2},{24,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{24,24}}";
            sourceSize = "{24,24}";
        };
        "achievement_star_cell-sg_00009.png" =         {
            frame = "{{2,2},{24,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{24,24}}";
            sourceSize = "{24,24}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "achievement_star.png";
        size = "{64,128}";
        smartupdate = "$TexturePacker:SmartUpdate:a5209c2fbb82f80e247f2ca58fd4419d$";
        textureFileName = "achievement_star.png";
    };
}
~~~~~~~~~~~~~
\section Plist4 achievement_tip.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/achievement_tip.plist
[ ->] {
    frames =     {
        "achievement_tip_cell-01.png" =         {
            frame = "{{2,422},{85,79}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,3},{85,79}}";
            sourceSize = "{85,85}";
        };
        "achievement_tip_cell-1.png" =         {
            frame = "{{2,190},{141,116}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{141,116}}";
            sourceSize = "{141,116}";
        };
        "achievement_tip_cell-2.png" =         {
            frame = "{{2,2},{124,44}}";
            offset = "{1,-1}";
            rotated = 0;
            sourceColorRect = "{{4,4},{124,44}}";
            sourceSize = "{130,50}";
        };
        "achievement_tip_cell-2d.png" =         {
            frame = "{{2,48},{140,119}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{140,119}}";
            sourceSize = "{140,119}";
        };
        "achievement_tip_cell-3d.png" =         {
            frame = "{{83,461},{31,32}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{31,32}}";
            sourceSize = "{31,32}";
        };
        "achievement_tip_cell-4d.png" =         {
            frame = "{{83,422},{38,37}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{38,37}}";
            sourceSize = "{38,37}";
        };
        "achievement_tip_cell-5d.png" =         {
            frame = "{{2,333},{111,87}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{111,87}}";
            sourceSize = "{111,87}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "achievement_tip.png";
        size = "{128,512}";
        smartupdate = "$TexturePacker:SmartUpdate:0c052e41ac0a9c8841347a5c5226c7d0$";
        textureFileName = "achievement_tip.png";
    };
}
~~~~~~~~~~~~~
\section Plist5 angle.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/angle.plist
[ ->] {
    frames =     {
        "angle_cell-angle_1.png" =         {
            frame = "{{2,2},{123,123}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{123,123}}";
            sourceSize = "{123,123}";
        };
        "angle_cell-angle_2.png" =         {
            frame = "{{2,127},{89,89}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{89,89}}";
            sourceSize = "{89,89}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "angle.png";
        size = "{128,256}";
        smartupdate = "$TexturePacker:SmartUpdate:132af992cbaf8e2c93c46d60907e1fe2$";
        textureFileName = "angle.png";
    };
}
~~~~~~~~~~~~~
\section Plist6 arrow.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/arrow.plist
[ ->] {
    frames =     {
        "arrow_cell-arrow_1.png" =         {
            frame = "{{2,2},{55,67}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{55,67}}";
            sourceSize = "{55,67}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "arrow.png";
        size = "{64,128}";
        smartupdate = "$TexturePacker:SmartUpdate:2aea4a566cda76ea0657b4f8c62a596b$";
        textureFileName = "arrow.png";
    };
}
~~~~~~~~~~~~~
\section Plist7 award_01.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/award_01.plist
[ ->] {
    frames =     {
        "award_01_cell-100.png" =         {
            frame = "{{2,25},{389,241}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{389,241}}";
            sourceSize = "{389,241}";
        };
        "award_01_cell-111.png" =         {
            frame = "{{393,132},{103,97}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{103,97}}";
            sourceSize = "{103,97}";
        };
        "award_01_cell-222.png" =         {
            frame = "{{2,875},{99,96}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{99,96}}";
            sourceSize = "{99,96}";
        };
        "award_01_cell-333.png" =         {
            frame = "{{393,25},{109,105}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{109,105}}";
            sourceSize = "{109,105}";
        };
        "award_01_cell-444.png" =         {
            frame = "{{2,333},{540,238}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{540,238}}";
            sourceSize = "{540,238}";
        };
        "award_01_cell-666.png" =         {
            frame = "{{2,2},{440,21}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{440,21}}";
            sourceSize = "{440,21}";
        };
        "award_01_cell-777.png" =         {
            frame = "{{393,231},{100,100}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{100,100}}";
            sourceSize = "{100,100}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "award_01.png";
        size = "{512,1024}";
        smartupdate = "$TexturePacker:SmartUpdate:5f96092e4180a98d38cd532f2c00c9c0$";
        textureFileName = "award_01.png";
    };
}
~~~~~~~~~~~~~
\section Plist8 award_02.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/award_02.plist
[ ->] {
    frames =     {
        "award_02_cell-2.png" =         {
            frame = "{{2,2},{100,100}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{100,100}}";
            sourceSize = "{100,100}";
        };
        "award_02_cell-3.png" =         {
            frame = "{{2,104},{61,61}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{61,61}}";
            sourceSize = "{61,61}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "award_02.png";
        size = "{128,256}";
        smartupdate = "$TexturePacker:SmartUpdate:a245e6e22a23f923a36d357ec50065be$";
        textureFileName = "award_02.png";
    };
}
~~~~~~~~~~~~~
\section Plist9 bar_coin.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/bar_coin.plist
[ ->] {
    frames =     {
        "bar_coin_cell-001.png" =         {
            frame = "{{2,221},{20,17}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,1},{20,17}}";
            sourceSize = "{20,19}";
        };
        "bar_coin_cell-bar_coin_00001.png" =         {
            frame = "{{71,71},{67,71}}";
            offset = "{0,-3}";
            rotated = 0;
            sourceColorRect = "{{19,20},{67,71}}";
            sourceSize = "{105,105}";
        };
        "bar_coin_cell-bar_coin_00003.png" =         {
            frame = "{{144,2},{67,71}}";
            offset = "{0,-3}";
            rotated = 1;
            sourceColorRect = "{{19,20},{67,71}}";
            sourceSize = "{105,105}";
        };
        "bar_coin_cell-bar_coin_00005.png" =         {
            frame = "{{71,2},{67,71}}";
            offset = "{0,-3}";
            rotated = 1;
            sourceColorRect = "{{19,20},{67,71}}";
            sourceSize = "{105,105}";
        };
        "bar_coin_cell-bar_coin_00007.png" =         {
            frame = "{{2,148},{67,71}}";
            offset = "{0,-3}";
            rotated = 0;
            sourceColorRect = "{{19,20},{67,71}}";
            sourceSize = "{105,105}";
        };
        "bar_coin_cell-bar_coin_00009.png" =         {
            frame = "{{2,75},{67,71}}";
            offset = "{0,-3}";
            rotated = 0;
            sourceColorRect = "{{19,20},{67,71}}";
            sourceSize = "{105,105}";
        };
        "bar_coin_cell-bar_coin_00010.png" =         {
            frame = "{{2,2},{67,71}}";
            offset = "{0,-3}";
            rotated = 0;
            sourceColorRect = "{{19,20},{67,71}}";
            sourceSize = "{105,105}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "bar_coin.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:6354fa0bbbe49e4f29f6d3f8e0c14c02$";
        textureFileName = "bar_coin.png";
    };
}
~~~~~~~~~~~~~
\section Plist10 bar_crystal.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/bar_crystal.plist
[ ->] {
    frames =     {
        "bar_crystal_cell-002.png" =         {
            frame = "{{2,209},{40,44}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{40,44}}";
            sourceSize = "{40,44}";
        };
        "bar_crystal_cell-bar_crystal_00000.png" =         {
            frame = "{{67,67},{63,67}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{21,20},{63,67}}";
            sourceSize = "{105,105}";
        };
        "bar_crystal_cell-bar_crystal_00002.png" =         {
            frame = "{{136,2},{63,67}}";
            offset = "{0,-1}";
            rotated = 1;
            sourceColorRect = "{{21,20},{63,67}}";
            sourceSize = "{105,105}";
        };
        "bar_crystal_cell-bar_crystal_00004.png" =         {
            frame = "{{67,2},{63,67}}";
            offset = "{0,-1}";
            rotated = 1;
            sourceColorRect = "{{21,20},{63,67}}";
            sourceSize = "{105,105}";
        };
        "bar_crystal_cell-bar_crystal_00006.png" =         {
            frame = "{{2,140},{63,67}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{21,20},{63,67}}";
            sourceSize = "{105,105}";
        };
        "bar_crystal_cell-bar_crystal_00008.png" =         {
            frame = "{{2,71},{63,67}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{21,20},{63,67}}";
            sourceSize = "{105,105}";
        };
        "bar_crystal_cell-bar_crystal_00010.png" =         {
            frame = "{{2,2},{63,67}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{21,20},{63,67}}";
            sourceSize = "{105,105}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "bar_crystal.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:0e2dcd547b8c15e257649a6c59dd7ab8$";
        textureFileName = "bar_crystal.png";
    };
}
~~~~~~~~~~~~~
\section Plist11 bigwin.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/bigwin.plist
[ ->] {
    frames =     {
        "bigwin_cell-besg.png" =         {
            frame = "{{250,124},{65,65}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{5,6},{65,65}}";
            sourceSize = "{75,75}";
        };
        "bigwin_cell-bj_1_00057_1.png" =         {
            frame = "{{2,213},{150,152}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{150,152}}";
            sourceSize = "{150,152}";
        };
        "bigwin_cell-ui_score_004.png" =         {
            frame = "{{330,2},{245,87}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{245,87}}";
            sourceSize = "{245,87}";
        };
        "bigwin_cell-ui_score_007.png" =         {
            frame = "{{2,124},{246,87}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{246,87}}";
            sourceSize = "{246,87}";
        };
        "bigwin_cell-ui_score_008.png" =         {
            frame = "{{306,328},{200,77}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{200,77}}";
            sourceSize = "{200,77}";
        };
        "bigwin_cell-ui_score_010.png" =         {
            frame = "{{2,2},{326,120}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{326,120}}";
            sourceSize = "{326,120}";
        };
        "bigwin_cell-ui_score_014.png" =         {
            frame = "{{419,2},{225,84}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{225,84}}";
            sourceSize = "{225,84}";
        };
        "bigwin_cell-ui_score_1.png" =         {
            frame = "{{306,249},{200,77}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{200,77}}";
            sourceSize = "{200,77}";
        };
        "bigwin_cell-ui_score_star02.png" =         {
            frame = "{{2,367},{150,143}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{150,143}}";
            sourceSize = "{150,143}";
        };
        "bigwin_cell-ui_score_star1.png" =         {
            frame = "{{306,407},{25,25}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{25,25}}";
            sourceSize = "{25,25}";
        };
        "bigwin_cell-ui_score_star3.png" =         {
            frame = "{{154,213},{150,142}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{150,142}}";
            sourceSize = "{150,142}";
        };
        "bigwin_cell-ui_score_star4.png" =         {
            frame = "{{154,357},{150,133}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{150,133}}";
            sourceSize = "{150,133}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "bigwin.png";
        size = "{512,512}";
        smartupdate = "$TexturePacker:SmartUpdate:eaec5bfab00e48b12186295ce5194f6a$";
        textureFileName = "bigwin.png";
    };
}
~~~~~~~~~~~~~
\section Plist12 bomb.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/bomb.plist
[ ->] {
    frames =     {
        "bomb_cell-1.png" =         {
            frame = "{{2,57},{52,53}}";
            offset = "{-2,2}";
            rotated = 1;
            sourceColorRect = "{{0,0},{52,53}}";
            sourceSize = "{56,57}";
        };
        "bomb_cell-bomb_2.png" =         {
            frame = "{{2,2},{56,53}}";
            offset = "{0,1}";
            rotated = 0;
            sourceColorRect = "{{0,1},{56,53}}";
            sourceSize = "{56,57}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "bomb.png";
        size = "{64,128}";
        smartupdate = "$TexturePacker:SmartUpdate:ecb91fd2c60d91b2158e551a103ddcbd$";
        textureFileName = "bomb.png";
    };
}
~~~~~~~~~~~~~
\section Plist13 bullet.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/bullet.plist
[ ->] {
    frames =     {
        "bullet_cell-bullet_1.png" =         {
            frame = "{{2,2},{26,51}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{26,51}}";
            sourceSize = "{26,51}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "bullet.png";
        size = "{32,64}";
        smartupdate = "$TexturePacker:SmartUpdate:0191343d614a5610175715f27ab1d96a$";
        textureFileName = "bullet.png";
    };
}
~~~~~~~~~~~~~
\section Plist14 bullet_bomb.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/bullet_bomb.plist
[ ->] {
    frames =     {
        "bullet_bomb_cell-a001.png" =         {
            frame = "{{229,134},{121,31}}";
            offset = "{1,-2}";
            rotated = 0;
            sourceColorRect = "{{3,5},{121,31}}";
            sourceSize = "{125,37}";
        };
        "bullet_bomb_cell-a002.png" =         {
            frame = "{{68,329},{29,29}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{5,4},{29,29}}";
            sourceSize = "{37,37}";
        };
        "bullet_bomb_cell-a003.png" =         {
            frame = "{{306,220},{62,50}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{62,50}}";
            sourceSize = "{62,50}";
        };
        "bullet_bomb_cell-a004.png" =         {
            frame = "{{114,248},{75,75}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{75,75}}";
            sourceSize = "{75,75}";
        };
        "bullet_bomb_cell-a005.png" =         {
            frame = "{{2,2},{225,148}}";
            offset = "{0,1}";
            rotated = 0;
            sourceColorRect = "{{0,0},{225,148}}";
            sourceSize = "{225,150}";
        };
        "bullet_bomb_cell-b001.png" =         {
            frame = "{{75,264},{63,33}}";
            offset = "{-2,-2}";
            rotated = 1;
            sourceColorRect = "{{4,4},{63,33}}";
            sourceSize = "{75,37}";
        };
        "bullet_bomb_cell-b002.png" =         {
            frame = "{{2,264},{63,71}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{6,2},{63,71}}";
            sourceSize = "{75,75}";
        };
        "bullet_bomb_cell-b003.png" =         {
            frame = "{{2,395},{57,57}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{57,57}}";
            sourceSize = "{57,57}";
        };
        "bullet_bomb_cell-b004.png" =         {
            frame = "{{114,152},{96,94}}";
            offset = "{0,2}";
            rotated = 0;
            sourceColorRect = "{{2,1},{96,94}}";
            sourceSize = "{100,100}";
        };
        "bullet_bomb_cell-b005.png" =         {
            frame = "{{212,167},{92,92}}";
            offset = "{-3,1}";
            rotated = 0;
            sourceColorRect = "{{1,3},{92,92}}";
            sourceSize = "{100,100}";
        };
        "bullet_bomb_cell-b006.png" =         {
            frame = "{{2,454},{48,48}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{2,2},{48,48}}";
            sourceSize = "{52,52}";
        };
        "bullet_bomb_cell-b007.png" =         {
            frame = "{{361,132},{121,121}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{3,2},{121,121}}";
            sourceSize = "{125,125}";
        };
        "bullet_bomb_cell-b009.png" =         {
            frame = "{{229,2},{130,130}}";
            offset = "{-3,0}";
            rotated = 0;
            sourceColorRect = "{{2,5},{130,130}}";
            sourceSize = "{140,140}";
        };
        "bullet_bomb_cell-bullet_bomb_2.png" =         {
            frame = "{{2,329},{64,64}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,64}}";
            sourceSize = "{64,64}";
        };
        "bullet_bomb_cell-bullet_bomb_3.png" =         {
            frame = "{{110,325},{64,64}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,64}}";
            sourceSize = "{64,64}";
        };
        "bullet_bomb_cell-bullet_bomb_7.png" =         {
            frame = "{{191,261},{64,64}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,64}}";
            sourceSize = "{64,64}";
        };
        "bullet_bomb_cell-c002.png" =         {
            frame = "{{2,152},{110,110}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{110,110}}";
            sourceSize = "{110,110}";
        };
        "bullet_bomb_cell-c003.png" =         {
            frame = "{{361,2},{130,128}}";
            offset = "{-1,3}";
            rotated = 0;
            sourceColorRect = "{{9,8},{130,128}}";
            sourceSize = "{150,150}";
        };
        "bullet_bomb_cell-c004.png" =         {
            frame = "{{306,167},{53,51}}";
            offset = "{0,1}";
            rotated = 0;
            sourceColorRect = "{{11,11},{53,51}}";
            sourceSize = "{75,75}";
        };
        "bullet_bomb_cell-d001.png" =         {
            frame = "{{257,261},{30,30}}";
            offset = "{1,-1}";
            rotated = 0;
            sourceColorRect = "{{6,6},{30,30}}";
            sourceSize = "{40,40}";
        };
        "bullet_bomb_cell-ss.png" =         {
            frame = "{{493,2},{16,36}}";
            offset = "{1,-3}";
            rotated = 0;
            sourceColorRect = "{{23,15},{16,36}}";
            sourceSize = "{60,60}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "bullet_bomb.png";
        size = "{512,512}";
        smartupdate = "$TexturePacker:SmartUpdate:6d3b315126905d55c20ff9c56b4e650c$";
        textureFileName = "bullet_bomb.png";
    };
}
~~~~~~~~~~~~~
\section Plist15 bullet_circle.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/bullet_circle.plist
[ ->] {
    frames =     {
        "bullet_circle_cell-001.png" =         {
            frame = "{{2,2},{168,168}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{168,168}}";
            sourceSize = "{168,168}";
        };
        "bullet_circle_cell-002.png" =         {
            frame = "{{2,172},{128,133}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{128,133}}";
            sourceSize = "{128,133}";
        };
        "bullet_circle_cell-003.png" =         {
            frame = "{{172,2},{64,64}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,64}}";
            sourceSize = "{64,64}";
        };
        "bullet_circle_cell-004.png" =         {
            frame = "{{2,302},{128,128}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{128,128}}";
            sourceSize = "{128,128}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "bullet_circle.png";
        size = "{256,512}";
        smartupdate = "$TexturePacker:SmartUpdate:ebf28e85195ad42c7255001da6e40dfc$";
        textureFileName = "bullet_circle.png";
    };
}
~~~~~~~~~~~~~
\section Plist16 bullet_fire.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/bullet_fire.plist
[ ->] {
    frames =     {
        "bullet_fire_cell-bullet_fire_1.png" =         {
            frame = "{{2,2},{52,138}}";
            offset = "{1,-3}";
            rotated = 0;
            sourceColorRect = "{{5,9},{52,138}}";
            sourceSize = "{60,150}";
        };
        "bullet_fire_cell-fire2_0.png" =         {
            frame = "{{120,2},{60,75}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{11,0},{60,75}}";
            sourceSize = "{80,75}";
        };
        "bullet_fire_cell-fire2_2.png" =         {
            frame = "{{56,2},{62,75}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{9,0},{62,75}}";
            sourceSize = "{80,75}";
        };
        "bullet_fire_cell-fire2_3.png" =         {
            frame = "{{56,79},{60,75}}";
            offset = "{-2,0}";
            rotated = 1;
            sourceColorRect = "{{8,0},{60,75}}";
            sourceSize = "{80,75}";
        };
        "bullet_fire_cell-fire2_4.png" =         {
            frame = "{{182,2},{58,69}}";
            offset = "{-2,3}";
            rotated = 1;
            sourceColorRect = "{{9,0},{58,69}}";
            sourceSize = "{80,75}";
        };
        "bullet_fire_cell-fire2_5.png" =         {
            frame = "{{182,62},{56,49}}";
            offset = "{0,13}";
            rotated = 0;
            sourceColorRect = "{{12,0},{56,49}}";
            sourceSize = "{80,75}";
        };
        "bullet_fire_cell-fire_0.png" =         {
            frame = "{{32,142},{13,80}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{2,0},{13,80}}";
            sourceSize = "{15,80}";
        };
        "bullet_fire_cell-fire_1.png" =         {
            frame = "{{17,142},{13,80}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{2,0},{13,80}}";
            sourceSize = "{15,80}";
        };
        "bullet_fire_cell-fire_3.png" =         {
            frame = "{{2,224},{13,80}}";
            offset = "{1,0}";
            rotated = 1;
            sourceColorRect = "{{2,0},{13,80}}";
            sourceSize = "{15,80}";
        };
        "bullet_fire_cell-fire_4.png" =         {
            frame = "{{2,142},{13,80}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{2,0},{13,80}}";
            sourceSize = "{15,80}";
        };
        "bullet_fire_cell-fire_5.png" =         {
            frame = "{{47,142},{11,80}}";
            offset = "{2,0}";
            rotated = 0;
            sourceColorRect = "{{4,0},{11,80}}";
            sourceSize = "{15,80}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "bullet_fire.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:1b8e4de8e9d3f982a6d4b402449b2212$";
        textureFileName = "bullet_fire.png";
    };
}
~~~~~~~~~~~~~
\section Plist17 bullet_gas.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/bullet_gas.plist
[ ->] {
    frames =     {
        "bullet_gas_cell-bullet_gas_1.png" =         {
            frame = "{{2,2},{45,110}}";
            offset = "{1,-2}";
            rotated = 0;
            sourceColorRect = "{{14,12},{45,110}}";
            sourceSize = "{71,130}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "bullet_gas.png";
        size = "{64,128}";
        smartupdate = "$TexturePacker:SmartUpdate:8ae0b1de8839eec4874d77929676a819$";
        textureFileName = "bullet_gas.png";
    };
}
~~~~~~~~~~~~~
\section Plist18 bullet_harpoon.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/bullet_harpoon.plist
[ ->] {
    frames =     {
        "bullet_harpoon_cell-bullet_harpoon_1.png" =         {
            frame = "{{2,2},{44,90}}";
            offset = "{1,-2}";
            rotated = 0;
            sourceColorRect = "{{7,10},{44,90}}";
            sourceSize = "{56,106}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "bullet_harpoon.png";
        size = "{64,128}";
        smartupdate = "$TexturePacker:SmartUpdate:08abf3af698da177a3aa87b1cae00780$";
        textureFileName = "bullet_harpoon.png";
    };
}
~~~~~~~~~~~~~
\section Plist19 bullet_moonlight.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/bullet_moonlight.plist
[ ->] {
    frames =     {
        "bullet_moonlight_cell-bullet_moonlight_1.png" =         {
            frame = "{{2,2},{126,41}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{126,41}}";
            sourceSize = "{126,41}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "bullet_moonlight.png";
        size = "{64,256}";
        smartupdate = "$TexturePacker:SmartUpdate:d69ce24ee2214bf58041433f8416667b$";
        textureFileName = "bullet_moonlight.png";
    };
}
~~~~~~~~~~~~~
\section Plist20 bullet_projectile.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/bullet_projectile.plist
[ ->] {
    frames =     {
        "bullet_projectile_cell-001.png" =         {
            frame = "{{78,2},{52,52}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{52,52}}";
            sourceSize = "{52,52}";
        };
        "bullet_projectile_cell-002.png" =         {
            frame = "{{2,2},{74,147}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{74,147}}";
            sourceSize = "{74,147}";
        };
        "bullet_projectile_cell-003.png" =         {
            frame = "{{134,2},{64,64}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,64}}";
            sourceSize = "{64,64}";
        };
        "bullet_projectile_cell-004.png" =         {
            frame = "{{68,151},{64,64}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,64}}";
            sourceSize = "{64,64}";
        };
        "bullet_projectile_cell-005.png" =         {
            frame = "{{2,217},{32,30}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{32,30}}";
            sourceSize = "{32,30}";
        };
        "bullet_projectile_cell-006.png" =         {
            frame = "{{2,151},{64,64}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,64}}";
            sourceSize = "{64,64}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "bullet_projectile.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:ca2e7e2123cff378038531579bab820e$";
        textureFileName = "bullet_projectile.png";
    };
}
~~~~~~~~~~~~~
\section Plist21 bullet_rolling.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/bullet_rolling.plist
[ ->] {
    frames =     {
        "bullet_rolling_cell-bullet_rolling_1.png" =         {
            frame = "{{2,2},{45,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{45,45}}";
            sourceSize = "{45,45}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "bullet_rolling.png";
        size = "{64,64}";
        smartupdate = "$TexturePacker:SmartUpdate:25413294ad31beb72d5a317a33daad97$";
        textureFileName = "bullet_rolling.png";
    };
}
~~~~~~~~~~~~~
\section Plist22 bullet_shoulijjian.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/bullet_shoulijjian.plist
[ ->] {
    frames =     {
        "bullet_shoulijjian_cell-bullet_shoulijjian_1.png" =         {
            frame = "{{2,2},{54,54}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{54,54}}";
            sourceSize = "{54,54}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "bullet_shoulijjian.png";
        size = "{64,64}";
        smartupdate = "$TexturePacker:SmartUpdate:54d42a7931c64f0988906809ccebe940$";
        textureFileName = "bullet_shoulijjian.png";
    };
}
~~~~~~~~~~~~~
\section Plist23 choose_go.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/choose_go.plist
[ ->] {
    frames =     {
        "choose_go_cell-001.png" =         {
            frame = "{{2,157},{30,39}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{30,39}}";
            sourceSize = "{30,39}";
        };
        "choose_go_cell-002.png" =         {
            frame = "{{2,2},{66,58}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{66,58}}";
            sourceSize = "{66,58}";
        };
        "choose_go_cell-00_00000.png" =         {
            frame = "{{2,95},{41,27}}";
            offset = "{-13,-58}";
            rotated = 0;
            sourceColorRect = "{{4,116},{41,27}}";
            sourceSize = "{75,143}";
        };
        "choose_go_cell-00_00001.png" =         {
            frame = "{{86,107},{39,33}}";
            offset = "{-18,-55}";
            rotated = 0;
            sourceColorRect = "{{0,110},{39,33}}";
            sourceSize = "{75,143}";
        };
        "choose_go_cell-00_00002.png" =         {
            frame = "{{43,132},{31,39}}";
            offset = "{-22,-51}";
            rotated = 1;
            sourceColorRect = "{{0,103},{31,39}}";
            sourceSize = "{75,143}";
        };
        "choose_go_cell-00_00003.png" =         {
            frame = "{{84,142},{29,39}}";
            offset = "{-23,-48}";
            rotated = 1;
            sourceColorRect = "{{0,100},{29,39}}";
            sourceSize = "{75,143}";
        };
        "choose_go_cell-00_00004.png" =         {
            frame = "{{45,66},{29,41}}";
            offset = "{-23,-44}";
            rotated = 1;
            sourceColorRect = "{{0,95},{29,41}}";
            sourceSize = "{75,143}";
        };
        "choose_go_cell-00_00005.png" =         {
            frame = "{{2,62},{31,41}}";
            offset = "{-22,-40}";
            rotated = 1;
            sourceColorRect = "{{0,91},{31,41}}";
            sourceSize = "{75,143}";
        };
        "choose_go_cell-00_00006.png" =         {
            frame = "{{70,33},{31,41}}";
            offset = "{-3,42}";
            rotated = 1;
            sourceColorRect = "{{19,9},{31,41}}";
            sourceSize = "{75,143}";
        };
        "choose_go_cell-00_00007.png" =         {
            frame = "{{2,124},{31,39}}";
            offset = "{-1,45}";
            rotated = 1;
            sourceColorRect = "{{21,7},{31,39}}";
            sourceSize = "{75,143}";
        };
        "choose_go_cell-00_00008.png" =         {
            frame = "{{88,66},{33,39}}";
            offset = "{1,49}";
            rotated = 0;
            sourceColorRect = "{{22,3},{33,39}}";
            sourceSize = "{75,143}";
        };
        "choose_go_cell-00_00009.png" =         {
            frame = "{{45,97},{39,33}}";
            offset = "{6,53}";
            rotated = 0;
            sourceColorRect = "{{24,2},{39,33}}";
            sourceSize = "{75,143}";
        };
        "choose_go_cell-00_00010.png" =         {
            frame = "{{70,2},{43,29}}";
            offset = "{11,55}";
            rotated = 0;
            sourceColorRect = "{{27,2},{43,29}}";
            sourceSize = "{75,143}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "choose_go.png";
        size = "{128,256}";
        smartupdate = "$TexturePacker:SmartUpdate:9c4bf6337c268f39f3b33c25a7ad3432$";
        textureFileName = "choose_go.png";
    };
}
~~~~~~~~~~~~~
\section Plist24 cristal.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/cristal.plist
[ ->] {
    frames =     {
        "cristal_cell-cristal_1.png" =         {
            frame = "{{94,2},{90,95}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{90,95}}";
            sourceSize = "{90,95}";
        };
        "cristal_cell-cristal_2.png" =         {
            frame = "{{2,99},{90,95}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{90,95}}";
            sourceSize = "{90,95}";
        };
        "cristal_cell-cristal_3.png" =         {
            frame = "{{94,94},{86,95}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{3,0},{86,95}}";
            sourceSize = "{90,95}";
        };
        "cristal_cell-cristal_4.png" =         {
            frame = "{{2,2},{90,95}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{90,95}}";
            sourceSize = "{90,95}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "cristal.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:f55b7039aa1f2e72784b1fe1bb029e0f$";
        textureFileName = "cristal.png";
    };
}
~~~~~~~~~~~~~
\section Plist25 ef_bomb.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_bomb.plist
[ ->] {
    frames =     {
        "ef_bomb_cell-ef_bomb_1.png" =         {
            frame = "{{2,38},{57,16}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{0,9},{57,16}}";
            sourceSize = "{57,32}";
        };
        "ef_bomb_cell-ef_bomb_2.png" =         {
            frame = "{{2,20},{57,16}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{0,9},{57,16}}";
            sourceSize = "{57,32}";
        };
        "ef_bomb_cell-ef_bomb_3.png" =         {
            frame = "{{2,2},{57,16}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{0,9},{57,16}}";
            sourceSize = "{57,32}";
        };
        "ef_bomb_cell-ef_bomb_4.png" =         {
            frame = "{{2,56},{54,33}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{54,33}}";
            sourceSize = "{54,33}";
        };
        "ef_bomb_cell-ef_bomb_5.png" =         {
            frame = "{{2,91},{25,13}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{1,0},{25,13}}";
            sourceSize = "{27,13}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_bomb.png";
        size = "{64,128}";
        smartupdate = "$TexturePacker:SmartUpdate:583de471cf202d7777767c91f18c12fa$";
        textureFileName = "ef_bomb.png";
    };
}
~~~~~~~~~~~~~
\section Plist26 ef_bursts_1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_bursts_1.plist
[ ->] {
    frames =     {
        "ef_bursts_1_cell-effect_pao_1_1.png" =         {
            frame = "{{2,41},{18,30}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{18,30}}";
            sourceSize = "{18,30}";
        };
        "ef_bursts_1_cell-effect_pao_1_2.png" =         {
            frame = "{{2,2},{23,37}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{23,37}}";
            sourceSize = "{23,37}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_bursts_1.png";
        size = "{64,64}";
        smartupdate = "$TexturePacker:SmartUpdate:a2d5695aed762bcb43ce91d7ffa9c6ca$";
        textureFileName = "ef_bursts_1.png";
    };
}
~~~~~~~~~~~~~
\section Plist27 ef_bursts_2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_bursts_2.plist
[ ->] {
    frames =     {
        "ef_bursts_2_cell-effect_pao_1_1.png" =         {
            frame = "{{2,2},{94,98}}";
            offset = "{-3,1}";
            rotated = 0;
            sourceColorRect = "{{0,0},{94,98}}";
            sourceSize = "{100,100}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_bursts_2.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:5425c92ae343a7717a0679b313afd7d6$";
        textureFileName = "ef_bursts_2.png";
    };
}
~~~~~~~~~~~~~
\section Plist28 ef_bursts_3.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_bursts_3.plist
[ ->] {
    frames =     {
        "ef_bursts_3_1.png" =         {
            frame = "{{2,2},{82,50}}";
            offset = "{1,0}";
            rotated = 1;
            sourceColorRect = "{{10,0},{82,50}}";
            sourceSize = "{100,50}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_bursts_3.png";
        size = "{64,128}";
        smartupdate = "$TexturePacker:SmartUpdate:42343820c2f9df313d295619205313a2$";
        textureFileName = "ef_bursts_3.png";
    };
}
~~~~~~~~~~~~~
\section Plist29 ef_cannon_1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_cannon_1.plist
[ ->] {
    frames =     {
        "ef_cannon_1_cell-fangdaquan_00006.png" =         {
            frame = "{{84,76},{41,11}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{0,7},{41,11}}";
            sourceSize = "{41,23}";
        };
        "ef_cannon_1_cell-zhuan_00000.png" =         {
            frame = "{{40,60},{42,16}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,4},{42,16}}";
            sourceSize = "{42,24}";
        };
        "ef_cannon_1_cell-zhuan_00002.png" =         {
            frame = "{{22,42},{42,16}}";
            offset = "{0,-1}";
            rotated = 1;
            sourceColorRect = "{{0,5},{42,16}}";
            sourceSize = "{42,24}";
        };
        "ef_cannon_1_cell-zhuan_00004.png" =         {
            frame = "{{46,22},{42,18}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{0,4},{42,18}}";
            sourceSize = "{42,24}";
        };
        "ef_cannon_1_cell-zhuan_00006.png" =         {
            frame = "{{2,22},{42,18}}";
            offset = "{0,-2}";
            rotated = 0;
            sourceColorRect = "{{0,5},{42,18}}";
            sourceSize = "{42,24}";
        };
        "ef_cannon_1_cell-zhuan_00008.png" =         {
            frame = "{{2,46},{42,18}}";
            offset = "{0,-2}";
            rotated = 1;
            sourceColorRect = "{{0,5},{42,18}}";
            sourceSize = "{42,24}";
        };
        "ef_cannon_1_cell-zhuan_00010.png" =         {
            frame = "{{90,2},{42,18}}";
            offset = "{0,-2}";
            rotated = 1;
            sourceColorRect = "{{0,5},{42,18}}";
            sourceSize = "{42,24}";
        };
        "ef_cannon_1_cell-zhuan_00012.png" =         {
            frame = "{{2,108},{42,16}}";
            offset = "{0,-3}";
            rotated = 0;
            sourceColorRect = "{{0,7},{42,16}}";
            sourceSize = "{42,24}";
        };
        "ef_cannon_1_cell-zhuan_00014.png" =         {
            frame = "{{84,60},{42,14}}";
            offset = "{0,-4}";
            rotated = 0;
            sourceColorRect = "{{0,9},{42,14}}";
            sourceSize = "{42,24}";
        };
        "ef_cannon_1_cell-zhuan_00016.png" =         {
            frame = "{{46,2},{42,18}}";
            offset = "{0,-2}";
            rotated = 0;
            sourceColorRect = "{{0,5},{42,18}}";
            sourceSize = "{42,24}";
        };
        "ef_cannon_1_cell-zhuan_00018.png" =         {
            frame = "{{2,2},{42,18}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{0,4},{42,18}}";
            sourceSize = "{42,24}";
        };
        "ef_cannon_1_cell-zhuan_00020.png" =         {
            frame = "{{46,42},{42,16}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{0,5},{42,16}}";
            sourceSize = "{42,24}";
        };
        "ef_cannon_1_cell-zhuan_00022.png" =         {
            frame = "{{2,90},{42,16}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{0,5},{42,16}}";
            sourceSize = "{42,24}";
        };
        "ef_cannon_1_cell-zhuan_00024.png" =         {
            frame = "{{110,2},{42,16}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,4},{42,16}}";
            sourceSize = "{42,24}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_cannon_1.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:ba7a19f6f19728c2114e584e50e84a28$";
        textureFileName = "ef_cannon_1.png";
    };
}
~~~~~~~~~~~~~
\section Plist30 ef_circle.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_circle.plist
[ ->] {
    frames =     {
        "ef_circle_cell-ef_circle_1.png" =         {
            frame = "{{22,33},{28,8}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{28,8}}";
            sourceSize = "{28,8}";
        };
        "ef_circle_cell-ef_circle_2.png" =         {
            frame = "{{12,33},{28,8}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{28,8}}";
            sourceSize = "{28,8}";
        };
        "ef_circle_cell-ef_circle_3.png" =         {
            frame = "{{2,33},{28,8}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{28,8}}";
            sourceSize = "{28,8}";
        };
        "ef_circle_cell-ef_circle_4.png" =         {
            frame = "{{32,33},{10,10}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{10,10}}";
            sourceSize = "{10,10}";
        };
        "ef_circle_cell-ef_circle_5.png" =         {
            frame = "{{2,2},{29,29}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{29,29}}";
            sourceSize = "{29,29}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_circle.png";
        size = "{64,64}";
        smartupdate = "$TexturePacker:SmartUpdate:115f8d374b10e6ff741882601f83e149$";
        textureFileName = "ef_circle.png";
    };
}
~~~~~~~~~~~~~
\section Plist31 ef_fire_1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_fire_1.plist
[ ->] {
    frames =     {
        "ef_fire_1_cell-ef_fire_1_1.png" =         {
            frame = "{{2,2},{124,124}}";
            offset = "{0,-3}";
            rotated = 0;
            sourceColorRect = "{{13,16},{124,124}}";
            sourceSize = "{150,150}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_fire_1.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:48e53182ce41c473de0de23dc4798a83$";
        textureFileName = "ef_fire_1.png";
    };
}
~~~~~~~~~~~~~
\section Plist32 ef_fire_2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_fire_2.plist
[ ->] {
    frames =     {
        "ef_fire_2_cell-11.png" =         {
            frame = "{{2,2},{40,60}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{40,60}}";
            sourceSize = "{40,60}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_fire_2.png";
        size = "{64,64}";
        smartupdate = "$TexturePacker:SmartUpdate:cc9a1f031957e687d4203a01362fda74$";
        textureFileName = "ef_fire_2.png";
    };
}
~~~~~~~~~~~~~
\section Plist33 ef_gas.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_gas.plist
[ ->] {
    frames =     {
        "ef_gas_cell-ef_gas_0.png" =         {
            frame = "{{129,37},{17,49}}";
            offset = "{19,0}";
            rotated = 1;
            sourceColorRect = "{{48,13},{17,49}}";
            sourceSize = "{75,75}";
        };
        "ef_gas_cell-ef_gas_1.png" =         {
            frame = "{{97,127},{21,53}}";
            offset = "{17,1}";
            rotated = 0;
            sourceColorRect = "{{44,10},{21,53}}";
            sourceSize = "{75,75}";
        };
        "ef_gas_cell-ef_gas_2.png" =         {
            frame = "{{74,37},{23,53}}";
            offset = "{15,0}";
            rotated = 1;
            sourceColorRect = "{{41,11},{23,53}}";
            sourceSize = "{75,75}";
        };
        "ef_gas_cell-ef_gas_3.png" =         {
            frame = "{{70,72},{23,53}}";
            offset = "{13,0}";
            rotated = 0;
            sourceColorRect = "{{39,11},{23,53}}";
            sourceSize = "{75,75}";
        };
        "ef_gas_cell-ef_gas_5.png" =         {
            frame = "{{68,140},{27,53}}";
            offset = "{-1,0}";
            rotated = 0;
            sourceColorRect = "{{23,11},{27,53}}";
            sourceSize = "{75,75}";
        };
        "ef_gas_cell-ef_gas_6.png" =         {
            frame = "{{72,220},{23,53}}";
            offset = "{-8,0}";
            rotated = 1;
            sourceColorRect = "{{18,11},{23,53}}";
            sourceSize = "{75,75}";
        };
        "ef_gas_cell-ef_gas_7.png" =         {
            frame = "{{68,195},{23,53}}";
            offset = "{-10,0}";
            rotated = 1;
            sourceColorRect = "{{16,11},{23,53}}";
            sourceSize = "{75,75}";
        };
        "ef_gas_cell-ef_gas_8.png" =         {
            frame = "{{95,72},{21,53}}";
            offset = "{-11,1}";
            rotated = 0;
            sourceColorRect = "{{16,10},{21,53}}";
            sourceSize = "{75,75}";
        };
        "ef_gas_cell-ef_gas_9.png" =         {
            frame = "{{118,72},{17,49}}";
            offset = "{-13,0}";
            rotated = 0;
            sourceColorRect = "{{16,13},{17,49}}";
            sourceSize = "{75,75}";
        };
        "ef_gas_cell-ef_gas_e3111.png" =         {
            frame = "{{180,37},{17,17}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{17,17}}";
            sourceSize = "{17,17}";
        };
        "ef_gas_cell-zhuan01.png" =         {
            frame = "{{2,37},{70,33}}";
            offset = "{2,0}";
            rotated = 0;
            sourceColorRect = "{{8,6},{70,33}}";
            sourceSize = "{82,45}";
        };
        "ef_gas_cell-zhuan03.png" =         {
            frame = "{{2,146},{72,31}}";
            offset = "{1,-2}";
            rotated = 1;
            sourceColorRect = "{{6,9},{72,31}}";
            sourceSize = "{82,45}";
        };
        "ef_gas_cell-zhuan05.png" =         {
            frame = "{{224,2},{68,29}}";
            offset = "{-2,-3}";
            rotated = 1;
            sourceColorRect = "{{5,11},{68,29}}";
            sourceSize = "{82,45}";
        };
        "ef_gas_cell-zhuan07.png" =         {
            frame = "{{35,72},{66,33}}";
            offset = "{-4,-1}";
            rotated = 1;
            sourceColorRect = "{{4,7},{66,33}}";
            sourceSize = "{82,45}";
        };
        "ef_gas_cell-zhuan09.png" =         {
            frame = "{{150,2},{72,33}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{5,6},{72,33}}";
            sourceSize = "{82,45}";
        };
        "ef_gas_cell-zhuan11.png" =         {
            frame = "{{76,2},{72,33}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{5,6},{72,33}}";
            sourceSize = "{82,45}";
        };
        "ef_gas_cell-zhuan13.png" =         {
            frame = "{{2,72},{72,31}}";
            offset = "{1,2}";
            rotated = 1;
            sourceColorRect = "{{6,5},{72,31}}";
            sourceSize = "{82,45}";
        };
        "ef_gas_cell-zhuan15.png" =         {
            frame = "{{35,140},{66,31}}";
            offset = "{6,1}";
            rotated = 1;
            sourceColorRect = "{{14,6},{66,31}}";
            sourceSize = "{82,45}";
        };
        "ef_gas_cell-zhuan17.png" =         {
            frame = "{{2,220},{68,33}}";
            offset = "{5,0}";
            rotated = 0;
            sourceColorRect = "{{12,6},{68,33}}";
            sourceSize = "{82,45}";
        };
        "ef_gas_cell-zhuan19.png" =         {
            frame = "{{2,2},{72,33}}";
            offset = "{2,0}";
            rotated = 0;
            sourceColorRect = "{{7,6},{72,33}}";
            sourceSize = "{82,45}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_gas.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:145810eab66d719c4ce8567fbe22359e$";
        textureFileName = "ef_gas.png";
    };
}
~~~~~~~~~~~~~
\section Plist34 ef_harpoon_1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_harpoon_1.plist
[ ->] {
    frames =     {
        "ef_harpoon_1_cell-1.png" =         {
            frame = "{{2,2},{125,122}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{125,122}}";
            sourceSize = "{125,122}";
        };
        "ef_harpoon_1_cell-2.png" =         {
            frame = "{{2,129},{59,79}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{59,79}}";
            sourceSize = "{59,79}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_harpoon_1.png";
        size = "{128,256}";
        smartupdate = "$TexturePacker:SmartUpdate:3a5947ee6016932f4e98d9a1815c38ef$";
        textureFileName = "ef_harpoon_1.png";
    };
}
~~~~~~~~~~~~~
\section Plist35 ef_lighting.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_lighting.plist
[ ->] {
    frames =     {
        "ef_lighting_cell-3dian_00015.png" =         {
            frame = "{{2,64},{60,56}}";
            offset = "{0,2}";
            rotated = 0;
            sourceColorRect = "{{0,0},{60,56}}";
            sourceSize = "{60,60}";
        };
        "ef_lighting_cell-3dian_00016.png" =         {
            frame = "{{2,396},{52,44}}";
            offset = "{2,-4}";
            rotated = 0;
            sourceColorRect = "{{6,12},{52,44}}";
            sourceSize = "{60,60}";
        };
        "ef_lighting_cell-3dian_00017.png" =         {
            frame = "{{2,350},{52,44}}";
            offset = "{2,-4}";
            rotated = 0;
            sourceColorRect = "{{6,12},{52,44}}";
            sourceSize = "{60,60}";
        };
        "ef_lighting_cell-biansediyun_00012.png" =         {
            frame = "{{2,2},{60,60}}";
            offset = "{2,0}";
            rotated = 0;
            sourceColorRect = "{{22,20},{60,60}}";
            sourceSize = "{100,100}";
        };
        "ef_lighting_cell-diyun_00012.png" =         {
            frame = "{{2,260},{53,88}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{53,88}}";
            sourceSize = "{53,88}";
        };
        "ef_lighting_cell-jiaocha_00020.png" =         {
            frame = "{{2,442},{48,62}}";
            offset = "{-1,-1}";
            rotated = 0;
            sourceColorRect = "{{5,20},{48,62}}";
            sourceSize = "{60,100}";
        };
        "ef_lighting_cell-jiaocha_00021.png" =         {
            frame = "{{2,122},{46,60}}";
            offset = "{0,-2}";
            rotated = 1;
            sourceColorRect = "{{7,22},{46,60}}";
            sourceSize = "{60,100}";
        };
        "ef_lighting_cell-jiaocha_00022.png" =         {
            frame = "{{2,170},{44,58}}";
            offset = "{2,-3}";
            rotated = 1;
            sourceColorRect = "{{10,24},{44,58}}";
            sourceSize = "{60,100}";
        };
        "ef_lighting_cell-jiaocha_00023.png" =         {
            frame = "{{2,216},{42,56}}";
            offset = "{0,-1}";
            rotated = 1;
            sourceColorRect = "{{9,23},{42,56}}";
            sourceSize = "{60,100}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_lighting.png";
        size = "{64,512}";
        smartupdate = "$TexturePacker:SmartUpdate:537119564de62abf83332ebbcd382a20$";
        textureFileName = "ef_lighting.png";
    };
}
~~~~~~~~~~~~~
\section Plist36 ef_lighting_1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_lighting_1.plist
[ ->] {
    frames =     {
        "ef_lighting_1_cell-biansediyun_00012.png" =         {
            frame = "{{2,2},{60,60}}";
            offset = "{2,0}";
            rotated = 0;
            sourceColorRect = "{{22,20},{60,60}}";
            sourceSize = "{100,100}";
        };
        "ef_lighting_1_cell-diyun_00012.png" =         {
            frame = "{{2,64},{53,88}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{53,88}}";
            sourceSize = "{53,88}";
        };
        "ef_lighting_1_cell-kaihuo_00030.png" =         {
            frame = "{{2,154},{45,90}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{45,90}}";
            sourceSize = "{45,90}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_lighting_1.png";
        size = "{64,256}";
        smartupdate = "$TexturePacker:SmartUpdate:ee3e0aa39becf60a3e1ba400cb49b29c$";
        textureFileName = "ef_lighting_1.png";
    };
}
~~~~~~~~~~~~~
\section Plist37 ef_moonlight.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_moonlight.plist
[ ->] {
    frames =     {
        "ef_moonlight_cell-ef_moonlight_01.png" =         {
            frame = "{{34,36},{68,32}}";
            offset = "{1,-1}";
            rotated = 0;
            sourceColorRect = "{{14,17},{68,32}}";
            sourceSize = "{94,64}";
        };
        "ef_moonlight_cell-ef_moonlight_02.png" =         {
            frame = "{{2,2},{70,30}}";
            offset = "{-1,-1}";
            rotated = 1;
            sourceColorRect = "{{11,18},{70,30}}";
            sourceSize = "{94,64}";
        };
        "ef_moonlight_cell-ef_moonlight_03.png" =         {
            frame = "{{2,144},{62,32}}";
            offset = "{-7,0}";
            rotated = 0;
            sourceColorRect = "{{9,16},{62,32}}";
            sourceSize = "{94,64}";
        };
        "ef_moonlight_cell-ef_moonlight_04.png" =         {
            frame = "{{34,178},{68,28}}";
            offset = "{-3,2}";
            rotated = 1;
            sourceColorRect = "{{10,16},{68,28}}";
            sourceSize = "{94,64}";
        };
        "ef_moonlight_cell-ef_moonlight_05.png" =         {
            frame = "{{2,178},{68,30}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{13,17},{68,30}}";
            sourceSize = "{94,64}";
        };
        "ef_moonlight_cell-ef_moonlight_06.png" =         {
            frame = "{{34,2},{68,32}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{13,16},{68,32}}";
            sourceSize = "{94,64}";
        };
        "ef_moonlight_cell-ef_moonlight_07.png" =         {
            frame = "{{67,70},{68,30}}";
            offset = "{2,-1}";
            rotated = 1;
            sourceColorRect = "{{15,18},{68,30}}";
            sourceSize = "{94,64}";
        };
        "ef_moonlight_cell-ef_moonlight_08.png" =         {
            frame = "{{64,214},{62,30}}";
            offset = "{7,-2}";
            rotated = 0;
            sourceColorRect = "{{23,19},{62,30}}";
            sourceSize = "{94,64}";
        };
        "ef_moonlight_cell-ef_moonlight_09.png" =         {
            frame = "{{98,140},{68,26}}";
            offset = "{2,-4}";
            rotated = 1;
            sourceColorRect = "{{15,23},{68,26}}";
            sourceSize = "{94,64}";
        };
        "ef_moonlight_cell-ef_moonlight_10.png" =         {
            frame = "{{66,144},{68,30}}";
            offset = "{1,-2}";
            rotated = 1;
            sourceColorRect = "{{14,19},{68,30}}";
            sourceSize = "{94,64}";
        };
        "ef_moonlight_cell-ef_moonlight_3.png" =         {
            frame = "{{2,74},{63,68}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{63,68}}";
            sourceSize = "{63,68}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_moonlight.png";
        size = "{128,256}";
        smartupdate = "$TexturePacker:SmartUpdate:fef261221e29214ea64d912d6289bb50$";
        textureFileName = "ef_moonlight.png";
    };
}
~~~~~~~~~~~~~
\section Plist38 ef_projectile_1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_projectile_1.plist
[ ->] {
    frames =     {
        "0_00086.png" =         {
            frame = "{{60,90},{51,29}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{0,24},{51,29}}";
            sourceSize = "{51,75}";
        };
        "ef_projectile_1_cell-1_00060.png" =         {
            frame = "{{2,2},{37,7}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{37,7}}";
            sourceSize = "{37,7}";
        };
        "ef_projectile_1_cell-2_00058.png" =         {
            frame = "{{41,2},{35,7}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{35,7}}";
            sourceSize = "{35,7}";
        };
        "ef_projectile_1_cell-3_00058.png" =         {
            frame = "{{78,2},{42,8}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{42,8}}";
            sourceSize = "{42,8}";
        };
        "ef_projectile_1_cell-biansediyun_00012.png" =         {
            frame = "{{30,90},{28,28}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{28,28}}";
            sourceSize = "{28,28}";
        };
        "ef_projectile_1_cell-qiuzhuan_00053.png" =         {
            frame = "{{2,89},{26,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{26,24}}";
            sourceSize = "{26,26}";
        };
        "ef_projectile_1_cell-qiuzhuan_00055.png" =         {
            frame = "{{86,64},{26,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{26,24}}";
            sourceSize = "{26,26}";
        };
        "ef_projectile_1_cell-qiuzhuan_00057.png" =         {
            frame = "{{58,64},{26,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{26,24}}";
            sourceSize = "{26,26}";
        };
        "ef_projectile_1_cell-qiuzhuan_00059.png" =         {
            frame = "{{30,63},{26,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{26,24}}";
            sourceSize = "{26,26}";
        };
        "ef_projectile_1_cell-qiuzhuan_00061.png" =         {
            frame = "{{2,63},{26,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{26,24}}";
            sourceSize = "{26,26}";
        };
        "ef_projectile_1_cell-qiuzhuan_00063.png" =         {
            frame = "{{86,38},{26,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{26,24}}";
            sourceSize = "{26,26}";
        };
        "ef_projectile_1_cell-qiuzhuan_00065.png" =         {
            frame = "{{58,38},{26,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{26,24}}";
            sourceSize = "{26,26}";
        };
        "ef_projectile_1_cell-qiuzhuan_00067.png" =         {
            frame = "{{30,37},{26,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{26,24}}";
            sourceSize = "{26,26}";
        };
        "ef_projectile_1_cell-qiuzhuan_00069.png" =         {
            frame = "{{2,37},{26,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{26,24}}";
            sourceSize = "{26,26}";
        };
        "ef_projectile_1_cell-qiuzhuan_00071.png" =         {
            frame = "{{86,12},{26,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{26,24}}";
            sourceSize = "{26,26}";
        };
        "ef_projectile_1_cell-qiuzhuan_00073.png" =         {
            frame = "{{58,12},{26,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{26,24}}";
            sourceSize = "{26,26}";
        };
        "ef_projectile_1_cell-qiuzhuan_00075.png" =         {
            frame = "{{30,11},{26,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{26,24}}";
            sourceSize = "{26,26}";
        };
        "ef_projectile_1_cell-qiuzhuan_00077.png" =         {
            frame = "{{2,11},{26,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{26,24}}";
            sourceSize = "{26,26}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_projectile_1.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:5c26f4cfb96a6fe11706399d1d217053$";
        textureFileName = "ef_projectile_1.png";
    };
}
~~~~~~~~~~~~~
\section Plist39 ef_rolling.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/ef_rolling.plist
[ ->] {
    frames =     {
        "ef_rolling_cell-ef_rolling_1.png" =         {
            frame = "{{2,2},{49,18}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{49,18}}";
            sourceSize = "{49,18}";
        };
        "ef_rolling_cell-ef_rolling_2.png" =         {
            frame = "{{2,22},{35,35}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{35,35}}";
            sourceSize = "{35,35}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "ef_rolling.png";
        size = "{64,64}";
        smartupdate = "$TexturePacker:SmartUpdate:746ae04cb421cbced178d9cfc8c0f853$";
        textureFileName = "ef_rolling.png";
    };
}
~~~~~~~~~~~~~
\section Plist40 finger.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/finger.plist
[ ->] {
    frames =     {
        "finger_cell-3.png" =         {
            frame = "{{2,99},{92,106}}";
            offset = "{0,-5}";
            rotated = 1;
            sourceColorRect = "{{1,13},{92,106}}";
            sourceSize = "{94,122}";
        };
        "finger_cell-finger_1.png" =         {
            frame = "{{2,2},{95,122}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{95,122}}";
            sourceSize = "{95,122}";
        };
        "finger_cell-finger_3.png" =         {
            frame = "{{2,193},{24,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{24,24}}";
            sourceSize = "{24,24}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "finger.png";
        size = "{128,256}";
        smartupdate = "$TexturePacker:SmartUpdate:72eb98d85cdd6c817ee059085ab91877$";
        textureFileName = "finger.png";
    };
}
~~~~~~~~~~~~~
\section Plist41 firefish.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/firefish.plist
[ ->] {
    frames =     {
        "firefish_cell-firefish_1.png" =         {
            frame = "{{2,76},{142,90}}";
            offset = "{4,0}";
            rotated = 0;
            sourceColorRect = "{{8,0},{142,90}}";
            sourceSize = "{150,90}";
        };
        "firefish_cell-firefish_2.png" =         {
            frame = "{{2,2},{144,72}}";
            offset = "{3,-4}";
            rotated = 0;
            sourceColorRect = "{{6,13},{144,72}}";
            sourceSize = "{150,90}";
        };
        "firefish_cell-firefish_3.png" =         {
            frame = "{{148,2},{100,75}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{100,75}}";
            sourceSize = "{100,75}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "firefish.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:ec094be04ada0c5fb1a43aa9b1085d61$";
        textureFileName = "firefish.png";
    };
}
~~~~~~~~~~~~~
\section Plist42 fish_dead.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/fish_dead.plist
[ ->] {
    frames =     {
        "fish_dead_cell-mx2049_00081_1.png" =         {
            frame = "{{2,2},{94,27}}";
            offset = "{2,0}";
            rotated = 1;
            sourceColorRect = "{{5,24},{94,27}}";
            sourceSize = "{100,75}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "fish_dead.png";
        size = "{32,128}";
        smartupdate = "$TexturePacker:SmartUpdate:fa8cbe1b691c2da1aa9048e2d6876196$";
        textureFileName = "fish_dead.png";
    };
}
~~~~~~~~~~~~~
\section Plist43 fish_dead_1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/fish_dead_1.plist
[ ->] {
    frames =     {
        "fish_dead_1_cell-fish_dead_2.png" =         {
            frame = "{{2,2},{60,57}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{60,57}}";
            sourceSize = "{60,57}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "fish_dead_1.png";
        size = "{64,64}";
        smartupdate = "$TexturePacker:SmartUpdate:79fad5fa48447e7657e4e0b80ddfca01$";
        textureFileName = "fish_dead_1.png";
    };
}
~~~~~~~~~~~~~
\section Plist44 fish_dead_2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/fish_dead_2.plist
[ ->] {
    frames =     {
        "fish_dead_2_cell-siwnagxulie_00000.png" =         {
            frame = "{{97,200},{89,93}}";
            offset = "{2,2}";
            rotated = 1;
            sourceColorRect = "{{10,4},{89,93}}";
            sourceSize = "{105,105}";
        };
        "fish_dead_2_cell-siwnagxulie_00001.png" =         {
            frame = "{{2,200},{93,93}}";
            offset = "{2,0}";
            rotated = 0;
            sourceColorRect = "{{8,6},{93,93}}";
            sourceSize = "{105,105}";
        };
        "fish_dead_2_cell-siwnagxulie_00002.png" =         {
            frame = "{{103,101},{99,97}}";
            offset = "{1,-1}";
            rotated = 0;
            sourceColorRect = "{{4,5},{99,97}}";
            sourceSize = "{105,105}";
        };
        "fish_dead_2_cell-siwnagxulie_00003.png" =         {
            frame = "{{2,2},{99,99}}";
            offset = "{2,0}";
            rotated = 0;
            sourceColorRect = "{{5,3},{99,99}}";
            sourceSize = "{105,105}";
        };
        "fish_dead_2_cell-siwnagxulie_00004.png" =         {
            frame = "{{103,2},{99,97}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{4,4},{99,97}}";
            sourceSize = "{105,105}";
        };
        "fish_dead_2_cell-siwnagxulie_00005.png" =         {
            frame = "{{2,103},{95,95}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{6,5},{95,95}}";
            sourceSize = "{105,105}";
        };
        "fish_dead_2_cell-siwnagxulie_00006.png" =         {
            frame = "{{97,291},{89,91}}";
            offset = "{2,0}";
            rotated = 1;
            sourceColorRect = "{{10,7},{89,91}}";
            sourceSize = "{105,105}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "fish_dead_2.png";
        size = "{256,512}";
        smartupdate = "$TexturePacker:SmartUpdate:949ae214ee8c1487282bc3ef78311af6$";
        textureFileName = "fish_dead_2.png";
    };
}
~~~~~~~~~~~~~
\section Plist45 fort.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/fort.plist
[ ->] {
    frames =     {
        "fort_cell-4_00023.png" =         {
            frame = "{{206,438},{68,64}}";
            offset = "{3,10}";
            rotated = 1;
            sourceColorRect = "{{69,58},{68,64}}";
            sourceSize = "{200,200}";
        };
        "fort_cell-4_00024.png" =         {
            frame = "{{248,2},{82,80}}";
            offset = "{1,10}";
            rotated = 1;
            sourceColorRect = "{{60,50},{82,80}}";
            sourceSize = "{200,200}";
        };
        "fort_cell-4_00025.png" =         {
            frame = "{{210,112},{92,90}}";
            offset = "{0,8}";
            rotated = 1;
            sourceColorRect = "{{54,47},{92,90}}";
            sourceSize = "{200,200}";
        };
        "fort_cell-4_00026.png" =         {
            frame = "{{106,364},{100,98}}";
            offset = "{0,7}";
            rotated = 1;
            sourceColorRect = "{{50,44},{100,98}}";
            sourceSize = "{200,200}";
        };
        "fort_cell-4_00027.png" =         {
            frame = "{{106,146},{108,102}}";
            offset = "{0,6}";
            rotated = 1;
            sourceColorRect = "{{46,43},{108,102}}";
            sourceSize = "{200,200}";
        };
        "fort_cell-4_00028.png" =         {
            frame = "{{2,376},{112,102}}";
            offset = "{-1,5}";
            rotated = 1;
            sourceColorRect = "{{43,44},{112,102}}";
            sourceSize = "{200,200}";
        };
        "fort_cell-4_00029.png" =         {
            frame = "{{2,146},{114,102}}";
            offset = "{-1,4}";
            rotated = 1;
            sourceColorRect = "{{42,45},{114,102}}";
            sourceSize = "{200,200}";
        };
        "fort_cell-4_00030.png" =         {
            frame = "{{2,262},{112,102}}";
            offset = "{0,4}";
            rotated = 1;
            sourceColorRect = "{{44,45},{112,102}}";
            sourceSize = "{200,200}";
        };
        "fort_cell-4_00031.png" =         {
            frame = "{{146,2},{108,100}}";
            offset = "{1,3}";
            rotated = 1;
            sourceColorRect = "{{47,47},{108,100}}";
            sourceSize = "{200,200}";
        };
        "fort_cell-4_00032.png" =         {
            frame = "{{106,256},{106,82}}";
            offset = "{2,10}";
            rotated = 1;
            sourceColorRect = "{{49,49},{106,82}}";
            sourceSize = "{200,200}";
        };
        "fort_cell-4_00033.png" =         {
            frame = "{{190,256},{100,80}}";
            offset = "{1,10}";
            rotated = 1;
            sourceColorRect = "{{51,50},{100,80}}";
            sourceSize = "{200,200}";
        };
        "fort_cell-4_00034.png" =         {
            frame = "{{2,490},{4,4}}";
            offset = "{49,-28}";
            rotated = 0;
            sourceColorRect = "{{147,126},{4,4}}";
            sourceSize = "{200,200}";
        };
        "fort_cell-fort_1.png" =         {
            frame = "{{206,358},{76,78}}";
            offset = "{2,0}";
            rotated = 0;
            sourceColorRect = "{{14,11},{76,78}}";
            sourceSize = "{100,100}";
        };
        "fort_cell-fort_2.png" =         {
            frame = "{{2,2},{142,142}}";
            offset = "{3,-4}";
            rotated = 0;
            sourceColorRect = "{{32,33},{142,142}}";
            sourceSize = "{200,200}";
        };
        "fort_cell-fort_3.png" =         {
            frame = "{{106,466},{40,40}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{40,40}}";
            sourceSize = "{40,40}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "fort.png";
        size = "{512,512}";
        smartupdate = "$TexturePacker:SmartUpdate:75476efa3df2d0a08bcd9ec57da092da$";
        textureFileName = "fort.png";
    };
}
~~~~~~~~~~~~~
\section Plist46 frozen.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/frozen.plist
[ ->] {
    frames =     {
        "frozen_cell-frozen_2.png" =         {
            frame = "{{2,2},{179,134}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{179,134}}";
            sourceSize = "{179,134}";
        };
        "frozen_cell-frozen_3.png" =         {
            frame = "{{183,134},{64,64}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,64}}";
            sourceSize = "{64,64}";
        };
        "frozen_cell-frozen_4.png" =         {
            frame = "{{183,68},{64,64}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,64}}";
            sourceSize = "{64,64}";
        };
        "frozen_cell-frozen_5.png" =         {
            frame = "{{183,2},{64,64}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,64}}";
            sourceSize = "{64,64}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "frozen.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:2a84b5ed8b60447769784f59b8471731$";
        textureFileName = "frozen.png";
    };
}
~~~~~~~~~~~~~
\section Plist47 guessfinger_bubble.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/guessfinger_bubble.plist
[ ->] {
    frames =     {
        "ef_bzc_cell-ef_bzc_pp.png" =         {
            frame = "{{2,2},{166,166}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{166,166}}";
            sourceSize = "{166,166}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "guessfinger_bubble.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:cf54b3d0048ceda7c2a20c5f7743faee$";
        textureFileName = "guessfinger_bubble.png";
    };
}
~~~~~~~~~~~~~
\section Plist48 hetun_explode.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/hetun_explode.plist
[ ->] {
    frames =     {
        "hetun_explode_cell-001.png" =         {
            frame = "{{2,2},{104,104}}";
            offset = "{3,-4}";
            rotated = 0;
            sourceColorRect = "{{7,8},{104,104}}";
            sourceSize = "{112,112}";
        };
        "hetun_explode_cell-002.png" =         {
            frame = "{{100,108},{70,70}}";
            offset = "{-1,5}";
            rotated = 0;
            sourceColorRect = "{{20,16},{70,70}}";
            sourceSize = "{112,112}";
        };
        "hetun_explode_cell-003.png" =         {
            frame = "{{2,108},{96,100}}";
            offset = "{-2,-4}";
            rotated = 0;
            sourceColorRect = "{{6,10},{96,100}}";
            sourceSize = "{112,112}";
        };
        "hetun_explode_cell-guang4.png" =         {
            frame = "{{2,210},{31,33}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{4,2},{31,33}}";
            sourceSize = "{37,37}";
        };
        "hetun_explode_cell-guang5_00000.png" =         {
            frame = "{{51,210},{18,18}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{1,1},{18,18}}";
            sourceSize = "{20,20}";
        };
        "hetun_explode_cell-xiantiao.png" =         {
            frame = "{{35,210},{14,30}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{3,0},{14,30}}";
            sourceSize = "{20,30}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "hetun_explode.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:dec5c595eee119e621f2ac5799f3066e$";
        textureFileName = "hetun_explode.png";
    };
}
~~~~~~~~~~~~~
\section Plist49 huangjinpao.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/huangjinpao.plist
[ ->] {
    frames =     {
        "huangjinpao_cell-glod_1.png" =         {
            frame = "{{85,58},{27,68}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{27,68}}";
            sourceSize = "{27,68}";
        };
        "huangjinpao_cell-glod_2.png" =         {
            frame = "{{2,61},{55,22}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{55,22}}";
            sourceSize = "{55,22}";
        };
        "huangjinpao_cell-glod_3.png" =         {
            frame = "{{26,61},{21,22}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{21,22}}";
            sourceSize = "{21,22}";
        };
        "huangjinpao_cell-glod_4.png" =         {
            frame = "{{2,2},{81,57}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{81,57}}";
            sourceSize = "{81,57}";
        };
        "huangjinpao_cell-glod_5.png" =         {
            frame = "{{85,30},{31,26}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{31,26}}";
            sourceSize = "{31,26}";
        };
        "huangjinpao_cell-glod_6.png" =         {
            frame = "{{85,2},{26,37}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{26,37}}";
            sourceSize = "{26,37}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "huangjinpao.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:72506d3c4759bc09ab1a8dbc1b2b01aa$";
        textureFileName = "huangjinpao.png";
    };
}
~~~~~~~~~~~~~
\section Plist50 huangjinpaodan.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/huangjinpaodan.plist
[ ->] {
    frames =     {
        "huangjinpaodan_cell-1_00000.png" =         {
            frame = "{{21,101},{17,18}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{0,2},{17,18}}";
            sourceSize = "{17,20}";
        };
        "huangjinpaodan_cell-1_00001.png" =         {
            frame = "{{2,101},{17,20}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{17,20}}";
            sourceSize = "{17,20}";
        };
        "huangjinpaodan_cell-1_00002.png" =         {
            frame = "{{2,82},{17,20}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{17,20}}";
            sourceSize = "{17,20}";
        };
        "huangjinpaodan_cell-1_00003.png" =         {
            frame = "{{24,72},{17,20}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{17,20}}";
            sourceSize = "{17,20}";
        };
        "huangjinpaodan_cell-1_00004.png" =         {
            frame = "{{2,63},{17,20}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{17,20}}";
            sourceSize = "{17,20}";
        };
        "huangjinpaodan_cell-1_00005.png" =         {
            frame = "{{31,53},{17,20}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{17,20}}";
            sourceSize = "{17,20}";
        };
        "huangjinpaodan_cell-1_00006.png" =         {
            frame = "{{2,82},{17,20}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{17,20}}";
            sourceSize = "{17,20}";
        };
        "huangjinpaodan_cell-1_00007.png" =         {
            frame = "{{31,34},{17,20}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{17,20}}";
            sourceSize = "{17,20}";
        };
        "huangjinpaodan_cell-pd_00005.png" =         {
            frame = "{{2,34},{27,27}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{4,4},{27,27}}";
            sourceSize = "{35,35}";
        };
        "huangjinpaodan_cell-tw.png" =         {
            frame = "{{2,2},{30,55}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{30,55}}";
            sourceSize = "{30,55}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "huangjinpaodan.png";
        size = "{64,128}";
        smartupdate = "$TexturePacker:SmartUpdate:bd34078d0346192dd6e5c370d368df2b$";
        textureFileName = "huangjinpaodan.png";
    };
}
~~~~~~~~~~~~~
\section Plist51 jinbi.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/jinbi.plist
[ ->] {
    frames =     {
        "jinbi_cell-jinbi_1.png" =         {
            frame = "{{49,2},{27,41}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{9,2},{27,41}}";
            sourceSize = "{45,45}";
        };
        "jinbi_cell-jinbi_2.png" =         {
            frame = "{{45,49},{39,41}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{3,2},{39,41}}";
            sourceSize = "{45,45}";
        };
        "jinbi_cell-jinbi_3.png" =         {
            frame = "{{2,49},{41,41}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{2,2},{41,41}}";
            sourceSize = "{45,45}";
        };
        "jinbi_cell-jinbi_4.png" =         {
            frame = "{{2,2},{45,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{45,45}}";
            sourceSize = "{45,45}";
        };
        "jinbi_cell-jinbi_5.png" =         {
            frame = "{{2,92},{27,41}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{9,2},{27,41}}";
            sourceSize = "{45,45}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "jinbi.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:48d06f6669a8c1e0306c26ae0e801681$";
        textureFileName = "jinbi.png";
    };
}
~~~~~~~~~~~~~
\section Plist52 jinbi_1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/jinbi_1.plist
[ ->] {
    frames =     {
        "jinbi_cell-jinbi_1.png" =         {
            frame = "{{49,2},{27,41}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{9,2},{27,41}}";
            sourceSize = "{45,45}";
        };
        "jinbi_cell-jinbi_2.png" =         {
            frame = "{{45,49},{39,41}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{3,2},{39,41}}";
            sourceSize = "{45,45}";
        };
        "jinbi_cell-jinbi_3.png" =         {
            frame = "{{2,49},{41,41}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{2,2},{41,41}}";
            sourceSize = "{45,45}";
        };
        "jinbi_cell-jinbi_4.png" =         {
            frame = "{{2,2},{45,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{45,45}}";
            sourceSize = "{45,45}";
        };
        "jinbi_cell-jinbi_5.png" =         {
            frame = "{{2,92},{27,41}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{9,2},{27,41}}";
            sourceSize = "{45,45}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "jinbi_1.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:aa62cad2c9c9a56d789f3cba1272adcb$";
        textureFileName = "jinbi_1.png";
    };
}
~~~~~~~~~~~~~
\section Plist53 lahuji_4.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/lahuji_4.plist
[ ->] {
    frames =     {
        "lahuji_4_cell-laohuji_xing_4.png" =         {
            frame = "{{2,2},{29,29}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{29,29}}";
            sourceSize = "{29,29}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "lahuji_4.png";
        size = "{64,64}";
        smartupdate = "$TexturePacker:SmartUpdate:1594d5f6b4dc7e261bc2e105e5d8625c$";
        textureFileName = "lahuji_4.png";
    };
}
~~~~~~~~~~~~~
\section Plist54 laohuji_jinbi_1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/laohuji_jinbi_1.plist
[ ->] {
    frames =     {
        "laohuji_jinbi_cell-star_1_1.png" =         {
            frame = "{{2,2},{45,43}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{45,43}}";
            sourceSize = "{45,43}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "laohuji_jinbi_1.png";
        size = "{64,64}";
        smartupdate = "$TexturePacker:SmartUpdate:f1937785fcaa27720d9f72f93a9b43a9$";
        textureFileName = "laohuji_jinbi_1.png";
    };
}
~~~~~~~~~~~~~
\section Plist55 laohuji_jinbi_2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/laohuji_jinbi_2.plist
[ ->] {
    frames =     {
        "laohuji_jinbi_2_cell-coin.png" =         {
            frame = "{{2,2},{120,123}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{120,123}}";
            sourceSize = "{120,123}";
        };
        "laohuji_jinbi_2_cell-icon_crysta.png" =         {
            frame = "{{2,124},{116,122}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{116,122}}";
            sourceSize = "{116,122}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "laohuji_jinbi_2.png";
        size = "{128,256}";
        smartupdate = "$TexturePacker:SmartUpdate:27870d49212b6fdf24283480ec93b748$";
        textureFileName = "laohuji_jinbi_2.png";
    };
}
~~~~~~~~~~~~~
\section Plist56 laohuji_shoubing.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/laohuji_shoubing.plist
[ ->] {
    frames =     {
        "laohuji_shoubing_cell-laohuji_shoubing_2.png" =         {
            frame = "{{56,2},{6,62}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{2,0},{6,62}}";
            sourceSize = "{8,62}";
        };
        "laohuji_shoubing_cell-laohuji_shoubing_3.png" =         {
            frame = "{{2,22},{30,30}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{30,30}}";
            sourceSize = "{30,30}";
        };
        "laohuji_shoubing_cell-laohuji_shoubing_4.png" =         {
            frame = "{{34,22},{18,28}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{18,28}}";
            sourceSize = "{18,28}";
        };
        "laohuji_shoubing_cell-laohuji_shoubing_5.png" =         {
            frame = "{{2,2},{18,52}}";
            offset = "{0,-1}";
            rotated = 1;
            sourceColorRect = "{{0,2},{18,52}}";
            sourceSize = "{18,54}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "laohuji_shoubing.png";
        size = "{64,128}";
        smartupdate = "$TexturePacker:SmartUpdate:86c18bec8c31f53a01a053a9edc4e9e1$";
        textureFileName = "laohuji_shoubing.png";
    };
}
~~~~~~~~~~~~~
\section Plist57 levelup.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/levelup.plist
[ ->] {
    frames =     {
        "levelup_cell-a001.png" =         {
            frame = "{{2,2},{246,59}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{3,3},{246,59}}";
            sourceSize = "{250,65}";
        };
        "levelup_cell-a002.png" =         {
            frame = "{{2,63},{111,111}}";
            offset = "{2,-1}";
            rotated = 0;
            sourceColorRect = "{{9,8},{111,111}}";
            sourceSize = "{125,125}";
        };
        "levelup_cell-baiqun.png" =         {
            frame = "{{176,165},{43,43}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{1,1},{43,43}}";
            sourceSize = "{45,45}";
        };
        "levelup_cell-ccc.png" =         {
            frame = "{{115,63},{98,100}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{2,0},{98,100}}";
            sourceSize = "{100,100}";
        };
        "levelup_cell-ddd.png" =         {
            frame = "{{2,176},{75,75}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{75,75}}";
            sourceSize = "{75,75}";
        };
        "levelup_cell-gaoguang.png" =         {
            frame = "{{79,176},{51,45}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{51,45}}";
            sourceSize = "{51,45}";
        };
        "levelup_cell-quan.png" =         {
            frame = "{{215,63},{32,32}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{32,32}}";
            sourceSize = "{32,32}";
        };
        "levelup_cell-xx.png" =         {
            frame = "{{126,165},{50,48}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,1},{50,48}}";
            sourceSize = "{50,50}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "levelup.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:d5af58579c201e1a673f61fbaf6e6030$";
        textureFileName = "levelup.png";
    };
}
~~~~~~~~~~~~~
\section Plist58 light_background.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/light_background.plist
[ ->] {
    frames =     {
        "light_background_cell-light_background_1.png" =         {
            frame = "{{2,2},{224,224}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{224,224}}";
            sourceSize = "{224,224}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "light_background.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:b1bd8d4ad37be5a3d742286f44ef94d2$";
        textureFileName = "light_background.png";
    };
}
~~~~~~~~~~~~~
\section Plist59 lightening.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/lightening.plist
[ ->] {
    frames =     {
        "lightening_cell-lightening_1.png" =         {
            frame = "{{89,2},{63,63}}";
            offset = "{0,-2}";
            rotated = 0;
            sourceColorRect = "{{11,13},{63,63}}";
            sourceSize = "{85,85}";
        };
        "lightening_cell-lightening_2.png" =         {
            frame = "{{2,172},{81,81}}";
            offset = "{0,-2}";
            rotated = 0;
            sourceColorRect = "{{2,4},{81,81}}";
            sourceSize = "{85,85}";
        };
        "lightening_cell-lightening_3.png" =         {
            frame = "{{2,2},{85,85}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{85,85}}";
            sourceSize = "{85,85}";
        };
        "lightening_cell-lightening_4.png" =         {
            frame = "{{85,170},{77,77}}";
            offset = "{0,-2}";
            rotated = 0;
            sourceColorRect = "{{4,6},{77,77}}";
            sourceSize = "{85,85}";
        };
        "lightening_cell-lightening_5.png" =         {
            frame = "{{85,89},{79,79}}";
            offset = "{0,-2}";
            rotated = 0;
            sourceColorRect = "{{3,5},{79,79}}";
            sourceSize = "{85,85}";
        };
        "lightening_cell-lightening_6.png" =         {
            frame = "{{2,89},{81,81}}";
            offset = "{0,-2}";
            rotated = 0;
            sourceColorRect = "{{2,4},{81,81}}";
            sourceSize = "{85,85}";
        };
        "lightening_cell-lightening_7.png" =         {
            frame = "{{164,170},{67,69}}";
            offset = "{0,-2}";
            rotated = 0;
            sourceColorRect = "{{9,10},{67,69}}";
            sourceSize = "{85,85}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "lightening.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:ed4a31a31d417a55250f806d151c617c$";
        textureFileName = "lightening.png";
    };
}
~~~~~~~~~~~~~
\section Plist60 mission_text.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/mission_text.plist
[ ->] {
    frames =     {
        "mission_text_cell-mission_text_02.png" =         {
            frame = "{{2,2},{553,122}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{553,122}}";
            sourceSize = "{553,122}";
        };
        "mission_text_cell-mission_text_04.png" =         {
            frame = "{{2,659},{75,100}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{75,100}}";
            sourceSize = "{75,100}";
        };
        "mission_text_cell-mission_text_05.png" =         {
            frame = "{{2,557},{111,100}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{111,100}}";
            sourceSize = "{111,100}";
        };
        "mission_text_cell-mission_text_06.png" =         {
            frame = "{{2,736},{37,35}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{37,35}}";
            sourceSize = "{37,37}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "mission_text.png";
        size = "{128,1024}";
        smartupdate = "$TexturePacker:SmartUpdate:5605db5d48a6a34cffe48a8268aa5663$";
        textureFileName = "mission_text.png";
    };
}
~~~~~~~~~~~~~
\section Plist61 mission_text_1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/mission_text_1.plist
[ ->] {
    frames =     {
        "mission_text_1_cell-mission_text_07.png" =         {
            frame = "{{2,2},{62,62}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{62,62}}";
            sourceSize = "{62,62}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "mission_text_1.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:4d89bfab144f957b68fea17b04bbfd44$";
        textureFileName = "mission_text_1.png";
    };
}
~~~~~~~~~~~~~
\section Plist62 net.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/net.plist
[ ->] {
    frames =     {
        "net_cell-001.png" =         {
            frame = "{{88,265},{79,81}}";
            offset = "{-1,0}";
            rotated = 1;
            sourceColorRect = "{{12,12},{79,81}}";
            sourceSize = "{105,105}";
        };
        "net_cell-002.png" =         {
            frame = "{{222,2},{24,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{24,24}}";
            sourceSize = "{24,24}";
        };
        "net_cell-003.png" =         {
            frame = "{{122,346},{37,37}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{37,37}}";
            sourceSize = "{37,37}";
        };
        "net_cell-004.png" =         {
            frame = "{{188,227},{47,47}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{47,47}}";
            sourceSize = "{47,47}";
        };
        "net_cell-006.png" =         {
            frame = "{{97,176},{89,87}}";
            offset = "{2,-2}";
            rotated = 0;
            sourceColorRect = "{{5,6},{89,87}}";
            sourceSize = "{95,95}";
        };
        "net_cell-007.png" =         {
            frame = "{{171,276},{77,77}}";
            offset = "{3,-1}";
            rotated = 0;
            sourceColorRect = "{{12,10},{77,77}}";
            sourceSize = "{95,95}";
        };
        "net_cell-008.png" =         {
            frame = "{{2,100},{93,91}}";
            offset = "{1,-2}";
            rotated = 0;
            sourceColorRect = "{{2,4},{93,91}}";
            sourceSize = "{95,95}";
        };
        "net_cell-009.png" =         {
            frame = "{{2,277},{71,71}}";
            offset = "{-2,-1}";
            rotated = 0;
            sourceColorRect = "{{0,3},{71,71}}";
            sourceSize = "{75,75}";
        };
        "net_cell-1_00000.png" =         {
            frame = "{{112,90},{106,84}}";
            offset = "{5,-17}";
            rotated = 0;
            sourceColorRect = "{{17,40},{106,84}}";
            sourceSize = "{130,130}";
        };
        "net_cell-1_00001.png" =         {
            frame = "{{2,193},{84,82}}";
            offset = "{7,-4}";
            rotated = 0;
            sourceColorRect = "{{30,28},{84,82}}";
            sourceSize = "{130,130}";
        };
        "net_cell-1_00002.png" =         {
            frame = "{{112,2},{108,86}}";
            offset = "{0,-2}";
            rotated = 0;
            sourceColorRect = "{{11,24},{108,86}}";
            sourceSize = "{130,130}";
        };
        "net_cell-1_00003.png" =         {
            frame = "{{2,2},{96,108}}";
            offset = "{-1,-2}";
            rotated = 1;
            sourceColorRect = "{{16,13},{96,108}}";
            sourceSize = "{130,130}";
        };
        "net_cell-222.png" =         {
            frame = "{{75,346},{45,45}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{6,5},{45,45}}";
            sourceSize = "{55,55}";
        };
        "net_cell-333.png" =         {
            frame = "{{188,176},{53,49}}";
            offset = "{-1,-1}";
            rotated = 0;
            sourceColorRect = "{{10,14},{53,49}}";
            sourceSize = "{75,75}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "net.png";
        size = "{256,512}";
        smartupdate = "$TexturePacker:SmartUpdate:fc67655862b043ac87f9befecd758fc5$";
        textureFileName = "net.png";
    };
}
~~~~~~~~~~~~~
\section Plist63 new_record.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/new_record.plist
[ ->] {
    frames =     {
        "new_record_cell-score_star_1.png" =         {
            frame = "{{370,2},{207,114}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{207,114}}";
            sourceSize = "{207,114}";
        };
        "new_record_cell-score_star_2.png" =         {
            frame = "{{2,2},{366,338}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{366,338}}";
            sourceSize = "{366,338}";
        };
        "new_record_cell-score_star_3.png" =         {
            frame = "{{370,211},{245,102}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{245,102}}";
            sourceSize = "{245,102}";
        };
        "new_record_cell-ui_score_star1.png" =         {
            frame = "{{474,211},{25,25}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{25,25}}";
            sourceSize = "{25,25}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "new_record.png";
        size = "{512,512}";
        smartupdate = "$TexturePacker:SmartUpdate:075781d8c6748e664248d9153e9f9660$";
        textureFileName = "new_record.png";
    };
}
~~~~~~~~~~~~~
\section Plist64 pao_1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/pao_1.plist
[ ->] {
    frames =     {
        "pao_1_cell-bursts_1.png" =         {
            frame = "{{53,109},{49,31}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{49,31}}";
            sourceSize = "{49,31}";
        };
        "pao_1_cell-bursts_10.png" =         {
            frame = "{{88,2},{38,17}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{1,1},{38,17}}";
            sourceSize = "{40,19}";
        };
        "pao_1_cell-bursts_11.png" =         {
            frame = "{{2,60},{68,26}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{68,26}}";
            sourceSize = "{68,26}";
        };
        "pao_1_cell-bursts_2.png" =         {
            frame = "{{2,2},{84,56}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{84,56}}";
            sourceSize = "{84,56}";
        };
        "pao_1_cell-bursts_4.png" =         {
            frame = "{{49,169},{42,18}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{42,18}}";
            sourceSize = "{42,18}";
        };
        "pao_1_cell-bursts_5.png" =         {
            frame = "{{2,171},{34,18}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{34,18}}";
            sourceSize = "{34,18}";
        };
        "pao_1_cell-bursts_6.png" =         {
            frame = "{{2,223},{13,13}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{13,13}}";
            sourceSize = "{13,13}";
        };
        "pao_1_cell-bursts_9.png" =         {
            frame = "{{104,165},{13,19}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{13,19}}";
            sourceSize = "{13,19}";
        };
        "pao_1_cell-harpoon_3.png" =         {
            frame = "{{2,88},{68,19}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{68,19}}";
            sourceSize = "{68,19}";
        };
        "pao_1_cell-harpoon_4.png" =         {
            frame = "{{93,180},{23,26}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{23,26}}";
            sourceSize = "{23,26}";
        };
        "pao_1_cell-harpoon_5.png" =         {
            frame = "{{88,85},{25,10}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{25,10}}";
            sourceSize = "{25,10}";
        };
        "pao_1_cell-harpoon_7.png" =         {
            frame = "{{2,109},{41,49}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{41,49}}";
            sourceSize = "{41,49}";
        };
        "pao_1_cell-moonlight_1.png" =         {
            frame = "{{72,97},{51,10}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{51,10}}";
            sourceSize = "{51,10}";
        };
        "pao_1_cell-moonlight_2.png" =         {
            frame = "{{88,21},{62,37}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{62,37}}";
            sourceSize = "{62,37}";
        };
        "pao_1_cell-moonlight_20.png" =         {
            frame = "{{104,130},{33,19}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{33,19}}";
            sourceSize = "{33,19}";
        };
        "pao_1_cell-moonlight_3.png" =         {
            frame = "{{72,60},{14,35}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{14,35}}";
            sourceSize = "{14,35}";
        };
        "pao_1_cell-moonlight_5.png" =         {
            frame = "{{2,60},{68,26}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{68,26}}";
            sourceSize = "{68,26}";
        };
        "pao_1_cell-rolling_1.png" =         {
            frame = "{{2,152},{45,17}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{45,17}}";
            sourceSize = "{45,17}";
        };
        "pao_1_cell-rolling_2.png" =         {
            frame = "{{2,205},{16,12}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{16,12}}";
            sourceSize = "{16,12}";
        };
        "pao_1_cell-rolling_20.png" =         {
            frame = "{{104,109},{20,19}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{20,19}}";
            sourceSize = "{20,19}";
        };
        "pao_1_cell-rolling_3.png" =         {
            frame = "{{53,142},{49,25}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{49,25}}";
            sourceSize = "{49,25}";
        };
        "pao_1_cell-rolling_4.png" =         {
            frame = "{{2,60},{68,26}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{68,26}}";
            sourceSize = "{68,26}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "pao_1.png";
        size = "{128,256}";
        smartupdate = "$TexturePacker:SmartUpdate:ebb96b42fe186d51f917ca20fdc4e075$";
        textureFileName = "pao_1.png";
    };
}
~~~~~~~~~~~~~
\section Plist65 pao_2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/pao_2.plist
[ ->] {
    frames =     {
        "pao2_cell-bomb_1.png" =         {
            frame = "{{89,35},{53,18}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{53,18}}";
            sourceSize = "{53,18}";
        };
        "pao2_cell-bomb_1_0001.png" =         {
            frame = "{{175,112},{26,17}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{26,17}}";
            sourceSize = "{26,17}";
        };
        "pao2_cell-bomb_1_0002.png" =         {
            frame = "{{83,128},{22,17}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{2,0},{22,17}}";
            sourceSize = "{26,17}";
        };
        "pao2_cell-bomb_1_0003.png" =         {
            frame = "{{142,129},{22,17}}";
            offset = "{-1,0}";
            rotated = 0;
            sourceColorRect = "{{1,0},{22,17}}";
            sourceSize = "{26,17}";
        };
        "pao2_cell-bomb_2.png" =         {
            frame = "{{219,18},{35,10}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{3,0},{35,10}}";
            sourceSize = "{39,10}";
        };
        "pao2_cell-bomb_20.png" =         {
            frame = "{{213,71},{40,19}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{40,19}}";
            sourceSize = "{40,19}";
        };
        "pao2_cell-bomb_4.png" =         {
            frame = "{{131,116},{23,9}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{23,9}}";
            sourceSize = "{23,9}";
        };
        "pao2_cell-bomb_5.png" =         {
            frame = "{{219,30},{18,33}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{18,33}}";
            sourceSize = "{18,33}";
        };
        "pao2_cell-bullet_1.png" =         {
            frame = "{{2,124},{39,19}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{39,19}}";
            sourceSize = "{39,19}";
        };
        "pao2_cell-bullet_2.png" =         {
            frame = "{{219,2},{35,14}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{3,0},{35,14}}";
            sourceSize = "{39,14}";
        };
        "pao2_cell-bullet_4.png" =         {
            frame = "{{197,63},{26,14}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{26,14}}";
            sourceSize = "{26,14}";
        };
        "pao2_cell-bullet_5_1.png" =         {
            frame = "{{143,103},{30,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{30,24}}";
            sourceSize = "{30,24}";
        };
        "pao2_cell-bullet_5_2.png" =         {
            frame = "{{143,77},{30,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{30,24}}";
            sourceSize = "{30,24}";
        };
        "pao2_cell-bullet_5_3.png" =         {
            frame = "{{2,145},{30,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{30,24}}";
            sourceSize = "{30,24}";
        };
        "pao2_cell-bullet_5_4.png" =         {
            frame = "{{51,111},{30,24}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{30,24}}";
            sourceSize = "{30,24}";
        };
        "pao2_cell-bullet_6.png" =         {
            frame = "{{34,145},{18,4}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{18,4}}";
            sourceSize = "{18,4}";
        };
        "pao2_cell-bullet_7.png" =         {
            frame = "{{157,2},{30,60}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{30,60}}";
            sourceSize = "{30,60}";
        };
        "pao2_cell-bullet_g_1.png" =         {
            frame = "{{175,77},{16,18}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{16,18}}";
            sourceSize = "{16,18}";
        };
        "pao2_cell-bullet_g_2.png" =         {
            frame = "{{175,95},{15,18}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{15,18}}";
            sourceSize = "{15,18}";
        };
        "pao2_cell-bullet_g_3.png" =         {
            frame = "{{195,91},{15,18}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{15,18}}";
            sourceSize = "{15,18}";
        };
        "pao2_cell-lighting_1.png" =         {
            frame = "{{54,60},{49,31}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{49,31}}";
            sourceSize = "{49,31}";
        };
        "pao2_cell-lighting_11.png" =         {
            frame = "{{213,50},{41,19}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{41,19}}";
            sourceSize = "{41,19}";
        };
        "pao2_cell-lighting_12.png" =         {
            frame = "{{213,50},{41,19}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{41,19}}";
            sourceSize = "{41,19}";
        };
        "pao2_cell-lighting_2.png" =         {
            frame = "{{2,2},{85,56}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{85,56}}";
            sourceSize = "{85,56}";
        };
        "pao2_cell-lighting_3.png" =         {
            frame = "{{135,69},{45,6}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{45,6}}";
            sourceSize = "{45,6}";
        };
        "pao2_cell-lighting_4.png" =         {
            frame = "{{143,63},{52,12}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{52,12}}";
            sourceSize = "{52,12}";
        };
        "pao2_cell-lighting_5.png" =         {
            frame = "{{144,35},{26,11}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{26,11}}";
            sourceSize = "{26,11}";
        };
        "pao2_cell-lighting_7.png" =         {
            frame = "{{89,2},{31,66}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{31,66}}";
            sourceSize = "{31,66}";
        };
        "pao2_cell-lighting_8.png" =         {
            frame = "{{2,60},{37,50}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{37,50}}";
            sourceSize = "{37,50}";
        };
        "pao2_cell-projectile_10.png" =         {
            frame = "{{87,107},{19,42}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{19,42}}";
            sourceSize = "{19,42}";
        };
        "pao2_cell-projectile_3.png" =         {
            frame = "{{157,34},{54,27}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{54,27}}";
            sourceSize = "{54,27}";
        };
        "pao2_cell-projectile_4.png" =         {
            frame = "{{89,55},{52,12}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{52,12}}";
            sourceSize = "{52,12}";
        };
        "pao2_cell-projectile_5.png" =         {
            frame = "{{2,99},{47,23}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{47,23}}";
            sourceSize = "{47,23}";
        };
        "pao2_cell-projectile_6.png" =         {
            frame = "{{87,69},{46,36}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{46,36}}";
            sourceSize = "{46,36}";
        };
        "pao2_cell-projectile_7.png" =         {
            frame = "{{43,124},{22,6}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{22,6}}";
            sourceSize = "{22,6}";
        };
        "pao2_cell-projectile_8.png" =         {
            frame = "{{212,92},{28,28}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{28,28}}";
            sourceSize = "{28,28}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "pao_2.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:af94892e7e9ef60d27ca2fb2e5139a54$";
        textureFileName = "pao_2.png";
    };
}
~~~~~~~~~~~~~
\section Plist66 pao_3.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/pao_3.plist
[ ->] {
    frames =     {
        "pao_3_cell-bursts_10.png" =         {
            frame = "{{46,87},{40,19}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{40,19}}";
            sourceSize = "{40,19}";
        };
        "pao_3_cell-circle_1.png" =         {
            frame = "{{88,123},{24,8}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{24,8}}";
            sourceSize = "{24,8}";
        };
        "pao_3_cell-circle_10.png" =         {
            frame = "{{174,2},{72,36}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{72,36}}";
            sourceSize = "{72,36}";
        };
        "pao_3_cell-circle_11.png" =         {
            frame = "{{174,40},{69,22}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{69,22}}";
            sourceSize = "{69,22}";
        };
        "pao_3_cell-circle_12.png" =         {
            frame = "{{46,129},{40,18}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{40,18}}";
            sourceSize = "{40,18}";
        };
        "pao_3_cell-circle_2.png" =         {
            frame = "{{80,60},{24,8}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{24,8}}";
            sourceSize = "{24,8}";
        };
        "pao_3_cell-circle_3.png" =         {
            frame = "{{70,60},{24,8}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{24,8}}";
            sourceSize = "{24,8}";
        };
        "pao_3_cell-circle_4.png" =         {
            frame = "{{60,60},{24,8}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{24,8}}";
            sourceSize = "{24,8}";
        };
        "pao_3_cell-circle_5.png" =         {
            frame = "{{50,60},{24,8}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{24,8}}";
            sourceSize = "{24,8}";
        };
        "pao_3_cell-circle_6.png" =         {
            frame = "{{2,157},{62,40}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{62,40}}";
            sourceSize = "{62,40}";
        };
        "pao_3_cell-circle_7.png" =         {
            frame = "{{141,60},{13,12}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{13,12}}";
            sourceSize = "{13,12}";
        };
        "pao_3_cell-gas_1.png" =         {
            frame = "{{245,40},{45,8}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{45,8}}";
            sourceSize = "{45,8}";
        };
        "pao_3_cell-gas_2.png" =         {
            frame = "{{2,87},{68,26}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{68,26}}";
            sourceSize = "{68,26}";
        };
        "pao_3_cell-gas_3.png" =         {
            frame = "{{88,2},{84,56}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{84,56}}";
            sourceSize = "{84,56}";
        };
        "pao_3_cell-gas_4.png" =         {
            frame = "{{88,87},{34,18}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{34,18}}";
            sourceSize = "{34,18}";
        };
        "pao_3_cell-gas_5.png" =         {
            frame = "{{123,60},{12,16}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{12,16}}";
            sourceSize = "{12,16}";
        };
        "pao_3_cell-gas_6.png" =         {
            frame = "{{2,64},{21,36}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{21,36}}";
            sourceSize = "{21,36}";
        };
        "pao_3_cell-shoulijian_1.png" =         {
            frame = "{{2,221},{49,31}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{49,31}}";
            sourceSize = "{49,31}";
        };
        "pao_3_cell-shoulijian_2.png" =         {
            frame = "{{2,2},{84,56}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{84,56}}";
            sourceSize = "{84,56}";
        };
        "pao_3_cell-shoulijian_3.png" =         {
            frame = "{{30,87},{60,14}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{60,14}}";
            sourceSize = "{60,14}";
        };
        "pao_3_cell-shoulijian_4.png" =         {
            frame = "{{53,209},{44,56}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{44,56}}";
            sourceSize = "{44,56}";
        };
        "pao_3_cell-shoulijian_5.png" =         {
            frame = "{{40,64},{8,21}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{8,21}}";
            sourceSize = "{8,21}";
        };
        "pao_3_cell-shoulijian_6.png" =         {
            frame = "{{44,149},{46,58}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{46,58}}";
            sourceSize = "{46,58}";
        };
        "pao_3_cell-shoulijian_8.png" =         {
            frame = "{{90,64},{31,19}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{31,19}}";
            sourceSize = "{31,19}";
        };
        "pao_3_cell-shoulijian_9.png" =         {
            frame = "{{67,87},{39,19}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{39,19}}";
            sourceSize = "{39,19}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "pao_3.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:0a0263aa9203407d734e7d6bfd12ed72$";
        textureFileName = "pao_3.png";
    };
}
~~~~~~~~~~~~~
\section Plist67 radar.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/radar.plist
[ ->] {
    frames =     {
        "radar_cell-radar_1.png" =         {
            frame = "{{2,2},{79,80}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{79,80}}";
            sourceSize = "{79,80}";
        };
        "radar_cell-radar_2.png" =         {
            frame = "{{84,2},{8,8}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{8,8}}";
            sourceSize = "{8,8}";
        };
        "radar_cell-radar_3.png" =         {
            frame = "{{2,83},{73,73}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{73,73}}";
            sourceSize = "{73,73}";
        };
        "radar_cell-radar_4.png" =         {
            frame = "{{2,158},{60,60}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{60,60}}";
            sourceSize = "{60,60}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "radar.png";
        size = "{128,256}";
        smartupdate = "$TexturePacker:SmartUpdate:48537712b99827e5faa9d85365a81caa$";
        textureFileName = "radar.png";
    };
}
~~~~~~~~~~~~~
\section Plist68 reward_get.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/reward_get.plist
[ ->] {
    frames =     {
        "reward_get_cell-1.png" =         {
            frame = "{{2,57},{47,47}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{47,47}}";
            sourceSize = "{47,47}";
        };
        "reward_get_cell-2.png" =         {
            frame = "{{2,2},{53,53}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{53,53}}";
            sourceSize = "{53,53}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "reward_get.png";
        size = "{64,128}";
        smartupdate = "$TexturePacker:SmartUpdate:504674b3c544e5d63e2f918cca2d423c$";
        textureFileName = "reward_get.png";
    };
}
~~~~~~~~~~~~~
\section Plist69 shayuyayouxi_3.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/shayuyayouxi_3.plist
[ ->] {
    frames =     {
        "shayuyayouxi_3_cell-hengxian.png" =         {
            frame = "{{2,193},{256,75}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{256,75}}";
            sourceSize = "{256,75}";
        };
        "shayuyayouxi_3_cell-xiexian.png" =         {
            frame = "{{2,2},{113,93}}";
            offset = "{3,0}";
            rotated = 0;
            sourceColorRect = "{{15,22},{113,93}}";
            sourceSize = "{137,137}";
        };
        "shayuyayouxi_3_cell-yuan.png" =         {
            frame = "{{2,97},{94,94}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{94,94}}";
            sourceSize = "{94,94}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "shayuyayouxi_3.png";
        size = "{128,512}";
        smartupdate = "$TexturePacker:SmartUpdate:a9965c675df997e91b4fbc430da72fe6$";
        textureFileName = "shayuyayouxi_3.png";
    };
}
~~~~~~~~~~~~~
\section Plist70 slot_3.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/slot_3.plist
[ ->] {
    frames =     {
        "slot_3_cell-zhuan_down.png" =         {
            frame = "{{2,100},{98,96}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{98,96}}";
            sourceSize = "{98,96}";
        };
        "slot_3_cell-zhuan_up.png" =         {
            frame = "{{2,2},{99,96}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{99,96}}";
            sourceSize = "{99,96}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "slot_3.png";
        size = "{128,256}";
        smartupdate = "$TexturePacker:SmartUpdate:41950ca972edef9ad07fb3404105df7e$";
        textureFileName = "slot_3.png";
    };
}
~~~~~~~~~~~~~
\section Plist71 slot_arrow.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/slot_arrow.plist
[ ->] {
    frames =     {
        "slot_arrow_cell-bar_coin.png" =         {
            frame = "{{2,43},{36,39}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{36,39}}";
            sourceSize = "{36,39}";
        };
        "slot_arrow_cell-icon_arrow.png" =         {
            frame = "{{2,2},{39,43}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{39,43}}";
            sourceSize = "{39,43}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "slot_arrow.png";
        size = "{64,128}";
        smartupdate = "$TexturePacker:SmartUpdate:23358fe24fa00b9a68704e14516cc873$";
        textureFileName = "slot_arrow.png";
    };
}
~~~~~~~~~~~~~
\section Plist72 slot_bg_light.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/slot_bg_light.plist
[ ->] {
    frames =     {
        "slot_bg_light_cell-aa.png" =         {
            frame = "{{2,130},{65,65}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{6,5},{65,65}}";
            sourceSize = "{75,75}";
        };
        "slot_bg_light_cell-z02_00069.png" =         {
            frame = "{{2,2},{121,126}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{121,126}}";
            sourceSize = "{121,126}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "slot_bg_light.png";
        size = "{128,256}";
        smartupdate = "$TexturePacker:SmartUpdate:efa9b6cc574f88d142393c0a7ec23a62$";
        textureFileName = "slot_bg_light.png";
    };
}
~~~~~~~~~~~~~
\section Plist73 slot_light1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/slot_light1.plist
[ ->] {
    frames =     {
        "slot_light1_cell-k01_00020.png" =         {
            frame = "{{2,2},{78,98}}";
            offset = "{-1,0}";
            rotated = 0;
            sourceColorRect = "{{5,6},{78,98}}";
            sourceSize = "{90,110}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "slot_light1.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:d299c88ca3e9ea2adda4f5e3573a45e3$";
        textureFileName = "slot_light1.png";
    };
}
~~~~~~~~~~~~~
\section Plist74 slot_light2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/slot_light2.plist
[ ->] {
    frames =     {
        "slot_light2_cell-001.png" =         {
            frame = "{{2,2},{36,46}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{36,46}}";
            sourceSize = "{36,46}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "slot_light2.png";
        size = "{64,64}";
        smartupdate = "$TexturePacker:SmartUpdate:49212ebffdfe9a63271c144d9980de33$";
        textureFileName = "slot_light2.png";
    };
}
~~~~~~~~~~~~~
\section Plist75 slot_light3.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/slot_light3.plist
[ ->] {
    frames =     {
        "slot_light3_cell-q01_00043.png" =         {
            frame = "{{2,2},{55,55}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{55,55}}";
            sourceSize = "{55,55}";
        };
        "slot_light3_cell-q02_00046.png" =         {
            frame = "{{2,59},{45,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{45,45}}";
            sourceSize = "{45,45}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "slot_light3.png";
        size = "{64,128}";
        smartupdate = "$TexturePacker:SmartUpdate:b34eb3ea49304bc14057be82edeb7f10$";
        textureFileName = "slot_light3.png";
    };
}
~~~~~~~~~~~~~
\section Plist76 star.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/star.plist
[ ->] {
    frames =     {
        "star_cell-star_1.png" =         {
            frame = "{{2,96},{25,45}}";
            offset = "{1,0}";
            rotated = 1;
            sourceColorRect = "{{11,0},{25,45}}";
            sourceSize = "{45,45}";
        };
        "star_cell-star_2.png" =         {
            frame = "{{2,49},{37,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{4,0},{37,45}}";
            sourceSize = "{45,45}";
        };
        "star_cell-star_3.png" =         {
            frame = "{{2,2},{45,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{45,45}}";
            sourceSize = "{45,45}";
        };
        "star_cell-star_4.png" =         {
            frame = "{{41,49},{33,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{6,0},{33,45}}";
            sourceSize = "{45,45}";
        };
        "star_cell-star_5.png" =         {
            frame = "{{49,2},{23,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{11,0},{23,45}}";
            sourceSize = "{45,45}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "star.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:1b1183003ec3d6cb9db43c45e7c934f4$";
        textureFileName = "star.png";
    };
}
~~~~~~~~~~~~~
\section Plist77 submarine1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/submarine1.plist
[ ->] {
    frames =     {
        "submarine1_cell-001.png" =         {
            frame = "{{2,2},{122,70}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{122,70}}";
            sourceSize = "{122,70}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "submarine1.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:c52f844bc30d730189d9ac34acc4bcb7$";
        textureFileName = "submarine1.png";
    };
}
~~~~~~~~~~~~~
\section Plist78 submarine2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/submarine2.plist
[ ->] {
    frames =     {
        "submarine2_cell-001.png" =         {
            frame = "{{2,2},{132,88}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{132,88}}";
            sourceSize = "{132,88}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "submarine2.png";
        size = "{128,256}";
        smartupdate = "$TexturePacker:SmartUpdate:1666456220fe3922c1f8338bf711cdf6$";
        textureFileName = "submarine2.png";
    };
}
~~~~~~~~~~~~~
\section Plist79 submarine3.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/submarine3.plist
[ ->] {
    frames =     {
        "submarine3_cell-001.png" =         {
            frame = "{{2,2},{135,77}}";
            offset = "{0,1}";
            rotated = 1;
            sourceColorRect = "{{0,0},{135,77}}";
            sourceSize = "{135,79}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "submarine3.png";
        size = "{128,256}";
        smartupdate = "$TexturePacker:SmartUpdate:cfab3379865c54813b807192e0ecb453$";
        textureFileName = "submarine3.png";
    };
}
~~~~~~~~~~~~~
\section Plist80 submarine4.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/submarine4.plist
[ ->] {
    frames =     {
        "submarine4_cell-001.png" =         {
            frame = "{{2,2},{132,97}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{132,97}}";
            sourceSize = "{132,97}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "submarine4.png";
        size = "{128,256}";
        smartupdate = "$TexturePacker:SmartUpdate:2096e4ed02b975e3a4d758ac1f651f6f$";
        textureFileName = "submarine4.png";
    };
}
~~~~~~~~~~~~~
\section Plist81 submarine_button_effect.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/submarine_button_effect.plist
[ ->] {
    frames =     {
        "submarine_button_effect_cell-daliuguang.png" =         {
            frame = "{{11,466},{174,11}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{174,11}}";
            sourceSize = "{174,11}";
        };
        "submarine_button_effect_cell-dianguang_00051.png" =         {
            frame = "{{187,200},{54,61}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{54,61}}";
            sourceSize = "{54,61}";
        };
        "submarine_button_effect_cell-shaoguang.png" =         {
            frame = "{{187,2},{196,61}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{196,61}}";
            sourceSize = "{196,61}";
        };
        "submarine_button_effect_cell-shaoguangaa _00051.png" =         {
            frame = "{{85,2},{87,77}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{87,77}}";
            sourceSize = "{87,77}";
        };
        "submarine_button_effect_cell-xiaoliuguang.png" =         {
            frame = "{{2,466},{39,7}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{39,7}}";
            sourceSize = "{39,7}";
        };
        "submarine_button_effect_cell-zimushaoguagn.png" =         {
            frame = "{{2,2},{462,81}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{462,81}}";
            sourceSize = "{462,81}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "submarine_button_effect.png";
        size = "{256,512}";
        smartupdate = "$TexturePacker:SmartUpdate:3c0741b53721c6dd5096e92190307258$";
        textureFileName = "submarine_button_effect.png";
    };
}
~~~~~~~~~~~~~
\section Plist82 tanhao_1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/tanhao_1.plist
[ ->] {
    frames =     {
        "tanhao_1_cell-star_1.png" =         {
            frame = "{{2,2},{61,61}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{61,61}}";
            sourceSize = "{61,61}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "tanhao_1.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:66a8829958c7f93493398d209060f315$";
        textureFileName = "tanhao_1.png";
    };
}
~~~~~~~~~~~~~
\section Plist83 tanhao_2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/tanhao_2.plist
[ ->] {
    frames =     {
        "tanhao_2_cell-tanhao_2.png" =         {
            frame = "{{2,2},{25,39}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{25,39}}";
            sourceSize = "{25,39}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "tanhao_2.png";
        size = "{32,64}";
        smartupdate = "$TexturePacker:SmartUpdate:c70cbd353e68b5206d276bcc944400ab$";
        textureFileName = "tanhao_2.png";
    };
}
~~~~~~~~~~~~~
\section Plist84 target.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/target.plist
[ ->] {
    frames =     {
        "target_cell-target_1.png" =         {
            frame = "{{2,56},{42,41}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{42,41}}";
            sourceSize = "{42,41}";
        };
        "target_cell-target_2.png" =         {
            frame = "{{2,2},{52,52}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{52,52}}";
            sourceSize = "{52,52}";
        };
        "target_cell-target_3.png" =         {
            frame = "{{45,56},{37,37}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{37,37}}";
            sourceSize = "{37,37}";
        };
        "target_cell-target_4.png" =         {
            frame = "{{45,95},{29,29}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{29,29}}";
            sourceSize = "{29,29}";
        };
        "target_cell-target_5.png" =         {
            frame = "{{2,100},{17,17}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{17,17}}";
            sourceSize = "{17,17}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "target.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:be68e007ab434c0e07d87cd3c1b74b17$";
        textureFileName = "target.png";
    };
}
~~~~~~~~~~~~~
\section Plist85 target_2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/target_2.plist
[ ->] {
    frames =     {
        "target_2_cell-target_2_2.png" =         {
            frame = "{{2,2},{104,107}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{104,107}}";
            sourceSize = "{104,107}";
        };
        "target_2_cell-target_2_3.png" =         {
            frame = "{{2,111},{97,97}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{97,97}}";
            sourceSize = "{97,97}";
        };
        "target_2_cell-target_2_4.png" =         {
            frame = "{{101,111},{72,72}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{72,72}}";
            sourceSize = "{72,72}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "target_2.png";
        size = "{256,256}";
        smartupdate = "$TexturePacker:SmartUpdate:9ce1ea3eec75945ff33838b657864e7e$";
        textureFileName = "target_2.png";
    };
}
~~~~~~~~~~~~~
\section Plist86 treasure.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/treasure.plist
[ ->] {
    frames =     {
        "treasure_cell-treasure_1.png" =         {
            frame = "{{106,2},{20,20}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{20,20}}";
            sourceSize = "{20,20}";
        };
        "treasure_cell-treasure_2.png" =         {
            frame = "{{2,80},{40,40}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{40,40}}";
            sourceSize = "{40,40}";
        };
        "treasure_cell-treasure_3.png" =         {
            frame = "{{2,2},{102,76}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{102,76}}";
            sourceSize = "{102,76}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "treasure.png";
        size = "{128,128}";
        smartupdate = "$TexturePacker:SmartUpdate:26b2781b397dc516d6a12f5b8fd6de7d$";
        textureFileName = "treasure.png";
    };
}
~~~~~~~~~~~~~
\section Plist87 vip.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/flash/vip.plist
[ ->] {
    frames =     {
        "vip_cell-vip_01.png" =         {
            frame = "{{2,170},{141,50}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{141,50}}";
            sourceSize = "{141,50}";
        };
        "vip_cell-vip_02.png" =         {
            frame = "{{170,96},{86,57}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{86,57}}";
            sourceSize = "{86,57}";
        };
        "vip_cell-vip_03.png" =         {
            frame = "{{170,42},{61,52}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{61,52}}";
            sourceSize = "{61,52}";
        };
        "vip_cell-vip_04.png" =         {
            frame = "{{170,2},{83,38}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{83,38}}";
            sourceSize = "{83,38}";
        };
        "vip_cell-vip_05.png" =         {
            frame = "{{2,2},{166,166}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{166,166}}";
            sourceSize = "{166,166}";
        };
        "vip_cell-vip_06.png" =         {
            frame = "{{186,250},{28,30}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{28,30}}";
            sourceSize = "{28,30}";
        };
        "vip_cell-vip_n1.png" =         {
            frame = "{{229,96},{23,39}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{23,39}}";
            sourceSize = "{23,39}";
        };
        "vip_cell-vip_n2.png" =         {
            frame = "{{187,217},{31,40}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{31,40}}";
            sourceSize = "{31,40}";
        };
        "vip_cell-vip_n3.png" =         {
            frame = "{{145,184},{32,40}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{32,40}}";
            sourceSize = "{32,40}";
        };
        "vip_cell-vip_n4.png" =         {
            frame = "{{145,218},{31,39}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{31,39}}";
            sourceSize = "{31,39}";
        };
        "vip_cell-vip_n5.png" =         {
            frame = "{{187,184},{31,40}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{31,40}}";
            sourceSize = "{31,40}";
        };
        "vip_cell-vip_quan.png" =         {
            frame = "{{2,222},{136,138}}";
            offset = "{1,-1}";
            rotated = 1;
            sourceColorRect = "{{8,7},{136,138}}";
            sourceSize = "{150,150}";
        };
        "vip_cell-vip_xing.png" =         {
            frame = "{{233,42},{16,16}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{2,2},{16,16}}";
            sourceSize = "{20,20}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "vip.png";
        size = "{256,512}";
        smartupdate = "$TexturePacker:SmartUpdate:126f62392a7ae5cba4fe11cf77e272c4$";
        textureFileName = "vip.png";
    };
}
~~~~~~~~~~~~~
\section Plist89 achievements.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/achievements.plist
[ ->] {
    frames =     {
        "achi_light.png" =         {
            frame = "{{2,281},{62,60}}";
            offset = "{0,1}";
            rotated = 0;
            sourceColorRect = "{{1,1},{62,60}}";
            sourceSize = "{64,64}";
        };
        "achieve_add.png" =         {
            frame = "{{204,2},{80,40}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{80,40}}";
            sourceSize = "{80,40}";
        };
        "achieve_tip3.png" =         {
            frame = "{{2,192},{111,87}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{111,87}}";
            sourceSize = "{111,87}";
        };
        "achieve_tip_star_1.png" =         {
            frame = "{{202,123},{31,32}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{31,32}}";
            sourceSize = "{31,32}";
        };
        "achieve_tip_star_2.png" =         {
            frame = "{{202,84},{38,37}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{38,37}}";
            sourceSize = "{38,37}";
        };
        "achievement_text_add1.png" =         {
            frame = "{{2,58},{158,32}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{158,32}}";
            sourceSize = "{158,32}";
        };
        "achievement_text_add2.png" =         {
            frame = "{{2,24},{158,32}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{158,32}}";
            sourceSize = "{158,32}";
        };
        "achievement_title_bg.png" =         {
            frame = "{{155,177},{98,151}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{98,151}}";
            sourceSize = "{98,151}";
        };
        "achievement_title_bg2.png" =         {
            frame = "{{2,92},{98,151}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{98,151}}";
            sourceSize = "{98,151}";
        };
        "progress_achieve.png" =         {
            frame = "{{2,2},{200,20}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{200,20}}";
            sourceSize = "{200,20}";
        };
        "title_locked.png" =         {
            frame = "{{162,24},{151,38}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{151,38}}";
            sourceSize = "{151,38}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "achievements.png";
        size = "{256,512}";
        textureFileName = "achievements.png";
    };
}
~~~~~~~~~~~~~
\section Plist90 activity.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/activity.plist
[ ->] {
    frames =     {
        "card_back.png" =         {
            frame = "{{538,2},{365,460}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{365,460}}";
            sourceSize = "{365,460}";
        };
        "card_front.png" =         {
            frame = "{{2,464},{365,460}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{365,460}}";
            sourceSize = "{365,460}";
        };
        "card_front1.png" =         {
            frame = "{{538,369},{363,460}}";
            offset = "{-1,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{363,460}}";
            sourceSize = "{365,460}";
        };
        "card_front2.png" =         {
            frame = "{{2,2},{365,460}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{365,460}}";
            sourceSize = "{365,460}";
        };
        "card_move.png" =         {
            frame = "{{521,734},{250,230}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{250,230}}";
            sourceSize = "{250,230}";
        };
        "card_side.png" =         {
            frame = "{{191,926},{155,49}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{155,49}}";
            sourceSize = "{155,49}";
        };
        "icon_album.png" =         {
            frame = "{{2,926},{66,53}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{66,53}}";
            sourceSize = "{66,53}";
        };
        "pop.png" =         {
            frame = "{{369,42},{135,123}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{135,123}}";
            sourceSize = "{135,123}";
        };
        "text_card_exchange.png" =         {
            frame = "{{365,973},{171,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{171,45}}";
            sourceSize = "{171,45}";
        };
        "text_card_exchange_gray.png" =         {
            frame = "{{348,926},{171,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{171,45}}";
            sourceSize = "{171,45}";
        };
        "text_result.png" =         {
            frame = "{{369,2},{151,38}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{151,38}}";
            sourceSize = "{151,38}";
        };
        "text_showall.png" =         {
            frame = "{{191,977},{172,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{172,45}}";
            sourceSize = "{172,45}";
        };
        "text_start.png" =         {
            frame = "{{57,926},{132,62}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{132,62}}";
            sourceSize = "{132,62}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "activity.png";
        size = "{1024,1024}";
        textureFileName = "activity.png";
    };
}
~~~~~~~~~~~~~
\section Plist91 button.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/button.plist
[ ->] {
    frames =     {
        "bar_int.png" =         {
            frame = "{{201,986},{19,20}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{19,20}}";
            sourceSize = "{19,20}";
        };
        "bg.png" =         {
            frame = "{{176,592},{132,216}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{132,216}}";
            sourceSize = "{132,216}";
        };
        "bg_huodong.png" =         {
            frame = "{{265,2},{358,179}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{358,179}}";
            sourceSize = "{358,179}";
        };
        "bonus_time_less.png" =         {
            frame = "{{924,921},{40,38}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{40,38}}";
            sourceSize = "{40,38}";
        };
        "bonus_time_more.png" =         {
            frame = "{{987,960},{37,35}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{37,35}}";
            sourceSize = "{37,35}";
        };
        "box_close.png" =         {
            frame = "{{310,585},{172,173}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{172,173}}";
            sourceSize = "{172,173}";
        };
        "box_empty.png" =         {
            frame = "{{2,812},{172,218}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{172,218}}";
            sourceSize = "{172,218}";
        };
        "box_open.png" =         {
            frame = "{{2,592},{172,218}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{172,218}}";
            sourceSize = "{172,218}";
        };
        "btn1.png" =         {
            frame = "{{872,776},{70,52}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{70,52}}";
            sourceSize = "{70,52}";
        };
        "btn1_click.png" =         {
            frame = "{{924,643},{70,52}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{70,52}}";
            sourceSize = "{70,52}";
        };
        "btn1_grey.png" =         {
            frame = "{{852,649},{70,52}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{70,52}}";
            sourceSize = "{70,52}";
        };
        "btn3.png" =         {
            frame = "{{566,255},{26,25}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{26,25}}";
            sourceSize = "{26,25}";
        };
        "btn3_gray.png" =         {
            frame = "{{996,666},{26,25}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{26,25}}";
            sourceSize = "{26,25}";
        };
        "btn3_hint.png" =         {
            frame = "{{996,639},{26,25}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{26,25}}";
            sourceSize = "{26,25}";
        };
        "btn4.png" =         {
            frame = "{{872,830},{61,36}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{61,36}}";
            sourceSize = "{61,36}";
        };
        "btn4_click.png" =         {
            frame = "{{801,857},{61,36}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{61,36}}";
            sourceSize = "{61,36}";
        };
        "btn4_grey.png" =         {
            frame = "{{568,788},{61,36}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{61,36}}";
            sourceSize = "{61,36}";
        };
        "btn_add.png" =         {
            frame = "{{811,968},{70,52}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{70,52}}";
            sourceSize = "{70,52}";
        };
        "btn_add_click.png" =         {
            frame = "{{923,589},{70,52}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{70,52}}";
            sourceSize = "{70,52}";
        };
        "btn_add_grey.png" =         {
            frame = "{{566,183},{70,52}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{70,52}}";
            sourceSize = "{70,52}";
        };
        "btn_green.png" =         {
            frame = "{{849,703},{64,71}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,71}}";
            sourceSize = "{64,71}";
        };
        "btn_green_click.png" =         {
            frame = "{{808,895},{64,71}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,71}}";
            sourceSize = "{64,71}";
        };
        "btn_page.png" =         {
            frame = "{{310,760},{190,60}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{190,60}}";
            sourceSize = "{190,60}";
        };
        "btn_page_select.png" =         {
            frame = "{{309,822},{174,86}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{174,86}}";
            sourceSize = "{174,86}";
        };
        "button_achievement.png" =         {
            frame = "{{762,585},{88,91}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{88,91}}";
            sourceSize = "{88,91}";
        };
        "button_achievement_click.png" =         {
            frame = "{{222,929},{88,91}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{88,91}}";
            sourceSize = "{88,91}";
        };
        "button_weixin.png" =         {
            frame = "{{840,340},{112,110}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{112,110}}";
            sourceSize = "{112,110}";
        };
        "button_weixin2.png" =         {
            frame = "{{840,228},{112,110}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{112,110}}";
            sourceSize = "{112,110}";
        };
        "card_countdown_icon.png" =         {
            frame = "{{864,514},{76,60}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{76,60}}";
            sourceSize = "{76,60}";
        };
        "choose_text_bg.png" =         {
            frame = "{{265,183},{299,109}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{299,109}}";
            sourceSize = "{299,109}";
        };
        "common_buy_text.png" =         {
            frame = "{{737,975},{72,37}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{72,37}}";
            sourceSize = "{72,37}";
        };
        "fk.png" =         {
            frame = "{{230,365},{32,32}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{32,32}}";
            sourceSize = "{32,32}";
        };
        "gift_nail.png" =         {
            frame = "{{1011,164},{11,11}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{11,11}}";
            sourceSize = "{11,11}";
        };
        "gift_tile_bg.png" =         {
            frame = "{{977,164},{32,57}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{32,57}}";
            sourceSize = "{32,57}";
        };
        "gift_title_bg_2.png" =         {
            frame = "{{967,750},{50,50}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{50,50}}";
            sourceSize = "{50,50}";
        };
        "icon_dollar.png" =         {
            frame = "{{995,589},{26,48}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{26,48}}";
            sourceSize = "{26,48}";
        };
        "icon_new.png" =         {
            frame = "{{532,490},{20,21}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{20,21}}";
            sourceSize = "{20,21}";
        };
        "icon_rmb.png" =         {
            frame = "{{966,921},{36,37}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{36,37}}";
            sourceSize = "{36,37}";
        };
        "icon_scene02.png" =         {
            frame = "{{602,270},{119,118}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{119,118}}";
            sourceSize = "{119,118}";
        };
        "icon_scene03.png" =         {
            frame = "{{411,401},{119,118}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{119,118}}";
            sourceSize = "{119,118}";
        };
        "icon_weapon_1.png" =         {
            frame = "{{491,294},{85,109}}";
            offset = "{1,-1}";
            rotated = 1;
            sourceColorRect = "{{2,4},{85,109}}";
            sourceSize = "{87,115}";
        };
        "icon_weapon_10.png" =         {
            frame = "{{412,910},{84,106}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{84,106}}";
            sourceSize = "{84,106}";
        };
        "icon_weapon_15.png" =         {
            frame = "{{598,390},{99,101}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{99,101}}";
            sourceSize = "{99,101}";
        };
        "icon_weapon_20.png" =         {
            frame = "{{585,908},{84,108}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{84,108}}";
            sourceSize = "{84,108}";
        };
        "icon_weapon_30.png" =         {
            frame = "{{498,908},{85,108}}";
            offset = "{0,-2}";
            rotated = 0;
            sourceColorRect = "{{0,7},{85,108}}";
            sourceSize = "{85,118}";
        };
        "icon_weapon_40.png" =         {
            frame = "{{699,390},{84,108}}";
            offset = "{1,-1}";
            rotated = 0;
            sourceColorRect = "{{2,4},{84,108}}";
            sourceSize = "{86,114}";
        };
        "icon_weapon_5.png" =         {
            frame = "{{569,568},{84,109}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{84,109}}";
            sourceSize = "{84,109}";
        };
        "icon_weapon_50.png" =         {
            frame = "{{762,678},{85,103}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{85,103}}";
            sourceSize = "{85,103}";
        };
        "icon_weapon_60_1.png" =         {
            frame = "{{222,810},{85,117}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{0,2},{85,117}}";
            sourceSize = "{85,119}";
        };
        "icon_weapon_60_2.png" =         {
            frame = "{{484,568},{83,113}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{83,113}}";
            sourceSize = "{83,113}";
        };
        "icon_weapon_70.png" =         {
            frame = "{{569,679},{84,107}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{84,107}}";
            sourceSize = "{84,107}";
        };
        "item_fever.png" =         {
            frame = "{{801,783},{69,72}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{69,72}}";
            sourceSize = "{69,72}";
        };
        "item_fever_click.png" =         {
            frame = "{{737,901},{69,72}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{69,72}}";
            sourceSize = "{69,72}";
        };
        "item_fever_unable.png" =         {
            frame = "{{615,493},{69,72}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{69,72}}";
            sourceSize = "{69,72}";
        };
        "item_slot.png" =         {
            frame = "{{942,516},{69,71}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{69,71}}";
            sourceSize = "{69,71}";
        };
        "item_slot_click.png" =         {
            frame = "{{852,576},{69,71}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{69,71}}";
            sourceSize = "{69,71}";
        };
        "item_slot_gray.png" =         {
            frame = "{{793,497},{69,71}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{69,71}}";
            sourceSize = "{69,71}";
        };
        "light1.png" =         {
            frame = "{{2,365},{226,225}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{226,225}}";
            sourceSize = "{226,225}";
        };
        "lv.png" =         {
            frame = "{{915,756},{27,14}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{27,14}}";
            sourceSize = "{27,14}";
        };
        "lv_lock.png" =         {
            frame = "{{785,387},{49,60}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{49,60}}";
            sourceSize = "{49,60}";
        };
        "number_bg.png" =         {
            frame = "{{2,1005},{17,17}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{17,17}}";
            sourceSize = "{17,17}";
        };
        "ok_text.png" =         {
            frame = "{{411,521},{108,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{108,45}}";
            sourceSize = "{108,45}";
        };
        "percent_ch.png" =         {
            frame = "{{874,921},{48,39}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{48,39}}";
            sourceSize = "{48,39}";
        };
        "percentage_bar.png" =         {
            frame = "{{2,986},{197,17}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{197,17}}";
            sourceSize = "{197,17}";
        };
        "percentage_bar_back.png" =         {
            frame = "{{944,779},{21,21}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{21,21}}";
            sourceSize = "{21,21}";
        };
        "photo.png" =         {
            frame = "{{954,444},{68,70}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{68,70}}";
            sourceSize = "{68,70}";
        };
        "photo_click.png" =         {
            frame = "{{954,372},{68,70}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{68,70}}";
            sourceSize = "{68,70}";
        };
        "progress_1.png" =         {
            frame = "{{631,788},{18,23}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{18,23}}";
            sourceSize = "{18,23}";
        };
        "progress_2.png" =         {
            frame = "{{944,756},{21,21}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{21,21}}";
            sourceSize = "{21,21}";
        };
        "rank_btn_add.png" =         {
            frame = "{{935,961},{50,51}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{50,51}}";
            sourceSize = "{50,51}";
        };
        "rank_btn_add_gray.png" =         {
            frame = "{{926,868},{50,51}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{50,51}}";
            sourceSize = "{50,51}";
        };
        "rank_btn_click .png" =         {
            frame = "{{967,697},{50,51}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{50,51}}";
            sourceSize = "{50,51}";
        };
        "rank_btn_close.png" =         {
            frame = "{{561,832},{74,74}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{74,74}}";
            sourceSize = "{74,74}";
        };
        "rank_btn_close_clcik.png" =         {
            frame = "{{485,832},{74,74}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{74,74}}";
            sourceSize = "{74,74}";
        };
        "rank_btn_close_gray.png" =         {
            frame = "{{484,683},{74,74}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{74,74}}";
            sourceSize = "{74,74}";
        };
        "redeem.png" =         {
            frame = "{{521,521},{92,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{92,45}}";
            sourceSize = "{92,45}";
        };
        "redeem_icon.png" =         {
            frame = "{{874,452},{78,60}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{78,60}}";
            sourceSize = "{78,60}";
        };
        "sale_title_bg.png" =         {
            frame = "{{655,755},{93,56}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{93,56}}";
            sourceSize = "{93,56}";
        };
        "share_text.png" =         {
            frame = "{{785,452},{87,43}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{87,43}}";
            sourceSize = "{87,43}";
        };
        "slot_info.png" =         {
            frame = "{{915,703},{50,51}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{50,51}}";
            sourceSize = "{50,51}";
        };
        "slot_info_click.png" =         {
            frame = "{{883,962},{50,51}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{50,51}}";
            sourceSize = "{50,51}";
        };
        "slot_info_gray.png" =         {
            frame = "{{874,868},{50,51}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{50,51}}";
            sourceSize = "{50,51}";
        };
        "store.png" =         {
            frame = "{{719,813},{80,86}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{80,86}}";
            sourceSize = "{80,86}";
        };
        "store_click.png" =         {
            frame = "{{637,813},{80,86}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{80,86}}";
            sourceSize = "{80,86}";
        };
        "store_number_bg.png" =         {
            frame = "{{655,670},{105,83}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{105,83}}";
            sourceSize = "{105,83}";
        };
        "store_number_bg_click.png" =         {
            frame = "{{655,585},{105,83}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{105,83}}";
            sourceSize = "{105,83}";
        };
        "store_number_gray.png" =         {
            frame = "{{686,500},{105,83}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{105,83}}";
            sourceSize = "{105,83}";
        };
        "submarine1.png" =         {
            frame = "{{625,151},{204,117}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{204,117}}";
            sourceSize = "{204,117}";
        };
        "submarine2.png" =         {
            frame = "{{625,2},{220,147}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{220,147}}";
            sourceSize = "{220,147}";
        };
        "submarine3.png" =         {
            frame = "{{847,2},{224,128}}";
            offset = "{0,2}";
            rotated = 1;
            sourceColorRect = "{{0,0},{224,128}}";
            sourceSize = "{224,132}";
        };
        "submarine_arrow.png" =         {
            frame = "{{671,901},{64,107}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,107}}";
            sourceSize = "{64,107}";
        };
        "submarine_arrow_click.png" =         {
            frame = "{{532,381},{64,107}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,107}}";
            sourceSize = "{64,107}";
        };
        "submarine_bg1.png" =         {
            frame = "{{2,2},{261,361}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{261,361}}";
            sourceSize = "{261,361}";
        };
        "submarine_frame.png" =         {
            frame = "{{230,401},{179,182}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{179,182}}";
            sourceSize = "{179,182}";
        };
        "submarine_grey.png" =         {
            frame = "{{502,759},{64,71}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,71}}";
            sourceSize = "{64,71}";
        };
        "submarine_skill_bg.png" =         {
            frame = "{{265,294},{224,105}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{224,105}}";
            sourceSize = "{224,105}";
        };
        "system.png" =         {
            frame = "{{954,300},{68,70}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{68,70}}";
            sourceSize = "{68,70}";
        };
        "system_click.png" =         {
            frame = "{{954,228},{68,70}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{68,70}}";
            sourceSize = "{68,70}";
        };
        "text_accept.png" =         {
            frame = "{{977,2},{103,45}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{103,45}}";
            sourceSize = "{103,45}";
        };
        "time_bg.png" =         {
            frame = "{{723,270},{115,115}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{115,115}}";
            sourceSize = "{115,115}";
        };
        "time_progress.png" =         {
            frame = "{{312,910},{98,98}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{98,98}}";
            sourceSize = "{98,98}";
        };
        "title_bg.png" =         {
            frame = "{{944,802},{38,30}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{38,30}}";
            sourceSize = "{38,30}";
        };
        "title_bg_2.png" =         {
            frame = "{{977,107},{55,45}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{55,45}}";
            sourceSize = "{55,45}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "button.png";
        size = "{1024,1024}";
        textureFileName = "button.png";
    };
}
~~~~~~~~~~~~~
\section Plist94 choose_new.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/choose_new.plist
[ ->] {
    frames =     {
        "choose_arrow.png" =         {
            frame = "{{989,2},{32,48}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{32,48}}";
            sourceSize = "{32,48}";
        };
        "choose_bg2.png" =         {
            frame = "{{2,2},{683,68}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{683,68}}";
            sourceSize = "{683,68}";
        };
        "choose_bg3.png" =         {
            frame = "{{2,72},{343,194}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{343,194}}";
            sourceSize = "{343,194}";
        };
        "choose_btn1.png" =         {
            frame = "{{463,407},{186,186}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{186,186}}";
            sourceSize = "{186,186}";
        };
        "choose_btn_bug.png" =         {
            frame = "{{2,762},{217,124}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{217,124}}";
            sourceSize = "{217,124}";
        };
        "choose_btn_buy_click.png" =         {
            frame = "{{2,636},{217,124}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{217,124}}";
            sourceSize = "{217,124}";
        };
        "choose_btn_go1.png" =         {
            frame = "{{221,533},{217,124}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{217,124}}";
            sourceSize = "{217,124}";
        };
        "choose_btn_go2.png" =         {
            frame = "{{2,510},{217,124}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{217,124}}";
            sourceSize = "{217,124}";
        };
        "choose_btn_go3.png" =         {
            frame = "{{244,407},{217,124}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{217,124}}";
            sourceSize = "{217,124}";
        };
        "choose_door1.png" =         {
            frame = "{{347,201},{247,204}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{247,204}}";
            sourceSize = "{247,204}";
        };
        "choose_door2.png" =         {
            frame = "{{347,150},{251,49}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{251,49}}";
            sourceSize = "{251,49}";
        };
        "choose_door3.png" =         {
            frame = "{{347,72},{251,76}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{251,76}}";
            sourceSize = "{251,76}";
        };
        "choose_glass.png" =         {
            frame = "{{2,268},{240,240}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{240,240}}";
            sourceSize = "{240,240}";
        };
        "choose_icon_lock.png" =         {
            frame = "{{244,268},{97,110}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{97,110}}";
            sourceSize = "{97,110}";
        };
        "choose_info2.png" =         {
            frame = "{{661,304},{236,150}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{236,150}}";
            sourceSize = "{236,150}";
        };
        "choose_light1.png" =         {
            frame = "{{427,809},{140,140}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{140,140}}";
            sourceSize = "{140,140}";
        };
        "choose_light2.png" =         {
            frame = "{{1006,112},{15,48}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{15,48}}";
            sourceSize = "{15,48}";
        };
        "choose_light3.png" =         {
            frame = "{{989,112},{15,48}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{15,48}}";
            sourceSize = "{15,48}";
        };
        "choose_o1.png" =         {
            frame = "{{687,2},{300,300}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{300,300}}";
            sourceSize = "{300,300}";
        };
        "choose_o_zhuangshi1.png" =         {
            frame = "{{600,72},{47,77}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{47,77}}";
            sourceSize = "{47,77}";
        };
        "choose_scene1.png" =         {
            frame = "{{427,659},{148,147}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{148,147}}";
            sourceSize = "{148,147}";
        };
        "choose_scene2.png" =         {
            frame = "{{278,809},{148,147}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{148,147}}";
            sourceSize = "{148,147}";
        };
        "choose_scene3.png" =         {
            frame = "{{278,659},{148,147}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{148,147}}";
            sourceSize = "{148,147}";
        };
        "choose_scene_locked.png" =         {
            frame = "{{128,762},{148,148}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{148,148}}";
            sourceSize = "{148,148}";
        };
        "zhuangshi_1.png" =         {
            frame = "{{596,222},{63,111}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{63,111}}";
            sourceSize = "{63,111}";
        };
        "zhuangshi_2.png" =         {
            frame = "{{600,121},{99,76}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{99,76}}";
            sourceSize = "{99,76}";
        };
        "zhuangshi_3.png" =         {
            frame = "{{989,52},{58,25}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{58,25}}";
            sourceSize = "{58,25}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "choose_new.png";
        size = "{1024,1024}";
        textureFileName = "choose_new.png";
    };
}
~~~~~~~~~~~~~
\section Plist95 common_icon.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/common_icon.plist
[ ->] {
    frames =     {
        "achievement_page1.png" =         {
            frame = "{{716,217},{71,57}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{71,57}}";
            sourceSize = "{71,57}";
        };
        "achievement_page4.png" =         {
            frame = "{{578,723},{73,49}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{73,49}}";
            sourceSize = "{73,49}";
        };
        "achievement_page5.png" =         {
            frame = "{{543,774},{51,64}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{51,64}}";
            sourceSize = "{51,64}";
        };
        "arrow.png" =         {
            frame = "{{918,2},{98,57}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{98,57}}";
            sourceSize = "{98,57}";
        };
        "bar_coin.png" =         {
            frame = "{{381,921},{94,101}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{94,101}}";
            sourceSize = "{94,101}";
        };
        "bar_cristal.png" =         {
            frame = "{{873,304},{89,93}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,1},{89,93}}";
            sourceSize = "{89,95}";
        };
        "bar_event.png" =         {
            frame = "{{968,368},{54,54}}";
            offset = "{0,-4}";
            rotated = 0;
            sourceColorRect = "{{0,8},{54,54}}";
            sourceSize = "{54,62}";
        };
        "bar_form.png" =         {
            frame = "{{968,304},{54,62}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{54,62}}";
            sourceSize = "{54,62}";
        };
        "bar_xp.png" =         {
            frame = "{{309,167},{58,60}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{58,60}}";
            sourceSize = "{58,60}";
        };
        "btn_login.png" =         {
            frame = "{{771,503},{88,91}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{88,91}}";
            sourceSize = "{88,91}";
        };
        "btn_login_click.png" =         {
            frame = "{{864,485},{88,91}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{88,91}}";
            sourceSize = "{88,91}";
        };
        "btn_mission.png" =         {
            frame = "{{771,413},{88,91}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{88,91}}";
            sourceSize = "{88,91}";
        };
        "btn_mission_click.png" =         {
            frame = "{{869,395},{88,91}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{88,91}}";
            sourceSize = "{88,91}";
        };
        "btn_service.png.png" =         {
            frame = "{{735,681},{69,72}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{69,72}}";
            sourceSize = "{69,72}";
        };
        "btn_service_click.png" =         {
            frame = "{{88,950},{69,72}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{69,72}}";
            sourceSize = "{69,72}";
        };
        "button_weibo.png" =         {
            frame = "{{576,448},{112,110}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{112,110}}";
            sourceSize = "{112,110}";
        };
        "button_weibo2.png" =         {
            frame = "{{609,2},{112,110}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{112,110}}";
            sourceSize = "{112,110}";
        };
        "exchange.png" =         {
            frame = "{{809,742},{50,51}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{50,51}}";
            sourceSize = "{50,51}";
        };
        "fish_bg.png" =         {
            frame = "{{671,611},{83,61}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{83,61}}";
            sourceSize = "{83,61}";
        };
        "fish_check.png" =         {
            frame = "{{196,756},{37,32}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{2,0},{37,32}}";
            sourceSize = "{41,32}";
        };
        "icon.png" =         {
            frame = "{{660,674},{73,70}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{73,70}}";
            sourceSize = "{73,70}";
        };
        "icon_bianfuyu.png" =         {
            frame = "{{313,752},{114,33}}";
            offset = "{0,-5}";
            rotated = 0;
            sourceColorRect = "{{0,33},{114,33}}";
            sourceSize = "{114,89}";
        };
        "icon_bianfuyu_orange.png" =         {
            frame = "{{775,242},{110,35}}";
            offset = "{1,-4}";
            rotated = 0;
            sourceColorRect = "{{3,31},{110,35}}";
            sourceSize = "{114,89}";
        };
        "icon_board.png" =         {
            frame = "{{304,228},{123,95}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{123,95}}";
            sourceSize = "{123,95}";
        };
        "icon_cardpoint.png" =         {
            frame = "{{321,118},{46,46}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{46,46}}";
            sourceSize = "{46,46}";
        };
        "icon_cheqiyu.png" =         {
            frame = "{{543,684},{74,33}}";
            offset = "{-1,-3}";
            rotated = 1;
            sourceColorRect = "{{19,31},{74,33}}";
            sourceSize = "{114,89}";
        };
        "icon_denglongyu.png" =         {
            frame = "{{468,344},{102,87}}";
            offset = "{2,0}";
            rotated = 1;
            sourceColorRect = "{{8,1},{102,87}}";
            sourceSize = "{114,89}";
        };
        "icon_dianmanyu.png" =         {
            frame = "{{804,2},{112,53}}";
            offset = "{0,-7}";
            rotated = 0;
            sourceColorRect = "{{1,25},{112,53}}";
            sourceSize = "{114,89}";
        };
        "icon_diaoyu.png" =         {
            frame = "{{499,568},{114,83}}";
            offset = "{0,-2}";
            rotated = 1;
            sourceColorRect = "{{0,5},{114,83}}";
            sourceSize = "{114,89}";
        };
        "icon_dinianyu.png" =         {
            frame = "{{429,712},{112,73}}";
            offset = "{1,-8}";
            rotated = 0;
            sourceColorRect = "{{2,16},{112,73}}";
            sourceSize = "{114,89}";
        };
        "icon_douyu.png" =         {
            frame = "{{864,575},{78,89}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{18,0},{78,89}}";
            sourceSize = "{114,89}";
        };
        "icon_douyu_red.png" =         {
            frame = "{{775,322},{92,89}}";
            offset = "{-6,0}";
            rotated = 0;
            sourceColorRect = "{{5,0},{92,89}}";
            sourceSize = "{114,89}";
        };
        "icon_emptyfish.png" =         {
            frame = "{{366,449},{118,89}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{1,0},{118,89}}";
            sourceSize = "{120,89}";
        };
        "icon_fangyu.png" =         {
            frame = "{{243,396},{114,51}}";
            offset = "{0,-5}";
            rotated = 0;
            sourceColorRect = "{{0,24},{114,51}}";
            sourceSize = "{114,89}";
        };
        "icon_g_shayu.png" =         {
            frame = "{{721,2},{112,81}}";
            offset = "{-1,1}";
            rotated = 1;
            sourceColorRect = "{{0,3},{112,81}}";
            sourceSize = "{114,89}";
        };
        "icon_gaiputi.png" =         {
            frame = "{{539,225},{114,47}}";
            offset = "{0,-2}";
            rotated = 1;
            sourceColorRect = "{{0,23},{114,47}}";
            sourceSize = "{114,89}";
        };
        "icon_haigui.png" =         {
            frame = "{{470,228},{114,67}}";
            offset = "{0,-9}";
            rotated = 1;
            sourceColorRect = "{{0,20},{114,67}}";
            sourceSize = "{114,89}";
        };
        "icon_haigui_ice.png" =         {
            frame = "{{401,228},{114,67}}";
            offset = "{0,-9}";
            rotated = 1;
            sourceColorRect = "{{0,20},{114,67}}";
            sourceSize = "{114,89}";
        };
        "icon_hetun.png" =         {
            frame = "{{688,525},{84,77}}";
            offset = "{-4,0}";
            rotated = 1;
            sourceColorRect = "{{11,6},{84,77}}";
            sourceSize = "{114,89}";
        };
        "icon_hujing.png" =         {
            frame = "{{471,2},{114,71}}";
            offset = "{0,-1}";
            rotated = 1;
            sourceColorRect = "{{0,10},{114,71}}";
            sourceSize = "{114,89}";
        };
        "icon_jianyu.png" =         {
            frame = "{{544,2},{114,63}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,13},{114,63}}";
            sourceSize = "{114,89}";
        };
        "icon_jinqiangyu.png" =         {
            frame = "{{398,2},{114,71}}";
            offset = "{0,1}";
            rotated = 1;
            sourceColorRect = "{{0,8},{114,71}}";
            sourceSize = "{114,89}";
        };
        "icon_jinqiangyu_gold.png" =         {
            frame = "{{321,2},{114,75}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,7},{114,75}}";
            sourceSize = "{114,89}";
        };
        "icon_jinqiangyu_red.png" =         {
            frame = "{{159,949},{112,73}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{1,9},{112,73}}";
            sourceSize = "{114,89}";
        };
        "icon_moregames.png" =         {
            frame = "{{713,345},{79,56}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{79,56}}";
            sourceSize = "{79,56}";
        };
        "icon_sale_coin_1.png" =         {
            frame = "{{589,118},{104,86}}";
            offset = "{0,-4}";
            rotated = 1;
            sourceColorRect = "{{38,51},{104,86}}";
            sourceSize = "{180,180}";
        };
        "icon_sale_coin_2.png" =         {
            frame = "{{371,787},{132,104}}";
            offset = "{-1,-7}";
            rotated = 1;
            sourceColorRect = "{{23,45},{132,104}}";
            sourceSize = "{180,180}";
        };
        "icon_sale_coin_3.png" =         {
            frame = "{{236,602},{148,116}}";
            offset = "{0,-1}";
            rotated = 1;
            sourceColorRect = "{{16,33},{148,116}}";
            sourceSize = "{180,180}";
        };
        "icon_sale_coin_4.png" =         {
            frame = "{{2,628},{160,150}}";
            offset = "{1,-3}";
            rotated = 1;
            sourceColorRect = "{{11,18},{160,150}}";
            sourceSize = "{180,180}";
        };
        "icon_sale_coin_5.png" =         {
            frame = "{{2,450},{172,176}}";
            offset = "{0,-2}";
            rotated = 0;
            sourceColorRect = "{{4,4},{172,176}}";
            sourceSize = "{180,180}";
        };
        "icon_sale_crystal_1.png" =         {
            frame = "{{767,593},{88,86}}";
            offset = "{6,-3}";
            rotated = 0;
            sourceColorRect = "{{52,50},{88,86}}";
            sourceSize = "{180,180}";
        };
        "icon_sale_crystal_2.png" =         {
            frame = "{{371,118},{112,108}}";
            offset = "{4,1}";
            rotated = 0;
            sourceColorRect = "{{38,35},{112,108}}";
            sourceSize = "{180,180}";
        };
        "icon_sale_crystal_3.png" =         {
            frame = "{{256,449},{148,108}}";
            offset = "{10,-8}";
            rotated = 1;
            sourceColorRect = "{{26,44},{148,108}}";
            sourceSize = "{180,180}";
        };
        "icon_sale_crystal_4.png" =         {
            frame = "{{2,790},{128,158}}";
            offset = "{-5,-11}";
            rotated = 0;
            sourceColorRect = "{{21,22},{128,158}}";
            sourceSize = "{180,180}";
        };
        "icon_sale_crystal_5.png" =         {
            frame = "{{166,228},{136,166}}";
            offset = "{-2,-7}";
            rotated = 0;
            sourceColorRect = "{{20,14},{136,166}}";
            sourceSize = "{180,180}";
        };
        "icon_service.png" =         {
            frame = "{{688,443},{81,80}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{81,80}}";
            sourceSize = "{81,80}";
        };
        "icon_shayu.png" =         {
            frame = "{{890,223},{110,79}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{2,5},{110,79}}";
            sourceSize = "{114,89}";
        };
        "icon_shiziyu.png" =         {
            frame = "{{804,57},{110,96}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{2,10},{110,96}}";
            sourceSize = "{114,114}";
        };
        "icon_skill1.png" =         {
            frame = "{{946,702},{74,74}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{74,74}}";
            sourceSize = "{74,74}";
        };
        "icon_skill2.png" =         {
            frame = "{{916,61},{97,82}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{97,82}}";
            sourceSize = "{97,82}";
        };
        "icon_skill3.png" =         {
            frame = "{{955,588},{72,66}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{72,66}}";
            sourceSize = "{72,66}";
        };
        "icon_skill4.png" =         {
            frame = "{{653,746},{70,68}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{2,0},{70,68}}";
            sourceSize = "{72,68}";
        };
        "icon_skill5.png" =         {
            frame = "{{957,522},{64,64}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{4,1},{64,64}}";
            sourceSize = "{72,66}";
        };
        "icon_skill6.png" =         {
            frame = "{{584,562},{84,85}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{84,85}}";
            sourceSize = "{84,85}";
        };
        "icon_snowflake.png" =         {
            frame = "{{2,950},{72,84}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{72,84}}";
            sourceSize = "{72,84}";
        };
        "icon_task.png" =         {
            frame = "{{584,648},{73,74}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{73,74}}";
            sourceSize = "{73,74}";
        };
        "icon_tianshiyu.png" =         {
            frame = "{{401,596},{114,96}}";
            offset = "{0,-1}";
            rotated = 1;
            sourceColorRect = "{{0,10},{114,96}}";
            sourceSize = "{114,114}";
        };
        "icon_xiaochouyu.png" =         {
            frame = "{{778,155},{110,85}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{2,3},{110,85}}";
            sourceSize = "{114,89}";
        };
        "icon_xiaohuangyu.png" =         {
            frame = "{{235,756},{76,29}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{19,30},{76,29}}";
            sourceSize = "{114,89}";
        };
        "icon_yingzuiyu.png" =         {
            frame = "{{631,224},{112,83}}";
            offset = "{1,0}";
            rotated = 1;
            sourceColorRect = "{{2,3},{112,83}}";
            sourceSize = "{114,89}";
        };
        "light_background.png" =         {
            frame = "{{2,2},{224,224}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{224,224}}";
            sourceSize = "{224,224}";
        };
        "mission_box.png" =         {
            frame = "{{273,921},{106,101}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{106,101}}";
            sourceSize = "{106,101}";
        };
        "notice_goto.png" =         {
            frame = "{{775,279},{96,41}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{96,41}}";
            sourceSize = "{96,41}";
        };
        "notice_recieve1.png" =         {
            frame = "{{154,628},{154,40}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{154,40}}";
            sourceSize = "{154,40}";
        };
        "notice_recieve2.png" =         {
            frame = "{{588,225},{114,41}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{114,41}}";
            sourceSize = "{114,41}";
        };
        "package_1.png" =         {
            frame = "{{359,353},{107,94}}";
            offset = "{0,-3}";
            rotated = 0;
            sourceColorRect = "{{0,6},{107,94}}";
            sourceSize = "{107,100}";
        };
        "package_2.png" =         {
            frame = "{{614,341},{97,100}}";
            offset = "{0,-3}";
            rotated = 0;
            sourceColorRect = "{{0,6},{97,100}}";
            sourceSize = "{97,106}";
        };
        "package_3.png" =         {
            frame = "{{457,449},{117,117}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{117,117}}";
            sourceSize = "{117,117}";
        };
        "package_4.png" =         {
            frame = "{{677,116},{99,99}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{99,99}}";
            sourceSize = "{99,99}";
        };
        "page_cristal.png" =         {
            frame = "{{962,465},{55,55}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{55,55}}";
            sourceSize = "{55,55}";
        };
        "page_gift.png" =         {
            frame = "{{228,167},{79,59}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{79,59}}";
            sourceSize = "{79,59}";
        };
        "page_icon.png" =         {
            frame = "{{716,290},{53,57}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{53,57}}";
            sourceSize = "{53,57}";
        };
        "page_submarine.png" =         {
            frame = "{{166,396},{75,52}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{75,52}}";
            sourceSize = "{75,52}";
        };
        "page_weapon.png" =         {
            frame = "{{477,787},{62,61}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{62,61}}";
            sourceSize = "{62,61}";
        };
        "payment_1.png" =         {
            frame = "{{132,790},{157,104}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{157,104}}";
            sourceSize = "{157,104}";
        };
        "payment_2.png" =         {
            frame = "{{238,787},{132,131}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{132,131}}";
            sourceSize = "{132,131}";
        };
        "payment_3.png" =         {
            frame = "{{228,2},{163,91}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{163,91}}";
            sourceSize = "{163,91}";
        };
        "sale_gift.png" =         {
            frame = "{{857,655},{87,85}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{3,0},{87,85}}";
            sourceSize = "{91,85}";
        };
        "serve_icon.png" =         {
            frame = "{{962,424},{56,39}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{56,39}}";
            sourceSize = "{56,39}";
        };
        "store_more.png" =         {
            frame = "{{916,145},{76,96}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{76,96}}";
            sourceSize = "{76,96}";
        };
        "submarine4.png" =         {
            frame = "{{2,228},{220,162}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{220,162}}";
            sourceSize = "{220,162}";
        };
        "submarine_star.png" =         {
            frame = "{{304,353},{34,39}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{34,39}}";
            sourceSize = "{34,39}";
        };
        "text_daily.png" =         {
            frame = "{{216,450},{150,38}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{150,38}}";
            sourceSize = "{150,38}";
        };
        "text_mini_confirm.png" =         {
            frame = "{{196,603},{151,38}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{151,38}}";
            sourceSize = "{151,38}";
        };
        "text_mission_get.png" =         {
            frame = "{{557,341},{105,55}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{105,55}}";
            sourceSize = "{105,55}";
        };
        "text_mission_goto.png" =         {
            frame = "{{532,118},{105,55}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{105,55}}";
            sourceSize = "{105,55}";
        };
        "text_more_game.png" =         {
            frame = "{{366,569},{93,25}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{93,25}}";
            sourceSize = "{93,25}";
        };
        "text_pay_off.png" =         {
            frame = "{{354,599},{129,45}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{129,45}}";
            sourceSize = "{129,45}";
        };
        "text_store.png" =         {
            frame = "{{485,118},{108,45}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{108,45}}";
            sourceSize = "{108,45}";
        };
        "text_wancheng.png" =         {
            frame = "{{176,450},{151,38}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{151,38}}";
            sourceSize = "{151,38}";
        };
        "txt_notice.png" =         {
            frame = "{{946,662},{75,38}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{75,38}}";
            sourceSize = "{75,38}";
        };
        "xiahuaxian.png" =         {
            frame = "{{154,784},{2,2}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{2,2}}";
            sourceSize = "{2,2}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "common_icon.png";
        size = "{1024,1024}";
        textureFileName = "common_icon.png";
    };
}
~~~~~~~~~~~~~
\section Plist96 common_image.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/common_image.plist
[ ->] {
    frames =     {
        "avatar_shark.png" =         {
            frame = "{{296,167},{147,211}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{147,211}}";
            sourceSize = "{147,211}";
        };
        "bg_gold_kuang.png" =         {
            frame = "{{132,434},{37,34}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{37,34}}";
            sourceSize = "{37,34}";
        };
        "bg_notice.png" =         {
            frame = "{{132,473},{34,34}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{34,34}}";
            sourceSize = "{34,34}";
        };
        "bg_qianting2.png" =         {
            frame = "{{168,508},{119,63}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{119,63}}";
            sourceSize = "{119,63}";
        };
        "bg_qianting4.png" =         {
            frame = "{{447,481},{119,63}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{119,63}}";
            sourceSize = "{119,63}";
        };
        "bg_sale_2.png" =         {
            frame = "{{2,284},{166,108}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{166,108}}";
            sourceSize = "{166,108}";
        };
        "box_bg.png" =         {
            frame = "{{2,140},{226,142}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{226,142}}";
            sourceSize = "{226,142}";
        };
        "btn_blue.png" =         {
            frame = "{{2,616},{64,71}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{64,71}}";
            sourceSize = "{64,71}";
        };
        "btn_blue_click.png" =         {
            frame = "{{228,435},{64,71}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,71}}";
            sourceSize = "{64,71}";
        };
        "btn_character_share.png" =         {
            frame = "{{462,371},{108,45}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{108,45}}";
            sourceSize = "{108,45}";
        };
        "btnb_gray.png" =         {
            frame = "{{228,362},{64,71}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,71}}";
            sourceSize = "{64,71}";
        };
        "btnb_pk.png" =         {
            frame = "{{228,289},{64,71}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,71}}";
            sourceSize = "{64,71}";
        };
        "btnb_pk_clicked.png" =         {
            frame = "{{230,216},{64,71}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,71}}";
            sourceSize = "{64,71}";
        };
        "ditu3.png" =         {
            frame = "{{2,434},{128,96}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{128,96}}";
            sourceSize = "{128,96}";
        };
        "gift_bg.png" =         {
            frame = "{{2,2},{298,136}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{298,136}}";
            sourceSize = "{298,136}";
        };
        "start_bg.png" =         {
            frame = "{{478,2},{100,17}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{100,17}}";
            sourceSize = "{100,17}";
        };
        "store_bg1.png" =         {
            frame = "{{170,386},{100,56}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{100,56}}";
            sourceSize = "{100,56}";
        };
        "store_bg2.png" =         {
            frame = "{{170,284},{100,56}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{100,56}}";
            sourceSize = "{100,56}";
        };
        "store_light2.png" =         {
            frame = "{{462,316},{53,47}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{53,47}}";
            sourceSize = "{53,47}";
        };
        "store_light3.png" =         {
            frame = "{{2,532},{105,82}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{105,82}}";
            sourceSize = "{105,82}";
        };
        "text_chongzhi.png" =         {
            frame = "{{109,532},{86,45}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{86,45}}";
            sourceSize = "{86,45}";
        };
        "text_vip_store.png" =         {
            frame = "{{294,400},{161,38}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{161,38}}";
            sourceSize = "{161,38}";
        };
        "tip_avatar.png" =         {
            frame = "{{294,440},{131,151}}";
            offset = "{1,-2}";
            rotated = 1;
            sourceColorRect = "{{4,4},{131,151}}";
            sourceSize = "{137,155}";
        };
        "tip_bg.png" =         {
            frame = "{{230,140},{74,64}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{74,64}}";
            sourceSize = "{74,64}";
        };
        "title_prize.png" =         {
            frame = "{{2,394},{150,38}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{150,38}}";
            sourceSize = "{150,38}";
        };
        "user_tip.png" =         {
            frame = "{{302,2},{163,174}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{163,174}}";
            sourceSize = "{163,174}";
        };
        "vip_title_2.png" =         {
            frame = "{{294,316},{166,82}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{166,82}}";
            sourceSize = "{166,82}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "common_image.png";
        size = "{512,1024}";
        textureFileName = "common_image.png";
    };
}
~~~~~~~~~~~~~
\section Plist102 dachinko.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/dachinko.plist
[ ->] {
    frames =     {
        "1_00000.png" =         {
            frame = "{{286,435},{50,51}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{50,51}}";
            sourceSize = "{50,51}";
        };
        "dachinko_try.png" =         {
            frame = "{{393,285},{105,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{105,45}}";
            sourceSize = "{105,45}";
        };
        "dachinko_try_gray.png" =         {
            frame = "{{286,285},{105,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{105,45}}";
            sourceSize = "{105,45}";
        };
        "dachinko_x2.png" =         {
            frame = "{{286,332},{94,101}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{94,101}}";
            sourceSize = "{94,101}";
        };
        "laohuhji_1.png" =         {
            frame = "{{65,445},{57,179}}";
            offset = "{-1,1}";
            rotated = 1;
            sourceColorRect = "{{5,15},{57,179}}";
            sourceSize = "{69,211}";
        };
        "machine3.png" =         {
            frame = "{{246,2},{134,252}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{134,252}}";
            sourceSize = "{134,252}";
        };
        "slot_bg1.png" =         {
            frame = "{{2,2},{236,441}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{236,441}}";
            sourceSize = "{236,441}";
        };
        "slot_bg2.png" =         {
            frame = "{{246,138},{236,145}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{236,145}}";
            sourceSize = "{236,145}";
        };
        "slot_indro.png" =         {
            frame = "{{246,285},{188,38}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{188,38}}";
            sourceSize = "{188,38}";
        };
        "slot_lv_bg.png" =         {
            frame = "{{484,138},{21,19}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{21,19}}";
            sourceSize = "{21,19}";
        };
        "slot_redlight.png" =         {
            frame = "{{2,445},{61,61}}";
            offset = "{1,-1}";
            rotated = 0;
            sourceColorRect = "{{3,3},{61,61}}";
            sourceSize = "{65,65}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "dachinko.png";
        size = "{512,512}";
        textureFileName = "dachinko.png";
    };
}
~~~~~~~~~~~~~
\section Plist105 debug_panel.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/debug_panel.plist
[ ->] {
    frames =     {
        "button_common_box02_001-ipadhd.png" =         {
            frame = "{{2,174},{170,80}}";
            offset = "{-1,0}";
            rotated = 0;
            sourceColorRect = "{{2,0},{170,80}}";
            sourceSize = "{176,80}";
        };
        "button_common_box02_002-ipadhd.png" =         {
            frame = "{{174,2},{170,80}}";
            offset = "{-1,0}";
            rotated = 1;
            sourceColorRect = "{{2,0},{170,80}}";
            sourceSize = "{176,80}";
        };
        "button_common_box02_003-ipadhd.png" =         {
            frame = "{{2,2},{170,80}}";
            offset = "{-1,0}";
            rotated = 0;
            sourceColorRect = "{{2,0},{170,80}}";
            sourceSize = "{176,80}";
        };
        "check_box_active.png" =         {
            frame = "{{236,206},{14,14}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{14,14}}";
            sourceSize = "{14,14}";
        };
        "check_box_active_disable.png" =         {
            frame = "{{220,199},{14,14}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{14,14}}";
            sourceSize = "{14,14}";
        };
        "check_box_active_press.png" =         {
            frame = "{{204,206},{14,14}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{14,14}}";
            sourceSize = "{14,14}";
        };
        "check_box_normal.png" =         {
            frame = "{{239,190},{14,14}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{14,14}}";
            sourceSize = "{14,14}";
        };
        "check_box_normal_disable.png" =         {
            frame = "{{239,174},{14,14}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{14,14}}";
            sourceSize = "{14,14}";
        };
        "check_box_normal_press.png" =         {
            frame = "{{193,234},{14,14}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{14,14}}";
            sourceSize = "{14,14}";
        };
        "prizesign1-hd.png" =         {
            frame = "{{174,234},{17,20}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{17,20}}";
            sourceSize = "{17,22}";
        };
        "selected01.png" =         {
            frame = "{{174,204},{28,28}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{28,28}}";
            sourceSize = "{28,28}";
        };
        "selected02.png" =         {
            frame = "{{209,174},{28,23}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{28,23}}";
            sourceSize = "{28,23}";
        };
        "selected03.png" =         {
            frame = "{{209,174},{28,23}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{28,23}}";
            sourceSize = "{28,23}";
        };
        "selected04.png" =         {
            frame = "{{174,174},{28,28}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{28,28}}";
            sourceSize = "{28,28}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "debug_panel.png";
        size = "{256,256}";
        textureFileName = "debug_panel.png";
    };
}
~~~~~~~~~~~~~
\section Plist115 guess_finger.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/guess_finger.plist
[ ->] {
    frames =     {
        "bu1.png" =         {
            frame = "{{258,301},{141,139}}";
            offset = "{2,-3}";
            rotated = 1;
            sourceColorRect = "{{15,17},{141,139}}";
            sourceSize = "{167,167}";
        };
        "bu2.png" =         {
            frame = "{{117,301},{141,139}}";
            offset = "{2,-3}";
            rotated = 1;
            sourceColorRect = "{{15,17},{141,139}}";
            sourceSize = "{167,167}";
        };
        "bu3.png" =         {
            frame = "{{301,2},{97,95}}";
            offset = "{-1,0}";
            rotated = 1;
            sourceColorRect = "{{0,2},{97,95}}";
            sourceSize = "{99,99}";
        };
        "gf_01_00000.png" =         {
            frame = "{{2,2},{150,150}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{150,150}}";
            sourceSize = "{150,150}";
        };
        "gf_02_00000.png" =         {
            frame = "{{117,444},{64,64}}";
            offset = "{-2,-1}";
            rotated = 0;
            sourceColorRect = "{{16,19},{64,64}}";
            sourceSize = "{100,100}";
        };
        "gf_03_00000.png" =         {
            frame = "{{183,444},{62,62}}";
            offset = "{-2,0}";
            rotated = 0;
            sourceColorRect = "{{7,9},{62,62}}";
            sourceSize = "{80,80}";
        };
        "jiandao1.png" =         {
            frame = "{{2,303},{113,147}}";
            offset = "{-1,-2}";
            rotated = 0;
            sourceColorRect = "{{26,12},{113,147}}";
            sourceSize = "{167,167}";
        };
        "jiandao2.png" =         {
            frame = "{{2,154},{113,147}}";
            offset = "{-1,-2}";
            rotated = 0;
            sourceColorRect = "{{26,12},{113,147}}";
            sourceSize = "{167,167}";
        };
        "jiandao3.png" =         {
            frame = "{{399,101},{77,99}}";
            offset = "{-3,0}";
            rotated = 1;
            sourceColorRect = "{{8,0},{77,99}}";
            sourceSize = "{99,99}";
        };
        "shitou1.png" =         {
            frame = "{{154,2},{145,145}}";
            offset = "{4,-2}";
            rotated = 0;
            sourceColorRect = "{{15,13},{145,145}}";
            sourceSize = "{167,167}";
        };
        "shitou2.png" =         {
            frame = "{{117,154},{145,145}}";
            offset = "{4,-2}";
            rotated = 0;
            sourceColorRect = "{{15,13},{145,145}}";
            sourceSize = "{167,167}";
        };
        "shitou3.png" =         {
            frame = "{{399,2},{99,97}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,1},{99,97}}";
            sourceSize = "{99,99}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "guess_finger.png";
        size = "{512,512}";
        textureFileName = "guess_finger.png";
    };
}
~~~~~~~~~~~~~
\section Plist122 levelup_new.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/levelup_new.plist
[ ->] {
    frames =     {
        "bg_light.png" =         {
            frame = "{{364,64},{650,55}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{650,55}}";
            sourceSize = "{650,55}";
        };
        "bg_line.png" =         {
            frame = "{{506,2},{640,1}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{640,1}}";
            sourceSize = "{640,1}";
        };
        "btn_character_back.png" =         {
            frame = "{{259,64},{103,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{103,45}}";
            sourceSize = "{103,45}";
        };
        "btn_character_go.png" =         {
            frame = "{{259,17},{172,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{172,45}}";
            sourceSize = "{172,45}";
        };
        "light_white.png" =         {
            frame = "{{2,2},{502,13}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{502,13}}";
            sourceSize = "{502,13}";
        };
        "lv_bg.png" =         {
            frame = "{{2,644},{255,336}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{255,336}}";
            sourceSize = "{255,336}";
        };
        "lv_dark.png" =         {
            frame = "{{433,17},{74,64}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{74,64}}";
            sourceSize = "{74,64}";
        };
        "lv_text.png" =         {
            frame = "{{51,982},{49,26}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{49,26}}";
            sourceSize = "{49,26}";
        };
        "lv_top_bg.png" =         {
            frame = "{{2,982},{40,47}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{40,47}}";
            sourceSize = "{40,47}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "levelup_new.png";
        size = "{512,1024}";
        textureFileName = "levelup_new.png";
    };
}
~~~~~~~~~~~~~
\section Plist126 main_screen.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/main_screen.plist
[ ->] {
    frames =     {
        "ad_button.png" =         {
            frame = "{{92,208},{35,38}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{35,38}}";
            sourceSize = "{35,38}";
        };
        "ad_pop.png" =         {
            frame = "{{92,152},{54,44}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{54,44}}";
            sourceSize = "{54,44}";
        };
        "ad_tip.png" =         {
            frame = "{{233,140},{49,19}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{49,19}}";
            sourceSize = "{49,19}";
        };
        "bar.png" =         {
            frame = "{{244,50},{5,4}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{5,4}}";
            sourceSize = "{5,4}";
        };
        "button_book.png" =         {
            frame = "{{140,140},{88,91}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{88,91}}";
            sourceSize = "{88,91}";
        };
        "button_book_click.png" =         {
            frame = "{{151,50},{88,91}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{88,91}}";
            sourceSize = "{88,91}";
        };
        "icon_weishequ.png" =         {
            frame = "{{132,230},{69,72}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{69,72}}";
            sourceSize = "{69,72}";
        };
        "icon_weishequ2.png" =         {
            frame = "{{2,152},{69,72}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{69,72}}";
            sourceSize = "{69,72}";
        };
        "lv_bar.png" =         {
            frame = "{{73,152},{72,17}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{72,17}}";
            sourceSize = "{72,17}";
        };
        "main_bg.png" =         {
            frame = "{{151,2},{99,46}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{99,46}}";
            sourceSize = "{99,46}";
        };
        "middle.png" =         {
            frame = "{{2,2},{147,74}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{147,74}}";
            sourceSize = "{147,74}";
        };
        "weapon_bar.png" =         {
            frame = "{{2,116},{136,34}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{136,34}}";
            sourceSize = "{136,34}";
        };
        "weapon_bar_bg.png" =         {
            frame = "{{2,78},{138,36}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{2,0},{138,36}}";
            sourceSize = "{140,36}";
        };
        "wenzi-wohaishi.png" =         {
            frame = "{{46,230},{277,42}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{277,42}}";
            sourceSize = "{277,42}";
        };
        "wenzi-wojiushi.png" =         {
            frame = "{{2,230},{277,42}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{277,42}}";
            sourceSize = "{277,42}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "main_screen.png";
        size = "{256,512}";
        textureFileName = "main_screen.png";
    };
}
~~~~~~~~~~~~~
\section Plist128 mission.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/mission.plist
[ ->] {
    frames =     {
        "mission_anycannon.png" =         {
            frame = "{{275,174},{87,83}}";
            offset = "{2,-3}";
            rotated = 0;
            sourceColorRect = "{{4,6},{87,83}}";
            sourceSize = "{91,89}";
        };
        "mission_anyfish.png" =         {
            frame = "{{281,87},{89,85}}";
            offset = "{-1,-2}";
            rotated = 0;
            sourceColorRect = "{{0,4},{89,85}}";
            sourceSize = "{91,89}";
        };
        "mission_cannon.png" =         {
            frame = "{{2,91},{89,89}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{2,0},{89,89}}";
            sourceSize = "{91,89}";
        };
        "mission_cards.png" =         {
            frame = "{{2,182},{89,77}}";
            offset = "{-1,-1}";
            rotated = 0;
            sourceColorRect = "{{0,7},{89,77}}";
            sourceSize = "{91,89}";
        };
        "mission_chouka.png" =         {
            frame = "{{93,178},{89,79}}";
            offset = "{-1,0}";
            rotated = 0;
            sourceColorRect = "{{0,5},{89,79}}";
            sourceSize = "{91,89}";
        };
        "mission_coin.png" =         {
            frame = "{{93,91},{89,85}}";
            offset = "{1,-2}";
            rotated = 0;
            sourceColorRect = "{{2,4},{89,85}}";
            sourceSize = "{91,89}";
        };
        "mission_cristal.png" =         {
            frame = "{{2,332},{73,67}}";
            offset = "{2,3}";
            rotated = 0;
            sourceColorRect = "{{11,8},{73,67}}";
            sourceSize = "{91,89}";
        };
        "mission_earn.png" =         {
            frame = "{{95,2},{91,87}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{0,2},{91,87}}";
            sourceSize = "{91,89}";
        };
        "mission_evaluate.png" =         {
            frame = "{{372,87},{89,71}}";
            offset = "{1,-3}";
            rotated = 0;
            sourceColorRect = "{{2,12},{89,71}}";
            sourceSize = "{91,89}";
        };
        "mission_exchange.png" =         {
            frame = "{{374,2},{91,83}}";
            offset = "{0,-3}";
            rotated = 0;
            sourceColorRect = "{{0,6},{91,83}}";
            sourceSize = "{91,89}";
        };
        "mission_fever.png" =         {
            frame = "{{188,87},{91,75}}";
            offset = "{0,-4}";
            rotated = 0;
            sourceColorRect = "{{0,11},{91,75}}";
            sourceSize = "{91,89}";
        };
        "mission_fishing.png" =         {
            frame = "{{2,2},{91,87}}";
            offset = "{0,-1}";
            rotated = 0;
            sourceColorRect = "{{0,2},{91,87}}";
            sourceSize = "{91,89}";
        };
        "mission_fishtide.png" =         {
            frame = "{{281,2},{91,83}}";
            offset = "{0,-3}";
            rotated = 0;
            sourceColorRect = "{{0,6},{91,83}}";
            sourceSize = "{91,89}";
        };
        "mission_laohuji.png" =         {
            frame = "{{372,160},{87,87}}";
            offset = "{-2,-1}";
            rotated = 0;
            sourceColorRect = "{{0,2},{87,87}}";
            sourceSize = "{91,89}";
        };
        "mission_mini.png" =         {
            frame = "{{93,259},{81,81}}";
            offset = "{1,-1}";
            rotated = 0;
            sourceColorRect = "{{6,5},{81,81}}";
            sourceSize = "{91,89}";
        };
        "mission_pay.png" =         {
            frame = "{{184,251},{87,77}}";
            offset = "{1,0}";
            rotated = 0;
            sourceColorRect = "{{3,6},{87,77}}";
            sourceSize = "{91,89}";
        };
        "mission_photo.png" =         {
            frame = "{{2,261},{79,69}}";
            offset = "{-1,-3}";
            rotated = 0;
            sourceColorRect = "{{5,13},{79,69}}";
            sourceSize = "{91,89}";
        };
        "mission_rate.png" =         {
            frame = "{{184,164},{85,89}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{3,0},{85,89}}";
            sourceSize = "{91,89}";
        };
        "mission_scene.png" =         {
            frame = "{{188,2},{91,83}}";
            offset = "{0,-2}";
            rotated = 0;
            sourceColorRect = "{{0,5},{91,83}}";
            sourceSize = "{91,89}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "mission.png";
        size = "{512,512}";
        textureFileName = "mission.png";
    };
}
~~~~~~~~~~~~~
\section Plist137 reward.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/reward.plist
[ ->] {
    frames =     {
        "btn.png" =         {
            frame = "{{121,718},{64,71}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,71}}";
            sourceSize = "{64,71}";
        };
        "btn_click.png" =         {
            frame = "{{125,645},{64,71}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{64,71}}";
            sourceSize = "{64,71}";
        };
        "done.png" =         {
            frame = "{{191,677},{87,89}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{87,89}}";
            sourceSize = "{87,89}";
        };
        "item_bg.png" =         {
            frame = "{{2,595},{121,116}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{121,116}}";
            sourceSize = "{121,116}";
        };
        "item_bg1.png" =         {
            frame = "{{2,442},{151,151}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{151,151}}";
            sourceSize = "{151,151}";
        };
        "item_bg1_today.png" =         {
            frame = "{{354,400},{151,151}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{151,151}}";
            sourceSize = "{151,151}";
        };
        "item_bg2.png" =         {
            frame = "{{201,362},{151,151}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{151,151}}";
            sourceSize = "{151,151}";
        };
        "item_bg3.png" =         {
            frame = "{{2,101},{198,198}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{198,198}}";
            sourceSize = "{198,198}";
        };
        "item_bg_grey1.png" =         {
            frame = "{{346,657},{107,102}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{107,102}}";
            sourceSize = "{107,102}";
        };
        "item_bg_grey2.png" =         {
            frame = "{{350,553},{107,102}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{107,102}}";
            sourceSize = "{107,102}";
        };
        "item_bg_today.png" =         {
            frame = "{{356,247},{151,151}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{151,151}}";
            sourceSize = "{151,151}";
        };
        "numberbg 1.png" =         {
            frame = "{{459,605},{50,51}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{50,51}}";
            sourceSize = "{50,51}";
        };
        "numberbg 2.png" =         {
            frame = "{{459,553},{50,51}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{50,51}}";
            sourceSize = "{50,51}";
        };
        "numberbg.png" =         {
            frame = "{{176,301},{106,23}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{106,23}}";
            sourceSize = "{106,23}";
        };
        "reward_back.png" =         {
            frame = "{{202,239},{121,152}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{121,152}}";
            sourceSize = "{121,152}";
        };
        "reward_btn.png" =         {
            frame = "{{2,348},{172,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{172,45}}";
            sourceSize = "{172,45}";
        };
        "reward_com.png" =         {
            frame = "{{489,112},{11,4}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{11,4}}";
            sourceSize = "{11,4}";
        };
        "reward_titile.png" =         {
            frame = "{{2,2},{362,97}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{362,97}}";
            sourceSize = "{362,97}";
        };
        "reward_titile_2.png" =         {
            frame = "{{155,492},{151,38}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{151,38}}";
            sourceSize = "{151,38}";
        };
        "rw_arrow.png" =         {
            frame = "{{366,2},{141,89}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{141,89}}";
            sourceSize = "{141,89}";
        };
        "rw_bg_1.png" =         {
            frame = "{{195,549},{32,119}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{32,119}}";
            sourceSize = "{32,119}";
        };
        "rw_bg_2.png" =         {
            frame = "{{175,409},{27,22}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{27,22}}";
            sourceSize = "{27,22}";
        };
        "rw_bg_3.png" =         {
            frame = "{{316,515},{32,119}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{32,119}}";
            sourceSize = "{32,119}";
        };
        "rw_bg_dark.png" =         {
            frame = "{{195,515},{32,119}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{32,119}}";
            sourceSize = "{32,119}";
        };
        "rw_card_3.png" =         {
            frame = "{{155,442},{48,39}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{48,39}}";
            sourceSize = "{48,39}";
        };
        "rw_card_positive.png" =         {
            frame = "{{366,93},{121,152}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{121,152}}";
            sourceSize = "{121,152}";
        };
        "rw_freeget.png" =         {
            frame = "{{2,395},{171,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{171,45}}";
            sourceSize = "{171,45}";
        };
        "rw_number_bg.png" =         {
            frame = "{{489,93},{17,17}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{17,17}}";
            sourceSize = "{17,17}";
        };
        "rw_title_bg.png" =         {
            frame = "{{314,636},{38,30}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{38,30}}";
            sourceSize = "{38,30}";
        };
        "star_1.png" =         {
            frame = "{{282,738},{59,59}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{59,59}}";
            sourceSize = "{59,59}";
        };
        "star_2.png" =         {
            frame = "{{282,677},{59,59}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{59,59}}";
            sourceSize = "{59,59}";
        };
        "text_next.png" =         {
            frame = "{{2,301},{172,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{172,45}}";
            sourceSize = "{172,45}";
        };
        "txt_vip_double_1.png" =         {
            frame = "{{455,728},{69,50}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{69,50}}";
            sourceSize = "{69,50}";
        };
        "txt_vip_double_2.png" =         {
            frame = "{{455,657},{69,50}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{69,50}}";
            sourceSize = "{69,50}";
        };
        "vip_double.png" =         {
            frame = "{{2,713},{117,92}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{117,92}}";
            sourceSize = "{117,92}";
        };
        "vip_double_grey.png" =         {
            frame = "{{195,583},{117,92}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{117,92}}";
            sourceSize = "{117,92}";
        };
        "word.png" =         {
            frame = "{{202,170},{154,67}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{154,67}}";
            sourceSize = "{154,67}";
        };
        "word_grey.png" =         {
            frame = "{{202,101},{154,67}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{154,67}}";
            sourceSize = "{154,67}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "reward.png";
        size = "{512,1024}";
        textureFileName = "reward.png";
    };
}
~~~~~~~~~~~~~
\section Plist140 sale.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/sale.plist
[ ->] {
    frames =     {
        "buy_text.png" =         {
            frame = "{{2,466},{108,44}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{108,44}}";
            sourceSize = "{108,44}";
        };
        "gift_number_bg.png" =         {
            frame = "{{2,278},{68,68}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{68,68}}";
            sourceSize = "{68,68}";
        };
        "gift_text_buy.png" =         {
            frame = "{{48,356},{103,45}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{103,45}}";
            sourceSize = "{103,45}";
        };
        "icon_dollar1.png" =         {
            frame = "{{479,2},{26,48}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{26,48}}";
            sourceSize = "{26,48}";
        };
        "icon_rmb1.png" =         {
            frame = "{{72,278},{36,37}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{36,37}}";
            sourceSize = "{36,37}";
        };
        "sale_bg.png" =         {
            frame = "{{2,140},{298,136}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{298,136}}";
            sourceSize = "{298,136}";
        };
        "sale_free_bg.png" =         {
            frame = "{{2,2},{298,136}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{298,136}}";
            sourceSize = "{298,136}";
        };
        "sale_label.png" =         {
            frame = "{{302,2},{242,175}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{242,175}}";
            sourceSize = "{242,175}";
        };
        "sale_text_bg.png" =         {
            frame = "{{302,246},{150,76}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{150,76}}";
            sourceSize = "{150,76}";
        };
        "text_look.png" =         {
            frame = "{{454,246},{108,45}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{108,45}}";
            sourceSize = "{108,45}";
        };
        "text_look1.png" =         {
            frame = "{{2,356},{108,44}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{108,44}}";
            sourceSize = "{108,44}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "sale.png";
        size = "{512,512}";
        textureFileName = "sale.png";
    };
}
~~~~~~~~~~~~~
\section Plist147 setting.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/setting.plist
[ ->] {
    frames =     {
        "contact_us.png" =         {
            frame = "{{2,199},{151,38}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{151,38}}";
            sourceSize = "{151,38}";
        };
        "list_bg.png" =         {
            frame = "{{415,114},{36,36}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{36,36}}";
            sourceSize = "{36,36}";
        };
        "option_btn_small.png" =         {
            frame = "{{317,319},{47,40}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{47,40}}";
            sourceSize = "{47,40}";
        };
        "option_btn_small_click.png" =         {
            frame = "{{366,309},{47,40}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{47,40}}";
            sourceSize = "{47,40}";
        };
        "option_buyubaike.png" =         {
            frame = "{{415,152},{91,86}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{91,86}}";
            sourceSize = "{91,86}";
        };
        "option_lianxiwomen.png" =         {
            frame = "{{158,156},{65,84}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{65,84}}";
            sourceSize = "{65,84}";
        };
        "option_music_bar.png" =         {
            frame = "{{453,114},{18,18}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{18,18}}";
            sourceSize = "{18,18}";
        };
        "option_music_bg.png" =         {
            frame = "{{36,424},{26,26}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{26,26}}";
            sourceSize = "{26,26}";
        };
        "option_paihangbang.png" =         {
            frame = "{{155,223},{103,82}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{103,82}}";
            sourceSize = "{103,82}";
        };
        "option_text_about.png" =         {
            frame = "{{2,156},{154,41}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{154,41}}";
            sourceSize = "{154,41}";
        };
        "option_text_help.png" =         {
            frame = "{{239,230},{78,41}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{78,41}}";
            sourceSize = "{78,41}";
        };
        "option_text_mainmenu.png" =         {
            frame = "{{258,2},{214,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{214,45}}";
            sourceSize = "{214,45}";
        };
        "option_text_music.png" =         {
            frame = "{{2,347},{63,32}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{63,32}}";
            sourceSize = "{63,32}";
        };
        "option_text_option.png" =         {
            frame = "{{375,183},{75,38}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{75,38}}";
            sourceSize = "{75,38}";
        };
        "option_text_ranking.png" =         {
            frame = "{{244,183},{129,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{129,45}}";
            sourceSize = "{129,45}";
        };
        "option_text_sfx.png" =         {
            frame = "{{2,424},{63,32}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{63,32}}";
            sourceSize = "{63,32}";
        };
        "option_zhizuorenyuan.png" =         {
            frame = "{{415,240},{102,80}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{1,1},{102,80}}";
            sourceSize = "{104,82}";
        };
        "setting_logo.png" =         {
            frame = "{{2,2},{254,152}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{254,152}}";
            sourceSize = "{254,152}";
        };
        "wiki_bar.png" =         {
            frame = "{{453,134},{13,9}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{13,9}}";
            sourceSize = "{13,9}";
        };
        "wiki_bar_bg.png" =         {
            frame = "{{488,344},{20,15}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{20,15}}";
            sourceSize = "{20,15}";
        };
        "wiki_bar_btn.png" =         {
            frame = "{{366,351},{71,36}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{71,36}}";
            sourceSize = "{71,36}";
        };
        "wiki_bar_btn_click.png" =         {
            frame = "{{415,344},{71,36}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{71,36}}";
            sourceSize = "{71,36}";
        };
        "wiki_dot1.png" =         {
            frame = "{{244,156},{11,11}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{11,11}}";
            sourceSize = "{11,11}";
        };
        "wiki_fishcard.png" =         {
            frame = "{{258,49},{132,155}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{132,155}}";
            sourceSize = "{132,155}";
        };
        "wiki_fishcard_bg.png" =         {
            frame = "{{239,273},{76,72}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{76,72}}";
            sourceSize = "{76,72}";
        };
        "wiki_fishname_bg1.png" =         {
            frame = "{{368,260},{47,43}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{47,43}}";
            sourceSize = "{47,43}";
        };
        "wiki_fishname_bg2.png" =         {
            frame = "{{319,230},{47,43}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{47,43}}";
            sourceSize = "{47,43}";
        };
        "wiki_text_bg.png" =         {
            frame = "{{415,49},{63,52}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{63,52}}";
            sourceSize = "{63,52}";
        };
        "wiki_text_title.png" =         {
            frame = "{{474,2},{148,36}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{148,36}}";
            sourceSize = "{148,36}";
        };
        "wiki_value_bg.png" =         {
            frame = "{{317,275},{42,47}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{42,47}}";
            sourceSize = "{42,47}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "setting.png";
        size = "{512,512}";
        textureFileName = "setting.png";
    };
}
~~~~~~~~~~~~~
\section Plist151 start.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/start.plist
[ ->] {
    frames =     {
        "360_start_text.png" =         {
            frame = "{{194,401},{170,44}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{170,44}}";
            sourceSize = "{170,44}";
        };
        "arrow_start.png" =         {
            frame = "{{468,308},{55,40}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{55,40}}";
            sourceSize = "{55,40}";
        };
        "coco.png" =         {
            frame = "{{369,308},{70,97}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{70,97}}";
            sourceSize = "{70,97}";
        };
        "cocos.png" =         {
            frame = "{{368,380},{79,84}}";
            offset = "{1,2}";
            rotated = 1;
            sourceColorRect = "{{9,6},{79,84}}";
            sourceSize = "{95,100}";
        };
        "exit_start.png" =         {
            frame = "{{362,461},{57,50}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{57,50}}";
            sourceSize = "{57,50}";
        };
        "logo.png" =         {
            frame = "{{2,2},{507,304}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{507,304}}";
            sourceSize = "{507,304}";
        };
        "percentage.png" =         {
            frame = "{{468,365},{47,39}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{47,39}}";
            sourceSize = "{47,39}";
        };
        "quan.png" =         {
            frame = "{{194,447},{151,151}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{151,151}}";
            sourceSize = "{151,151}";
        };
        "quandi.png" =         {
            frame = "{{2,308},{190,190}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{190,190}}";
            sourceSize = "{190,190}";
        };
        "setting_strat.png" =         {
            frame = "{{454,414},{51,51}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{51,51}}";
            sourceSize = "{51,51}";
        };
        "start_point.png" =         {
            frame = "{{347,447},{28,13}}";
            offset = "{2,0}";
            rotated = 1;
            sourceColorRect = "{{8,10},{28,13}}";
            sourceSize = "{40,33}";
        };
        "start_text.png" =         {
            frame = "{{194,354},{172,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{172,45}}";
            sourceSize = "{172,45}";
        };
        "visitor_start_text.png" =         {
            frame = "{{194,308},{173,44}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{173,44}}";
            sourceSize = "{173,44}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "start.png";
        size = "{512,1024}";
        textureFileName = "start.png";
    };
}
~~~~~~~~~~~~~
\section Plist155 store.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/store.plist
[ ->] {
    frames =     {
        "bg3.png" =         {
            frame = "{{2,397},{237,291}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{237,291}}";
            sourceSize = "{237,291}";
        };
        "bg_free.png" =         {
            frame = "{{154,672},{129,304}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{129,304}}";
            sourceSize = "{129,304}";
        };
        "bg_purple.png" =         {
            frame = "{{258,978},{100,36}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{100,36}}";
            sourceSize = "{100,36}";
        };
        "bg_qianting2dark.png" =         {
            frame = "{{993,210},{18,14}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{18,14}}";
            sourceSize = "{18,14}";
        };
        "bg_qianting3.png" =         {
            frame = "{{993,230},{165,3}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{165,3}}";
            sourceSize = "{165,3}";
        };
        "bg_sale.png" =         {
            frame = "{{687,210},{131,304}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{131,304}}";
            sourceSize = "{131,304}";
        };
        "bg_sale_1.png" =         {
            frame = "{{679,343},{129,304}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{129,304}}";
            sourceSize = "{129,304}";
        };
        "btn_draw.png" =         {
            frame = "{{416,324},{87,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{87,45}}";
            sourceSize = "{87,45}";
        };
        "buy_title.png" =         {
            frame = "{{295,476},{151,38}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{151,38}}";
            sourceSize = "{151,38}";
        };
        "discount_line.png" =         {
            frame = "{{287,325},{127,16}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{127,16}}";
            sourceSize = "{127,16}";
        };
        "free_cristal.png" =         {
            frame = "{{182,343},{178,49}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{178,49}}";
            sourceSize = "{178,49}";
        };
        "free_gold.png" =         {
            frame = "{{2,343},{178,49}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{178,49}}";
            sourceSize = "{178,49}";
        };
        "icon_card.png" =         {
            frame = "{{335,517},{71,53}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{71,53}}";
            sourceSize = "{71,53}";
        };
        "light.png" =         {
            frame = "{{1009,163},{214,10}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{214,10}}";
            sourceSize = "{214,10}";
        };
        "number_bg_free.png" =         {
            frame = "{{287,218},{105,83}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{105,83}}";
            sourceSize = "{105,83}";
        };
        "restore.png" =         {
            frame = "{{362,343},{172,45}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{172,45}}";
            sourceSize = "{172,45}";
        };
        "restore_gray.png" =         {
            frame = "{{505,313},{172,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{172,45}}";
            sourceSize = "{172,45}";
        };
        "skill_bg.png" =         {
            frame = "{{295,435},{39,62}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{39,62}}";
            sourceSize = "{39,62}";
        };
        "skill_bg_gray.png" =         {
            frame = "{{295,394},{39,62}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{39,62}}";
            sourceSize = "{39,62}";
        };
        "store_title_bg_1.png" =         {
            frame = "{{409,371},{93,56}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{93,56}}";
            sourceSize = "{93,56}";
        };
        "store_title_bg_2.png" =         {
            frame = "{{410,266},{93,56}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{93,56}}";
            sourceSize = "{93,56}";
        };
        "text_exchange.png" =         {
            frame = "{{241,218},{108,44}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{108,44}}";
            sourceSize = "{108,44}";
        };
        "tip_1.png" =         {
            frame = "{{199,218},{110,40}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{110,40}}";
            sourceSize = "{110,40}";
        };
        "tip_2.png" =         {
            frame = "{{157,218},{110,40}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{110,40}}";
            sourceSize = "{110,40}";
        };
        "tip_3.png" =         {
            frame = "{{115,218},{110,40}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{110,40}}";
            sourceSize = "{110,40}";
        };
        "tip_4.png" =         {
            frame = "{{73,218},{110,40}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{110,40}}";
            sourceSize = "{110,40}";
        };
        "vip_bar.png" =         {
            frame = "{{998,379},{256,22}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{256,22}}";
            sourceSize = "{256,22}";
        };
        "vip_bar_bg.png" =         {
            frame = "{{410,218},{274,46}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{274,46}}";
            sourceSize = "{274,46}";
        };
        "vip_bg_1.png" =         {
            frame = "{{360,672},{45,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{45,45}}";
            sourceSize = "{45,45}";
        };
        "vip_bg_2.png" =         {
            frame = "{{2,989},{117,30}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{117,30}}";
            sourceSize = "{117,30}";
        };
        "vip_bg_3.png" =         {
            frame = "{{687,2},{159,332}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{159,332}}";
            sourceSize = "{159,332}";
        };
        "vip_gift_icon.png" =         {
            frame = "{{285,719},{204,146}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,2},{204,146}}";
            sourceSize = "{204,150}";
        };
        "vip_gift_icon_2.png" =         {
            frame = "{{495,444},{202,182}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{202,182}}";
            sourceSize = "{202,182}";
        };
        "vip_item_bg.png" =         {
            frame = "{{679,474},{146,146}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{146,146}}";
            sourceSize = "{146,146}";
        };
        "vip_item_icon.png" =         {
            frame = "{{827,474},{133,127}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{133,127}}";
            sourceSize = "{133,127}";
        };
        "vip_light_1.png" =         {
            frame = "{{687,163},{320,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{320,45}}";
            sourceSize = "{320,45}";
        };
        "vip_light_2.png" =         {
            frame = "{{998,230},{143,3}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{143,3}}";
            sourceSize = "{143,3}";
        };
        "vip_light_3.png" =         {
            frame = "{{189,636},{214,34}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{214,34}}";
            sourceSize = "{214,34}";
        };
        "vip_line_1.png" =         {
            frame = "{{78,637},{350,74}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{1,0},{350,74}}";
            sourceSize = "{352,74}";
        };
        "vip_line_2.png" =         {
            frame = "{{2,637},{350,74}}";
            offset = "{1,0}";
            rotated = 1;
            sourceColorRect = "{{2,0},{350,74}}";
            sourceSize = "{352,74}";
        };
        "vip_line_3.png" =         {
            frame = "{{372,218},{100,36}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{100,36}}";
            sourceSize = "{100,36}";
        };
        "vip_line_4.png" =         {
            frame = "{{2,168},{683,48}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{683,48}}";
            sourceSize = "{683,48}";
        };
        "vip_off.png" =         {
            frame = "{{2,85},{683,81}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{683,81}}";
            sourceSize = "{683,81}";
        };
        "vip_on.png" =         {
            frame = "{{2,2},{683,81}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{683,81}}";
            sourceSize = "{683,81}";
        };
        "vip_send.png" =         {
            frame = "{{2,218},{111,69}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{111,69}}";
            sourceSize = "{111,69}";
        };
        "vip_store_title.png" =         {
            frame = "{{409,429},{273,84}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{273,84}}";
            sourceSize = "{273,84}";
        };
        "vip_text_store.png" =         {
            frame = "{{505,266},{172,45}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{172,45}}";
            sourceSize = "{172,45}";
        };
        "vip_title_1.png" =         {
            frame = "{{121,989},{66,28}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{66,28}}";
            sourceSize = "{66,28}";
        };
        "vip_title_3.png" =         {
            frame = "{{189,978},{67,37}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{67,37}}";
            sourceSize = "{67,37}";
        };
        "vip_title_4.png" =         {
            frame = "{{335,572},{67,37}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{67,37}}";
            sourceSize = "{67,37}";
        };
        "vip_title_gray.png" =         {
            frame = "{{505,360},{166,82}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{166,82}}";
            sourceSize = "{166,82}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "store.png";
        size = "{1024,1024}";
        textureFileName = "store.png";
    };
}
~~~~~~~~~~~~~
\section Plist157 submarine.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/960x640/ui/submarine.plist
[ ->] {
    frames =     {
        "pro_back.png" =         {
            frame = "{{2,219},{27,27}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{27,27}}";
            sourceSize = "{27,27}";
        };
        "pro_bar.png" =         {
            frame = "{{2,155},{62,15}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{62,15}}";
            sourceSize = "{62,15}";
        };
        "pro_cut.png" =         {
            frame = "{{245,2},{2,19}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{2,19}}";
            sourceSize = "{2,19}";
        };
        "submarine_bg2.png" =         {
            frame = "{{122,155},{125,50}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{125,50}}";
            sourceSize = "{125,50}";
        };
        "text_equipment.png" =         {
            frame = "{{2,57},{101,53}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{101,53}}";
            sourceSize = "{101,53}";
        };
        "text_equipment_gray.png" =         {
            frame = "{{19,155},{101,53}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{101,53}}";
            sourceSize = "{101,53}";
        };
        "text_levelup_all.png" =         {
            frame = "{{2,2},{201,53}}";
            offset = "{0,0}";
            rotated = 0;
            sourceColorRect = "{{0,0},{201,53}}";
            sourceSize = "{201,53}";
        };
        "text_my_submarine.png" =         {
            frame = "{{205,2},{151,38}}";
            offset = "{0,0}";
            rotated = 1;
            sourceColorRect = "{{0,0},{151,38}}";
            sourceSize = "{151,38}";
        };
    };
    metadata =     {
        format = 2;
        realTextureFileName = "submarine.png";
        size = "{256,256}";
        textureFileName = "submarine.png";
    };
}
~~~~~~~~~~~~~
\section Plist166 Root.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/ChanceAdRes.bundle/Root.plist
[ ->] {
    PreferenceSpecifiers =     (
                {
            Title = Group;
            Type = PSGroupSpecifier;
        },
                {
            AutocapitalizationType = None;
            AutocorrectionType = No;
            DefaultValue = "";
            IsSecure = 0;
            Key = "name_preference";
            KeyboardType = Alphabet;
            Title = Name;
            Type = PSTextFieldSpecifier;
        },
                {
            DefaultValue = 1;
            Key = "enabled_preference";
            Title = Enabled;
            Type = PSToggleSwitchSpecifier;
        },
                {
            DefaultValue = 0.5;
            Key = "slider_preference";
            MaximumValue = 1;
            MaximumValueImage = "";
            MinimumValue = 0;
            MinimumValueImage = "";
            Type = PSSliderSpecifier;
        }
    );
    StringsTable = Root;
}
~~~~~~~~~~~~~
\section Plist167 FishingJoy3_iphone_zh_cn.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/FishingJoy3_iphone_zh_cn.plist
[ ->] {
    CFBundleDevelopmentRegion = "zh_CN";
    CFBundleDisplayName = "${PRODUCT_NAME}";
    CFBundleExecutable = "${PRODUCT_NAME}";
    CFBundleIcons =     {
    };
    "CFBundleIcons~ipad" =     {
    };
    CFBundleIdentifier = "com.punchbox.fishingjoy3";
    CFBundleInfoDictionaryVersion = "6.0";
    CFBundleName = "${PRODUCT_NAME}";
    CFBundlePackageType = APPL;
    CFBundleShortVersionString = "1.0.4";
    CFBundleSignature = "????";
    CFBundleURLTypes =     (
                {
            CFBundleTypeRole = Editor;
            CFBundleURLSchemes =             (
                wxe6eacb86e505f0d4
            );
        },
                {
            CFBundleTypeRole = Editor;
            CFBundleURLSchemes =             (
                wb3548423266
            );
        }
    );
    CFBundleVersion = "1.8";
    CocoAppVersion = 10000298;
    LSApplicationCategoryType = 0;
    LSRequiresIPhoneOS = 1;
    UIFileSharingEnabled = 0;
    UIPrerenderedIcon = 1;
    UIRequiredDeviceCapabilities =     {
        accelerometer = 1;
        "opengles-1" = 1;
    };
    UIStatusBarHidden = 1;
    UISupportedInterfaceOrientations =     (
        UIInterfaceOrientationLandscapeLeft,
        UIInterfaceOrientationLandscapeRight
    );
    "UISupportedInterfaceOrientations~ipad" =     (
        UIInterfaceOrientationLandscapeLeft,
        UIInterfaceOrientationLandscapeRight
    );
    UIViewControllerBasedStatusBarAppearance = 0;
}
~~~~~~~~~~~~~
\section Plist168 Info.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/Info.plist
[ ->] {
    BuildMachineOSBuild = 14A389;
    CFBundleDevelopmentRegion = "zh_CN";
    CFBundleDisplayName = "\U6355\U9c7c\U8fbe\U4eba3";
    CFBundleExecutable = "\U6355\U9c7c\U8fbe\U4eba3";
    CFBundleIcons =     {
        CFBundlePrimaryIcon =         {
            CFBundleIconFiles =             (
                AppIcon29x29,
                AppIcon40x40,
                AppIcon57x57,
                AppIcon60x60
            );
            UIPrerenderedIcon = 1;
        };
    };
    "CFBundleIcons~ipad" =     {
        CFBundlePrimaryIcon =         {
            CFBundleIconFiles =             (
                AppIcon29x29,
                AppIcon40x40,
                AppIcon57x57,
                AppIcon60x60,
                AppIcon50x50,
                AppIcon72x72,
                AppIcon76x76
            );
            UIPrerenderedIcon = 1;
        };
    };
    CFBundleIdentifier = "com.punchbox.fishingjoy3";
    CFBundleInfoDictionaryVersion = "6.0";
    CFBundleName = "\U6355\U9c7c\U8fbe\U4eba3";
    CFBundlePackageType = APPL;
    CFBundleShortVersionString = "1.0.4";
    CFBundleSignature = "????";
    CFBundleSupportedPlatforms =     (
        iPhoneOS
    );
    CFBundleURLTypes =     (
                {
            CFBundleTypeRole = Editor;
            CFBundleURLSchemes =             (
                wxe6eacb86e505f0d4
            );
        },
                {
            CFBundleTypeRole = Editor;
            CFBundleURLSchemes =             (
                wb3548423266
            );
        }
    );
    CFBundleVersion = "1.8";
    CocoAppVersion = 10000298;
    DTCompiler = "com.apple.compilers.llvm.clang.1_0";
    DTPlatformBuild = 12B411;
    DTPlatformName = iphoneos;
    DTPlatformVersion = "8.1";
    DTSDKBuild = 12B411;
    DTSDKName = "iphoneos8.1";
    DTXcode = 0610;
    DTXcodeBuild = 6A1052d;
    LSApplicationCategoryType = 0;
    LSRequiresIPhoneOS = 1;
    MinimumOSVersion = "6.0";
    UIDeviceFamily =     (
        1
    );
    UIFileSharingEnabled = 0;
    UILaunchImageFile = LaunchImage;
    UIPrerenderedIcon = 1;
    UIRequiredDeviceCapabilities =     {
        accelerometer = 1;
        "opengles-1" = 1;
    };
    UIStatusBarHidden = 1;
    UISupportedInterfaceOrientations =     (
        UIInterfaceOrientationLandscapeLeft,
        UIInterfaceOrientationLandscapeRight
    );
    "UISupportedInterfaceOrientations~ipad" =     (
        UIInterfaceOrientationLandscapeLeft,
        UIInterfaceOrientationLandscapeRight
    );
    UIViewControllerBasedStatusBarAppearance = 0;
}
~~~~~~~~~~~~~
\section Plist169 Manifest.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/SC_Info/Manifest.plist
[ ->] {
    SinfPaths =     (
        "SC_Info/\U6355\U9c7c\U8fbe\U4eba3.sinf"
    );
    SinfReplicationPaths =     (
        "SC_Info/\U6355\U9c7c\U8fbe\U4eba3.sinf"
    );
}
~~~~~~~~~~~~~
\section Plist170 CodeResources 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/_CodeSignature/CodeResources
[ ->] {
    files =     {
        "3d_ios/common/debug.material" = <676dca5d 4e67772d 596b6913 d223a4a9 4b70538f>;
        "3d_ios/common/default.material" = <a62b88c1 8a1cf174 7daf8e4a 2172c5ae c482cd19>;
        "3d_ios/common/default.pvr" = <7b4638de 44f6d6eb 35f715b9 03f2b145 f304cda7>;
        "3d_ios/common/default_skin.material" = <fa0c7f4e 7b6726a0 5f0d17ab 7116ce17 c52ed764>;
        "3d_ios/common/particle/particle.material" = <bc9a2aed dc87a2a9 b4e24a64 f1401d59 f7b0dd5e>;
        "3d_ios/common/particle/particle_tex.material" = <5bec9cd3 139ba706 94474e3f 047dad54 2c0b4a2a>;
        "3d_ios/config/effect.config" = <28d8c800 b219e0c7 e5076fe1 dc9f87be 923e5b00>;
        "3d_ios/effect/caustics.pvr" = <6fc91016 1b1d505e 63b6247b 2f67c5bc d3634bc8>;
        "3d_ios/fish/beijingyu/beijingyu.cbx" = <86360562 fc2ca400 2f515742 d38d49b9 b6963716>;
        "3d_ios/fish/beijingyu/beijingyu.ckb" = <0a74a536 bc8d1eb3 7694ba29 d8de8797 70ab8c5e>;
        "3d_ios/fish/beijingyu/beijingyu.pvr" = <2d000108 4cb4d803 17fd626f 846d211e 97b96fd9>;
        "3d_ios/fish/bianfuyu/bianfuyu.cbx" = <295f4e7d b14a19d6 e55c5e1a cad9dadb b2d8ca78>;
        "3d_ios/fish/bianfuyu/bianfuyu.ckb" = <4100b360 ea1c7987 89644a56 654b1a8c 67615dd9>;
        "3d_ios/fish/bianfuyu/bianfuyu.pvr" = <892198d9 a1862cb0 dbeca346 d46c94a6 682cf404>;
        "3d_ios/fish/bianfuyu/bianfuyu_gold.pvr" = <e896cc3f 6f42cc0d 321e2f3a 95fb3e22 57230b9e>;
        "3d_ios/fish/cheqiyu/cheqiyu.cbx" = <8f369749 af20fb4b 34826ec8 c69179c3 68ac02fd>;
        "3d_ios/fish/cheqiyu/cheqiyu.ckb" = <22a38241 e56cc27f 40bb7d70 5ffff0fc 3364f9cc>;
        "3d_ios/fish/cheqiyu/cheqiyu.pvr" = <266b1534 49a54c9e df0fcaa5 bbbd9a49 0a6352f5>;
        "3d_ios/fish/denglongyu/denglongyu.cbx" = <3432173e a75aa639 48a3e395 79615d47 f3ab41a8>;
        "3d_ios/fish/denglongyu/denglongyu.ckb" = <4642d501 6b4d17cf 401cf6e9 14627288 ce391fae>;
        "3d_ios/fish/denglongyu/denglongyu.pvr" = <ad743f7b f94c4088 98c5fdaf d880e4c2 118c0245>;
        "3d_ios/fish/dianmanyu/dianmanyu.cbx" = <e881a2fb fc4bd597 1645968b 361bc127 ac51d0ce>;
        "3d_ios/fish/dianmanyu/dianmanyu.ckb" = <fc31b30e 4d84553d 372206df 12852e77 1cf79eb9>;
        "3d_ios/fish/dianmanyu/dianmanyu.pvr" = <4420b0bc 56243c03 0723c032 87083b7f 607dc1dc>;
        "3d_ios/fish/dianmanyu/shandian1024.png" = <f3d10427 bd4d854d ea42379d 0c22ce39 245c9998>;
        "3d_ios/fish/diaoyu/diaoyu.cbx" = <73404428 bd157c3a 606ae238 ce7e92c1 247fac05>;
        "3d_ios/fish/diaoyu/diaoyu.ckb" = <73b373bb 8675f37c ebd0892a 308f6b9b cbad84ea>;
        "3d_ios/fish/diaoyu/diaoyu.pvr" = <543d9d2f 45029d41 478f8da8 62854313 a674cf11>;
        "3d_ios/fish/dinianyu/dinianyu.cbx" = <baf19c62 286fdc72 f8b103b7 54e843f2 2f0e0765>;
        "3d_ios/fish/dinianyu/dinianyu.ckb" = <214465b0 5ef32768 97aa05db fa4bed1b 80ae0dec>;
        "3d_ios/fish/dinianyu/dinianyu.pvr" = <07e1efcb db722382 50c0ce42 9a075a68 e7b16e91>;
        "3d_ios/fish/douyu/douyu.cbx" = <14d9cdfc e6277d5f b183709e 24fffdc2 846f64e2>;
        "3d_ios/fish/douyu/douyu.ckb" = <69e1b654 357f6591 ddd28215 16eec871 de516f56>;
        "3d_ios/fish/douyu/douyu.pvr" = <a81be35e 5f1ad330 81b98c2f 98470743 893bc548>;
        "3d_ios/fish/douyu/douyu_huo.pvr" = <e4ca688b ffe6c888 67b8ad6e 2849e436 0807920d>;
        "3d_ios/fish/fangyu/fangyu.cbx" = <ca80df67 35e1cc78 4abd4e7c 3f223f8f 964b2784>;
        "3d_ios/fish/fangyu/fangyu.ckb" = <0fb4a3d6 ca97315b 6d2c0dfb b3bb79e0 441cdc9e>;
        "3d_ios/fish/fangyu/fangyu.pvr" = <4ccff637 3683fe19 f5d157f3 cd771545 da92d2ad>;
        "3d_ios/fish/gaiputi/gaiputi.cbx" = <8c2592af 437fa9a1 f1d5ef5a d4a7a16c 2d51e4de>;
        "3d_ios/fish/gaiputi/gaiputi.ckb" = <890b794c d4ac883d 70be6480 59654e88 21a7532f>;
        "3d_ios/fish/gaiputi/gaiputi.pvr" = <431b06d4 acfdbd52 a3a9baac 06777026 6f3f4e71>;
        "3d_ios/fish/haigui/haigui.cbx" = <97e073b5 77abb5e2 9f2f0669 552c81f6 41984185>;
        "3d_ios/fish/haigui/haigui.ckb" = <c77bdaf7 b588b0f0 b0b05d88 85f1f019 6f875ba1>;
        "3d_ios/fish/haigui/haigui.pvr" = <be48182b f72ad634 de1545eb e00e112a 29d685ae>;
        "3d_ios/fish/haigui/haigui_ice.pvr" = <b05392fa 655a235b e04e4915 a1523665 b5de7632>;
        "3d_ios/fish/hetun/hetun.cbx" = <d842c4fc 0a478f65 551c6d8d 95dcf22b 6a606551>;
        "3d_ios/fish/hetun/hetun.ckb" = <622836d4 b0b1a734 3fc7a4e9 130ef6e7 9ceeb298>;
        "3d_ios/fish/hetun/hetun.pvr" = <f3446519 9220d51e f24232c5 9ecbe733 e02a1520>;
        "3d_ios/fish/hujing/hujing.cbx" = <9ecaf23d 792cd616 0737a098 e5c17e8c 3624bfe4>;
        "3d_ios/fish/hujing/hujing.ckb" = <f01defbd 0369af9b 0f4696e6 8cd7a0b2 3c8b644b>;
        "3d_ios/fish/hujing/hujing.pvr" = <ac3b4d3e e18cc447 bb15d2b4 12b86443 6cc93085>;
        "3d_ios/fish/jianyu/jianyu.cbx" = <7faa5ecb 3e967744 b01e3402 98fa4d71 dada016f>;
        "3d_ios/fish/jianyu/jianyu.ckb" = <24185343 16dc6e8f 669403f7 9902706a 3bc1a381>;
        "3d_ios/fish/jianyu/jianyu.pvr" = <7d6b3461 6e2e9693 3da3b17f b12bf883 5c137793>;
        "3d_ios/fish/jinqiangyu/jinqiangyu.cbx" = <32a4d9ca 2f6c05b0 c74443ec 61ee8c4e 90e3a789>;
        "3d_ios/fish/jinqiangyu/jinqiangyu.ckb" = <d3416e0b 0f6bb2d8 2bfc0757 80d7cd4d ec34bc03>;
        "3d_ios/fish/jinqiangyu/jinqiangyu_gold.pvr" = <0cf7b755 338d7604 29745f46 021a364a c696d5bd>;
        "3d_ios/fish/jinqiangyu/jinqiangyu_green.pvr" = <77c6ae79 540f8bc1 f7d9ab5d cd37d3f2 f8fb1565>;
        "3d_ios/fish/jinqiangyu/jinqiangyu_red.pvr" = <c20b97ed 32923c47 8886e061 483700e7 602fddbe>;
        "3d_ios/fish/shayu/box.ckb" = <5ae3f5e4 937edc84 e98e6219 791198ce 3faedc9e>;
        "3d_ios/fish/shayu/shayu.cbx" = <bbbb7580 f97eb70f b777f5ac 4de6d02b c8bdba10>;
        "3d_ios/fish/shayu/shayu.ckb" = <1468af16 624f1b80 c1a75f04 ee3490a5 97a0b5be>;
        "3d_ios/fish/shayu/shayu.pvr" = <01e7dda4 5206e504 993358cd 787eb91d 4fc5ae2c>;
        "3d_ios/fish/shayu/shayu_box.pvr" = <36715a84 57b08f61 300dcee0 8dfa8297 95209da0>;
        "3d_ios/fish/shayu/shayu_j2.pvr" = <83932081 9e969d93 9de7a140 5c1e7747 1a6d0725>;
        "3d_ios/fish/shayuya/shayuya.cbx" = <82b84333 ecdfd15f c83c1b70 06366ea0 9c7b0bce>;
        "3d_ios/fish/shayuya/shayuya.ckb" = <341a5692 a84e4205 ccc92ef5 9209a3ff 4c738fe3>;
        "3d_ios/fish/shiziyu/shiziyu.cbx" = <fcc92712 99d27ad0 aaf65bfa 7abc30b3 703bc6d3>;
        "3d_ios/fish/shiziyu/shiziyu.ckb" = <b2b9e6b8 c106c673 39d96c19 cecbe6d5 b24b242a>;
        "3d_ios/fish/shiziyu/shiziyu.pvr" = <086cded0 864d8b63 b60ad7fd 68f603b1 10d7f8ab>;
        "3d_ios/fish/tianshiyu/tianshiyu.cbx" = <b7d8ae1a 18b2e847 042f4c0e 4358b07e 022fb6c0>;
        "3d_ios/fish/tianshiyu/tianshiyu.ckb" = <e14b870c 0e097c79 17ba0717 58f549ff db93ce63>;
        "3d_ios/fish/tianshiyu/tianshiyu.pvr" = <c26e707c 27b4ba95 c42e4a1d de3fc196 cc8a2893>;
        "3d_ios/fish/xiaochouyu/xiaochouyu.cbx" = <81ed3b79 b414d902 80c463ba 691fb17e 901403f6>;
        "3d_ios/fish/xiaochouyu/xiaochouyu.ckb" = <a782983e c9e0de0c 80565511 9146848c 39943d10>;
        "3d_ios/fish/xiaochouyu/xiaochouyu.pvr" = <17be4eac 8314b951 25f57a55 2db044f5 e555e59b>;
        "3d_ios/fish/xiaohuangyu/xiaohuangyu.cbx" = <de8a2f93 fd30af95 fc82515f 565e62f0 0ed661c0>;
        "3d_ios/fish/xiaohuangyu/xiaohuangyu.ckb" = <8ac29166 69031a25 4e8837ef ac3771dd 5aa63d3c>;
        "3d_ios/fish/xiaohuangyu/xiaohuangyu.pvr" = <a376956e da3bc0fd fd0f6cbd 3246a54f 919ab635>;
        "3d_ios/fish/yingzuiyu/yingzuiyu.cbx" = <1be5fbc9 fab41e14 d7fc457d 65ec1b44 9906f629>;
        "3d_ios/fish/yingzuiyu/yingzuiyu.ckb" = <d556f6ed 8fea2de0 3f5d1921 596c9a5d 55b65336>;
        "3d_ios/fish/yingzuiyu/yingzuiyu.pvr" = <67950f52 8f30903c f42fb6fd 42da498c 37dbc2c6>;
        "3d_ios/fish_low/beijingyu/beijingyu.cbx" = <86360562 fc2ca400 2f515742 d38d49b9 b6963716>;
        "3d_ios/fish_low/beijingyu/beijingyu.ckb" = <0a74a536 bc8d1eb3 7694ba29 d8de8797 70ab8c5e>;
        "3d_ios/fish_low/beijingyu/beijingyu.pvr" = <ab784066 844f99ab 6a106eda 2951b2e4 e37edc83>;
        "3d_ios/fish_low/bianfuyu/bianfuyu.cbx" = <cc0154b7 96106497 6fc7f7b8 d2d7caeb b2611b92>;
        "3d_ios/fish_low/bianfuyu/bianfuyu.ckb" = <8939f2bf f95a21c0 15b1f2fa 1b48dd3a 1e6ae264>;
        "3d_ios/fish_low/bianfuyu/bianfuyu.pvr" = <ce3d1044 40813e2a 55cd3d38 be0babac e3103983>;
        "3d_ios/fish_low/bianfuyu/bianfuyu_gold.pvr" = <ff6e487a 90136f52 499702be 56a90a3f e78901fc>;
        "3d_ios/fish_low/bianfuyu/bianfuyu_green.pvr" = <ca39bc29 18a55026 975bbe39 313829a5 33e1e000>;
        "3d_ios/fish_low/cheqiyu/cheqiyu.cbx" = <314de387 13bd04f8 858ea6f2 ecec5479 ce296d7c>;
        "3d_ios/fish_low/cheqiyu/cheqiyu.ckb" = <af3df271 ed3aa25c f728ace8 41130111 547fb2da>;
        "3d_ios/fish_low/cheqiyu/cheqiyu.pvr" = <88ec6169 7eec57ca 4c62fc86 93778712 ba4fe229>;
        "3d_ios/fish_low/denglongyu/denglongyu.cbx" = <a084af39 43f61d1e 6e24b6ef b8a4f409 f78b3f61>;
        "3d_ios/fish_low/denglongyu/denglongyu.ckb" = <062db9a8 f118a69e 63109e23 923d2363 9ed65a97>;
        "3d_ios/fish_low/denglongyu/denglongyu.pvr" = <01ce8472 c1a97851 f93b49bc ff1e45a0 2fa481bc>;
        "3d_ios/fish_low/dianmanyu/dianmanyu.cbx" = <fc907769 86354996 c7da4704 527b6454 37f53198>;
        "3d_ios/fish_low/dianmanyu/dianmanyu.ckb" = <2c19a2e0 bba081f8 307c1c4b df499dc1 deec398f>;
        "3d_ios/fish_low/dianmanyu/dianmanyu.pvr" = <48ad184d 25c49e27 78b72ca3 3d107659 f3a4d6f2>;
        "3d_ios/fish_low/dianmanyu/shandian1024.png" = <0040747f b29bf67b e14b5bdd d8062f44 18875356>;
        "3d_ios/fish_low/diaoyu/diaoyu.cbx" = <cee7cee6 4beaa00b dcd68de4 dcf318c3 4bbe0bd4>;
        "3d_ios/fish_low/diaoyu/diaoyu.ckb" = <6fe4f12d 3ac60096 092fd471 962e9288 4b863ed8>;
        "3d_ios/fish_low/diaoyu/diaoyu.pvr" = <111cf4a3 86f29bb9 6c63bee3 6e3a313f 2ff0732f>;
        "3d_ios/fish_low/dinianyu/dinianyu.cbx" = <84332d5b 909e374e 16c2dbde 9e3542f9 2ec04cf6>;
        "3d_ios/fish_low/dinianyu/dinianyu.ckb" = <60490765 721db631 5a421213 e1f422fb 3da960ef>;
        "3d_ios/fish_low/dinianyu/dinianyu.pvr" = <bc9da009 78c2703b ad4622de 3abacb98 b7a64214>;
        "3d_ios/fish_low/douyu/douyu.cbx" = <7833e330 efe15792 7341519b 3fa89722 a9a29027>;
        "3d_ios/fish_low/douyu/douyu.ckb" = <d7ef9d1a 067e3ed6 28510097 8381bfe2 439b5e3c>;
        "3d_ios/fish_low/douyu/douyu.pvr" = <ef8b0e6b b0eb6f68 4e6e7156 635d3ac8 ce448cb7>;
        "3d_ios/fish_low/douyu/douyu_huo.pvr" = <739182ef b0b674e8 91ca1a27 26b4d48e 8b800d96>;
        "3d_ios/fish_low/fangyu/fangyu.cbx" = <f9cd7216 1fa33ff9 143375c0 eeeb8089 d2e319bc>;
        "3d_ios/fish_low/fangyu/fangyu.ckb" = <6f98119c f3c45084 d5e0e2f1 50e36761 8bbcdd91>;
        "3d_ios/fish_low/fangyu/fangyu.pvr" = <68b0026c aee32e9e fd0ebd08 c52eac7e 8896f376>;
        "3d_ios/fish_low/gaiputi/gaiputi.cbx" = <500af12d 8b65c8cf 7b40bc48 2b92d649 25a0f4d6>;
        "3d_ios/fish_low/gaiputi/gaiputi.ckb" = <853006e9 eecac006 7ba0e881 99db3c3b dd810a5a>;
        "3d_ios/fish_low/gaiputi/gaiputi.pvr" = <d6612e0b b40fa6d9 8a3fcd2b 0228b041 63346277>;
        "3d_ios/fish_low/haigui/haigui.cbx" = <c2e7516b 542d8d4b 4ac110a3 eeeaae6e 2c204d13>;
        "3d_ios/fish_low/haigui/haigui.ckb" = <da818299 2d567b0f cb4fc869 f454235f 11d39503>;
        "3d_ios/fish_low/haigui/haigui.pvr" = <3a900de1 96e9698b a1aab11c 8228d085 1f2a7167>;
        "3d_ios/fish_low/haigui/haigui_ice.pvr" = <0151b3eb 2c9d2cb0 d4172447 1e5a8897 3f87ef19>;
        "3d_ios/fish_low/hetun/hetun.cbx" = <42f04368 f743eeeb 64a5ffc9 2f4132e6 01fdd429>;
        "3d_ios/fish_low/hetun/hetun.ckb" = <38d500a3 3adde7ef 6da70d16 d6218614 3e5c7107>;
        "3d_ios/fish_low/hetun/hetun.pvr" = <33e0ad70 301087a4 3c938c76 a2b61b6d d2d8716b>;
        "3d_ios/fish_low/hujing/hujing.cbx" = <9ecaf23d 792cd616 0737a098 e5c17e8c 3624bfe4>;
        "3d_ios/fish_low/hujing/hujing.ckb" = <f01defbd 0369af9b 0f4696e6 8cd7a0b2 3c8b644b>;
        "3d_ios/fish_low/hujing/hujing.pvr" = <20d61210 f2b8ac2f c5022e94 65c695da 01b9acef>;
        "3d_ios/fish_low/jianyu/jianyu.cbx" = <1ef0ad56 67d98fbb 186ce510 25d2cf83 068e9e0d>;
        "3d_ios/fish_low/jianyu/jianyu.ckb" = <02535801 f1442b34 712d3413 ef28ec73 6987bc91>;
        "3d_ios/fish_low/jianyu/jianyu.pvr" = <589e2a87 2972761b 185da25a ea58f31c bc63e03d>;
        "3d_ios/fish_low/jinqiangyu/jinqiangyu.cbx" = <0c5f4d66 6b891839 2a9b06e6 62d330e3 2ba061eb>;
        "3d_ios/fish_low/jinqiangyu/jinqiangyu.ckb" = <d25f56e0 0467a57a 848dff1f b1ea6ee8 11fad430>;
        "3d_ios/fish_low/jinqiangyu/jinqiangyu_gold.pvr" = <adf8ba8e 6e365cfa 3671682d 59fa2b82 908aaf6b>;
        "3d_ios/fish_low/jinqiangyu/jinqiangyu_green.pvr" = <6def8cd5 8187a342 d89b9dcf 8c531f5f 58565459>;
        "3d_ios/fish_low/jinqiangyu/jinqiangyu_red.pvr" = <c8d092b8 d83b3b48 74d29cc0 0631079e 1caa0318>;
        "3d_ios/fish_low/shayu/box.ckb" = <5ae3f5e4 937edc84 e98e6219 791198ce 3faedc9e>;
        "3d_ios/fish_low/shayu/shayu.cbx" = <8b4dda5d 807bd7ed e10bc118 6069eb52 b9e00864>;
        "3d_ios/fish_low/shayu/shayu.ckb" = <d2778409 d4424677 8bccf4be 16b30019 215f4e44>;
        "3d_ios/fish_low/shayu/shayu.pvr" = <7d071977 269156f7 40c7038b 83f8b652 2897aadd>;
        "3d_ios/fish_low/shayu/shayu_box.pvr" = <36715a84 57b08f61 300dcee0 8dfa8297 95209da0>;
        "3d_ios/fish_low/shayu/shayu_j2.pvr" = <0057eaa4 985964ce 3a6dbcf3 8930095e 8a62f97a>;
        "3d_ios/fish_low/shayuya/shayuya.cbx" = <ae87e56f 11a88d1c 478b8cf8 bad41704 a1f0458a>;
        "3d_ios/fish_low/shayuya/shayuya.ckb" = <81178565 509fe9f4 2636ca19 a60a697e f9e296ef>;
        "3d_ios/fish_low/shiziyu/shiziyu.cbx" = <f049d74e 9ade4654 1b39c2c3 71978714 7b13d650>;
        "3d_ios/fish_low/shiziyu/shiziyu.ckb" = <03c2f839 618a3cee 97d495b8 c7a77397 0d628163>;
        "3d_ios/fish_low/shiziyu/shiziyu.pvr" = <1a1f23a1 95f14352 6dd78c2d 97cfbb9f b3c9b088>;
        "3d_ios/fish_low/tianshiyu/tianshiyu.cbx" = <5c8e5ca5 ae0f86a7 9320c1b9 d8cf1ad9 810f99bc>;
        "3d_ios/fish_low/tianshiyu/tianshiyu.ckb" = <69b1b00e 2968814e b1148471 9bd6c498 52e22d5d>;
        "3d_ios/fish_low/tianshiyu/tianshiyu.pvr" = <42f49f04 78262caf 5bdfba29 92e08296 cdf0985f>;
        "3d_ios/fish_low/xiaochouyu/xiaochouyu.cbx" = <005e0bee 8cf71d69 4efddf6d dda2fafd aafee2d3>;
        "3d_ios/fish_low/xiaochouyu/xiaochouyu.ckb" = <bc5c60b5 6fd44cff 082060af 2c19ea98 ed3c7ab7>;
        "3d_ios/fish_low/xiaochouyu/xiaochouyu.pvr" = <c8bfe5a4 40dc3e55 132ba01c 0e2c16ee b7f88386>;
        "3d_ios/fish_low/xiaohuangyu/xiaohuangyu.cbx" = <5eecc0b4 9fa4e0f4 1fae05da 53d6c1e5 98fb1189>;
        "3d_ios/fish_low/xiaohuangyu/xiaohuangyu.ckb" = <62b163f4 533cc13a 15974123 5df206f3 5cb4a173>;
        "3d_ios/fish_low/xiaohuangyu/xiaohuangyu.pvr" = <6852be27 6c073564 eca5f122 d1e0d65a d9d32729>;
        "3d_ios/fish_low/yingzuiyu/yingzuiyu.cbx" = <475d48d2 e73a15b7 397ed13a 4847475f 7271304d>;
        "3d_ios/fish_low/yingzuiyu/yingzuiyu.ckb" = <934c1e12 1c98e06c 70f82e6c bbda3f41 d5d3f557>;
        "3d_ios/fish_low/yingzuiyu/yingzuiyu.pvr" = <0ebfa3a5 239c0047 fd719446 05ab205e 096de19d>;
        "3d_ios/material/background.material" = <4f57bc81 76a27bf8 130e3403 ea502065 45ba233a>;
        "3d_ios/material/base.material" = <df587125 6d01a236 7dcc68cf e67c26c7 1418e57d>;
        "3d_ios/material/base_no_anim_light.material" = <1d6ed03f 3103d89a 94c67f03 be13c293 0c3dc1b0>;
        "3d_ios/material/beijingyu.material" = <32d4bf98 1b0d6813 76cced8e bec269a3 13461c92>;
        "3d_ios/material/bublelight.material" = <a61f74a0 e7d37d6a 045f380b 6a3bfaeb 119eafde>;
        "3d_ios/material/bublewarp.material" = <243fbc9c e816fc83 2fb4ad35 b4e1ae9c 7502cd34>;
        "3d_ios/material/deng.material" = <a1581e7f 5cac6428 f7f6c9c1 a2d535bc 03f886a1>;
        "3d_ios/material/dian.material" = <fda79767 fdb3a1e8 ffcc09af 0b28f9e1 ddca7a3e>;
        "3d_ios/material/lightning.material" = <261fbb67 64bbfe3c 25674374 100c3db8 c381b173>;
        "3d_ios/material/lightning_cover.material" = <c98d0c9c 02d4b5fb 89feea5f 20bd0ad7 2e98ecb7>;
        "3d_ios/material/lightning_fever.material" = <69bcaee6 d48eb2e2 f3190f0e d20c6a21 b83986a2>;
        "3d_ios/material/lightning_red.material" = <2d2b4aaf eb5b7b6f 38e522e1 9e9162a8 5b769b93>;
        "3d_ios/material/ocean.material" = <712f92bf aa0ed1bf 4f82ce36 1680bdd7 5b466bf6>;
        "3d_ios/material/ocean_back.material" = <4486ffeb 712c49fc afd57ad0 45e919cf 0e47ec1e>;
        "3d_ios/material/ocean_front.material" = <a39cd1d0 e16cd6ae 90a2a177 f06c2911 371d1b93>;
        "3d_ios/material/ocean_line.material" = <e12ac98a a3b47e57 d04c9487 3d23e068 af41168b>;
        "3d_ios/material/shiziyu.material" = <902ac3da 27d0b0fc 49291c56 7d16f892 ff94a196>;
        "3d_ios/material/shuihua.material" = <44eef142 f75edea3 e4e52022 df4b3936 0689068c>;
        "3d_ios/material/sky_back.material" = <50f0b418 c85f6b11 199fdf50 0c97d1fa 297a427c>;
        "3d_ios/material/water_drop.material" = <f952af46 94bd53d2 d0c4715c e57b3e43 8fc6a2ab>;
        "3d_ios/material/ya.material" = <e653c64c 0867c076 32c5011f 9bd193d6 dc430071>;
        "3d_ios/particles/beijing_shanshoudiandian.particle" = <6a31bd64 a12e4609 4d776028 8584a476 e8515a5a>;
        "3d_ios/particles/beijingchenai01.particle" = <5c5374fd 072930ef 0438a9e8 05cd2fd2 fe7df8f1>;
        "3d_ios/particles/chenai.pvr" = <520f027e d73cce5a 2abdfd48 77899977 bcfa953c>;
        "3d_ios/particles/fever_guangban-1024.particle" = <a5762dc6 8ad31230 7b9c77be 51c31af7 11f694c6>;
        "3d_ios/particles/fever_guangban.particle" = <b26405ce 117f4d09 b022e1b5 d48681f6 28f6da35>;
        "3d_ios/particles/flare05_7.pvr" = <db96047c e918686d 21215ecb 4ed5fdde ea58965a>;
        "3d_ios/particles/guangban.pvr" = <2e0d2cfc d10a13ff 5d632e6a 3c689e6c 15df30c1>;
        "3d_ios/particles/qipao.particle" = <ba1cdaaa 8a754216 cd4ac6a7 ee489e16 aef930d4>;
        "3d_ios/particles/qipao.pvr" = <4b520f2a e223e9bb 5e2642ed e684cb7d 2b7f159d>;
        "3d_ios/particles/qipao_01.particle" = <353e6408 365b14c6 263279c5 096aec4c 450bc4d7>;
        "3d_ios/particles/qipao_02.particle" = <a9835ece 766eb212 c0cdc3f7 73397760 6defce81>;
        "3d_ios/particles/qipao_03.particle" = <b057c341 b9841d61 39923103 4fe7054f 36ec57fe>;
        "3d_ios/particles/qipao_03.pvr" = <edbbe7f9 621b71d5 d34d3086 bc8c1979 42570cb1>;
        "3d_ios/particles/shanshuodiandian01.pvr" = <1aa38d2b 90224d3c 5e4742cb db747cdc 778888e1>;
        "3d_ios/particles/shanshuodiandian02.pvr" = <c43d31b4 4aea4769 26c20738 2b09e25e e641413a>;
        "3d_ios/particles/yuanguang.png" = <9f6e8054 2d55fb9c 9c086be7 87e6d06d 62820013>;
        "3d_ios/posteffect/postprocess.material" = <c53b7f2c 0319a26f 6753eb31 67393aef b52e38c3>;
        "3d_ios/posteffect/postprocess_bloom.material" = <49a43ee2 8aff3be0 0b60ae91 429a4c6e da4f35ef>;
        "3d_ios/posteffect/postprocess_blur.material" = <4203d92f cad17521 e60efa3d 42099c95 f2d4c342>;
        "3d_ios/posteffect/postprocess_color.material" = <104a3460 01bcceca 62626846 f5750454 30b262fd>;
        "3d_ios/posteffect/postprocess_glow.material" = <61bde524 e4e6106e 0ebb3767 5b729ec3 9fe11af3>;
        "3d_ios/posteffect/postprocess_glow_and_point_warp.material" = <1470ed2b d176c4e2 9dc181b1 73a034ce 4835cd7a>;
        "3d_ios/posteffect/postprocess_outline.material" = <46acf144 3c776655 6226362f c24bef44 3812c813>;
        "3d_ios/posteffect/postprocess_scenechange.material" = <9f5d98ab cbeab8c0 b8b2d9cc b72dbe2e a8277b7d>;
        "3d_ios/posteffect/postprocess_scenecopy.material" = <723f464c 10416ecf 1a5540a1 4e40cc99 772778e2>;
        "3d_ios/posteffect/postprocess_spacewrap.material" = <80fe8a5d 2da92d85 1ce9e96c c2f5cebb 1e4001e8>;
        "3d_ios/posteffect/postprocess_vortex.material" = <3c6bc8af e596ba37 cdd4f05b f32d0a0d cf8791a7>;
        "3d_ios/scene/beike/beike.ckb" = <ed92e7f5 0d81458f 94051fb0 5772b8fb 9951f4b7>;
        "3d_ios/scene/beike/beike.pvr" = <a89008ed b98ed843 7d4e98ff 1702cb6c bc16f122>;
        "3d_ios/scene/bublelight/bublelight.ckb" = <c44d2355 0225ae3f d7659f37 7b5705d5 dc3d160f>;
        "3d_ios/scene/bublelight/bublelight01.png" = <d118242c 5510d85e 72acdbb1 004ef7d6 37c9e1df>;
        "3d_ios/scene/bublelight/bublelight02.png" = <7b93b426 ac191acb 48127fb4 121f38ec 5b74d649>;
        "3d_ios/scene/bublewarp/bublewarp.ckb" = <4bbd796a 6a094ab4 c673bb9d 69a4b7a7 bf44396f>;
        "3d_ios/scene/bublewarp/bublewarp.pvr" = <e0d969b9 300d3db2 2ff50d86 67a8742c e1cb50f3>;
        "3d_ios/scene/hua_02/hua_02.cbx" = <86360562 fc2ca400 2f515742 d38d49b9 b6963716>;
        "3d_ios/scene/hua_02/hua_02.ckb" = <329920f9 46b5bc05 4be7f6c2 e8dd4d4d fa44b989>;
        "3d_ios/scene/hua_02/hua_02.pvr" = <aff5ae34 e16be750 beb41709 f9d86d39 6c75b0d0>;
        "3d_ios/scene/lightning/lighting_cover.png" = <57ce4a64 9ebbce51 ff5148ff 969d4d32 18574491>;
        "3d_ios/scene/lightning/lightning.ckb" = <baf48a01 b6cac8a8 f158d44f 3b4584ea aa7d2d6a>;
        "3d_ios/scene/lightning/lightning.png" = <90b88449 98924d2a f3370658 9871fe5f fcb39c74>;
        "3d_ios/scene/lightning/lightning_fever.png" = <f2007a8c 0e68207f 0b10c51d 8c868b8b c136edc4>;
        "3d_ios/scene/lightning/lightning_red.png" = <5005b62a 8e030ffb 463a802a 13cdfef0 c79542ef>;
        "3d_ios/scene/pad_background/pad_background.ckb" = <fa03e89a 74beb75a dfbe02ad c9770262 26c3425a>;
        "3d_ios/scene/pad_background/pad_changjing07_beijing.jpg" = <f82ce325 7b047d3d 9ee0bac9 f576c23c c84c563d>;
        "3d_ios/scene/pad_background/pad_changjing08_beijing.jpg" = <5ce81b04 72c13b1c 2f36f7cc 97b689de 6d15e6f5>;
        "3d_ios/scene/pad_background/pad_changjing09_beijing.jpg" = <c4574154 572602fe f36ebb2e e32096e1 530f27ca>;
        "3d_ios/scene/phone_background/phone_background.ckb" = <601934dc cbcece87 3cd0da95 86651baa bb3c0448>;
        "3d_ios/scene/phone_background/phone_changjing07_beijing.jpg" = <477b3f4b e94f8d40 7b07c4de 54f0f9e8 add1c4eb>;
        "3d_ios/scene/phone_background/phone_changjing08_beijing.jpg" = <bc05662f 4b042901 989df601 e8517c2c 1ac881de>;
        "3d_ios/scene/phone_background/phone_changjing09_beijing.jpg" = <f2b6b327 1fd3e8f6 75a34afa 5f995214 dad28668>;
        "3d_ios/scene/start/ocean_back.jpg" = <1a665f7e ab6d04a1 bd73dd49 c3903d36 9d4380da>;
        "3d_ios/scene/start/ocean_front.tga" = <e91ea5a3 5e08bf60 1e391d7f b48ade55 e586248b>;
        "3d_ios/scene/start/ocean_line.png" = <b2fc3037 0633edd5 41cbf4e7 8d0f7edc 0dc1889d>;
        "3d_ios/scene/start/shuihua.ckb" = <f67a822f dac5086b 83088ed3 6072b178 6ae6d520>;
        "3d_ios/scene/start/shuihua.png" = <19c040c4 98b7a621 c3287030 e2fdffae 249ec250>;
        "3d_ios/scene/start/sky_back.jpg" = <dd940660 b661fda7 5c167286 90b396a0 3cc83504>;
        "3d_ios/scene/start/sky_back1.png" = <74c5bfcb b12d6e77 9fc0e061 4c1d1804 0037bb93>;
        "3d_ios/scene/start/sky_back2.png" = <70d9b1f1 9e613857 ceeca92b eae56e9b 6ea7b425>;
        "3d_ios/scene/start/sky_back3.png" = <d20b9e69 746857f1 14863f5e b7bac701 65ef1796>;
        "3d_ios/scene/start/sky_back4.png" = <c72a6368 cac516e2 dbc927a7 8a3afad7 6b478d1b>;
        "3d_ios/scene/start/water_drop.png" = <65d13c55 52c05d5a e83f0de0 12cfed75 db437ca6>;
        "3d_ios/scene/start/wave_normal.pvr" = <229bd12a 32c99666 742256a1 c0790f3d b52e4099>;
        "3d_ios/shaders/background.fsh" = <5d69c496 372f1f33 27e8823d 7bfa4bc9 4d2a2da1>;
        "3d_ios/shaders/background.vsh" = <c4daf186 3c72ec87 c4c35bbb 977a1958 094789e1>;
        "3d_ios/shaders/debug.fsh" = <fa56e9aa c0cb58f0 bd4dafe5 644fec5e d01eaa57>;
        "3d_ios/shaders/debug.vsh" = <5061d869 fef6b941 1cf0e146 2d66225e 76c8b68f>;
        "3d_ios/shaders/fish_basic.fsh" = <86691f90 d9ac376d be596c61 e8b9c38b 71a785ea>;
        "3d_ios/shaders/fish_basic.vsh" = <b01f03e0 3f7a5a2b 3f2f31ed b0d1e06d a3f51145>;
        "3d_ios/shaders/lighting_cover.fsh" = <5f1a8792 292564c5 87431b20 ed3fcd24 022a6e9e>;
        "3d_ios/shaders/lighting_cover.vsh" = <ced522a9 0676bd5c d0abbdd5 0c8e3aac 92419552>;
        "3d_ios/shaders/line_render.fsh" = <5d70fdd8 3c904b11 2ab5aae3 4b6408cc 2162aa5f>;
        "3d_ios/shaders/line_render.vsh" = <f052d5a6 2dfe6acb 5959e7e6 a93ce9af 3b59b732>;
        "3d_ios/shaders/ocean.fsh" = <8d2d552b 1bdba8af fa64e966 1964b440 b964164d>;
        "3d_ios/shaders/ocean.vsh" = <a31e88ae f1beaae0 880253a4 b16763e3 3a945c9f>;
        "3d_ios/shaders/ocean_front.fsh" = <0a3c8449 dec861f1 4467f687 cfbe37fd c7f137d4>;
        "3d_ios/shaders/ocean_front.vsh" = <cbc69141 e53e3722 1e21a5af 9f4c1971 241942e5>;
        "3d_ios/shaders/paopaoguang.fsh" = <ebbaee88 d984c412 c608b3f4 58a9c421 96c1cfe1>;
        "3d_ios/shaders/paopaoguang.vsh" = <8556937f 7d1c7bff a969ae5f 862e9777 4275ca26>;
        "3d_ios/shaders/paopaoniu.fsh" = <48619e01 f6b583a1 21928aa7 b3b36210 e789a125>;
        "3d_ios/shaders/paopaoniu.vsh" = <867349b7 7b41cad1 d2d1b380 9d63ea0e c191a8da>;
        "3d_ios/shaders/particle.fsh" = <70e2787a beb631fa d249b67e 1052665d 7db8ec9a>;
        "3d_ios/shaders/particle.vsh" = <77a1d591 ac92fb22 cd8635a6 19a1962e b49c0b3e>;
        "3d_ios/shaders/postprocess/postprocess.fsh" = <2c326dbb 896a7c24 5a13bb9d 2664f2b4 db033449>;
        "3d_ios/shaders/postprocess/postprocess.vsh" = <dfa221ef aaa77cae 7db57a61 35615545 871743a0>;
        "3d_ios/shaders/postprocess/postprocess_bloom.fsh" = <440df968 62233f83 ce8813b1 4f01be23 3582cd79>;
        "3d_ios/shaders/postprocess/postprocess_bloom_blur.fsh" = <ef2c537b 03a65d27 dd8606dc 1dd7b959 0b370a34>;
        "3d_ios/shaders/postprocess/postprocess_bloom_extract_bright.fsh" = <977101d7 4fadcc3b 83e87d24 744df36a e69ef239>;
        "3d_ios/shaders/postprocess/postprocess_blur.fsh" = <716d4ff9 263239f2 1af52007 0292cf5f b060ccf9>;
        "3d_ios/shaders/postprocess/postprocess_color.fsh" = <4674a5de b1c9a6d8 c3ddb155 0a7b4bc5 832d3b40>;
        "3d_ios/shaders/postprocess/postprocess_glow.fsh" = <c05eb36e 5ad1ebe2 4a5b8828 043d8c41 5ce73a46>;
        "3d_ios/shaders/postprocess/postprocess_outline.fsh" = <3238b95f b5ad87c5 68963932 5577ac9e a9257725>;
        "3d_ios/shaders/postprocess/postprocess_point_warp.vsh" = <ea86da91 d4b66215 6e878170 ed1e58ef 5f80b083>;
        "3d_ios/shaders/postprocess/postprocess_scene_change.fsh" = <41feab6e bebc2b11 8d934d6c 26f72b03 f85debd5>;
        "3d_ios/shaders/postprocess/postprocess_scene_copy.fsh" = <8494fde4 2c92c768 14a45b5d c4094169 665a7c25>;
        "3d_ios/shaders/postprocess/postprocess_vortex.vsh" = <21818ff9 b5a83396 66fd141a cf092706 69f2ead9>;
        "3d_ios/shaders/postprocess/postprocess_wrap.fsh" = <bc483614 2e63e9fa 7a17dd6e 26ca5431 b5da0899>;
        "3d_ios/shaders/postprocess/postprocess_wrap.vsh" = <1dd82c5d 4f48d6ac 6cdd8ec4 e1287e52 40aa7f28>;
        "3d_ios/shaders/skinned_general.h" = <9ab5cf88 2c9679a5 d58d76a9 0fb72d73 070488c8>;
        "3d_ios/shaders/sky_back.fsh" = <308c6827 67ce08d9 303289ce a3f6c05f 10839711>;
        "3d_ios/shaders/sky_back.vsh" = <c4daf186 3c72ec87 c4c35bbb 977a1958 094789e1>;
        "3d_ios/shaders/texture.fsh" = <3cec6ef8 ce9f0f47 0f64746b 6303d550 5a109f8c>;
        "3d_ios/shaders/texture.vsh" = <ffca60f6 3fd33f1d 32e9f173 212e8154 4707b260>;
        "3d_ios/shaders/textured.fsh" = <cce01110 77db470b 8558d170 2bfd0422 232e52d0>;
        "3d_ios/shaders/textured.vsh" = <1fd2d2bb 792ed078 7484d9eb 2a4eb9f3 6483da5c>;
        "3d_ios/shaders/uv_animation.fsh" = <8a94bb46 3db77b46 01166c1d 25e5cfcc 33acf5da>;
        "3d_ios/shaders/uv_animation.vsh" = <eb959d23 05927e3a 2b50edd5 a786efac 7e7b1678>;
        "3d_ios/shaders/water_drop.fsh" = <72545506 9d23ab95 877c3746 dc628dba b65e904b>;
        "3d_ios/shaders/water_drop.vsh" = <f6a52fd9 13b26374 870b60f2 25b031a0 f10af776>;
        "960x640/flash/achievement_light.plist" = <ace7fdf1 2a83a057 02b0c1de 742a2aa1 6e9a2f1c>;
        "960x640/flash/achievement_light.png" = <88e5e08f 501fa6dc 4ec945fd fc58ae0c 5a0a185e>;
        "960x640/flash/achievement_light.xml" = <6a71204c 2ed6b40e 5087d141 4e049303 5c0490cd>;
        "960x640/flash/achievement_max.plist" = <bdc28bb6 9493ab67 9ec57487 58954ff8 9e036755>;
        "960x640/flash/achievement_max.png" = <93d33aaa 7258f740 9e2c18ef bc764720 a14036fa>;
        "960x640/flash/achievement_max.xml" = <2347a79d 966b2d9b dda407c8 2554e4ee d4967dd7>;
        "960x640/flash/achievement_star.plist" = <02bc6158 06203f8b d2281174 97eee0a2 fc2d68f4>;
        "960x640/flash/achievement_star.png" = <6ab00b9b 7e7c3489 e5e2830c 99c29c9f c6b689ca>;
        "960x640/flash/achievement_star.xml" = <cbc692ca 5c30346c 88b6c832 3b5ab05d ef8ca615>;
        "960x640/flash/achievement_tip.plist" = <09cf5b8d 94ef77b8 0453cb84 ddc5eca4 00a58666>;
        "960x640/flash/achievement_tip.png" = <378ab800 3c0f03b0 449579d1 d8be0385 400281e1>;
        "960x640/flash/achievement_tip.xml" = <972016b3 a61f554c 6d531137 78b03888 0e55b787>;
        "960x640/flash/angle.plist" = <8f788a9b 96427394 16b9f9bc 8f4485a6 85d97ff2>;
        "960x640/flash/angle.png" = <aebe60d6 08b65fd0 9982c71a 6d3514e5 fbbe595a>;
        "960x640/flash/angle.xml" = <68877059 aa65132f df39cf82 e4102bdd 53be641c>;
        "960x640/flash/arrow.plist" = <f39a916a 788de2cd 64de30c0 526811a0 19b7ee1c>;
        "960x640/flash/arrow.png" = <fb563f14 753730bd f78bbd35 8b8ad57d 99875297>;
        "960x640/flash/arrow.xml" = <15ec2c68 5057261c a333cda8 c387c877 8c30c60d>;
        "960x640/flash/award_01.plist" = <87e314b1 46ec23f6 0ef26bef a0166c61 bb7e8587>;
        "960x640/flash/award_01.png" = <5c1b6a78 128f181b 150256b9 8d1bece9 31983e76>;
        "960x640/flash/award_01.xml" = <2aa9fb43 a1321cc4 fc299ee2 6964c65e b4eb5b0c>;
        "960x640/flash/award_02.plist" = <e09de10c d370388e 04d1b290 55b92902 6e9f04a9>;
        "960x640/flash/award_02.png" = <ca5df3ac 9c59b8b1 6a88bec9 cd3f991c d96be9ae>;
        "960x640/flash/award_02.xml" = <365629b6 23687a23 8c9eefcd df2feeb5 a396e325>;
        "960x640/flash/bar_coin.plist" = <376302b1 b4f18daa 301b4fc0 bd35312e 00e7505b>;
        "960x640/flash/bar_coin.png" = <e07d5de1 d8ddeb37 364f3d69 a17fb8e6 7746a6a0>;
        "960x640/flash/bar_coin.xml" = <9a7a20b7 c9b796c4 825abf3a 47f55ae7 fe13ee47>;
        "960x640/flash/bar_crystal.plist" = <8923c2c2 0de5cb26 5dce1b1a e0ee95c2 4358952b>;
        "960x640/flash/bar_crystal.png" = <7642fe43 e2b17f82 05d25f64 c083cf38 277cada5>;
        "960x640/flash/bar_crystal.xml" = <c32f9c54 e63e921b b1e4eb86 1e75477f 2af0d905>;
        "960x640/flash/bigwin.plist" = <51c8b95c 6b6102db 331db406 86bb8d7e 8d62d9b6>;
        "960x640/flash/bigwin.png" = <7431c44d b3836028 0d0b6ddd a20901d2 3e3a495e>;
        "960x640/flash/bigwin.xml" = <d8da446d b05acf09 ae7749ba 3a1d59be 2ebb97bf>;
        "960x640/flash/bomb.plist" = <1188e413 5ebbc465 208bbe3f f812bf06 190cb5b0>;
        "960x640/flash/bomb.png" = <687664f0 ce6d59e2 bc1cbd13 8204f34b 4d734c8a>;
        "960x640/flash/bomb.xml" = <292c3d6a df60928c 70e08d10 ecfee58b f6624f5d>;
        "960x640/flash/bullet.plist" = <9a0d12db 2f2a1100 42b79671 9b6dfd32 ff433271>;
        "960x640/flash/bullet.png" = <ad0bdfa9 9e98ba2d 71045c15 a5171fab 137686cc>;
        "960x640/flash/bullet.xml" = <cf296a6d 4eb886e9 3bfb62d2 bfdf55ba 257c361d>;
        "960x640/flash/bullet_bomb.plist" = <c8dc618a 7ece60c1 83ed1568 2b7a866e 5e1ad56a>;
        "960x640/flash/bullet_bomb.png" = <74c6d4e6 67cafda9 3ac70c79 3efc47f3 d966f9ae>;
        "960x640/flash/bullet_bomb.xml" = <7fce71ea c6adc2c8 3ea79d40 e3401068 f96f5c39>;
        "960x640/flash/bullet_circle.plist" = <9166b99c 33578133 cb0fd570 62c62652 f37ee23a>;
        "960x640/flash/bullet_circle.png" = <8844524a b56e0df8 19a3f58b ab157e3a a4a9ab06>;
        "960x640/flash/bullet_circle.xml" = <2881b8e0 ca304eb5 1335864e 56e52787 543c9b7f>;
        "960x640/flash/bullet_fire.plist" = <2e90bec5 cdbd541e 2c690d74 d744acb8 853c2a8c>;
        "960x640/flash/bullet_fire.png" = <3e1bdc79 10799090 f2121efb 5a584387 1b4af515>;
        "960x640/flash/bullet_fire.xml" = <945aab04 a09039ef 33a7e680 0848b200 6ddb24a0>;
        "960x640/flash/bullet_gas.plist" = <ccf977fd 819c14e6 7978e3d4 3e40c671 da28045a>;
        "960x640/flash/bullet_gas.png" = <a5020b8b 6b9fc939 eeefbe39 476bffb3 e3e7b26f>;
        "960x640/flash/bullet_gas.xml" = <98b92280 f52e80a4 aedaf483 acbe09ed 9bdce6b6>;
        "960x640/flash/bullet_harpoon.plist" = <7eff53a5 92dfc506 73d95b5a 7a49e68e 16e75132>;
        "960x640/flash/bullet_harpoon.png" = <0b7deaa7 2b9c462f 5150af98 3b019814 816beabc>;
        "960x640/flash/bullet_harpoon.xml" = <6e49600f aef3ee07 6a62ab3d fd5c4e01 ce5c9202>;
        "960x640/flash/bullet_moonlight.plist" = <525c478d a5906d14 75dad5e0 33cc9a6d a9107c55>;
        "960x640/flash/bullet_moonlight.png" = <0547a918 0eb550ab 77cd6a38 d4b5c404 15ad9aab>;
        "960x640/flash/bullet_moonlight.xml" = <1df6ff82 89390f41 c234bf10 2f6f3239 c6f7db17>;
        "960x640/flash/bullet_projectile.plist" = <45384188 1944351b 98d7591f 21566e2e 1ec6706b>;
        "960x640/flash/bullet_projectile.png" = <37f50ba8 8ad3a739 810813a5 abb7a86b 4ba95c4a>;
        "960x640/flash/bullet_projectile.xml" = <bc103225 9eb525b4 296da55a 9c37dff0 15e8d226>;
        "960x640/flash/bullet_rolling.plist" = <3aedea32 0c7da844 024ebe13 c7f91945 bbefed46>;
        "960x640/flash/bullet_rolling.png" = <0ea98f6b e9ea10e4 a6603b12 58567632 884c4ffe>;
        "960x640/flash/bullet_rolling.xml" = <61911065 f83186bf f3fabdd1 1a8a43bf acc0e95b>;
        "960x640/flash/bullet_shoulijjian.plist" = <7fef9be3 61af8e6a e646c57f f9800b09 828eb098>;
        "960x640/flash/bullet_shoulijjian.png" = <61a220d6 3f630f80 80e9ade1 a29d95c5 2356e387>;
        "960x640/flash/bullet_shoulijjian.xml" = <4994e5c3 3c8222cf fea7e37f f30eb7a8 7bd11a36>;
        "960x640/flash/choose_go.plist" = <298437c8 66a28367 f7475934 1a108e6b ead6898b>;
        "960x640/flash/choose_go.png" = <2e13ddb4 b4f1bdf1 daf33d7c 173aa89c a182a6c7>;
        "960x640/flash/choose_go.xml" = <e780edd1 e22fab0e 4369b2ba 6a78be82 408463e6>;
        "960x640/flash/cristal.plist" = <9e4e347b dc93f51c a59a06c4 2a19d575 7f9e725e>;
        "960x640/flash/cristal.png" = <ce5a3532 25dc2e88 318e5afc 5649763d 024b116e>;
        "960x640/flash/cristal.xml" = <aee8c825 691e97bc 0516cb7a ec6f26a6 f985b4c2>;
        "960x640/flash/ef_bomb.plist" = <36b0601f 1cfcf8ec bb02cee0 726ec036 c3e86eb8>;
        "960x640/flash/ef_bomb.png" = <d4020399 fb2fcbcc c729e2d9 493c91ec e2fef128>;
        "960x640/flash/ef_bomb.xml" = <c30f6558 c2ac423d b8fd70c8 26c1fe88 22f2504c>;
        "960x640/flash/ef_bursts_1.plist" = <33b8c48e 28cd0134 05abf92a 1aac3aa2 64315f55>;
        "960x640/flash/ef_bursts_1.png" = <fb05293e 4d27d4bd 683eea0d 87679906 082ecbce>;
        "960x640/flash/ef_bursts_1.xml" = <ddd4e21b 1c391405 217c94f0 1652980a 6e3f165e>;
        "960x640/flash/ef_bursts_2.plist" = <d08f4757 673d5d34 2f770dc8 0bc40816 041d5d5f>;
        "960x640/flash/ef_bursts_2.png" = <7ec32519 9283101b c587f9fc 5fb51496 c9711d3e>;
        "960x640/flash/ef_bursts_2.xml" = <342512b5 e9165cf1 577d1875 6930f27f a3f5a716>;
        "960x640/flash/ef_bursts_3.plist" = <7f9b5980 04918d1e 9d6c5073 bd3db1cf 32d53b04>;
        "960x640/flash/ef_bursts_3.png" = <9cdd8361 d9626bfb 0ca98219 a35e9c32 d4310214>;
        "960x640/flash/ef_bursts_3.xml" = <97f4266e 54c97d12 d1cd2380 9af3b364 6b05ab39>;
        "960x640/flash/ef_cannon_1.plist" = <92c94e1e ffd4c809 a0dfed68 2d03ee58 044f20e5>;
        "960x640/flash/ef_cannon_1.png" = <a68096e8 f9c84b84 5b749e95 63f41875 94980695>;
        "960x640/flash/ef_cannon_1.xml" = <46d195b2 f7c8027a 318c9b08 3c7668ca e9d557f3>;
        "960x640/flash/ef_circle.plist" = <a065ed70 033baa63 d0720899 3aba3ee3 4d43201d>;
        "960x640/flash/ef_circle.png" = <bc43509c 871aa58e 3a4914e4 19f3fd1e 5f02f169>;
        "960x640/flash/ef_circle.xml" = <4e2026d7 7a0ba71c 4c08937e f875513e 069dfddb>;
        "960x640/flash/ef_fire_1.plist" = <1440bbb0 c8c4721e fff79f11 abf82d89 d56da486>;
        "960x640/flash/ef_fire_1.png" = <b1276f5a 347e0916 95a62960 d9a1a492 884bb0b1>;
        "960x640/flash/ef_fire_1.xml" = <4ec924d6 2d2f4586 0bbe838c 9f4f06ba fb7e68fb>;
        "960x640/flash/ef_fire_2.plist" = <9bcc9bcf c739587d 60c8776c 149a4480 fdc99eec>;
        "960x640/flash/ef_fire_2.png" = <c75e0414 5509dc37 6f484c19 428c0585 68a0b49d>;
        "960x640/flash/ef_fire_2.xml" = <a75a9b6f b6f9dde2 18664344 1a49873b bc1bd2e3>;
        "960x640/flash/ef_gas.plist" = <0c770386 65aaa379 714c8904 c62d4c78 7de023c5>;
        "960x640/flash/ef_gas.png" = <fae3ae21 7e5915a9 6e3ee3c1 51cb21c2 6ee7363a>;
        "960x640/flash/ef_gas.xml" = <98a4adc1 dae20239 a18893a3 b4444116 e1b83820>;
        "960x640/flash/ef_harpoon_1.plist" = <6e3fa060 fdcfc3c9 a9262aff e2b14711 14369158>;
        "960x640/flash/ef_harpoon_1.png" = <f543c03c 7a6d495b d3793f78 d3ae77b4 0ccaf62c>;
        "960x640/flash/ef_harpoon_1.xml" = <98ede7dc 6026d840 917275a3 d401052b 006b2771>;
        "960x640/flash/ef_lighting.plist" = <b3d4c7da 532144c5 cc0e7757 5b9e5058 6bdee171>;
        "960x640/flash/ef_lighting.png" = <d438cda6 aa3ab1ca b8a7f511 83106373 0394523a>;
        "960x640/flash/ef_lighting.xml" = <e8e960d8 7d471efe 848bca39 cb882cf7 49ef51b8>;
        "960x640/flash/ef_lighting_1.plist" = <68e974e8 6ba18ef5 8ccf8116 46d0148e b0cb410f>;
        "960x640/flash/ef_lighting_1.png" = <8d572503 c94e2a16 7ddff16c 1fdc302d 3c9dc4cc>;
        "960x640/flash/ef_lighting_1.xml" = <a0c6d43c 102c69b6 a7792fbb bb8387b9 b7a82f56>;
        "960x640/flash/ef_moonlight.plist" = <6b55ac36 706ec66d 26ef48b2 a0bf7805 0e09da5c>;
        "960x640/flash/ef_moonlight.png" = <be0902bf 89d9495e 697c8219 6b32a48f aaed11f5>;
        "960x640/flash/ef_moonlight.xml" = <b7b46301 d2a04d89 fd0c484f 6a0320cf 0b52c0bc>;
        "960x640/flash/ef_projectile_1.plist" = <e22360c6 c1451d45 04e6da1c fa481a2b 28d31d0b>;
        "960x640/flash/ef_projectile_1.png" = <63ce4b02 66cacd22 4bea5646 b1486d38 ee732719>;
        "960x640/flash/ef_projectile_1.xml" = <e4db2bed fd6d1bca 878c972f 7634beaf 840a5b15>;
        "960x640/flash/ef_rolling.plist" = <4c7ba2b8 53e49263 481b0bc4 fc796316 fc344114>;
        "960x640/flash/ef_rolling.png" = <a0cdb40e 61b40ace 1417d60c ea1529b0 b66e035c>;
        "960x640/flash/ef_rolling.xml" = <56dba59a 7eb02ab3 367dfcf7 ee2769d9 973e923c>;
        "960x640/flash/finger.plist" = <5cd506a7 83a7334d abede1ba c0aa3c44 6a7466e9>;
        "960x640/flash/finger.png" = <7de5f813 2a6b1265 09a63f60 21b75629 a1d71c1d>;
        "960x640/flash/finger.xml" = <7f89307d b24651dc 2051b374 267afb14 79d4ad00>;
        "960x640/flash/firefish.plist" = <d2b73f91 fbac7791 47643a48 a046c552 f1643e00>;
        "960x640/flash/firefish.png" = <871cc7aa a44dd240 e171bbff 31075f1f 99b879f0>;
        "960x640/flash/firefish.xml" = <3a4d8bf1 525c9a43 d7a3c9b6 d86bde2c b1482c31>;
        "960x640/flash/fish_dead.plist" = <7543385c 693d07c4 45e9e97c 26312c19 f4206baf>;
        "960x640/flash/fish_dead.png" = <cafea2b0 54ba2bc7 73b1ef25 878489f6 125c2bc1>;
        "960x640/flash/fish_dead.xml" = <a9ba1bbf efc3f907 ac0969b0 5dcedd0f 7b86f3da>;
        "960x640/flash/fish_dead_1.plist" = <58854d04 472526f0 88b0ff7f 33fa0acb 0f2bafda>;
        "960x640/flash/fish_dead_1.png" = <5710183b 725d95a8 d607e90b 3bfe3169 f1bb1d91>;
        "960x640/flash/fish_dead_1.xml" = <7b6719bc e1c6e407 46007a82 bde24d71 5c63ceb2>;
        "960x640/flash/fish_dead_2.plist" = <ec1c696d 32d6d494 56ee83ca 94271c1e 10046ec2>;
        "960x640/flash/fish_dead_2.png" = <9b3ff4af 3583717f 54994d3c b7498b8c 7f0fd168>;
        "960x640/flash/fish_dead_2.xml" = <8649ef25 5d5fe0f5 cac76ca0 129d8780 0c8fa4d3>;
        "960x640/flash/fort.plist" = <7107648f 60e65284 847e0afd a2c5a3d6 8f268dab>;
        "960x640/flash/fort.png" = <ecb668c3 c1e8ad99 f2c028d3 f4497dbe c64d964d>;
        "960x640/flash/fort.xml" = <f95dc4db 7e5878a9 53cc9d41 caf89e1f e32559a4>;
        "960x640/flash/frozen.plist" = <fb9b58e8 ae4734a1 6a00ab19 0d5898be 1cf6eeae>;
        "960x640/flash/frozen.png" = <fdc4c325 6c623a8f 8efe03f7 07edd9d1 6e3f26dc>;
        "960x640/flash/frozen.xml" = <a4081048 79375602 5b42be1e 2494e5e8 ceea8481>;
        "960x640/flash/guessfinger_bubble.plist" = <a68fcf4c a4a689b6 68729572 2678903e 88dd001d>;
        "960x640/flash/guessfinger_bubble.png" = <c70a0119 73159c29 10bc5441 ed3fd3ce f60cd7c7>;
        "960x640/flash/guessfinger_bubble.xml" = <71f19ff3 8467c0fc c3389682 557c42dc 7aca6260>;
        "960x640/flash/hetun_explode.plist" = <3ea3c3f4 af43a48c 6898caa2 a6242258 887afc46>;
        "960x640/flash/hetun_explode.png" = <d52fbc9b 29052795 7448386c 195d68f8 2a9fbd3f>;
        "960x640/flash/hetun_explode.xml" = <77d3e442 baf3a029 3daa516f b621fdcd 95189823>;
        "960x640/flash/huangjinpao.plist" = <6874fd0a b753b3d8 f815e0ff dce02116 1d37be50>;
        "960x640/flash/huangjinpao.png" = <877a6043 a7efe34d 4e180485 f69bcc7d b4e2640e>;
        "960x640/flash/huangjinpao.xml" = <cd22dbcd 92698d9c 49d6dc6d 22452963 6a65d071>;
        "960x640/flash/huangjinpaodan.plist" = <93ae8b1c a951b6b3 dd566a44 b6e945cb 0b7a6eda>;
        "960x640/flash/huangjinpaodan.png" = <44e4fbb7 e77c14b7 460e0198 84845833 5412e907>;
        "960x640/flash/huangjinpaodan.xml" = <20e241db 5c947789 8715b8b1 6f3f5e0e 14f26978>;
        "960x640/flash/jinbi.plist" = <d30009ee 318834ae edb6716e f44210ff 30f6281d>;
        "960x640/flash/jinbi.png" = <bff1cb7b a54d41c5 8cf76e81 0cc20a5d 087d9a64>;
        "960x640/flash/jinbi.xml" = <5e3cc831 f8391aa4 3149150c c1c0d071 feddea7a>;
        "960x640/flash/jinbi_1.plist" = <73ca559f acabb869 0e1022bd bd44287e 15c733b6>;
        "960x640/flash/jinbi_1.png" = <bff1cb7b a54d41c5 8cf76e81 0cc20a5d 087d9a64>;
        "960x640/flash/jinbi_1.xml" = <499f4540 c95fd593 c6fe0c82 9b3e0b0d 3733820d>;
        "960x640/flash/lahuji_4.plist" = <c5fffe38 f3954b78 5d8b8ef8 2fb93f89 ddb3dd02>;
        "960x640/flash/lahuji_4.png" = <7e777174 e6e048ab c5265065 ffaefb74 7dfae3d2>;
        "960x640/flash/lahuji_4.xml" = <49c17b0d af7d95ac 766e8f9e df08c863 5ccf479a>;
        "960x640/flash/laohuji_jinbi_1.plist" = <6953f5e1 6d94b7d8 e3651f58 4da2818a 10a68f4c>;
        "960x640/flash/laohuji_jinbi_1.png" = <c464c2d6 8cb2661a d8431117 8838c2e8 4a5c45da>;
        "960x640/flash/laohuji_jinbi_1.xml" = <43073170 8643e6b0 cf385a7d 3d48ad1f 6fb166d7>;
        "960x640/flash/laohuji_jinbi_2.plist" = <5d5011c3 7e9e0157 d5893da7 e9c06ccf 178da99d>;
        "960x640/flash/laohuji_jinbi_2.png" = <ef8e175d de9bc286 70fa61a2 810ee5df aa94ec48>;
        "960x640/flash/laohuji_jinbi_2.xml" = <955b225a 8ef304c9 79f6f820 7f217a1e 7faff638>;
        "960x640/flash/laohuji_shoubing.plist" = <a550df01 d5af0a85 b6267be5 7962ca18 5e90bff9>;
        "960x640/flash/laohuji_shoubing.png" = <20b258fd 23be0abd a8ab67bd 1c4df301 3cca8eec>;
        "960x640/flash/laohuji_shoubing.xml" = <de841419 52521999 bfea3ca4 deec5c9f 57a9ae86>;
        "960x640/flash/levelup.plist" = <504c5c4f abc0abd0 6ed6c022 5768ac9a fcd9d9bc>;
        "960x640/flash/levelup.png" = <3f336774 7b7911dc 4a2af0f9 adf4c393 20fc6fa6>;
        "960x640/flash/levelup.xml" = <7cdf694d d19219b4 79ce57bb cbe017f6 a6e80648>;
        "960x640/flash/light_background.plist" = <da79dd09 5c35726e 992e059c 24184efd a034f850>;
        "960x640/flash/light_background.png" = <51f27688 ad0166ba 58c07541 d0bc4151 74c825f3>;
        "960x640/flash/light_background.xml" = <c6606f0d 6ffb6027 233ae927 5b3661b7 fcc1e965>;
        "960x640/flash/lightening.plist" = <68acb773 2edca92d a26441b9 092da3cb ca3ef43f>;
        "960x640/flash/lightening.png" = <070d1176 3832f130 256e7d68 97685f3a 9f89dcf2>;
        "960x640/flash/lightening.xml" = <48fc2294 57cea6df 6394581c d6ed9d7a e523c2ce>;
        "960x640/flash/mission_text.plist" = <7feccd43 18020d80 35ac94a9 2ae00374 50d26c5d>;
        "960x640/flash/mission_text.png" = <b07117ec 8219b150 a3be21f4 11735bfa f35e0ee7>;
        "960x640/flash/mission_text.xml" = <dc048914 37dc6d19 a6e483ed 7f2782d4 33330d9b>;
        "960x640/flash/mission_text_1.plist" = <34a2feef d58dba0c 53dbf2c4 bec8097a c13d9ef9>;
        "960x640/flash/mission_text_1.png" = <ad0b2bd2 06fa6742 2d482105 9c6421bf 329148af>;
        "960x640/flash/mission_text_1.xml" = <1d8ab118 027c303d c24ae8e5 c691fae3 81081ee2>;
        "960x640/flash/net.plist" = <8851d525 18c1d590 efc7353d 67e4f71c 97874de4>;
        "960x640/flash/net.png" = <1b388251 e150f9b1 4830630a 2d07fab0 6707f8f4>;
        "960x640/flash/net.xml" = <4059b746 36771f5b 0863183a eb59836a 3c9d9a7c>;
        "960x640/flash/new_record.plist" = <07ecdfbd 9442eec0 3607d6f2 a65a1478 736a4808>;
        "960x640/flash/new_record.png" = <f0ebaf18 0f068c1b d849e0eb 271c800c 78466613>;
        "960x640/flash/new_record.xml" = <21b4daab f9716211 20b784d1 e8b51e9e b67c9d5d>;
        "960x640/flash/pao_1.plist" = <8c1b4d6d 7097764e c7e79d2b 821e449f 3aa7b08f>;
        "960x640/flash/pao_1.png" = <c00282f3 115ca4eb bda2d2f4 2ea83435 13249feb>;
        "960x640/flash/pao_1.xml" = <3aae4524 b29a1794 0e02f065 59385234 872e8031>;
        "960x640/flash/pao_2.plist" = <3129ad64 860324ab 3fee1bbc 98eba4a8 37a349ca>;
        "960x640/flash/pao_2.png" = <726f7d9d e3a01d7e ec249c13 b37ea516 3dd98604>;
        "960x640/flash/pao_2.xml" = <7c2f93b2 072508dc 9a4305bd 60acac3d 2a6f607b>;
        "960x640/flash/pao_3.plist" = <ee3a07d5 7cf764ad edb92bb7 a3a82925 6a2f6a82>;
        "960x640/flash/pao_3.png" = <ac4a5fac 4d35aeeb b26a43bd 17cfda19 dcee74bf>;
        "960x640/flash/pao_3.xml" = <3180f743 52694b5a dbcefc4b 9580e92e 2825f71e>;
        "960x640/flash/radar.plist" = <6f7152cf dab8457e 99401d08 b0a6eefa e44efa9b>;
        "960x640/flash/radar.png" = <fe2d663f 196b86bb 49482816 4765563b 894290ca>;
        "960x640/flash/radar.xml" = <f504a397 36234034 488a23e3 3b115ef0 62da3522>;
        "960x640/flash/reward_get.plist" = <690099d8 ae99b846 337309cc 4b2f05b2 c96551aa>;
        "960x640/flash/reward_get.png" = <b017a8aa c3ce4c6a 8f0c824c 71397ba7 0d067e0d>;
        "960x640/flash/reward_get.xml" = <71dd530f 24ee22d3 ddee9ed5 5acb749e 64e68b9f>;
        "960x640/flash/shayuyayouxi_3.plist" = <ac5dc1e0 eeceb610 c5f9eef6 69eb6fcd d0d211ec>;
        "960x640/flash/shayuyayouxi_3.png" = <9d78868a f9792901 063c03bd 368d4217 971bb133>;
        "960x640/flash/shayuyayouxi_3.xml" = <2e50587c 26f7123e ee034bb0 a5ebde70 accba5ff>;
        "960x640/flash/slot_3.plist" = <d9f4b492 4c93a147 058bb994 8fcad6d7 599ee385>;
        "960x640/flash/slot_3.png" = <5d1b2d8b ae533753 f6a7a835 bab7d405 6db67c54>;
        "960x640/flash/slot_3.xml" = <e1cd3714 b9ab5fab 98817130 35731402 e9734d6e>;
        "960x640/flash/slot_arrow.plist" = <cfde1c23 8a96ec90 8488d7fd 66154d51 50ddd975>;
        "960x640/flash/slot_arrow.png" = <3f9905a4 3da0beb8 14cf169c 3e1a3862 b98b0fcc>;
        "960x640/flash/slot_arrow.xml" = <69c00121 642beb84 82178911 80d87b3e d35cb290>;
        "960x640/flash/slot_bg_light.plist" = <ed513cb0 8c8d608a 744f7f00 6570117a e235426e>;
        "960x640/flash/slot_bg_light.png" = <9cfb70ff e51eb39f 1da44050 502d6e97 bf80dced>;
        "960x640/flash/slot_bg_light.xml" = <afaf550b 10db4ea5 e0babcd9 b014705f bcdbcc77>;
        "960x640/flash/slot_light1.plist" = <e02ac01f 7ba8d349 8f8b71b6 3f18ae32 b75ff9b0>;
        "960x640/flash/slot_light1.png" = <7911dcdf 4cd29d09 20582976 9d331b88 00ba3879>;
        "960x640/flash/slot_light1.xml" = <feb8c7be cd62b36f 7c5a1ce6 deecfe82 e93c5de5>;
        "960x640/flash/slot_light2.plist" = <5d93a434 b7b40a75 831d23e4 ee274cc1 589214eb>;
        "960x640/flash/slot_light2.png" = <31ba94a2 a4502a7e c66ee14f e2fcc61b c634a570>;
        "960x640/flash/slot_light2.xml" = <5beda8da 44ec7c0e 02847d0d 47b7a684 6785c04b>;
        "960x640/flash/slot_light3.plist" = <e65a17b5 7c873b3d 40c7a8e8 f0636aa4 b6ea8c45>;
        "960x640/flash/slot_light3.png" = <2ecd2bb5 91e27eb1 25b31555 ea995cd2 b1d05b29>;
        "960x640/flash/slot_light3.xml" = <1c3ca0ab 3821581c e6f07c52 4a267885 1448db68>;
        "960x640/flash/star.plist" = <1fe74893 4949ae93 7d6185a3 f1fde442 1476cf5c>;
        "960x640/flash/star.png" = <a1b4d67c 585e52de 6f627823 243dd8bb ebaa717a>;
        "960x640/flash/star.xml" = <43e8f697 a1d0ae8e e8685d14 0576c8af 0b3ededf>;
        "960x640/flash/submarine1.plist" = <30f2af32 6423145b a4cfdd87 b69c5e01 44272a6d>;
        "960x640/flash/submarine1.png" = <9f9e4dd2 f55ecea9 beebca07 f3ab1acc 6a2c257f>;
        "960x640/flash/submarine1.xml" = <2cf6f2df bc9f7bb9 902607c6 ff039ab0 42dd6048>;
        "960x640/flash/submarine2.plist" = <e4388e8f fc5f483f 6ce80f7b 3dc7cca4 ddc6d57c>;
        "960x640/flash/submarine2.png" = <d33eb1fd 3e06c702 8e924b4a df1d5887 9e619952>;
        "960x640/flash/submarine2.xml" = <3428684c dc76325b 44bff60c 68b98e81 a934c6e8>;
        "960x640/flash/submarine3.plist" = <2c7b5eaf 1a050756 c1b7e0b4 77ec6bec b6be8da8>;
        "960x640/flash/submarine3.png" = <d65a858f 0d222476 5ef7613e 6d373f80 2f3af8c2>;
        "960x640/flash/submarine3.xml" = <e0f4afe9 3f123978 ae2d0cfa 60890290 9cecf07c>;
        "960x640/flash/submarine4.plist" = <719a329c 4957588f 9693ed7b 17ef33ca 190b0de9>;
        "960x640/flash/submarine4.png" = <06c09799 e280d78a 25b513e2 972a1c5a 3cc0a2ee>;
        "960x640/flash/submarine4.xml" = <f9510e48 14c16292 8cc25f1d a86ab6af 9f520260>;
        "960x640/flash/submarine_button_effect.plist" = <d71c4db6 cc4ee2dd 61a2297e 147e7e84 cfdecb23>;
        "960x640/flash/submarine_button_effect.png" = <1ec78087 55a77755 8184599b 9074fc13 d2d4d9aa>;
        "960x640/flash/submarine_button_effect.xml" = <65387b68 8c170d41 207eddf5 3783857c e4cac0aa>;
        "960x640/flash/tanhao_1.plist" = <22bbf9c8 798331c1 a62673c9 64073224 e3657b26>;
        "960x640/flash/tanhao_1.png" = <33f9ef77 f0239826 cba4c271 e05cf72d 44b147c9>;
        "960x640/flash/tanhao_1.xml" = <43f3b410 725213a3 b3288f51 039c6dfe 52de1f05>;
        "960x640/flash/tanhao_2.plist" = <289600c2 ecef1317 a0b23608 6f21e6a1 75af2fd1>;
        "960x640/flash/tanhao_2.png" = <0be123d1 dafc110a dc1363c4 cdeea260 5130c15f>;
        "960x640/flash/tanhao_2.xml" = <5acd88bc 57c3c6d2 96dad048 c8598bdc d96a3a90>;
        "960x640/flash/target.plist" = <8907190c 612da7bd f3475542 dbf5f4ea 59cfe689>;
        "960x640/flash/target.png" = <93c2aac1 c8838776 764a4ac1 f02eed3e d47fbedb>;
        "960x640/flash/target.xml" = <36db1d17 3da3b3a3 c14130ea deb459ea 1a421e85>;
        "960x640/flash/target_2.plist" = <45a87934 d746b282 03796e6e 15e8eb3f 3589e0e7>;
        "960x640/flash/target_2.png" = <39629e6a 1a74a892 07f06acc 6ecae79f df6e0a76>;
        "960x640/flash/target_2.xml" = <98d82320 97e4273f 3cb860b7 fb681cc0 0f790549>;
        "960x640/flash/treasure.plist" = <38b22202 94429169 160898cd 1c27c687 0402212f>;
        "960x640/flash/treasure.png" = <f1eded04 0cb14c1c a753b17c 0e6a5078 b73a408f>;
        "960x640/flash/treasure.xml" = <e572fc82 23bed3ce 3e98c417 f2740e68 63c92fbf>;
        "960x640/flash/vip.plist" = <eee5184c b5ea8c70 fd5fed84 c6bd7e13 b5f7bbb7>;
        "960x640/flash/vip.png" = <30262973 307b9589 d57aec9d 97dbf7a2 9dcdefda>;
        "960x640/flash/vip.xml" = <de33269e c50d1167 e73139ec 5ae30cd8 5ed1b1dd>;
        "960x640/image/angle.png" = <dc9bc1e8 d2f8e9b6 bfe7039f e1caee75 bc24429f>;
        "960x640/image/bar_light_move.png" = <3bcdd734 15f1f3c1 c12c477e 23df4a80 6c6297a6>;
        "960x640/image/bar_light_stop.png" = <a04a251e 715d17d7 61998b04 f12af412 63d34f26>;
        "960x640/image/blood.png" = <51a26ce2 5a53b4bc a3d66933 e0bed71a a157f8b1>;
        "960x640/image/card_fly.png" = <e51372da d337198c a725c753 9b278a04 a494f236>;
        "960x640/image/effect_res_gold1_003.png" = <7bee64b5 8e5da287 7bc7f48e 6c8791cf 1fe22578>;
        "960x640/image/event_weapon_flash.png" = <d8ba682c 6209c32a 41693b77 2556a8d3 e2d7a5f6>;
        "960x640/image/event_weapon_light.png" = <b47496f5 7b9db22b 1295ad4d 5d0ccec0 cc9169cf>;
        "960x640/image/fever.png" = <ccc207a6 47b0432e 774b5fc5 e27d392e 8e8986ed>;
        "960x640/image/fever_blue.png" = <0473ea0d bfd40a3b 547314af 8a059067 c977c54b>;
        "960x640/image/fever_light2.png" = <a080f534 c2b0596c 508c5290 e736df1e cc63e3a4>;
        "960x640/image/fever_light3.png" = <b1fb5cee d4d45f0f da9586c2 48956741 a74e271b>;
        "960x640/image/fever_line.png" = <83a66c2e d5fac771 2f3e9922 35e554b1 6c2eb1e2>;
        "960x640/image/feverbar_ready1.png" = <88279f3c 6f91a7dc 2467a772 4c906df3 51074e37>;
        "960x640/image/feverbar_ready2.png" = <b1b54e27 642edaf8 9990435d 7a5dd9fa 49b88077>;
        "960x640/image/feverbar_ready3.png" = <52ee6d52 787b1314 215e6737 1c2e18e5 7be818eb>;
        "960x640/image/jingbiguang.png" = <37cb4deb d4079198 fd5478b2 ba6f3fd6 95bd78b8>;
        "960x640/image/landscape.png" = <2fd862ef cf75f4bb 2083f901 46905ea1 b32b9697>;
        "960x640/image/mission_text_denglongyu.png" = <29551800 42c658d7 53a53270 3f8f1566 dc089227>;
        "960x640/image/mission_text_gameover.png" = <e1d58cc6 1c69066f c9bf020f b2220814 d502f1a3>;
        "960x640/image/mission_text_goldenshark.png" = <bb429cc0 d634dd5d 41c28fe2 deb78db5 cf09ff0e>;
        "960x640/image/mission_text_guessfinger.png" = <db7ff4a2 f836b53c fddca1cb f88b8d59 dd08f1b7>;
        "960x640/image/mission_text_hetun.png" = <ab9a5b0a aae50391 63ce55a6 bfbaf003 6fe9730c>;
        "960x640/image/mission_text_sharktooth.png" = <c83e5607 6ae1c967 2d2915fb 99b40b9a 9d1b4a73>;
        "960x640/image/mission_text_treasure.png" = <2c954229 f6695295 fb1390b8 f9797cf1 02d963a0>;
        "960x640/image/mission_text_yuchao.png" = <0877e180 5c0dbddd 0fcf293d 37f1f6a1 72cf013a>;
        "960x640/image/qiaoya.png" = <828278d1 739ab5ed c57f780c cf2ec6e4 1d746515>;
        "960x640/image/rado.png" = <dd52ebda ebcf2c01 524121b0 3838acd5 3f10285f>;
        "960x640/image/shark_chest.png" = <c1ed0510 298237f1 c5e4bae9 61ca9a19 b4df3de1>;
        "960x640/image/sprite_pos.png" = <edbbddf8 d99123a5 cccac43b 7bd85121 6db4327d>;
        "960x640/image/swirl_light_4.png" = <1f50f9a7 11776bad 3b681595 0629b48f 762cfb4d>;
        "960x640/image/timer_blue.png" = <fa58f082 0cb2b0f2 5fa90901 ed9fcead ca13d16a>;
        "960x640/image/timer_red.png" = <fafdc9ae c4d45e45 227938b6 794b50bd 737b8985>;
        "960x640/image/topbombred.png" = <559c3343 9de56345 35d5b861 95257552 35730d01>;
        "960x640/image/treasure_light_1.png" = <679227e3 31f1c406 84013a12 ec926789 c41a1990>;
        "960x640/image/treasure_light_2.png" = <1235cee9 70d7c475 5ec39635 8ecc91f2 3e44582f>;
        "960x640/ui/achievement_icon_0.png" = <63da48ce e41a10dc b7e60483 d4301907 a072f74e>;
        "960x640/ui/achievement_icon_1.png" = <a3ee2329 65271956 7d8810bc f85aeaa9 576ab3bc>;
        "960x640/ui/achievement_icon_10.png" = <a7851654 a86e4ca6 c68e451a 5f9c579a 3cde66ee>;
        "960x640/ui/achievement_icon_11.png" = <8aa9eade c14208bb 9f225a9f 9ef01ee2 143f03e1>;
        "960x640/ui/achievement_icon_12.png" = <f44478e8 fd59ec58 a8ee45d2 dc3ff75e 3e4f2272>;
        "960x640/ui/achievement_icon_13.png" = <325aafd7 e27ad6ac 99ca3a60 338c4d20 65571bc1>;
        "960x640/ui/achievement_icon_14.png" = <d2180360 b168c42a 90556c19 78f3f87f 4318436f>;
        "960x640/ui/achievement_icon_15.png" = <67432cbc 8e677873 60a0eea8 16fad0c9 8391fc27>;
        "960x640/ui/achievement_icon_16.png" = <c0e7c106 0f07261b 541484bf fa388fd7 d2ca2370>;
        "960x640/ui/achievement_icon_17.png" = <4392901e 225ca0f3 74393771 7ff66869 c0723444>;
        "960x640/ui/achievement_icon_18.png" = <38c8cd5f b7850a46 447166db eb4df14f ad9d6c62>;
        "960x640/ui/achievement_icon_19.png" = <f4904ea1 547aa191 fe6e2e91 44aa1531 7f6c680a>;
        "960x640/ui/achievement_icon_2.png" = <26f9bb98 bef9686b 4c795263 cecc7abb b1d5ffac>;
        "960x640/ui/achievement_icon_20.png" = <a73179b7 ee123c09 24395269 a53d7aee bc7e941d>;
        "960x640/ui/achievement_icon_21.png" = <d6b7bd60 9a25b147 6261de95 a43cc053 800448f7>;
        "960x640/ui/achievement_icon_22.png" = <33ee67a2 9408e0cf 96dd2780 11e1058b 0e826b0f>;
        "960x640/ui/achievement_icon_23.png" = <1de20284 170acd8a 2df08e90 2b538ae5 2ceaeb34>;
        "960x640/ui/achievement_icon_24.png" = <fec2a133 f6af1072 7e284704 d0a9b37a 282399ec>;
        "960x640/ui/achievement_icon_25.png" = <145d67e7 498de623 b07ef8eb 8aef4dfa 77fd7504>;
        "960x640/ui/achievement_icon_26.png" = <6a877cf4 4fa9ddf5 10c6e3ac 8ab0305b 84628222>;
        "960x640/ui/achievement_icon_27.png" = <c28cf7b2 4ea2f249 cd60fe0a ffdc2c93 165c7a3b>;
        "960x640/ui/achievement_icon_28.png" = <a9238d6e 183f15d4 ccd63777 14f16de3 e53ffe5e>;
        "960x640/ui/achievement_icon_29.png" = <abeaae1a c74c3d2d 220bead6 06e38623 e496d536>;
        "960x640/ui/achievement_icon_3.png" = <515fd154 3552bab6 79a7f22e ecc15648 ee81cc5a>;
        "960x640/ui/achievement_icon_30.png" = <f6f5a266 823898e0 20a1f7bf 4f5b75d3 c49fb1a1>;
        "960x640/ui/achievement_icon_31.png" = <c6cdcef6 51f691fa 90efc8cf 862a9239 8e66ef9d>;
        "960x640/ui/achievement_icon_32.png" = <537c4935 66ac00d7 2e864ad5 c453e349 564780d4>;
        "960x640/ui/achievement_icon_33.png" = <2257a7f6 89aadc24 58c7bb09 d65fd45b 5ff1bbf3>;
        "960x640/ui/achievement_icon_34.png" = <340502ab 45541a75 96d7c06e f37fe9d3 a1394887>;
        "960x640/ui/achievement_icon_35.png" = <e07844ce 6e01ba30 dde63c52 4c2b8024 a3d838de>;
        "960x640/ui/achievement_icon_36.png" = <475c0f19 ca442adb c6468e15 024eb4bb 714ec875>;
        "960x640/ui/achievement_icon_37.png" = <1e197450 d0276f8e 4637ffff b49a8e9e d0deceb8>;
        "960x640/ui/achievement_icon_38.png" = <c7d4ea55 068e04e1 d113de47 a660507d 5f77114e>;
        "960x640/ui/achievement_icon_39.png" = <7106e245 caca8cdb c3dba51d 0b86a83d 0f0861e7>;
        "960x640/ui/achievement_icon_4.png" = <313dd6b8 c7d81169 17b4bfcc 78dd1612 071ae504>;
        "960x640/ui/achievement_icon_40.png" = <54b143b7 5053731e 6c0b7c61 ce80d58a a965f431>;
        "960x640/ui/achievement_icon_41.png" = <907e7815 003737f3 f2fe2db1 75b3a03f cf418daf>;
        "960x640/ui/achievement_icon_42.png" = <df35f384 f2b9f169 9fe310aa 54b3f34e 67a92d2d>;
        "960x640/ui/achievement_icon_43.png" = <245c6757 d0606d89 a427790a d70936ce f19180bc>;
        "960x640/ui/achievement_icon_44.png" = <ad23bcb6 f0a35ff5 dbddc997 6f8f2a28 16c22ba5>;
        "960x640/ui/achievement_icon_45.png" = <3297198c d2dbba2d 09874a30 5437251b 889d164f>;
        "960x640/ui/achievement_icon_46.png" = <24b1061a 805ab688 3db6fd57 da2b0531 3d33f70a>;
        "960x640/ui/achievement_icon_47.png" = <64c9a755 4cb6ceb8 ac61583e e85815b9 7dcafdb2>;
        "960x640/ui/achievement_icon_48.png" = <b748b231 eafc609e 4a9b30d1 819707ce 0c91e149>;
        "960x640/ui/achievement_icon_49.png" = <015ad283 de2a3da0 20c39d26 5a103437 979eebc7>;
        "960x640/ui/achievement_icon_5.png" = <8fb54281 f4269d55 a60dd010 be8bec85 8baa547d>;
        "960x640/ui/achievement_icon_50.png" = <e2fd0c2c 6ed876bd 3821aea2 f54bf9b4 e91e129b>;
        "960x640/ui/achievement_icon_51.png" = <58201b73 71332af1 f8e42ff1 2ad63bb4 26c97c85>;
        "960x640/ui/achievement_icon_52.png" = <5eeda6be 1c08c97e 502d29f3 73ece6b1 0216f833>;
        "960x640/ui/achievement_icon_53.png" = <ad76a4e3 07dc31a8 5ca4ddf9 633a2157 cb92fd3b>;
        "960x640/ui/achievement_icon_54.png" = <893e0706 f6d203a5 4a2eafac 057de9a0 4e7d0a93>;
        "960x640/ui/achievement_icon_55.png" = <ab80e1bc dcf41159 359a9eb7 9cca2b20 2715de69>;
        "960x640/ui/achievement_icon_56.png" = <1efca115 8bfe62a8 30eb435e 532b800f c7654296>;
        "960x640/ui/achievement_icon_57.png" = <6d3ef66d 17233e0a 855df7dc 404e7f14 6729957a>;
        "960x640/ui/achievement_icon_6.png" = <9e7196c6 fc56e23f 90b0d3c0 86618034 72fce61b>;
        "960x640/ui/achievement_icon_7.png" = <829368fe d676b454 a19a51c3 180d0363 5523aa39>;
        "960x640/ui/achievement_icon_8.png" = <4fd71376 be8b90ea 0d677388 d87eceb5 5d377b10>;
        "960x640/ui/achievement_icon_9.png" = <c6617aaf 23144541 b0df819b 2b84e534 2dad9526>;
        "960x640/ui/achievements.json" = <263d50f2 54c3dcb1 7309d5ca 9a90f83e 2898a066>;
        "960x640/ui/achievements.plist" = <2dad2f66 b73a7d7a abbe26e2 54a0d2bb 8847629a>;
        "960x640/ui/achievements.png" = <51597da7 2a7fe4c2 3eb732ed 06b18ba8 d29a0bcf>;
        "960x640/ui/activity.plist" = <80c44fe1 cbdeda38 2465a8fd 74ecfc7d ddbaf160>;
        "960x640/ui/activity.png" = <40c365c9 53747f84 20348796 90bdd799 49e531ca>;
        "960x640/ui/atlas/atlas_blue.png" = <74198635 2d83fbdc 13c88e8d 1b8cf679 40933b50>;
        "960x640/ui/atlas/atlas_depth_blue.png" = <78436ec7 94b66ea5 3be313ab ac72d945 f58d459e>;
        "960x640/ui/atlas/atlas_gold.png" = <952704a5 08764b4e edc510e9 f6a24f6b 88cd4776>;
        "960x640/ui/atlas/atlas_gray.png" = <df346fa8 377b7404 43b6b243 9b546c65 773f23e6>;
        "960x640/ui/atlas/atlas_green.png" = <d1417587 faeb763f 526c2e4f bc977170 6a45427d>;
        "960x640/ui/atlas/atlas_he.png" = <4087a2d5 a2ccbebd 11648b14 1937587c f685eabf>;
        "960x640/ui/atlas/atlas_number_grey_2.png" = <4395da4d d105f573 59cfb9ee b60f3b1c 3f35fde1>;
        "960x640/ui/atlas/atlas_number_vip.png" = <834c8102 0778fdd5 c355fc2d fcfa5975 180fc798>;
        "960x640/ui/atlas/atlas_white.png" = <39414815 e63a67e6 2007fd8e 79fbf8b7 b5207ee0>;
        "960x640/ui/atlas/atlas_yellow.png" = <cd9cec6c 1d4627ed 6eaff4c0 c29ff543 23d4635f>;
        "960x640/ui/button.plist" = <10eae06e 62cb8d80 e28c20ea 51f243e0 d3ce5f25>;
        "960x640/ui/button.png" = <92b05ab6 b2b31265 28b5a88a 0dd0c82e 805a6cd9>;
        "960x640/ui/card.json" = <6c1282a6 aeb6cfe5 a774712f 07828e66 b10fc8f5>;
        "960x640/ui/choose_new.json" = <e21bb610 617482c5 34a4733d 5b0e486a 069b3519>;
        "960x640/ui/choose_new.plist" = <0e023f39 641cad87 f28df777 335b8dcb 7f046a3e>;
        "960x640/ui/choose_new.png" = <a835ac17 e69d02db 01ef847e cb02d873 2c4bd508>;
        "960x640/ui/common_icon.plist" = <10778132 61e30ae3 c48385f0 d476ca7f 003052e4>;
        "960x640/ui/common_icon.png" = <668a450a 587dcf34 784b3039 3bd7c9f5 7780a709>;
        "960x640/ui/common_image.plist" = <0dc7b43c c41247d5 3f3257a1 c29d0530 344502f9>;
        "960x640/ui/common_image.png" = <ec993faf 31bddea3 7726ec15 8649e320 34d30557>;
        "960x640/ui/confirm.json" = <9c214eb3 b6868a7b 19b77cfe c301c03d 577bbf33>;
        "960x640/ui/confirm_phone_num.json" = <6863926a 4a7e40a9 36189856 29783ac6 2f554294>;
        "960x640/ui/confirm_win.json" = <c44f96f6 40448d2f 144f0176 c05db437 e91550fc>;
        "960x640/ui/contact.json" = <1c3d5eb4 4f40d74a 473bc133 9b963fc9 395ca3a4>;
        "960x640/ui/dachinko.json" = <8c4a3366 c4d2d401 4a049939 b162624b 1528b37b>;
        "960x640/ui/dachinko.plist" = <83fc74cb c7b8c96e 91ae482a 093364f4 cb5d2b89>;
        "960x640/ui/dachinko.png" = <a02d2cc1 bafcb9d9 e1fe52cf 2793f557 0e2b3683>;
        "960x640/ui/dachinko_howtoplay.json" = <b96bad8b 71b81b28 ed28c454 5120546a 8dfa3cb1>;
        "960x640/ui/debug_panel.json" = <5ac4de40 6cbe036d 20bd1828 4c6e6354 c0e3cb07>;
        "960x640/ui/debug_panel.plist" = <43196605 8f93a952 04b362d5 3540a9b1 0555c4b7>;
        "960x640/ui/debug_panel.png" = <6166e6d9 9017f8d2 4c57129f 3e8b687f 86d7861f>;
        "960x640/ui/draw_card.json" = <ea293aec c58fc29b 1c0ec900 6b684dfe abfa6a26>;
        "960x640/ui/exchange.json" = <2f68ab13 e40f9205 13903b95 925fc233 dbeb70a4>;
        "960x640/ui/fish_data.json" = <8f21fc44 2074b399 30eb1eb1 c25ab037 5f958205>;
        "960x640/ui/fishwiki.json" = <df656066 03b9c6ef 9c50ab52 efaf1f1a 7b03f245>;
        "960x640/ui/fishwiki_1.json" = <0c707119 f4335d38 2a0cc699 10722486 f792024d>;
        "960x640/ui/friend_reward.json" = <dedf4c04 5692aa76 9935212c a2e427c4 709e5695>;
        "960x640/ui/friend_weapon.json" = <3f922894 7d4d297e ed4dd585 7d8ba339 19a09919>;
        "960x640/ui/gold_tips.json" = <cf5f05aa 0221afe4 27720488 471a2413 c2ebd0db>;
        "960x640/ui/guess_finger.json" = <d62bc1e4 ffd76eb3 f784b138 dfa9a199 0f6898e1>;
        "960x640/ui/guess_finger.plist" = <4bb423e0 c4000602 bae15220 e3935280 365da6e7>;
        "960x640/ui/guess_finger.png" = <10817c0b c5597273 3d463ad4 f2189539 72c63b51>;
        "960x640/ui/hintinfo.json" = <bcd968f1 8cac6c16 9845321c 01d75b63 2d810ad3>;
        "960x640/ui/introduce.json" = <0dcd2df3 07b57b90 067793c4 ca71ac0a a775e6de>;
        "960x640/ui/item.json" = <54b47f35 713b3fdd 82f76361 e976607b 024cc6db>;
        "960x640/ui/item_final_reward.json" = <61c70705 fb19ab98 ae203d82 068eeb25 5d9cbc52>;
        "960x640/ui/item_signin.json" = <fc643145 736ced26 366989dc 05c159fc 1175a42d>;
        "960x640/ui/levelup_new.json" = <fb094ac2 c247313b 60d3b9a6 e6ddefcb 23db86f6>;
        "960x640/ui/levelup_new.plist" = <c2181f56 d1d6209a 5b6d3ba5 fb8f8d86 4567f180>;
        "960x640/ui/levelup_new.png" = <855bac19 ef4c972f 9c366622 b98c47ed a6123088>;
        "960x640/ui/login_mission.json" = <9321b6d8 732d6b88 0991afb5 eeeb41bd 1f2a3089>;
        "960x640/ui/login_reward.json" = <3e67d2bf 26c2f96b d4f21186 f5fad6df 3e857045>;
        "960x640/ui/main_screen.json" = <e36a685b 6758587f 5c9ad567 2fe732e8 84856b8d>;
        "960x640/ui/main_screen.plist" = <8dd0edf4 ed0a78c3 abca7efc ffc55489 3a551d63>;
        "960x640/ui/main_screen.png" = <1a35d3cc 9ea163c4 2172d682 dae54d7c 6b649d4d>;
        "960x640/ui/mission.json" = <23ca8daa e2505ffd a022a29f 266cd936 e182b53e>;
        "960x640/ui/mission.plist" = <9c1f126c 4aa2a28d defea52e 1df27c53 d10a3296>;
        "960x640/ui/mission.png" = <5ee61bdd 34de5356 f68e7580 aec41817 2c8fa1c8>;
        "960x640/ui/mission_anycannon.png" = <ccc4a128 3091d611 53837099 2da80848 b5e91c25>;
        "960x640/ui/mission_anyfish.png" = <085c0fb7 20ce745d 9c558b3c baff9a04 4104d76d>;
        "960x640/ui/mission_cannon.png" = <0786a8bc 3ae5a038 f04da8c7 6400692e fad12786>;
        "960x640/ui/mission_cards.png" = <c32d9742 d9d5f9ef bc0f7c95 7062f8d7 06ccfcf8>;
        "960x640/ui/mission_chouka.png" = <668a2715 6c7e9e0c 4f6c7547 36283e0e 6a2cd3dd>;
        "960x640/ui/mission_coin.png" = <7d1c1539 7cd03a11 5286f74e f1fc3e5a aef0ae26>;
        "960x640/ui/mission_cristal.png" = <4dd5e817 848a4527 cac4a12b 97e8fdaf 260a721a>;
        "960x640/ui/mission_earn.png" = <827aa003 f20cf1b0 4455b475 5e74b6b1 60a3a38b>;
        "960x640/ui/mission_evaluate.png" = <1474a3d3 d5b86b4c c945f2bf 6d236c99 6cf7b769>;
        "960x640/ui/mission_exchange.png" = <64f2372e 4c6b7bc2 88c775c8 790aeeab 79b03b3f>;
        "960x640/ui/mission_fever.png" = <041d8b1f 5592f819 8ef81030 079f5e72 f02ae133>;
        "960x640/ui/mission_fishing.png" = <8c2c089f 574c913d 4ee97a9d 4aad6785 b6fcc845>;
        "960x640/ui/mission_fishtide.png" = <d9cd44b4 44c7c8db 7fde8505 f8515978 0250d0be>;
        "960x640/ui/mission_laohuji.png" = <a4dcb2b6 b7c8fe50 ec3b983c c8376b6b 9425bc7c>;
        "960x640/ui/mission_mini.png" = <a63587bc f031ad22 ac82f5ff 8e37ee19 af1b2dee>;
        "960x640/ui/mission_pay.png" = <341a1997 d0c25ab6 508a171f 07772654 1b10d198>;
        "960x640/ui/mission_photo.png" = <672c1f5d c3cc175e 4036562a fbd00036 4bd6d871>;
        "960x640/ui/mission_rate.png" = <1fcb2322 5a98abf2 ab6fcb2d c5e8f197 9cb6c127>;
        "960x640/ui/mission_scene.png" = <2dbc3e96 98b28bc3 90ac8d9d fabb39b7 e5fea742>;
        "960x640/ui/namelist.json" = <6a5e95c4 5948c335 71a29a0d 6012ece2 8aa5b7ca>;
        "960x640/ui/notice.json" = <e5ffc795 6bc20d47 61f96799 7b20d85c dc24b62e>;
        "960x640/ui/notice_image_02.png" = <96f02f52 cccf1938 0026d8fc 1332b68f 52124d73>;
        "960x640/ui/notice_item.json" = <5032d602 2ef42ba2 9445608c add8aaf2 b122da8f>;
        "960x640/ui/offline_reward.json" = <94edca50 17a7e6a6 96683db6 41118fa5 5c8c136e>;
        "960x640/ui/payments.json" = <d82811da 048e223c 4ec1b059 086190e7 3eb85874>;
        "960x640/ui/rank_data.json" = <d6fec711 38cc902c dd1be9d7 429b224f bb1675a9>;
        "960x640/ui/ranking.json" = <8100176f 91a9b960 bfa81dc3 306a6f1b 3f4baab2>;
        "960x640/ui/result.json" = <5726a50f 5a08785d 1fc4cdf3 3a22097b 716aa1a6>;
        "960x640/ui/reward.plist" = <507dd611 d3f05698 4e42d2be 0b17cdab bfd768d8>;
        "960x640/ui/reward.png" = <c235f78f 0d95485d bf6c70b9 c0398f5c 522f96bb>;
        "960x640/ui/reward_confirm.json" = <6ea30944 2b198d0e 6dc5b302 62353177 11096239>;
        "960x640/ui/reward_icon_1.png" = <e651cd2b 26c5c88b 95237156 3d4a30c5 cd3afedf>;
        "960x640/ui/reward_icon_2.png" = <52cdcb31 f2e628c1 2a04216a 71bb5fa6 68cc3985>;
        "960x640/ui/reward_icon_3.png" = <42ef27db 233d9932 c60673b3 626fd2e5 4a3623f8>;
        "960x640/ui/reward_icon_4.png" = <9587096d 65198715 7e1e5c04 f78ea5b1 b3b1cd40>;
        "960x640/ui/reward_icon_5.png" = <10eb4e2d b9650076 b2d6fef8 9e100b4c 11c13be8>;
        "960x640/ui/reward_icon_6.png" = <52bddb43 ba5ac0ef 28e611f0 02dbb0e6 35b1c6ce>;
        "960x640/ui/reward_icon_7.png" = <dbc65b99 e701560e e8159fb3 db763731 91e2c938>;
        "960x640/ui/reward_tip.json" = <fa9c80e6 2c0094fc 44093b16 8dd68318 4464be7a>;
        "960x640/ui/sale.plist" = <e2b93965 0680a5dd cba633da 8140a02f d8ee10d7>;
        "960x640/ui/sale.png" = <1291fa47 6b16cb60 59e7d6dd cb038545 b9f62f73>;
        "960x640/ui/sale_icon.json" = <2fe3bf74 6a4c8160 0afc2bd2 c9b77edf 8c014f6b>;
        "960x640/ui/sale_layer_locked_weapon.json" = <c74901f1 b5e62920 07240076 540fbac6 14df95a5>;
        "960x640/ui/sale_layer_locked_weapon_confirm.json" = <1e9dff01 4223a7cc bb620301 1f73aabc 08089410>;
        "960x640/ui/sale_panel2.json" = <f9af96fc 74edcab4 5ab4bf30 4c31da0f 7059ce83>;
        "960x640/ui/secrectbox.json" = <9afccd95 9e56f0a7 77466ddd 652b7b4a a627fa48>;
        "960x640/ui/setting.json" = <87d884d1 1c4e676b 4fb0e392 e90e4367 085dd16d>;
        "960x640/ui/setting.plist" = <10150e6c d042a9a4 785a1399 8d5dc4c4 e717cd10>;
        "960x640/ui/setting.png" = <f2f712ce b860a271 58d70d68 de73a638 ee3d8c25>;
        "960x640/ui/share.json" = <dc90cae1 92e41e12 3a0012c3 229f34f6 06287e59>;
        "960x640/ui/shark_teeth.json" = <950f83c9 7cc4da6c 4992e225 a2f1307c d6b889e0>;
        "960x640/ui/show_all.json" = <e109ad22 c6259525 e61ea478 1e7f8ba5 b1782afb>;
        "960x640/ui/start.plist" = <bf6866a9 267f69a7 a5dc426d ce6303e0 6d156077>;
        "960x640/ui/start.png" = <83a68e24 43ef1547 c177d05c 6f6bac37 f88f1eb3>;
        "960x640/ui/start_logo.json" = <ef01cb23 d26494df 40b5ad61 8ddb54a2 59d0f053>;
        "960x640/ui/start_menu.json" = <96d6b17e e96645d0 910fffe3 1a04f496 30fb1c6b>;
        "960x640/ui/store.json" = <e3eb17d4 e8a6c918 3e7bbf83 2f96e695 471291e8>;
        "960x640/ui/store.plist" = <1cfe64db 103262fd d21b0d9a 7f69d9ca 5b662e8a>;
        "960x640/ui/store.png" = <eb5da798 d3f9a0ed 596d918d fc1ba740 f92eaa1d>;
        "960x640/ui/submarine.json" = <6981f5da 80ea92a1 50ddd53a cbd939fc 86ba7b1e>;
        "960x640/ui/submarine.plist" = <4e8409c7 f8627578 ec34465d ee2bf68e 11606458>;
        "960x640/ui/submarine.png" = <3e729d61 4233ccdd f29c3cf7 5bede690 a84e606f>;
        "960x640/ui/tip_arrow.json" = <b082979f ef0de989 a1ab4982 624b945a 07a1c3d8>;
        "960x640/ui/tip_confirm.json" = <34bdb02a a0ad5fe3 a83cabd6 11fec71c de54ab91>;
        "960x640/ui/tip_general.json" = <ee888461 cec0aabf c7cfef44 c570355c 6d54972e>;
        "960x640/ui/tip_gold.json" = <6aa90a6b 36d0ef50 a0efdc43 412dbdec ebf9957c>;
        "960x640/ui/tip_panel.json" = <6f8a3439 88e941da 5b376a94 50cc9c3c 0012c5e7>;
        "960x640/ui/tip_panel_guess.json" = <ad8f02d0 a150f578 2270986d eb6034b8 39fbba9a>;
        "960x640/ui/user_guide_choose.json" = <a8090de2 bc62d611 029fd3b6 eb5fb556 c40b1a26>;
        "960x640/ui/vip_power.json" = <bc216212 51c121dc 734f2c23 9bb46fdd ea4ea086>;
        "AppIcon29x29.png" = <5a3bb2d5 cea58e40 c8dadb5b 58f2ad06 7ca407b5>;
        "AppIcon40x40@2x.png" = <ba39ea7b ca805a6a 7e20b27b 72977e86 921a247d>;
        "AppIcon40x40@2x~ipad.png" = <ba39ea7b ca805a6a 7e20b27b 72977e86 921a247d>;
        "AppIcon40x40~ipad.png" = <90b84887 f65441f9 0db4e3a0 b2680128 6f2bfac2>;
        "AppIcon50x50@2x~ipad.png" = <5bc8923d de167558 80f38883 95c8ef99 735e38f0>;
        "AppIcon50x50~ipad.png" = <899d758a a62b1004 d4769c4b 1288412d 101d6821>;
        "AppIcon57x57.png" = <916ed0ad f7c27981 32d141f1 84688bad bb92d473>;
        "AppIcon57x57@2x.png" = <0d2192d1 0ad30611 ba6cd9c2 39c2ab3f a48c1785>;
        "AppIcon60x60@2x.png" = <81189d9d 44e0b49f a10fd389 2dbd0aca d54f268a>;
        "AppIcon72x72@2x~ipad.png" = <8e872e8b 1fdbf071 05efec7f b79f067f bb5e0722>;
        "AppIcon72x72~ipad.png" = <e1d223f0 eff4c624 b70e7fa7 f56339bf 7383d39b>;
        "AppIcon76x76@2x~ipad.png" = <f2d27219 7c0f423b aab121fc bac5c9a5 c4e09e82>;
        "AppIcon76x76~ipad.png" = <e9be2d81 9c7687c8 e1d487c4 43b26b80 a23e17ca>;
        "ChanceAdRes.bundle/Root.plist" = <5656c74f 50cb8281 22f60148 3d0e814b a6d983f6>;
        "ChanceAdRes.bundle/cs_browser_back@2x.png" = <e8d054f4 7b28a22a 1e6eb35a 4455fd9f 69e7a6d7>;
        "ChanceAdRes.bundle/cs_browser_back_disabled@2x.png" = <a6837738 8dbf8839 636ed57a 030f9235 7f884ff5>;
        "ChanceAdRes.bundle/cs_browser_btnbg_hl@2x.png" = <676b370a cb5be9d5 d401e538 f849bc6a fec9c686>;
        "ChanceAdRes.bundle/cs_browser_btnbg_nor@2x.png" = <619fa50a 65774209 1097da75 851576d4 d64cf3cf>;
        "ChanceAdRes.bundle/cs_browser_cancelfullscreen@2x.png" = <bebfa56f fd96ae67 4b8bccea 577f97a3 5fcc1f97>;
        "ChanceAdRes.bundle/cs_browser_close@2x.png" = <1f91a1b9 c8c2e974 ecd13886 b0b91e46 d686fec6>;
        "ChanceAdRes.bundle/cs_browser_forward@2x.png" = <2c1ee075 6aa1c8cb 9aea4905 26acce2c bdba6f12>;
        "ChanceAdRes.bundle/cs_browser_forward_disabled@2x.png" = <f4c8bd30 442c50cf 02598ce0 05582351 b148207e>;
        "ChanceAdRes.bundle/cs_browser_fullscreen@2x.png" = <287e7203 2666abe4 ececa037 e833b63e a0105562>;
        "ChanceAdRes.bundle/cs_browser_refresh@2x.png" = <e32e3ffb 8ab28c55 88eea9f6 ec0341eb 8e3ac5d7>;
        "ChanceAdRes.bundle/cs_browser_share@2x.png" = <cd5407e0 e292f41a d25adc4a c1187cb0 197e9831>;
        "ChanceAdRes.bundle/cs_cocobear@2x.png" = <3e636c9e 559cb03c 5a7f520b 82fb7046 5d558da0>;
        "ChanceAdRes.bundle/cs_loading_0@2x.png" = <f1ff6f14 9fa2e327 8547eddf d37fc1c6 137091f7>;
        "ChanceAdRes.bundle/cs_loading_1@2x.png" = <25e98723 fe7366ed faa15064 364e08ff 4f4616db>;
        "ChanceAdRes.bundle/cs_loading_2@2x.png" = <b6ef345a 190f0162 f1959a43 18f6ffe5 ef851ae8>;
        "ChanceAdRes.bundle/cs_loading_3@2x.png" = <8c526032 b0cfcbfd 4e493662 35a3d213 264a595a>;
        "ChanceAdRes.bundle/cs_loading_4@2x.png" = <b6ef345a 190f0162 f1959a43 18f6ffe5 ef851ae8>;
        "ChanceAdRes.bundle/cs_loading_5@2x.png" = <25e98723 fe7366ed faa15064 364e08ff 4f4616db>;
        "ChanceAdRes.bundle/cs_reload_d@2x.png" = <440f5d61 72467efb 8ae59178 6f838a96 b2dc2dea>;
        "ChanceAdRes.bundle/cs_reload_n@2x.png" = <a80e0c83 bb8f7cf3 a2833c72 76654028 ad3d57e6>;
        "ChanceAdRes.bundle/csi_btn_close_normal@2x.png" = <0da2edcc 819b4c87 826ddb35 cbc90740 6f7f898a>;
        "ChanceAdRes.bundle/csi_btn_close_normal_iPad@2x.png" = <cedb2386 39a92ef9 af89553e e24bc675 4ba55ec5>;
        "ChanceAdRes.bundle/csm_btn_recom0@2x.png" = <c844a0c5 b4be6d13 2c66c924 be1d9bfc 5fea5227>;
        "ChanceAdRes.bundle/csm_btn_recom0@2x~ipad.png" = <481858b8 aa6ff637 356076f5 51df152b 4c90b84c>;
        "ChanceAdRes.bundle/csm_btn_recom10@2x.png" = <b793669e a70a29d8 57145cc0 b243fa35 bf68dc5f>;
        "ChanceAdRes.bundle/csm_btn_recom10@2x~ipad.png" = <b37a1fbd 4dd8588a ac05c24e c6c86c9b f2af0440>;
        "ChanceAdRes.bundle/csm_btn_recom11@2x.png" = <dab19677 41001a0a 7a94a7d2 a9b6a896 8dd45a1b>;
        "ChanceAdRes.bundle/csm_btn_recom11@2x~ipad.png" = <c3f61aca 588bb928 700d50e7 e68d8768 b463f59b>;
        "ChanceAdRes.bundle/csm_btn_recom1@2x.png" = <3697b9b7 f40b23bd 0e120530 19b27a64 a9224b35>;
        "ChanceAdRes.bundle/csm_btn_recom1@2x~ipad.png" = <287a8959 14ac8110 e609cbf4 ed0cf87c d2092b36>;
        "ChanceAdRes.bundle/csm_btn_recom2@2x.png" = <62544e88 b532522d c7e392a1 743160d3 80f27254>;
        "ChanceAdRes.bundle/csm_btn_recom2@2x~ipad.png" = <5e16a91e 5c668f44 d9ad62be 9e0bb881 5096f0dd>;
        "ChanceAdRes.bundle/csm_btn_recom3@2x.png" = <fbeb8bcc f6e1b374 9fb77f41 f85fa3de 88899efa>;
        "ChanceAdRes.bundle/csm_btn_recom3@2x~ipad.png" = <e689c255 7105213a 96ae7598 8ec189c8 03b972df>;
        "ChanceAdRes.bundle/csm_btn_recom4@2x.png" = <75780b03 90fb3939 770bcc75 97b25969 e98fe73f>;
        "ChanceAdRes.bundle/csm_btn_recom4@2x~ipad.png" = <bba5e652 efdec782 427f2be6 186fc539 64cd131a>;
        "ChanceAdRes.bundle/csm_btn_recom5@2x.png" = <621ec429 950b1a07 75120f7b 53cef79f 0050bcb6>;
        "ChanceAdRes.bundle/csm_btn_recom5@2x~ipad.png" = <9354eef2 26141f33 de83fe41 ef2e0592 183eb505>;
        "ChanceAdRes.bundle/csm_btn_recom6@2x.png" = <2d002ed9 3c912920 fe08b3de fa9194bb 93fa131b>;
        "ChanceAdRes.bundle/csm_btn_recom6@2x~ipad.png" = <7108717f 72eac824 3a933bef 47691fa5 ef49c405>;
        "ChanceAdRes.bundle/csm_btn_recom7@2x.png" = <a74f5666 c3484567 66bfe224 521064a2 c4ccdaad>;
        "ChanceAdRes.bundle/csm_btn_recom7@2x~ipad.png" = <56cd25c1 c7f54fff 46de6787 faf5e768 e6885b56>;
        "ChanceAdRes.bundle/csm_btn_recom8@2x.png" = <45b6d198 0b9cdd59 1391c5e2 259bc061 0ef682b9>;
        "ChanceAdRes.bundle/csm_btn_recom8@2x~ipad.png" = <efef7355 2ad54d92 ebcb8e61 3a471634 6698ba91>;
        "ChanceAdRes.bundle/csm_btn_recom9@2x.png" = <21118dfe 5cbd5e66 4dd0bd3e ac669d82 b26d3da1>;
        "ChanceAdRes.bundle/csm_btn_recom9@2x~ipad.png" = <3bc628a5 30416dcb 3eb38f72 250f7428 14b80dcc>;
        "ChanceAdRes.bundle/csnm_arrow@2x.png" = <c885b08e 6aaa22e0 06b08470 b0140fbe 153f923b>;
        "ChanceAdRes.bundle/csnm_focusView_default@2x.png" = <078a13ec c28b0620 35a2f58f 6a93187a 97ab5aa3>;
        "ChanceAdRes.bundle/csnm_nav_back@2x.png" = <d5ddd224 f164a41a 5610d50b 9c66efa6 f3358a34>;
        "ChanceAdRes.bundle/csnm_nav_back_ipad@2x.png" = <e74cbbea 6eb6d885 3efac2e2 931f2931 8784e2e3>;
        "ChanceAdRes.bundle/csnm_nav_back_sel@2x.png" = <e439d864 15aadde9 38ce5765 73700b28 5c0ddb87>;
        "ChanceAdRes.bundle/csnm_nav_back_sel_ipad@2x.png" = <406b6249 abad2a32 5bd33dc5 23ded3ab 82fe7aec>;
        "ChanceAdRes.bundle/csnm_nav_coco@2x.png" = <20fc7489 63eff479 2d38a47f 39b72539 32dc9c35>;
        "ChanceAdRes.bundle/csnm_sectionView_clickMore@2x.png" = <0a8b8ddd bd87096c 409886e3 11f6f99f fcae18ed>;
        "ChanceAdRes.bundle/csnm_sectionView_more@2x.png" = <0b7cd058 bc62008d 34b9ad58 153db4bc 984bed21>;
        "ChanceAdRes.bundle/csnm_sectionView_more_ipad@2x.png" = <52c0a720 ba51c7a4 9b2409ca 4bca06fa b3c7b125>;
        "ChanceAdRes.bundle/csnm_sectionView_more_sel@2x.png" = <dea437b7 170bf17e b65cbbb1 c9ed26fd 40694af7>;
        "ChanceAdRes.bundle/csnm_sectionView_more_sel_ipad@2x.png" = <d6d0d9d5 71b461ee 72877a3e 11a3cc3c b3e396e0>;
        "ChanceAdRes.bundle/csnm_singleView_charge@2x.png" = <366e163d 4e3cbcf2 41736050 ad1a58df 2b26f39e>;
        "ChanceAdRes.bundle/csnm_singleView_charge_ipad@2x.png" = <033d73f0 d31a9aae 314b375e 82c6dcd7 4daf233b>;
        "ChanceAdRes.bundle/csnm_singleView_charge_sel@2x.png" = <f1c79c27 5bfc4242 a460b4c4 d6829461 2278e5bc>;
        "ChanceAdRes.bundle/csnm_singleView_charge_sel_ipad@2x.png" = <a03d68ec bf4dd5c3 0c78ac13 67f92fdc 708280e9>;
        "ChanceAdRes.bundle/csnm_singleView_free@2x.png" = <d6297aa9 0d69eeb3 14b25ad3 2ccd3e69 3160327c>;
        "ChanceAdRes.bundle/csnm_singleView_free_ipad@2x.png" = <78e808b3 3e1fca45 8fbbb5b4 974322ab 887dc6f8>;
        "ChanceAdRes.bundle/csnm_singleView_free_sel@2x.png" = <a48950fc 2051484e 0f6b7dea 02acc324 e9427c5a>;
        "ChanceAdRes.bundle/csnm_singleView_free_sel_ipad@2x.png" = <c8fd73c2 3579df68 6db61dbf 503576b9 d9ee8df4>;
        "ChanceAdRes.bundle/csnm_singleView_iconDefault@2x.png" = <9184f47a a3abee19 da518a48 3e636949 1b78529e>;
        "ChanceAdRes.bundle/csnm_singleView_iconDefault_ipad@2x.png" = <c16c905d f66e6807 c52aa6ec 5e933ec5 e8c4e9bb>;
        "ChanceAdRes.bundle/csnm_singleView_icon_leftConer@2x.png" = <2cfcdc0f 16ed88f8 3c0d9d78 a1bf26e4 344b04a6>;
        "ChanceAdRes.bundle/csnm_singleView_icon_leftConer_ipad@2x.png" = <28ba57d4 3fb2a23b 5a243e4d 5126a381 e155560e>;
        "ChanceAdRes.bundle/csnm_singleView_limitFree@2x.png" = <ef6af672 235742c1 97bb1222 b2c2a27b c0d294f8>;
        "ChanceAdRes.bundle/csnm_singleView_limitFree_ipad@2x.png" = <e4c6c3c4 ba242615 3c495e88 4e6a24ca 002bb5dc>;
        "ChanceAdRes.bundle/csnm_singleView_limitFree_sel@2x.png" = <59c7fa46 9d849d87 9ab16843 757ae52a 65e90b24>;
        "ChanceAdRes.bundle/csnm_singleView_limitFree_sel_ipad@2x.png" = <7f2f87e6 dc49b467 eba85f2d 28fc5d69 8fd1d093>;
        "ChanceAdRes.bundle/csnmg_appinfofolder@2x.png" = <dd1aed88 7f10c1f6 10dab88a 4ff76126 c2a58192>;
        "ChanceAdRes.bundle/csnmg_appinfofolder_ipad@2x.png" = <bf89c380 3a5116b6 f61e53dd c10cbabf 6609bbf4>;
        "ChanceAdRes.bundle/csnmg_appinfoicon@2x.png" = <1ac44d14 bb88f503 bb874099 583b9f3a 31e1d54f>;
        "ChanceAdRes.bundle/csnmg_downloadapp@2x.png" = <ba7f993d a91f6cb2 0f654376 c51ca38e 7a582952>;
        "ChanceAdRes.bundle/en.lproj/Root.strings" =         {
            hash = <b78298f4 6878b888 8ebfb567 1ed46b28 4b8f458f>;
            optional = 1;
        };
        "FishingJoy3_iphone_zh_cn.plist" = <d53a1d35 dd7b7f5c 5e136264 571236cb e21cb731>;
        "Info.plist" = <b33dd471 03e2f483 68e6501b ed890660 5b1d8ae4>;
        "LaunchImage-568h@2x.png" = <cb9776fc a17794eb e8648f91 74d1ebd2 92492d33>;
        "LaunchImage.png" = <65da0ee6 2d8c033e 7e392667 e4adef75 a2562534>;
        "LaunchImage@2x.png" = <12191829 142ccd84 98af384f d768d3a2 5380ccb3>;
        PkgInfo = <9f9eea0c fe2d65f2 c3d6b092 e375b407 82d08f31>;
        "Resource.bundle/ActionSheet/ActionSheetBG.png" = <92391720 89c6d49e 490c1e70 0eaea273 68471e35>;
        "Resource.bundle/ActionSheet/ActionSheetBG@2x.png" = <2aaa5a1d 02689eda 854bd9b5 72c4b12c ef1a83fc>;
        "Resource.bundle/ActionSheet/ActionSheetCancelButtonBG.png" = <5b05bf52 c4adc3eb 6893fc10 1fc0b5cf 7c070d1a>;
        "Resource.bundle/ActionSheet/ActionSheetCancelButtonBG@2x.png" = <4ed91d4b 8262b5fb 394ac91e 3c5ddd3a 56a76240>;
        "Resource.bundle/ActionSheet/action-black-button-selected.png" = <c5991c7f e98091d3 07a8d03a 1b6d016c 3758e700>;
        "Resource.bundle/ActionSheet/action-black-button-selected@2x.png" = <33a1e3cf 23f21a70 2fea1c66 06843749 d218bef4>;
        "Resource.bundle/ActionSheet/action-black-button.png" = <c3236bb3 28194266 5b48bf0c a2e9135f 7ac98f6d>;
        "Resource.bundle/ActionSheet/action-black-button@2x.png" = <6ba4ee35 17bfacb3 70a7a7dd d8795489 a05944c4>;
        "Resource.bundle/ActionSheet/action-gray-button-selected.png" = <5905b128 80b38b3f 0b3a7390 a19a6036 bd038bfe>;
        "Resource.bundle/ActionSheet/action-gray-button-selected@2x.png" = <98a27241 5979e1ca a4a7c7e3 ac19b9a3 36675825>;
        "Resource.bundle/ActionSheet/action-gray-button.png" = <ae6db988 10152173 22d01849 9e2fa9db 896e358f>;
        "Resource.bundle/ActionSheet/action-gray-button@2x.png" = <89715b1b 05fab80c 8065e8b6 01a8c8e3 35630570>;
        "Resource.bundle/ActionSheet/action-red-button-selected.png" = <3ebd12f5 4a37b236 1060296d ecc3f682 2b0cc7bd>;
        "Resource.bundle/ActionSheet/action-red-button-selected@2x.png" = <8e6e401b 20045ad4 3975e466 f27ad371 94a22ad6>;
        "Resource.bundle/ActionSheet/action-red-button.png" = <a51a6916 72c371c9 6b057e8c a9d686ce 09814017>;
        "Resource.bundle/ActionSheet/action-red-button@2x.png" = <af26cb56 3aab1514 15cec4b2 7065abea 09f8516a>;
        "Resource.bundle/ActionSheet/action-sheet-panel.png" = <e9c8c82f 6d296baf 83f94a28 6412cdd1 b2d61cb4>;
        "Resource.bundle/ActionSheet/action-sheet-panel@2x.png" = <310309c3 df6d30c3 966f9f26 ba8d620d d792fe5c>;
        "Resource.bundle/AuthView/CheckBoxSelectedSkin.png" = <0b429194 6fc46756 c2177d9f 471041b0 9abfffc4>;
        "Resource.bundle/AuthView/CheckBoxSelectedSkin@2x.png" = <cebbabe1 ad6d40c9 06d5cd91 0a27f96b 69c1b463>;
        "Resource.bundle/AuthView/CheckBoxUpSkin.png" = <757d5580 51f6efb2 c075cd57 bd61c61f e35e84fe>;
        "Resource.bundle/AuthView/CheckBoxUpSkin@2x.png" = <90c16741 1c6056fd 64c258ef f0624181 6fd17a16>;
        "Resource.bundle/AuthView/CloseButton.png" = <67474cdd 8aa03353 c4e2ad00 8790125d 6908c2af>;
        "Resource.bundle/AuthView/CloseButton@2x.png" = <ab1aa879 e939fd92 f2b49351 f510cc1d eeee9256>;
        "Resource.bundle/AuthView/NavigationBG.png" = <2d8ae6ad 6bc04f24 6acec297 49d88bf8 081a2c7e>;
        "Resource.bundle/AuthView/NavigationBG@2x.png" = <67045a2e 87db3ed4 e4a44908 9602bae5 9b893c36>;
        "Resource.bundle/AuthView/PowerBy.png" = <832746cb 4f032143 ec766dfc 6f5aab19 eb7e4ffb>;
        "Resource.bundle/AuthView/PowerBy@2x.png" = <6285e480 bde77451 a7f2825b d0ca9033 53d35dd1>;
        "Resource.bundle/AuthView/ToolbarBG.png" = <bb09d254 adc17b1e 7f46dbf1 484d930b d93f6d9f>;
        "Resource.bundle/AuthView/ToolbarBG@2x.png" = <5ee6c42f a4a298d5 c782e2a2 29c06169 f29806fb>;
        "Resource.bundle/Common/AuthViewCloseButton.png" = <35a8e5df 2aec3f0b 35052239 d0590556 f91b8010>;
        "Resource.bundle/Common/AuthViewCloseButton@2x.png" = <36779356 fe74001c f5432bfa 87269d12 2259b237>;
        "Resource.bundle/Common/NavigationButtonBG.png" = <7e3bdb84 ac6b33af 4e0ee380 f278bb25 2f000a83>;
        "Resource.bundle/Common/NavigationButtonBG@2x.png" = <50db03b0 127c41ab 8c8ada34 844309d2 87002884>;
        "Resource.bundle/Common_Landscape/NavigationButtonBG.png" = <cd9189e2 9902dfac 8fb024dd 4465be5c 8bb99856>;
        "Resource.bundle/Common_Landscape/NavigationButtonBG@2x.png" = <da8b5051 a0adee40 b21b5893 8e157cc8 ae767ca4>;
        "Resource.bundle/Icon/sns_icon_1.png" = <e29f4975 376ea644 1bff0b05 f67e9926 e0a62a3b>;
        "Resource.bundle/Icon/sns_icon_10.png" = <0dc67a95 83b86604 6a29728b 0ccc22d4 e6d4d236>;
        "Resource.bundle/Icon/sns_icon_10@2x.png" = <fe29d435 89281a5c b7b3cdb3 937220f2 c84ec51d>;
        "Resource.bundle/Icon/sns_icon_10_s.png" = <ed97397d 5426642b cc0a4638 8fb8c57e b572c352>;
        "Resource.bundle/Icon/sns_icon_10_s@2x.png" = <a54a9bfd 4b5eae23 96575702 8e74d2ce 768ddc87>;
        "Resource.bundle/Icon/sns_icon_11.png" = <2e024fb9 56437612 c19433a6 0df06080 756c278c>;
        "Resource.bundle/Icon/sns_icon_11@2x.png" = <af95bc5c f776a5bb e53518e5 76aca815 13642a29>;
        "Resource.bundle/Icon/sns_icon_11_s.png" = <2d4cbed4 61c59c52 91690191 f32e3b94 f2300a3e>;
        "Resource.bundle/Icon/sns_icon_11_s@2x.png" = <b5e1e2c6 0624c221 95326553 b0a90413 cd38fde0>;
        "Resource.bundle/Icon/sns_icon_12.png" = <82d30020 7fd16f80 0ab830bc 9d6b328a b3977dfa>;
        "Resource.bundle/Icon/sns_icon_12@2x.png" = <26c0326c 6e8311d5 c978daa4 89c3d4c1 a6aa8661>;
        "Resource.bundle/Icon/sns_icon_13.png" = <3515eaf8 25775149 afda6b1b 7ea07fc4 41db997c>;
        "Resource.bundle/Icon/sns_icon_13@2x.png" = <559a47d2 ed54cf7c cef1b4f0 3799ad42 ea3f1bec>;
        "Resource.bundle/Icon/sns_icon_14.png" = <e3cb0922 392061bf 292ded84 48d2d70c fa9d8a90>;
        "Resource.bundle/Icon/sns_icon_14@2x.png" = <f1bb159e 7862d7d7 d4b0f42c 0bfca0db 320127ea>;
        "Resource.bundle/Icon/sns_icon_15.png" = <9dcc100a 4a0c8ef7 9ed90672 1e2440ff 5f24ac66>;
        "Resource.bundle/Icon/sns_icon_15@2x.png" = <cab2847c 245518a5 3610ae00 b476b9b1 bbecd158>;
        "Resource.bundle/Icon/sns_icon_16.png" = <a432d0d0 d72ff694 5d3baeb5 3959fece 895d01fa>;
        "Resource.bundle/Icon/sns_icon_16@2x.png" = <c29d2609 81b281cb 48f1e4ac 86495124 b77fd04b>;
        "Resource.bundle/Icon/sns_icon_17.png" = <7b5dfd4c 5183a095 84655aa7 dde85b50 413e762a>;
        "Resource.bundle/Icon/sns_icon_17@2x.png" = <d9cf2c7a 2a61616b bf3aa2e2 42e7dd74 21f44f06>;
        "Resource.bundle/Icon/sns_icon_18.png" = <015f9a9a 372e6871 782ce8bf 14786db5 ef2bf9d9>;
        "Resource.bundle/Icon/sns_icon_18@2x.png" = <15e79663 2320bb22 5f47768d 839a9437 289fe2bf>;
        "Resource.bundle/Icon/sns_icon_19.png" = <c085d04d 897a92bd 70d60a0e 1ec46b41 a9faa102>;
        "Resource.bundle/Icon/sns_icon_19@2x.png" = <38a1b573 7b94f09f 223b0ef4 47af4822 bc43a0ee>;
        "Resource.bundle/Icon/sns_icon_1@2x.png" = <237a7fed 901fe283 cc87d25b 3f1601c5 edb8d583>;
        "Resource.bundle/Icon/sns_icon_1_s.png" = <de1d7a87 f89ec4fc 609e3f62 98040d70 59f3b855>;
        "Resource.bundle/Icon/sns_icon_1_s@2x.png" = <409d38c9 37f1cfb6 3bb6f10d f7e8a9c8 ea09feeb>;
        "Resource.bundle/Icon/sns_icon_2.png" = <5f591cee 1c7e5e87 c4dda98d 4c313155 0e7c502e>;
        "Resource.bundle/Icon/sns_icon_20.png" = <e1175ef6 3586b7f4 647afb15 45a9814f b64b4073>;
        "Resource.bundle/Icon/sns_icon_20@2x.png" = <3f343068 423033e8 f46634ce 3ff9dfdb f7277b6e>;
        "Resource.bundle/Icon/sns_icon_21.png" = <09d9213f af25511b ba3966b7 a2fee75c a0e77666>;
        "Resource.bundle/Icon/sns_icon_21@2x.png" = <e271dee1 5d007032 8cc9ce12 3bfbeddb 44753b10>;
        "Resource.bundle/Icon/sns_icon_22.png" = <6d8d8dc7 42535b3b 3ee60d1e 659307b7 4c8303f1>;
        "Resource.bundle/Icon/sns_icon_22@2x.png" = <addb98aa ba1028cb 7ba147bd 138e50d0 817680d9>;
        "Resource.bundle/Icon/sns_icon_23.png" = <93478f8e 20327b38 bae1e04d 326ccafa d5e91229>;
        "Resource.bundle/Icon/sns_icon_23@2x.png" = <2313debe dcf0597c bb8fef48 6f8a571a b85832e9>;
        "Resource.bundle/Icon/sns_icon_24.png" = <5357e722 0e1692b1 196c03ff 38cc6d4f ebb0e3c0>;
        "Resource.bundle/Icon/sns_icon_24@2x.png" = <a976b463 50156a2d 006e93aa 5bcca8ba c28c0058>;
        "Resource.bundle/Icon/sns_icon_25.png" = <0e01c4ed 10f52922 048edbe6 434c738a 51b86831>;
        "Resource.bundle/Icon/sns_icon_25@2x.png" = <eb815066 fa664946 8cc7a255 a1c5dbca 79f8b9f4>;
        "Resource.bundle/Icon/sns_icon_26.png" = <7f917614 9dcafe35 f88b8d3c 86de8e1a 5e6afeba>;
        "Resource.bundle/Icon/sns_icon_26@2x.png" = <680ab78d eb4eb169 9cbe1645 39f26eee f7c7497b>;
        "Resource.bundle/Icon/sns_icon_27.png" = <e1d23d7b e051d595 95d2aa82 04c969d2 e0b2b1f0>;
        "Resource.bundle/Icon/sns_icon_27@2x.png" = <a46c9e6f 5a0a9c2d e1f2e1c4 866e42c2 716b3f7a>;
        "Resource.bundle/Icon/sns_icon_28.png" = <4c437d78 6d1f6c6c 5c1c7b7f 85269702 1da665e8>;
        "Resource.bundle/Icon/sns_icon_28@2x.png" = <b249d93f 81df3e0b de48744b 4eeb6d2c 1045136a>;
        "Resource.bundle/Icon/sns_icon_28_s.png" = <ea2ff5aa 79051120 feab1b4e ec9713dc da2dcc5b>;
        "Resource.bundle/Icon/sns_icon_28_s@2x.png" = <34077fba ef2d8013 a5186f1a 6547dfcd b1069fb0>;
        "Resource.bundle/Icon/sns_icon_2@2x.png" = <ec0dc479 2d96adc2 2265c085 f5e3350f 6afb7867>;
        "Resource.bundle/Icon/sns_icon_2_s.png" = <6dfa0879 ea3f3ab8 ff986717 8d38dd77 03512d43>;
        "Resource.bundle/Icon/sns_icon_2_s@2x.png" = <73418567 49e2ba88 e7397a69 4005764f 8debd7d2>;
        "Resource.bundle/Icon/sns_icon_3.png" = <b8219899 0c1218f6 356ccb5f d6ff1199 3f67bcab>;
        "Resource.bundle/Icon/sns_icon_30.png" = <b1f131a0 84bcbe71 432afe59 18bb56da 37b7df28>;
        "Resource.bundle/Icon/sns_icon_30@2x.png" = <437b815f 7b7ef1e8 6342ecd3 8e40419f 66ae696f>;
        "Resource.bundle/Icon/sns_icon_34.png" = <c8209104 050836b9 767782b1 5eb8b618 5eb584d9>;
        "Resource.bundle/Icon/sns_icon_34@2x.png" = <0842d8f0 9eae71d0 5e705314 71064473 8aad79bd>;
        "Resource.bundle/Icon/sns_icon_35.png" = <d250b316 6026ae67 4af6bd6c a4f60ff1 854c846c>;
        "Resource.bundle/Icon/sns_icon_35@2x.png" = <0131e00e d2b8e2e5 e20ee0c1 d0928c67 7516327b>;
        "Resource.bundle/Icon/sns_icon_36.png" = <9a83ca9d 43778880 c8696ea4 ca85a8a4 e295cb23>;
        "Resource.bundle/Icon/sns_icon_36@2x.png" = <11cfef8a 7155db59 3223f3ed 45a1d05a 45f3a62c>;
        "Resource.bundle/Icon/sns_icon_37.png" = <5f0c2fba c8003cb3 60f16424 7df4ae30 23c24d2b>;
        "Resource.bundle/Icon/sns_icon_37@2x.png" = <0bdc50e0 2b1c7adc 98310083 30633b2d 3ed1207c>;
        "Resource.bundle/Icon/sns_icon_38.png" = <91428389 85b675e6 552aba49 977588d2 ddbd4a08>;
        "Resource.bundle/Icon/sns_icon_38@2x.png" = <b463b649 ff29620f 59bb4695 bbd28e93 6804c45c>;
        "Resource.bundle/Icon/sns_icon_39.png" = <6796beac 1bab1073 ad20d521 ad3673c9 cd2f16c8>;
        "Resource.bundle/Icon/sns_icon_39@2x.png" = <057c8be2 852817c1 9b9e317b e818d682 95f1131c>;
        "Resource.bundle/Icon/sns_icon_3@2x.png" = <bcf23ea8 bad5c1b4 4c211bfc a276eae4 5c7b09a1>;
        "Resource.bundle/Icon/sns_icon_3_s.png" = <8436eeb1 4b8c5e41 f634ee66 2ba85239 6c6d630c>;
        "Resource.bundle/Icon/sns_icon_3_s@2x.png" = <0559a6d1 44406f85 50864876 9190cf30 37c84781>;
        "Resource.bundle/Icon/sns_icon_4.png" = <3c2a3e03 cd56fa1a 08d0c806 b42156a1 0c5c5ed2>;
        "Resource.bundle/Icon/sns_icon_41.png" = <796846d7 dca086d3 6d62cb2c bf53235d 877acbe4>;
        "Resource.bundle/Icon/sns_icon_41@2x.png" = <affcb792 72658580 f3c0a282 30954d89 ae82debc>;
        "Resource.bundle/Icon/sns_icon_42.png" = <259bd815 44f4e54a 7f21a220 58ad9f96 65d1cec2>;
        "Resource.bundle/Icon/sns_icon_42@2x.png" = <a16772da 05ea2ae2 213c2e99 3c628732 23d879ae>;
        "Resource.bundle/Icon/sns_icon_43.png" = <360efccf b71f132c 109894ca 61a2b8a2 84d1c890>;
        "Resource.bundle/Icon/sns_icon_43@2x.png" = <f42a23e0 78f4b60e 5117e55c 38649b33 dbd37460>;
        "Resource.bundle/Icon/sns_icon_44.png" = <0c9ccd03 9e35c80f 09f1cccd 255972d2 bf97dd40>;
        "Resource.bundle/Icon/sns_icon_44@2x.png" = <9a204c0a 719c821e 6d969448 13c56bc9 d338696a>;
        "Resource.bundle/Icon/sns_icon_45.png" = <42a4abc2 ee3c9c98 59b2009b 0d6e47d5 ecce65bd>;
        "Resource.bundle/Icon/sns_icon_45@2x.png" = <56c8a003 54a7644b 2ab4da83 cc2838f6 f2631038>;
        "Resource.bundle/Icon/sns_icon_4@2x.png" = <9f591520 9bf84918 2e44e7c9 f6d1d5d6 04be2991>;
        "Resource.bundle/Icon/sns_icon_4_s.png" = <30d77296 4f9254f3 6c3e3268 e66a0c92 7c1bb6b2>;
        "Resource.bundle/Icon/sns_icon_4_s@2x.png" = <39fe5214 aee80718 347e0a78 35cb77a5 89b131a8>;
        "Resource.bundle/Icon/sns_icon_5.png" = <7af14e7a 3f840ad3 f83654e8 d7d8c802 bd828f09>;
        "Resource.bundle/Icon/sns_icon_5@2x.png" = <0b75a42f c94bd0a9 014b8c62 d111bb4e b2f60b82>;
        "Resource.bundle/Icon/sns_icon_5_s.png" = <6a4b7c0e 61a94dcf c71aedcc 91d5c8d9 8da5cafc>;
        "Resource.bundle/Icon/sns_icon_5_s@2x.png" = <611c4fe9 3ceb02d1 fa7d234c 77d3c7aa 99722bb3>;
        "Resource.bundle/Icon/sns_icon_6.png" = <bbc7a0c2 6f6512c2 f76a8c18 02ff53b1 4f1dc6f1>;
        "Resource.bundle/Icon/sns_icon_6@2x.png" = <3693db7f 8eb6f987 8e6b956a 04e3247a b1d58d24>;
        "Resource.bundle/Icon/sns_icon_6_s.png" = <86baff92 ee2ff1fa c686bbda 8b508f3b 500bbd83>;
        "Resource.bundle/Icon/sns_icon_6_s@2x.png" = <647e548a ee5eaf72 16465da4 52cf5e45 64a0a330>;
        "Resource.bundle/Icon/sns_icon_7.png" = <7d2d8adf 120696d0 b745a85f a3519960 34b43c7e>;
        "Resource.bundle/Icon/sns_icon_7@2x.png" = <81ae8fd0 05cf0209 450abb20 0ad7155d 625025de>;
        "Resource.bundle/Icon/sns_icon_7_s.png" = <b43cfbfb 820dbf4f 70535810 b5c4ebe2 92e66d87>;
        "Resource.bundle/Icon/sns_icon_7_s@2x.png" = <064cfcea 5ced40aa 03c7703d 0f93dea9 836c434d>;
        "Resource.bundle/Icon/sns_icon_8.png" = <f80b6348 67d45b65 93b580a9 c8834b4e 9f5c32dd>;
        "Resource.bundle/Icon/sns_icon_8@2x.png" = <717d7716 ee69c262 f1e3e59a 4605c689 67cd90f9>;
        "Resource.bundle/Icon/sns_icon_8_s.png" = <8c786e81 8120a662 f758e9db 28ecc4aa 3611c1a6>;
        "Resource.bundle/Icon/sns_icon_8_s@2x.png" = <8c2846ed ef8e5db0 465f8c95 dfe9ded9 e8950cf0>;
        "Resource.bundle/Icon/sns_icon_9.png" = <5edc4c7d a510ca99 ab029236 77ca1b4a f34f6ab0>;
        "Resource.bundle/Icon/sns_icon_9@2x.png" = <4464c9f1 d1d23d9e 7bcdbda7 f5702c37 7f29547d>;
        "Resource.bundle/Icon_7/sns_icon_1.png" = <63967996 437b10e4 46f12d58 2a9ef92a 497cf95e>;
        "Resource.bundle/Icon_7/sns_icon_10.png" = <d8ba816d 5d619d96 69aa5aff c607ed26 cb6cf7f9>;
        "Resource.bundle/Icon_7/sns_icon_10@2x.png" = <b9db7112 b3c7ad80 fa975ad6 0980ea66 6074de5c>;
        "Resource.bundle/Icon_7/sns_icon_11.png" = <c51bf185 4af8e12d 7bc7df96 a0f95d3d 923a9655>;
        "Resource.bundle/Icon_7/sns_icon_11@2x.png" = <37e12f02 952f5935 285cc7a2 7de0a03d 593e9fd4>;
        "Resource.bundle/Icon_7/sns_icon_12.png" = <410aef5a 015fe302 4bfd227b 86fafe8c 57a128c9>;
        "Resource.bundle/Icon_7/sns_icon_12@2x.png" = <6f7d7760 b9873fab 234158a5 dfe84b4f e4d344ee>;
        "Resource.bundle/Icon_7/sns_icon_13.png" = <ad76d6bd 172bd5fd 899a516d 46e308a9 47f133f3>;
        "Resource.bundle/Icon_7/sns_icon_13@2x.png" = <523d6cfb 7aeb382b 9f98fa89 f9a3ff8d 0f5f8e41>;
        "Resource.bundle/Icon_7/sns_icon_14.png" = <51ce9de2 b25b1265 c8559964 9c3d0c1e 71038d27>;
        "Resource.bundle/Icon_7/sns_icon_14@2x.png" = <6cbc77b5 18b0687c ae948d36 a8bfcde9 138dc348>;
        "Resource.bundle/Icon_7/sns_icon_15.png" = <b93fae16 03e65907 d44851f0 e6c288ec b0732a05>;
        "Resource.bundle/Icon_7/sns_icon_15@2x.png" = <4073ad96 edc7d6ab 5bb2b462 0b5c0166 6a83b03f>;
        "Resource.bundle/Icon_7/sns_icon_16.png" = <ae3ce6a8 fe709315 0335c132 f61415c7 b416bf18>;
        "Resource.bundle/Icon_7/sns_icon_16@2x.png" = <c66ca192 bb51825e 65ba9a64 2e07de96 732681c0>;
        "Resource.bundle/Icon_7/sns_icon_17.png" = <2806dbb9 459ada48 04bc4c66 ca85887e 1b48cbd4>;
        "Resource.bundle/Icon_7/sns_icon_17@2x.png" = <eca1d0dd 1a6226ee dbcf5d02 195f1cfb aad8f5be>;
        "Resource.bundle/Icon_7/sns_icon_18.png" = <467e04d8 5948c838 59fa2351 9cecdad1 32246321>;
        "Resource.bundle/Icon_7/sns_icon_18@2x.png" = <6c1e63d4 dcd09d79 c5052320 8f81a261 9469c78d>;
        "Resource.bundle/Icon_7/sns_icon_19.png" = <c12442d7 39346591 f4b1fbba bd11651d 00d9db5f>;
        "Resource.bundle/Icon_7/sns_icon_19@2x.png" = <049eb988 25d43a8f 852ee855 0049d5b9 99320509>;
        "Resource.bundle/Icon_7/sns_icon_1@2x.png" = <5cc9260f 230252de cf95e88e e633aa62 1ff321b1>;
        "Resource.bundle/Icon_7/sns_icon_2.png" = <e4d0d06b f22d96f6 c97c73da 3901f57d 353f6c14>;
        "Resource.bundle/Icon_7/sns_icon_20.png" = <0e5bbf1c 1163aec0 2496910e 5528d149 a36e5686>;
        "Resource.bundle/Icon_7/sns_icon_20@2x.png" = <0b365aee f8d0cae9 f6374bd1 379e3354 57037ac9>;
        "Resource.bundle/Icon_7/sns_icon_21.png" = <136bae54 7a1a965e 2cbf971b 7297c5f9 9fb21162>;
        "Resource.bundle/Icon_7/sns_icon_21@2x.png" = <ea4282ea e7d1a51e 29184842 00d82068 579d5281>;
        "Resource.bundle/Icon_7/sns_icon_22.png" = <194165f5 054bc406 e4771f7d bbc60fe2 0e26c481>;
        "Resource.bundle/Icon_7/sns_icon_22@2x.png" = <71d6fb2d b118d978 9b968f9f 17064586 86ae5d10>;
        "Resource.bundle/Icon_7/sns_icon_23.png" = <51d613ec cbc42ae2 af1a18f5 d3781f1d 187c7256>;
        "Resource.bundle/Icon_7/sns_icon_23@2x.png" = <0a56d66e 35e78e75 e6741834 460a0f5c 942cde8e>;
        "Resource.bundle/Icon_7/sns_icon_24.png" = <632de99c 6dda6290 b123a0a5 592b32a3 243552a0>;
        "Resource.bundle/Icon_7/sns_icon_24@2x.png" = <eccec2f7 50d65524 00ee6be6 b2955df2 cd84e15b>;
        "Resource.bundle/Icon_7/sns_icon_25.png" = <93bd9570 89dbf583 39429ec1 11614670 c540f714>;
        "Resource.bundle/Icon_7/sns_icon_25@2x.png" = <67c9846f c758f416 2b85ba8b ef26647e 92e1e8b6>;
        "Resource.bundle/Icon_7/sns_icon_26.png" = <242ed62d 9fd1180e 63a1daed ed762417 00e1eb63>;
        "Resource.bundle/Icon_7/sns_icon_26@2x.png" = <73ad895b f7e68c03 962b8fa6 613328f7 ce038bc8>;
        "Resource.bundle/Icon_7/sns_icon_27.png" = <760eab88 9e4807b3 c12deac6 e1133406 46995809>;
        "Resource.bundle/Icon_7/sns_icon_27@2x.png" = <52fd754a cef14249 5f2dd69f 0ecd0a01 13cf2f93>;
        "Resource.bundle/Icon_7/sns_icon_28.png" = <43143449 e85433ae f6c83646 338103a0 bdcac15d>;
        "Resource.bundle/Icon_7/sns_icon_28@2x.png" = <41ff0908 4364c0fa d990bcf7 6e9ee36e 593b6247>;
        "Resource.bundle/Icon_7/sns_icon_2@2x.png" = <ec87611a 7a3d7fcf 519cc612 27b52b8e e7331e15>;
        "Resource.bundle/Icon_7/sns_icon_3.png" = <e19f1981 befd3d17 b91456e0 d8d11127 48879696>;
        "Resource.bundle/Icon_7/sns_icon_30.png" = <1780a3ee 1c33acf7 538dbf67 d153714a 54de59a2>;
        "Resource.bundle/Icon_7/sns_icon_30@2x.png" = <e76e2461 b99d70cb 85dbbe43 8584cf6d 41a61ed0>;
        "Resource.bundle/Icon_7/sns_icon_34.png" = <a287a033 692807b3 e4e7c8e3 7f94a63e 3511301e>;
        "Resource.bundle/Icon_7/sns_icon_34@2x.png" = <4cc6dd0b 7e709154 60f7e594 03d3ec69 713d3554>;
        "Resource.bundle/Icon_7/sns_icon_35.png" = <7b7e911a c1cf0127 bcdbaa94 f293c15c 56ef8396>;
        "Resource.bundle/Icon_7/sns_icon_35@2x.png" = <0b57f407 22b49399 15961a37 727dc50c 55589ff8>;
        "Resource.bundle/Icon_7/sns_icon_36.png" = <a24ce948 37ac74a4 3ec90e53 365e0ea9 b7082be7>;
        "Resource.bundle/Icon_7/sns_icon_36@2x.png" = <cbb88f21 a35d4b9e 1cd2e2d1 76811c25 338e0d08>;
        "Resource.bundle/Icon_7/sns_icon_37.png" = <a9a73fa2 93661fc8 0abdefea f3da8ab6 77661035>;
        "Resource.bundle/Icon_7/sns_icon_37@2x.png" = <bc17f356 581248df d3bdefb0 6cb34870 06eed5c4>;
        "Resource.bundle/Icon_7/sns_icon_38.png" = <d43f807a 094b2669 9b7c69d6 daaace86 4583553f>;
        "Resource.bundle/Icon_7/sns_icon_38@2x.png" = <0a810f95 a50c9886 2da81e4f 7d61ac33 caffe65a>;
        "Resource.bundle/Icon_7/sns_icon_39.png" = <ef28caf5 8fe37522 d65868c2 c7aa8292 d520189e>;
        "Resource.bundle/Icon_7/sns_icon_39@2x.png" = <0d856a8f f6c38353 daec29f7 3d552f71 aaa42711>;
        "Resource.bundle/Icon_7/sns_icon_3@2x.png" = <fe29bbdd d038687a af75ab86 6a694f4d 262b9725>;
        "Resource.bundle/Icon_7/sns_icon_4.png" = <75314313 a4375542 5e241266 b745af25 3baa8fa9>;
        "Resource.bundle/Icon_7/sns_icon_41.png" = <c5447fbc 94e1d3cc 6d266198 342344f0 b3655827>;
        "Resource.bundle/Icon_7/sns_icon_41@2x.png" = <674ac810 89f0855f 90073de0 c209be90 d98b2f45>;
        "Resource.bundle/Icon_7/sns_icon_42.png" = <5c8fd089 679e497f 5674d783 3cca968c 6c77550d>;
        "Resource.bundle/Icon_7/sns_icon_42@2x.png" = <2aafca8b 55124283 86a7e924 019c50b9 68d37d06>;
        "Resource.bundle/Icon_7/sns_icon_43.png" = <ae9cda2d ae854193 500500c7 50b174de d2d65ec4>;
        "Resource.bundle/Icon_7/sns_icon_43@2x.png" = <c14bd56a b00c730f 01df9d81 d79596c7 cac3e002>;
        "Resource.bundle/Icon_7/sns_icon_44.png" = <c042805c f2851a1f 4ee5e61b 1991f177 cf5f70d2>;
        "Resource.bundle/Icon_7/sns_icon_44@2x.png" = <86c08893 d8e0ecaf cce8152f b8907139 5cbb6b8c>;
        "Resource.bundle/Icon_7/sns_icon_45.png" = <d8c4ccc3 f8211608 f647091c b31c1ceb 20fe90bd>;
        "Resource.bundle/Icon_7/sns_icon_45@2x.png" = <97937c49 3176fa7d 2526fe4e 122caa04 d34fce74>;
        "Resource.bundle/Icon_7/sns_icon_4@2x.png" = <5a670c02 dfb99f59 7e41488b 6aeb4dd6 b55fa8c6>;
        "Resource.bundle/Icon_7/sns_icon_5.png" = <19d285ed 4dcdd79e 6d5a8593 7b92c294 13286fee>;
        "Resource.bundle/Icon_7/sns_icon_5@2x.png" = <1958086b f9a7b36b ed02dec9 68cfd3a1 a0605f8c>;
        "Resource.bundle/Icon_7/sns_icon_6.png" = <d55dbb7c 26a8a772 e9b7bd94 729811f2 d52ff67a>;
        "Resource.bundle/Icon_7/sns_icon_6@2x.png" = <115fabbc 02d38e2e c3ef8cd2 676a0798 c43713e1>;
        "Resource.bundle/Icon_7/sns_icon_7.png" = <a20500dc e3e64d54 c3e33353 a8f99ce8 b5b65652>;
        "Resource.bundle/Icon_7/sns_icon_7@2x.png" = <b984c62a 9c046de5 6106a0f5 e1b07dfe 455caeb1>;
        "Resource.bundle/Icon_7/sns_icon_8.png" = <7733944d 3a6674de 77a4bfec c502e03c 9ae003bd>;
        "Resource.bundle/Icon_7/sns_icon_8@2x.png" = <084eb5c3 787c9163 44943a64 dba1a50b c315e527>;
        "Resource.bundle/Icon_7/sns_icon_9.png" = <be848ba3 fa4d9139 ec176082 c9a3c884 c792ba1b>;
        "Resource.bundle/Icon_7/sns_icon_9@2x.png" = <a74212e5 2f38d21c 7b202622 088cb673 074d452e>;
        "SC_Info/Manifest.plist" = <1ba0b852 51419725 2a59b111 896dd036 6db1bbc5>;
        "ShareSDKFlatShareViewUI.bundle/en.lproj/ShareSDKFlatShareViewUI.strings" =         {
            hash = <4131f256 e8bed891 fdfcf457 fe602a20 d2f63119>;
            optional = 1;
        };
        "ShareSDKFlatShareViewUI.bundle/line.gif" = <ab4e80cf e76767a3 066187cf d1c992c1 0d26eef3>;
        "ShareSDKFlatShareViewUI.bundle/line@2x.gif" = <0367ff2d 46a259e6 e308f0f1 b84f4d7f 0f2e7bc2>;
        "ShareSDKFlatShareViewUI.bundle/zh-Hans.lproj/ShareSDKFlatShareViewUI.strings" =         {
            hash = <f0d5bc46 0d6e2647 466805cc 06da33c6 f8dfb46a>;
            optional = 1;
        };
        "data_table/achievement_achievement.bin" = <288a313d 1cc92d85 0863cc50 aebd6819 7113762a>;
        "data_table/achievement_detail_type.bin" = <f83d5360 77cd427a 6bc08c0c e5589f57 352b99ac>;
        "data_table/achievement_mission_common.bin" = <fc9a2830 0985d03e e730a826 2c240b3d d48c7aaf>;
        "data_table/achievement_mission_daily.bin" = <760e90c6 2d6cb3bd 1bc5fade 15d11bfe b4eeb363>;
        "data_table/achievement_mission_special.bin" = <c3c01c26 8df48bcf 8fb17198 2860828d f9f478d9>;
        "data_table/achievement_var_type.bin" = <e1ca3ece c8af3f80 e5f48365 3a8f3ce4 aa44855b>;
        "data_table/activity_activity_push.bin" = <854a63d9 c87ebdca 79c818bb 7ac2e4af 62fd459b>;
        "data_table/activity_box.bin" = <2dcc9500 d626f1a3 592c4bc4 06780e83 d1102f8f>;
        "data_table/activity_card.bin" = <a909baed ca8e35e6 44901d07 03eff036 dee06ac0>;
        "data_table/activity_card_exchange.bin" = <a47600a8 a7a00b50 09de551c a5c69d13 7ca33959>;
        "data_table/activity_card_probility.bin" = <b9a5405e 2784537d 4b590b87 9ac4a2fc 2f1708a5>;
        "data_table/activity_channel_reward.bin" = <371dd3ba 6cfd3349 2fd08a5a e4ed7dad 3e1d4e94>;
        "data_table/activity_land.bin" = <5a7228ba 36e79aa1 91e3dc6c 956bf50c e4819897>;
        "data_table/activity_local_reward.bin" = <186903d7 313568e7 519f0bd3 2201a4e6 82c746ec>;
        "data_table/activity_lottery_config.bin" = <60424d9b 19c85613 bc3986c6 48693bfd 55f26afa>;
        "data_table/activity_phone_number.bin" = <cd783450 2f1be0ea ba733638 b0a71f02 cfa825b5>;
        "data_table/activity_point_probility.bin" = <a783e42e 995f4c9d 45dda38d 4d5d0dc9 5c5e3ade>;
        "data_table/activity_recharge.bin" = <e0fb9b0b b244e8a8 0ff0945f 95ad772c e7fc25b0>;
        "data_table/activity_reward_weight.bin" = <ce56ecdd 01dc187f 9dbd4a6b 9f910015 febbaacc>;
        "data_table/activity_time_bonus.bin" = <21c43eca 14c97ab7 8b5d27ff 83d78b4c a4983d9f>;
        "data_table/ani2dconfig_ani_weapon.bin" = <ec6cf5ba 8d0429a7 33053a49 cc92b989 008ac006>;
        "data_table/bonus_bonus.bin" = <1cf3cb69 1e862e85 c664f643 3dcd5420 ea0af515>;
        "data_table/config_fish_line.bin" = <8c24088c 8a7484f8 aaa7c336 8b0e58de a0f20cfd>;
        "data_table/config_function.bin" = <17d00377 6ee26b8d 288e944e cc9169b4 1151b4b7>;
        "data_table/config_lv.bin" = <9db09ea7 6f0ff163 e5b0a15c c3548bd0 5f688310>;
        "data_table/config_magnification.bin" = <dc9b6d29 d0d610e5 3fb5d5aa 93fb8c78 c8f33632>;
        "data_table/config_player.bin" = <6753edb9 b29f0434 7892daf8 0704008e ab1f5869>;
        "data_table/config_rule.bin" = <bffea17f 7dd34d8c bd56cca9 d061806d 0a6943c8>;
        "data_table/config_setting.bin" = <fd906e1c 4d1d7190 4dbbbcc5 ad51ab42 2c4a5ce4>;
        "data_table/config_special_tide.bin" = <456224cd 9249e7c7 ad3ba476 053b9d87 f0d51300>;
        "data_table/config_strike_probability.bin" = <ded3990e dd0d4ca3 95c5b945 33fd66a4 d27b279b>;
        "data_table/config_track.bin" = <1f206766 80ecf418 9c22c319 bf212d71 7b352ca0>;
        "data_table/config_vip_level.bin" = <f8f67716 25f5801e 92eaac0f ce612978 3cfcb202>;
        "data_table/config_weapon.bin" = <bfa1feb6 50d6f5f0 4673f706 ce9f7221 9a4557fe>;
        "data_table/config_weapon_change.bin" = <77c5c953 651c2412 50e97647 0f6f8bfd 0995ae03>;
        "data_table/correction_correction.bin" = <378ed8c0 4d8e5764 aa88181f 48909b7a 6aec2a87>;
        "data_table/correction_hit_reward.bin" = <689745f6 18471596 3d70d3bc f1031003 c890d75a>;
        "data_table/fish_action_high.bin" = <af56d9f8 4bb27721 b2c76799 ae2eeec6 b1d8398e>;
        "data_table/fish_action_low.bin" = <3c12b9fd fe59037f ce7653bd 8cdc46f6 e11cbf64>;
        "data_table/fish_fish.bin" = <730ef987 012843b5 910e4da3 e1e2abb3 a3d5b4c0>;
        "data_table/gaming_cost_job.bin" = <1812c04c 3f91009d 5de5610a 595230c0 e6f36172>;
        "data_table/gaming_event_reward.bin" = <c044ba18 d71b1ecd bf83d9b5 1c6f43ec f29034ba>;
        "data_table/gaming_gaming.bin" = <3f50a9bc 153fb0df 56d30a24 a7c54528 4030c73f>;
        "data_table/gaming_golden_shark.bin" = <53fbe335 9525ae9f e3918ec7 d130be0a 30418b0f>;
        "data_table/gaming_golden_shark_discription.bin" = <f9c01522 74259bf1 d3cb9dba ff65bb5e f8c5e7d5>;
        "data_table/gaming_golden_shark_percent.bin" = <4b3770cd 36c3a203 2acdba13 a14e3521 7778cf1e>;
        "data_table/gaming_guessfinger_random.bin" = <6f1f6127 5e0985ca ccc74544 1f75996d 73a06812>;
        "data_table/gaming_guessfinger_type.bin" = <1e801d0d 38e4c125 0bfb87ef 3f5bc327 05755868>;
        "data_table/gaming_guessfinger_value.bin" = <56e464c1 f7040fc1 e4d4378f 99c048ef 846118b0>;
        "data_table/gaming_hetun.bin" = <5bc72a5c f5d58098 371ef93a c1ae94d7 a18e789f>;
        "data_table/gaming_job_gold_range.bin" = <bcd15a5b e8e4a1f4 fc8122e4 603ab5cf 0bb9d9c8>;
        "data_table/gaming_job_reward.bin" = <fcd924e0 574e1d87 de8e8b01 26cf1a0d 09ca9a47>;
        "data_table/gaming_login.bin" = <0c165865 b9e2fc99 7c7bd1d2 ae9009aa 03ab8c43>;
        "data_table/gaming_login_cost.bin" = <741b4d5e d2abd09f 5863aad8 05375ce5 f8a9f83f>;
        "data_table/gaming_login_discription.bin" = <53b19993 9748c74c 8acca44e 76eb4a89 a30a9eed>;
        "data_table/gaming_login_reaward.bin" = <699ed3dd fa48f88b cc3db368 f1cbff96 48c01492>;
        "data_table/gaming_login_reaward_correct.bin" = <77aa1211 539e685d b4085565 4921af67 6267656a>;
        "data_table/gaming_pachinko.bin" = <5f62fbd6 383c0f7e 50fa457f e8a21905 0f129b0e>;
        "data_table/gaming_pachinko_config.bin" = <a1987674 0901c4b8 80c3d0d5 2d0f2edc 8bf3aadb>;
        "data_table/gaming_pachinko_discription.bin" = <a2083fda ad997ba5 7a17dc00 a3a80aee 576d73d0>;
        "data_table/gaming_shark_tooth.bin" = <9679c269 f32cc60b 287c0a66 c25146a9 39264236>;
        "data_table/gaming_sign_in_base.bin" = <d477d859 107b6bd6 9d7909b2 1fe2cce0 ba244e1f>;
        "data_table/gaming_sign_in_config.bin" = <391698cc 7c4bcf02 af7d3c58 8c92529c dc57bafe>;
        "data_table/gaming_slignt.bin" = <605244b7 345e55d5 81e3994c 306714e6 07486556>;
        "data_table/gaming_treasure.bin" = <86f7d7a8 3a2b5aa3 15c97dd5 e111a4f0 0d95f30a>;
        "data_table/gaming_trigger_config.bin" = <1e82e236 38055e7a ee152b56 e622609d 10308308>;
        "data_table/gaming_trigger_job.bin" = <fdd3156e 8e506ba6 a24354c1 77a56b29 de246e49>;
        "data_table/log_event_log_event.bin" = <229afbea 68334f50 5df5c8b2 625f0528 de18f21b>;
        "data_table/mission_card_mission.bin" = <3a63611d 25442ded 8c66b284 4ab85b22 e0ec3b86>;
        "data_table/mission_fish_gather.bin" = <d7eb6263 0dd56857 3368ce5a cd5cc0e2 a1de30b2>;
        "data_table/mission_mission_detail.bin" = <3dd49aaa 500a2208 5ed7d932 979ed2a4 28b6f05f>;
        "data_table/mission_mission_fall.bin" = <c27ea65c faadccb4 49cae65f e275fb8a 33a1b4d1>;
        "data_table/mission_mission_stage.bin" = <c7ad7903 f0482ef5 927eb2f0 12f1b565 b2d8c679>;
        "data_table/mission_mission_type.bin" = <e2471f24 b9f60248 55af8dab a9b8f67c 62bb7892>;
        "data_table/music_music.bin" = <2e5ee023 a599a50b 92ab031f bcf9a3ca 641ee0ca>;
        "data_table/notice_notice_android.bin" = <1d63b4cd 9321970a 4962bab3 52744383 181631d3>;
        "data_table/notice_notice_ios.bin" = <8993ba79 8d29b505 8be52c03 d6913ae7 55b9083e>;
        "data_table/notice_web_notice.bin" = <3bffa690 63e2dc53 f3674168 04e42e8c 0091a2be>;
        "data_table/particle_particle_2d.bin" = <b94e7d61 ea5b953f 332e8a0d a03d9964 d32d4a97>;
        "data_table/particle_particle_3d.bin" = <7cb0544f c07103ce cfaf6cc2 b253780c cf7eaef5>;
        "data_table/sale_goods.bin" = <f0c84643 2594f852 3dfed090 d48867e4 eccad47c>;
        "data_table/sale_sale_android_cn.bin" = <2ca9d002 85e4ba27 5bb53627 9e2e44ba 5dc8c11f>;
        "data_table/sale_sale_ipad_cn.bin" = <3538109d 0c952ef9 9c0be61c c1493b0c 12947a6c>;
        "data_table/sale_sale_ipad_en.bin" = <680c5fb6 ace072ce 6891a86e e3164c6b 05bc951d>;
        "data_table/sale_sale_iphone_cn.bin" = <2ba81875 99265387 ef3feed6 20586c62 1ab88ca6>;
        "data_table/sale_sale_iphone_en.bin" = <fa73abc9 bdfbbfb2 6afe90ea d1ecd004 dec7b169>;
        "data_table/sale_sale_jailbreak_cn.bin" = <0208f441 6ccade9a fba70c97 44b1a735 0846a5d5>;
        "data_table/scene_fishfarm.bin" = <4913e314 2436bf90 1f1c2a9d 5e3f2978 85273ac3>;
        "data_table/scene_map.bin" = <3701f2fe f28deecf f025dcca 864ef1a4 2567de19>;
        "data_table/scene_shuicao.bin" = <67607c06 e03fa5e9 a0e02253 d6d9229f 11842762>;
        "data_table/store_crystal_android_cn.bin" = <02c659e5 d399c139 2afcfd0c a49320f2 4a7f3608>;
        "data_table/store_crystal_ipad_cn.bin" = <38414c06 84e5c38e c18af3ac 27d03aa1 33ed64ef>;
        "data_table/store_crystal_ipad_en.bin" = <8bec0302 4fa5a474 ad4ce0d8 da17b902 7a53f4d4>;
        "data_table/store_crystal_iphone_cn.bin" = <e80de12b ceee10f5 e089d9d2 1a447a49 a72cd628>;
        "data_table/store_crystal_iphone_en.bin" = <713fc080 4ffabd4a 4cb76d09 f7a1f282 d55fdddd>;
        "data_table/store_crystal_jailbreak_cn.bin" = <94c492fa f5fdecfa a8cd4c22 9d30e4c9 89d31f51>;
        "data_table/store_gold_android_cn.bin" = <bfe32c3b 0b07cf50 1b37ef1e 12e68aea 45c674c6>;
        "data_table/store_gold_ipad_cn.bin" = <f7b796c7 1ce71102 affa1c99 a9ce9bd5 17986c2f>;
        "data_table/store_gold_ipad_en.bin" = <fb8fb1d2 8c566d92 e4a34eee 6fdc7ea6 bd96193d>;
        "data_table/store_gold_iphone_cn.bin" = <24edb28b 2d2439e7 b390c9f8 800a2671 20a89f4a>;
        "data_table/store_gold_iphone_en.bin" = <3d0c56e3 da047e82 b6524afe 77fd2e7f 35440b86>;
        "data_table/store_gold_jailbreak_cn.bin" = <e16b9ccf a8da6792 9c8b93ec cd4a9d51 7c75998d>;
        "data_table/store_submarine_android_cn.bin" = <a90dbbd7 78d8561d 83236354 4d396a2f 16d64ede>;
        "data_table/store_submarine_ipad_cn.bin" = <09cb63a2 fa5794cf e8361e71 1a6ecd48 172b2dc5>;
        "data_table/store_submarine_ipad_en.bin" = <40b61d50 54650a49 9ef2df1d 3c48df56 45ec3e69>;
        "data_table/store_submarine_iphone_cn.bin" = <7e5cc5fc 7cd054bd 2ce5cbc6 30350aa8 5b2ca8c5>;
        "data_table/store_submarine_iphone_en.bin" = <d8282a9f ef522ede be2031d2 d569ccf0 c30dac54>;
        "data_table/store_submarine_jailbreak_cn.bin" = <948fff15 60e10cbb 53405287 fb2d851c 3e1f2cd6>;
        "data_table/store_vip_android_cn.bin" = <1837ba47 6aab1b85 2cd6983b a4eb7825 f6ed703f>;
        "data_table/store_vip_ipad_cn.bin" = <e7a59ad0 3b93c7f2 b27e062d 1412c496 c2ee108d>;
        "data_table/store_vip_ipad_en.bin" = <f141181a c778bf38 1364ee50 394fdf6e eedb52a9>;
        "data_table/store_vip_iphone_cn.bin" = <bab4d978 d2f35eb9 91c7b41a 420090d8 5a5c0c45>;
        "data_table/store_vip_iphone_en.bin" = <f141181a c778bf38 1364ee50 394fdf6e eedb52a9>;
        "data_table/store_vip_jailbreak_cn.bin" = <a716dc11 96e5858e ef5b0a41 7d070381 cd257de6>;
        "data_table/store_weapon_android_cn.bin" = <c8b38b7e 7099dd63 5f1793c8 4e058387 b51a9422>;
        "data_table/store_weapon_ipad_cn.bin" = <82b5ab34 392636e0 a9d70c61 eb57b00c 4cb4c253>;
        "data_table/store_weapon_ipad_en.bin" = <ec41ec79 61087dcb 31e0666d cb993d7f 782645c7>;
        "data_table/store_weapon_iphone_cn.bin" = <23bf4374 736ec289 05c2f58f e4619dd3 8e5412f2>;
        "data_table/store_weapon_iphone_en.bin" = <886248b3 59de31af 50fb797c 854d1f12 3550b327>;
        "data_table/store_weapon_jailbreak_cn.bin" = <8d146fa1 ddcfb70b c78cb449 bc34908f 52b4d4b1>;
        "data_table/string_string.bin" = <c19103d9 03088144 28027df3 8251a821 554ab353>;
        "data_table/submarine_skill_info.bin" = <debc3993 91308fdc 0a5e79b7 f1c8252f 81b68fbf>;
        "data_table/submarine_submarine_info.bin" = <f9a290b6 c9bdbd21 a83db119 bdd89e99 0cd41363>;
        "data_table/tariff_mall.bin" = <cc3983fe 489c9d20 18216886 d45b236c 860b6c67>;
        "data_table/tariff_sales_promotion.bin" = <f7852c56 1fc6b55c d4842295 37f5856a 7ae892cb>;
        "data_table/time_event_axis_event.bin" = <d7a40a1c cf7309c7 fd0dbbf1 5fe1e444 97e0b3a4>;
        "data_table/time_event_event.bin" = <57efb275 74de662b 144c6121 93df88a5 11c95520>;
        "data_table/time_event_quartz.bin" = <82e22867 cb55790e c617dab1 f993df1b 709329b6>;
        "data_table/time_event_time_control.bin" = <4c47969d bc5f827a 0747754a 2eb92448 42940a7f>;
        "data_table/tip_tip.bin" = <a21fa823 301424af 8b70cf98 ee46941b 8e335f3f>;
        "en.lproj/ShareSDKLocalizable.strings" =         {
            hash = <192d091c da7351a7 cc4d0725 9220f4c4 fbe477ef>;
            optional = 1;
        };
        "font/fj3.ttf" = <c664c796 725ef305 fb514361 82aa1cf4 836d5031>;
        "map/changjing_05.sce" = <9642366d c3afcb4c 86b1d904 0ca5c5e2 557244f5>;
        "map/changjing_07.sce" = <82c387a2 a385822d e95b560f a7ac01a8 cdf8df3c>;
        "map/changjing_08.sce" = <1b95677d 1d0c97c8 5e07c766 43576d97 b7eed983>;
        "map/changjing_09.sce" = <201be84e bd429c69 e4c17753 acaf9ef2 02761c72>;
        "music/bgm_boss.mp3" = <d10abccd aa115007 4c3547c9 ae37f4d7 05b736c8>;
        "music/bgm_fever.mp3" = <14087110 b51e9ae8 c09109a7 37bafef9 93f20625>;
        "music/bgm_minigame.mp3" = <d1a0db20 8c3bcd04 6dd6e042 8ca5809c ee48a5e3>;
        "music/bgm_opening.mp3" = <f0d47fa4 b313a865 73e5f6ed 2cf0e9cd fc67a7e7>;
        "music/bgm_scene1.mp3" = <c76d4234 29f15f29 5869f7b7 2ef78ca5 c2e56781>;
        "music/bgm_scene2.mp3" = <38463825 bc993c10 01d05dc4 5b8753e2 c6f7682f>;
        "music/bgm_scene3.mp3" = <71f71f19 b9173769 d4c2874d a8187246 cb53e797>;
        "music/bgm_select.mp3" = <847830bc 6ab28229 f8bcb658 68f49355 428d48f0>;
        "music/bgm_slot.mp3" = <a9a68da9 e5edf8a4 cbc7768f 3535991d 52e0ec4d>;
        "music/sfx_attention.mp3" = <ae94ea69 2eb5293f 9447a5e7 0ccd3b57 ac1855b0>;
        "music/sfx_babbleburst.mp3" = <b1469925 9cecfeea e9bc86f5 ab808549 42c8904f>;
        "music/sfx_blackhole.mp3" = <691cec9d 85b7595d 14f01d88 db399f4b 8e0e801f>;
        "music/sfx_box_appear.mp3" = <890c97d1 c7ed3048 aa87eab3 a09d5a31 0f37f67d>;
        "music/sfx_box_coin.mp3" = <4bcc705c 2519de7f 156b2e2e 1e76e249 b851309a>;
        "music/sfx_box_fish.mp3" = <c6b50cd6 90a35320 d141fe1c 1c9fd47d 4a9a9d47>;
        "music/sfx_boy_wow1.mp3" = <3772881f 6492df3e 60bb38be 903cf96a ea56f513>;
        "music/sfx_boy_wow2.mp3" = <540ffc98 ce3020dc a049e6ed 8056312f 8ef34b85>;
        "music/sfx_bubble_appear.mp3" = <bcdd987c 25cacdc8 510501d7 1e3a66e8 5a61d20b>;
        "music/sfx_bubble_broken.mp3" = <27b3055a 158ab118 ac405dde 20d19207 82b4a8ee>;
        "music/sfx_bursts.mp3" = <5d669abd 284d1d6f 2a3be296 5c60df56 b7eb2852>;
        "music/sfx_bursts_fever.mp3" = <aac4ccb7 2330db1a 6b560655 6807e11a 085cc9b1>;
        "music/sfx_button.mp3" = <6d67698b f0ab18a6 1070172e 223514d5 ba793764>;
        "music/sfx_camera.mp3" = <7b053a8a d3376f26 00fc86c6 280a25b4 6ffd96b8>;
        "music/sfx_cannon.mp3" = <ca0747d0 8d44cf00 bcb3a009 b5c647cd 7f3d55b6>;
        "music/sfx_cannon_fever.mp3" = <412decc7 bbf0dedf 55ac5534 16a5be45 62de6cd5>;
        "music/sfx_card_click.mp3" = <23dc1cee e2aef28a cf14ef3e c34d407c c26d2f8c>;
        "music/sfx_card_open.mp3" = <33ef029d eafd84aa f94f80f8 4b5f3bf1 81050957>;
        "music/sfx_circle.mp3" = <3bc66fe5 9743b5c9 fcc575e9 fe0752c9 180d6658>;
        "music/sfx_circle_fever.mp3" = <820e3dd6 6df8f926 f997380d 283aa124 1b21fdaf>;
        "music/sfx_coin.mp3" = <344f6be4 063730be c9099c7c 6e5436b0 593f72ae>;
        "music/sfx_coins.mp3" = <bd711168 8f6c8e77 79a257bf 02333414 aa078bfc>;
        "music/sfx_collect.mp3" = <874fcd6e 9750f268 ace28901 da6786d4 9a76852f>;
        "music/sfx_cristal_collect.mp3" = <fe8913eb 0740eedb 6e9f206b 7ef3582a eeff11cd>;
        "music/sfx_event.mp3" = <34f101ed 281a3aff 58776f9e 486a03e2 64bfd2ec>;
        "music/sfx_fever_apply.mp3" = <314c69b3 fa3d794f 61de0feb e1da5162 62641e1a>;
        "music/sfx_fire.mp3" = <63a596c1 283a920c b45e3282 094651da 31b23e23>;
        "music/sfx_fish_talk_1.mp3" = <eab26656 16b53d6d 0638c11f e693669c bdaec4fd>;
        "music/sfx_fish_talk_2.mp3" = <aadfb7ef c1612fb2 79074b91 d0c96310 65609fee>;
        "music/sfx_fish_talk_3.mp3" = <5af0e022 6b045088 3aefbc16 b834eba9 bb2140b4>;
        "music/sfx_flash.mp3" = <1438b659 3ba152b9 23d5adbb 67ab1ad6 4c3ed87f>;
        "music/sfx_goldweapon.mp3" = <61ef078e ccd889e5 a229061f d9c0c860 33016c9f>;
        "music/sfx_guess.mp3" = <9a456269 3631df8c 69e2df9f d8f6b66e 89f71dca>;
        "music/sfx_guess_lose.mp3" = <131cc4a2 df79462d baccf143 de09cc5c 52db639e>;
        "music/sfx_guess_win.mp3" = <4984e962 bc67d080 25039e65 a5c6158d fc7cff92>;
        "music/sfx_guess_win_gold.mp3" = <54284e5b d5e830c2 7d89c5b3 16c1a0f4 88fc3e6e>;
        "music/sfx_harpoon.mp3" = <1fb39ab0 9ebab1e4 e2cc864a 94376ace 074f87d4>;
        "music/sfx_harpoon_fever.mp3" = <a4f9faa7 18fa66a9 4b85e02e 5a580e15 36ffad40>;
        "music/sfx_levelup.mp3" = <3a3ed35f 4bffc450 25749c58 bd2b4e89 0cc21cd1>;
        "music/sfx_needmore.mp3" = <49eb966e 9de86c59 d55de5ea 026d7fb4 1bb9934c>;
        "music/sfx_net.mp3" = <74a893d8 ad9650d5 73d5a71f 07ef4ec5 c9d82269>;
        "music/sfx_open_card.mp3" = <8c574a68 455e97ae 2f817314 0761b39a 5072303f>;
        "music/sfx_rank_up.mp3" = <db620e1b b2a930a6 8c3f1ef0 6d93250f 9360bf5a>;
        "music/sfx_sharktooth_correct.mp3" = <281846e4 a6e4b1f2 31f84687 80009a76 12c15b1d>;
        "music/sfx_sharktooth_open.mp3" = <140534b9 ca5d24be b6b94f28 fdcd91c9 db03d6b8>;
        "music/sfx_sharktooth_wrong.mp3" = <71a836bd 33a01d73 7363590d 0b7d6220 bf5e53cc>;
        "music/sfx_shoalhint.mp3" = <211a219e c0f0dc65 ffb318cd f2e1bdc6 20d78d8d>;
        "music/sfx_slot_appear.mp3" = <3df4bd5a 20271d3f 8f587d1c e362934d 3de579d0>;
        "music/sfx_slot_push.mp3" = <44fe206d 29e7cf70 1712d18c 03237412 57b066a1>;
        "music/sfx_slot_rolling.mp3" = <ed631bfe 941d8fb0 f7452c95 3ede7c17 74b27fe3>;
        "music/sfx_slot_stop.mp3" = <57dfdae9 d8fee02f 32ada43a fda44d89 4cbd551f>;
        "music/sfx_slot_x2.mp3" = <44d38b09 f576db96 5624280c d3c9eb24 67d4edd7>;
        "music/sfx_supersoul.mp3" = <ca9a9070 6b3479bb b8412b47 23de31c0 21a17d69>;
        "music/sfx_superweapon.mp3" = <1f0c1f40 063bf224 e1270cac 35b4b176 8a43f8f9>;
        "music/sfx_superweapon_fever.mp3" = <7a17d7fe c5b9e5d7 68bdb627 9313509b c9484e38>;
        "music/sfx_switch.mp3" = <6c3ae56a c2d769f1 bb85c95a 4fb8a23c e7fec554>;
        "music/sfx_thunder.mp3" = <e0c0652e 048cab1c 00ef4b8b 09bf4f13 59a155a1>;
        "music/sfx_thunder_fever.mp3" = <ae152a2a a8571614 035f2076 f01eb794 1075b2ea>;
        "music/sfx_window_close.mp3" = <05064e7e ee414f8a 661ff4f7 a7460a3a edafa298>;
        "music/sfx_window_open.mp3" = <e2dcc742 a3b7307d 7afeea69 8fecbbb9 2888213b>;
        "particle/achievement_star.plist" = <2baa0729 2df51525 57fa0c9b 268a9061 ed698a8b>;
        "particle/achievement_star.png" = <ba9d4169 fca8ddea 8e9bbae8 121b2622 b45a23f2>;
        "particle/bao_li_zi_fen.plist" = <34b956c8 a0741753 3bcc43cb c5620315 8e8f21b6>;
        "particle/bao_li_zi_fen.png" = <6dae82ac edd283d7 f3a2ddda 401768d4 0d5a6d3e>;
        "particle/bao_li_zi_green.plist" = <20013901 b43ba5be 437bbe49 dd329dd1 42df5291>;
        "particle/bao_li_zi_green.png" = <d4113a86 ce53b4ad 8abed0df 9014e892 9199b6d9>;
        "particle/bao_li_zi_lan.plist" = <e3db5c16 a8794c12 29fcac04 a9a3d1e5 f2d6dbdf>;
        "particle/bao_li_zi_lan.png" = <6283d036 fafb52f4 f2c7489c 432f0b09 6d1e0924>;
        "particle/bao_li_zi_red.plist" = <56457588 856dfe53 683b9e37 65576ee0 c17d34a8>;
        "particle/bao_li_zi_red.png" = <34f4f52c 0442808e b89d1c16 3513e2d6 8f1a5c3b>;
        "particle/baoxiangbao.plist" = <bc84ce55 ad89ac62 da90f730 e295b3bb 05ff2dc3>;
        "particle/baoxiangbao.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/baoxiangshan.plist" = <0c1db644 64cfaab5 ec51d421 6e5dedf9 99468fa5>;
        "particle/baozha01.plist" = <b2797a0c a8538a24 05fb731a 8fb8f0ad 62d0e304>;
        "particle/baozha01.png" = <fea870b0 5a80538c e5a48fae 6a363b29 7ff6def7>;
        "particle/baozha02.plist" = <39545ddb ec1ca050 f9f6dd79 e15c3bd6 df274001>;
        "particle/baozha02.png" = <4eae1af4 c33e8250 2cf71f2d 09163595 d47c2b3c>;
        "particle/bar_get.plist" = <504dafd2 86baacda 945c546e 04fe475d 350cc3da>;
        "particle/bar_get.png" = <002ad3c2 f37b100b f25fafd1 375e77d3 d1c12d9a>;
        "particle/bigwin_blowout_1.plist" = <7c23c41e 95b1a737 8e4f11e7 2f53fefe f9677844>;
        "particle/bigwin_blowout_1.png" = <416db3da c07639ef 8d0a97d2 7103209a 29a674cc>;
        "particle/bigwin_blowout_2.plist" = <65e723c0 a50e56d8 0f4e203f 671a0998 d1ac53fb>;
        "particle/bigwin_blowout_2.png" = <416db3da c07639ef 8d0a97d2 7103209a 29a674cc>;
        "particle/bomb1.plist" = <b8d9b50a 33c7004d e2ffb879 257717be 6e432a9d>;
        "particle/bomb1.png" = <e97374a1 69495350 d7341d02 7dab4ef6 7f3e01b6>;
        "particle/bomb2.plist" = <514485d6 e3741449 0bfb1268 c823d4d3 ab21cfc3>;
        "particle/bomb2.png" = <ec6dc3cb 8b591408 f843ddcd 85a92fa7 4eac0af4>;
        "particle/bombparticle_sashuolizi.png" = <d45fbf40 f4b6e86d 48f6e72f ec65ce91 a20d3986>;
        "particle/bombparticle_shanshuolizi.plist" = <996153ea f259e779 c5112529 03103fea a107bd6a>;
        "particle/bombparticle_xslz.plist" = <e4e18a89 2d04dd6b d36fb535 324be1ed 18d2b8cd>;
        "particle/bombparticle_xslz.png" = <d45fbf40 f4b6e86d 48f6e72f ec65ce91 a20d3986>;
        "particle/bombparticle_xxbz.plist" = <f45cfc69 6260f15d f8866ae8 f2cc0500 6ab18956>;
        "particle/bombparticle_xxbz.png" = <4eae1af4 c33e8250 2cf71f2d 09163595 d47c2b3c>;
        "particle/bombparticle_zhxslz.plist" = <ca87e4dd 61155ea2 adad6097 89e8ac2f be366cfd>;
        "particle/bombparticle_zhxslz.png" = <6a853286 33a91544 d50dc9d4 3630a81f 53a967dc>;
        "particle/bujingcha.plist" = <54323007 820af679 5da81ee9 11e991ab 30e17f6f>;
        "particle/bujingcha.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/bullet_circle.plist" = <64782530 1d25f563 20a34ea1 bf66a27c ecc9a9ef>;
        "particle/bullet_circle.png" = <fea870b0 5a80538c e5a48fae 6a363b29 7ff6def7>;
        "particle/bullet_fire_01.plist" = <29e17a83 ca02388c f9c087bc 67bbfd9e 28f0a57f>;
        "particle/bullet_fire_01.png" = <8cda6e42 815bf4a7 d133858d f3243783 8c6d1169>;
        "particle/bullet_fire_02.plist" = <9cff0c0c df60772d 1d612729 780192a5 87b7beec>;
        "particle/bullet_fire_02.png" = <8b35959c f526df73 8c22ee02 f65c87d4 ade3df83>;
        "particle/card_1.plist" = <1b93b061 9758412a b67259d7 0f71fabc 67282a3b>;
        "particle/card_1.png" = <8cda6e42 815bf4a7 d133858d f3243783 8c6d1169>;
        "particle/card_2.plist" = <faba4e91 07761201 a8508552 3fd30a1c 6e6eb833>;
        "particle/card_2.png" = <331811e5 a4d2f9d7 9da2fcba 1c91d68c 004566dd>;
        "particle/combo.plist" = <10e3d5cc fea7a954 f61bdf28 c0ba9d90 8fd04662>;
        "particle/combo.png" = <eece2938 a78584a1 6911a17f b2287faf 840ac564>;
        "particle/cristal_tail.plist" = <71b9ac69 612bc346 19261ae5 7aa3450f a27e181c>;
        "particle/cristal_tail.png" = <6075ef47 e7e2dfb6 914ee0f5 58d4b554 443c3dd5>;
        "particle/dalizi.plist" = <82ebf086 b33cf42c 7491eac3 3c6db22f e4128781>;
        "particle/dalizi.png" = <fbec9d1a ee7edc27 89478412 9a855f92 cb5850be>;
        "particle/datuowei.plist" = <d08aec65 d2d73dba c2ecabef 45180f88 75d579db>;
        "particle/datuowei.png" = <774cea68 5150489e eae0e9b6 7e5bfbdd 318bcf93>;
        "particle/denglongyu1.plist" = <f08a38e1 dd08e10d d961bf19 0d565cb4 838498aa>;
        "particle/denglongyu1.png" = <84e382e9 0ef4212c 290ac8ef 24dc16a0 516d5556>;
        "particle/denglongyu2.plist" = <d6bc6dc4 a7af0225 9d711ac5 9a41d77a 6cb7ce7b>;
        "particle/denglongyu2.png" = <cb1b5f6a dc74e2e5 7af74c8d e064db2c 79f33deb>;
        "particle/door_close.plist" = <eddfe2cb d2064138 2e8f5ab6 8fac28ff b0230b4b>;
        "particle/door_close.png" = <e97374a1 69495350 d7341d02 7dab4ef6 7f3e01b6>;
        "particle/event_weapon.plist" = <85affd2f 3d9fab06 6e13f5d0 c79e999c 83734e8e>;
        "particle/event_weapon.png" = <87494852 02c1f295 bacab7ee 1caf1523 2982bbb5>;
        "particle/exploding.plist" = <a949fd8e 2187d654 8cf0dd83 4b7a7d9a 6b0495a2>;
        "particle/exploding.png" = <3a279d7e 534b7a6d f1bf7fa7 62339326 f563559e>;
        "particle/fever_ready.plist" = <685e10c7 01f26a9d 17f653fe ededb563 e945c63b>;
        "particle/fever_ready.png" = <331811e5 a4d2f9d7 9da2fcba 1c91d68c 004566dd>;
        "particle/fire02.plist" = <9cff0c0c df60772d 1d612729 780192a5 87b7beec>;
        "particle/fire02.png" = <8b35959c f526df73 8c22ee02 f65c87d4 ade3df83>;
        "particle/fire02_2.plist" = <29e17a83 ca02388c f9c087bc 67bbfd9e 28f0a57f>;
        "particle/fire02_2.png" = <8cda6e42 815bf4a7 d133858d f3243783 8c6d1169>;
        "particle/firefish_trail1.plist" = <a370e228 21e4d1c9 af697f44 aa3f30a1 9bba9840>;
        "particle/firefish_trail1.png" = <ce11ebc6 c74bb8b3 ead079ee 1a93653c 93f4bc8b>;
        "particle/firefish_trail2.plist" = <d6e7d450 a6fe6528 d4749203 3c2f0d0d 6baac366>;
        "particle/firefish_trail2.png" = <8cda6e42 815bf4a7 d133858d f3243783 8c6d1169>;
        "particle/fish_disappear.plist" = <c1877a86 05194e76 721aa4f4 ed9af5ed ba286a05>;
        "particle/fish_disappear.png" = <5cca50a8 b4916314 4c346c3a 278bff97 e469bc2f>;
        "particle/frozen.plist" = <ce6bfbc1 53a56e60 4abf9e99 e730cda8 d7e47260>;
        "particle/frozen.png" = <54332a39 99b12d57 40485edb 62ae77ef 058da342>;
        "particle/gaibao.plist" = <905c8596 d3b9a8b0 3a38b429 cb624736 aca5353f>;
        "particle/gaibao.png" = <0efad1e5 6aac21be d6770a2c 05e0a4ec 32d66025>;
        "particle/gas_disappear.plist" = <0691eb6d 4cb93b25 62a97a79 a1f4cf10 4fe16b75>;
        "particle/gas_disappear.png" = <774cea68 5150489e eae0e9b6 7e5bfbdd 318bcf93>;
        "particle/gas_explode.plist" = <e2c82c9e d554e4a4 7909f815 cbbc1db3 b441ac56>;
        "particle/gas_explode.png" = <25805ecd ea590adc a28391c9 1ece4313 c70a9300>;
        "particle/gas_loopbubble.plist" = <c32702a8 5c53d286 28093707 c752c65a 8ec5df14>;
        "particle/gas_loopbubble.png" = <77a09390 5bb5ed2e 535fc43f d4aa51d6 9cf85e7d>;
        "particle/gas_loopsmoke.plist" = <bd71d304 07e7ce16 ff16d031 b8aed988 9126f5b9>;
        "particle/gas_loopsmoke.png" = <3a279d7e 534b7a6d f1bf7fa7 62339326 f563559e>;
        "particle/gas_smoke.plist" = <b2f9ef8b 8c5d54f3 c89b9b16 5c02ce1d 1473016a>;
        "particle/gas_smoke.png" = <3a279d7e 534b7a6d f1bf7fa7 62339326 f563559e>;
        "particle/haigui_ice.plist" = <7c49e695 02cee292 e3c1c8cd 0c98d3e7 b723ffa0>;
        "particle/haigui_ice.png" = <774cea68 5150489e eae0e9b6 7e5bfbdd 318bcf93>;
        "particle/harpoon_explode1.plist" = <e1ef5859 5046572e e4ec6ac1 9ff8ee79 fc330c8a>;
        "particle/harpoon_explode1.png" = <d615cb73 854e54b3 c05b671a 9a2ed973 85ccf347>;
        "particle/harpoon_explode2.plist" = <59d92514 90642cf0 ebc59bc8 4318eef0 5181dd2a>;
        "particle/harpoon_explode2.png" = <25805ecd ea590adc a28391c9 1ece4313 c70a9300>;
        "particle/harpoon_standby.plist" = <e8e3ed1d 445498b0 02ea1ef7 c2caae08 67246a72>;
        "particle/harpoon_standby.png" = <a8ca8635 165db92a aa6c3455 e519888e f97122f7>;
        "particle/hetun_crash.plist" = <8203fb38 5bf76903 71ab2adb 6434cbe7 1ff58abd>;
        "particle/hetun_crash.png" = <c2ce95e1 252b3266 7547c41c 2e0c8f9b a7d1816e>;
        "particle/hetun_crash_blue.plist" = <3ca1dd96 28814d5d aa23d8e7 becc7757 1c41b0fa>;
        "particle/hetun_crash_blue.png" = <c52a31e9 946958f7 3b5cbf34 a5994802 27d3e2fa>;
        "particle/hetun_dead.plist" = <1bf598bd 46e66d64 bea088af 7b24e6f4 1607ef36>;
        "particle/hetun_dead.png" = <c2ce95e1 252b3266 7547c41c 2e0c8f9b a7d1816e>;
        "particle/hetun_disappear.plist" = <d7eeadaa 42871ef1 f90b9cd3 65304c61 c68d5d6f>;
        "particle/hetun_disappear.png" = <c52a31e9 946958f7 3b5cbf34 a5994802 27d3e2fa>;
        "particle/huangjinpao.plist" = <00d0d4b5 86f7cb66 170bad48 96e05195 f242e9d9>;
        "particle/huangjinpao.png" = <362903eb d523b3b5 98d28c16 a8bd1ec0 8ee6d9c9>;
        "particle/jbtwlz.plist" = <3b177d91 119d6cb5 00d34484 93a2e334 62605ab8>;
        "particle/jbtwlz.png" = <67032fe2 e47fe639 88ffe6fb 75d31353 1563310f>;
        "particle/jinbitubiao.plist" = <d7675d9d cc4ae4d0 08c3795c 8ccc1770 5cb92573>;
        "particle/jinbitubiao.png" = <ae5cbf10 4a43dbdf 8345eba8 d9138c0a 80e5cd04>;
        "particle/jizhonglizi.plist" = <00ba7e55 4a78aade 0290760a ffaf2998 e4f8a534>;
        "particle/jizhonglizi.png" = <fbec9d1a ee7edc27 89478412 9a855f92 cb5850be>;
        "particle/levelup_bubble.plist" = <efc347c5 9ed06999 88a3548c 835c914f fd4004e4>;
        "particle/levelup_bubble.png" = <d4e5ff5a 27248f47 5b9b2343 50b7f91d 027046ed>;
        "particle/levelup_button.plist" = <b552cd08 f9b726d7 357b16a3 e465f9ab 6222a7ec>;
        "particle/levelup_button.png" = <a8c46e55 f365e075 a93b16fb 1cc54566 e22a74f3>;
        "particle/pao1.plist" = <5662614c 5898e323 9ac8bbdd 82fe6cbc 696acb1f>;
        "particle/pao1.png" = <3a279d7e 534b7a6d f1bf7fa7 62339326 f563559e>;
        "particle/pao1_fever.plist" = <b0b5c0ac f33d8303 53205988 832f1731 684bbb86>;
        "particle/pao1_fever.png" = <d615cb73 854e54b3 c05b671a 9a2ed973 85ccf347>;
        "particle/pao2.plist" = <f2cbf7e3 f8139a62 899d2020 f85c31f3 e656cd74>;
        "particle/pao2.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/paopaoxiangshang.plist" = <aecf45bc 128ee4f3 d12d8868 e8c5bf74 99aee3c1>;
        "particle/paopaoxiangshang.png" = <36d04658 2dd27fc0 4fb51719 48b8a845 4fa78679>;
        "particle/qianshuitingqipao.plist" = <c0cdf4d6 6b729037 633e82d4 d91b1205 aadaf256>;
        "particle/qianshuitingqipao.png" = <553199b1 3f1abc11 775bfcdc eda5a674 590364f0>;
        "particle/qiaoya1.plist" = <e50da7eb 9aefd405 e82c429a 2c41aeae de075fba>;
        "particle/qiaoya1.png" = <543f2849 47a2339d 2a4b36ff 9c4ef71f 4ebcf7c3>;
        "particle/quanpin.plist" = <8b542d22 f748f36d a708d01c 709c75f3 9d59b2e0>;
        "particle/quanpin.png" = <2a0c7978 09abe2f5 77c04ff8 9e65b715 e9668faa>;
        "particle/shan_li_zi.plist" = <5cb7bcf7 68650b53 b8dda856 390fc7a8 0cdd1610>;
        "particle/shan_li_zi.png" = <eee617f4 c24b8dfc 2db17d4e d22b3718 f5d151c2>;
        "particle/shoulijian.plist" = <49937550 c56d6ed3 51835e83 a0dea8a2 a60d1562>;
        "particle/shoulijian.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/shousuo.plist" = <85c23f42 b1bff4d7 74104700 36bd8143 5bada725>;
        "particle/shousuo.png" = <3a279d7e 534b7a6d f1bf7fa7 62339326 f563559e>;
        "particle/shuxianlizi01.plist" = <20cfd748 018c46c4 705da861 d4a803d6 4c964200>;
        "particle/shuxianlizi01.png" = <fea870b0 5a80538c e5a48fae 6a363b29 7ff6def7>;
        "particle/shuxianlizi02.plist" = <d0f6dd6a 7c5d716b 9815e96f ed7f0a2e a73b17d4>;
        "particle/shuxianlizi02.png" = <82987ca2 a990ad4d 9dfd8fb0 65a9e90e 0ed4e43f>;
        "particle/sjlizicaise.plist" = <7d55ca31 55deef78 23fb0acd b64955b1 2f267aad>;
        "particle/sjlizicaise.png" = <a250c77e 6dfa2d01 f78f99a1 7bf3b04a 702b1d2f>;
        "particle/sjlizipaopao.plist" = <888d0f4b fd462d7b 8e6aa71c a883dc33 32ae6ca2>;
        "particle/sjlizipaopao.png" = <d4e5ff5a 27248f47 5b9b2343 50b7f91d 027046ed>;
        "particle/sjlizixingguang.plist" = <8a1d5295 78bb0668 88eed5e8 6f8ffa2a 82f360c8>;
        "particle/sjlizixingguang.png" = <15c53d63 cce55461 f0c8afbe 44a2f0a0 2027fc33>;
        "particle/sjlizixingxing.plist" = <2220044e 2041d2ed f15e4e58 79181517 4bc923d7>;
        "particle/sjlizixingxing.png" = <9cbddcaa 15c2828c 5565c7e8 7f5ff75d a4cdd25f>;
        "particle/sjliziyuandian.plist" = <a056c013 66b75f63 dd663416 c0eb2a7c cf938a03>;
        "particle/sjliziyuandian.png" = <bf80cc63 67f6b61a c046ab3f a0e9b070 b010f9e7>;
        "particle/slot_coin02.plist" = <0264e336 5d15a8c9 a6dbd72e b8f6cb34 0fdd66f2>;
        "particle/slot_coin02.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/slot_paper01.plist" = <2c916534 d7e6b038 d64b9f12 cc707898 1bd9019e>;
        "particle/slot_paper01.png" = <7fbeadcf 18b79ebd 05e1d682 7e54488f 70f8fa4a>;
        "particle/slot_paper02.plist" = <4a865778 730d84f5 79029300 e88c4ee2 e00e6b08>;
        "particle/slot_paper02.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/standard_circle.plist" = <3a30ef46 3cb65f50 48f12ae5 ba6ba127 6be726fd>;
        "particle/standard_circle.png" = <8ffa3814 d90de1a3 cd29d7c9 f0813442 b66d4270>;
        "particle/star.plist" = <a695f157 33f94566 8038a25f 6f4b882f b68abf84>;
        "particle/star.png" = <2801bc6d 388d4d58 24879fa2 8d80eac1 72b29bc0>;
        "particle/star1.plist" = <e8d415bc 843d04a8 0de590bb ff74606f 07613b36>;
        "particle/star1.png" = <4d2f0904 9b8b68d8 c4a5628f 8b1e4806 76a813ff>;
        "particle/star2.plist" = <6bdeb02d abbaf6b0 1d4f2a12 3f163c46 467f046f>;
        "particle/star2.png" = <ba9d4169 fca8ddea 8e9bbae8 121b2622 b45a23f2>;
        "particle/star2_icon.plist" = <c5030737 2c86c1c8 a1c23160 dad16e9c 1eb82fe2>;
        "particle/star2_icon.png" = <67032fe2 e47fe639 88ffe6fb 75d31353 1563310f>;
        "particle/star_fanpai.plist" = <6ac75dfe 1f21a47f 7bdd6b0a 5b7b4faa e2ad39fb>;
        "particle/star_fanpai.png" = <8cb440f1 e9d3d44a 5ac00097 f9399c17 1a7d87b3>;
        "particle/star_icon.plist" = <ce56f6cc bb107db9 12195224 c96ebf10 37def000>;
        "particle/star_icon.png" = <67032fe2 e47fe639 88ffe6fb 75d31353 1563310f>;
        "particle/submarine_change.plist" = <d741cad2 875d2b36 a1e3280c f38633b5 0667d0f6>;
        "particle/submarine_change.png" = <87494852 02c1f295 bacab7ee 1caf1523 2982bbb5>;
        "particle/tbblue.plist" = <b3ceacdd faa68e8b df8f9cfd 25bee4ea 85c87959>;
        "particle/tbblue.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/tbgreen.plist" = <43838b36 04144c84 99229112 0ecc8a61 1eab8d38>;
        "particle/tbgreen.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/tborange.plist" = <9fce9447 df13aaa0 7cd4e366 9615e326 11261f8c>;
        "particle/tborange.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/tbyellow.plist" = <a753eb25 caf668dd 70952eb3 79033f27 b56ca71e>;
        "particle/tbyellow.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/trigertip.plist" = <22710715 9b79eaf0 717a8e70 2c733f47 3d9f4d06>;
        "particle/trigertip.png" = <b6e9426e d1736424 25061f6e 6da9f303 e5059f58>;
        "particle/tuowei.plist" = <10864114 92391506 60ab2c3a 05ec4e9c b471a62a>;
        "particle/tuowei.png" = <002ad3c2 f37b100b f25fafd1 375e77d3 d1c12d9a>;
        "particle/xialuo_li_zi.plist" = <989cd2ee 0c0c64d2 3b386913 38ef150b 236bd6e2>;
        "particle/xialuo_li_zi.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/xiaolizi.plist" = <2cc2923c 9422176f 02885145 029c8fb8 9f2d7ab4>;
        "particle/xiaolizi.png" = <fbec9d1a ee7edc27 89478412 9a855f92 cb5850be>;
        "particle/xingxinglizi.plist" = <f8ce8c24 1140c009 da3d8d6e 42090574 72f5987c>;
        "particle/xingxinglizi.png" = <fea870b0 5a80538c e5a48fae 6a363b29 7ff6def7>;
        "particle/xingxinglizi_loop.plist" = <58721e87 c69c5873 aa02b8fc 3295f19d e5df19d4>;
        "particle/xuehualz.plist" = <dc7b690c 74768c6e d3028c7d 7658ea83 ba72fdc3>;
        "particle/xuehualz.png" = <d615cb73 854e54b3 c05b671a 9a2ed973 85ccf347>;
        "particle/yanhua.plist" = <a9126d03 5245924b 26504bbc 58f1475c 6ccd7101>;
        "particle/yanhua.png" = <002ad3c2 f37b100b f25fafd1 375e77d3 d1c12d9a>;
        "particle/yanhua_huang.plist" = <a80454ea b3322ca0 edbfa6c5 ee6758f9 c9b57361>;
        "particle/yanhua_huang.png" = <6169df92 8ac94d48 9b008829 e937e89e 39696789>;
        "particle/yanhua_l.plist" = <b508d418 0af82a51 cab32fa8 9fd31675 de214a90>;
        "particle/yanhua_l.png" = <b924c74d 6c42b147 c6617f93 5541e6ba 66111862>;
        "particle/yanhua_lan.plist" = <86c8177c 466e753a 2fbd556f c4719a49 91145c86>;
        "particle/yanhua_lan.png" = <b924c74d 6c42b147 c6617f93 5541e6ba 66111862>;
        "particle/yanhua_zi.plist" = <918c94e5 397a930e be7e2d37 b30bc2f1 56a9509a>;
        "particle/yanhua_zi.png" = <6169df92 8ac94d48 9b008829 e937e89e 39696789>;
        "particle/yihaopao.plist" = <b1d93339 8ad19952 de1def0d befdafe7 92c758af>;
        "particle/yihaopao.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/yuchaoshuipaotexiao.plist" = <92256e8f ee61cbc4 8e3ba6eb 84a7b608 48e391dd>;
        "particle/yuchaoshuipaotexiao.png" = <cf0af7dc 478b33f2 20519d9e 0c706f71 16ad0077>;
        "track/add_001.trackbin" = <372f4a1e dccf2178 05733a0a 7cf47dcc 87daa038>;
        "track/add_002.trackbin" = <49daab8c 4c8f70b1 eabe8931 79e3d12d f383af41>;
        "track/add_003.trackbin" = <995050f6 1375ce1c 48f6b6ff 0ebe179c 50bb5855>;
        "track/add_004.trackbin" = <eadda143 4bfb55e5 673d3f72 81bbe7c0 1eccae7c>;
        "track/add_005.trackbin" = <f80c5b0a 0277273a 4293876d 6ee7ff30 8049bfcf>;
        "track/add_006.trackbin" = <610da345 5935f2bf ce3a5002 d8e9d832 82fde8a4>;
        "track/add_007.trackbin" = <ffbd0a06 c93f38cc 94cbc7ec 2810dc6c 3654938b>;
        "track/add_008.trackbin" = <a5594e5f 7112e79c 28ed9f65 9b57459e 4c390285>;
        "track/add_009.trackbin" = <5a496100 8792e864 0e903034 739df274 f0fb8bf2>;
        "track/add_010.trackbin" = <2a5ae9df 1cb92786 f303e7a7 e0804b2c 2f456e22>;
        "track/add_011.trackbin" = <4faa7e36 64828958 8973d858 78c743ee c190b3ef>;
        "track/add_012.trackbin" = <6b639add 61d223c7 a988e0f9 9f2eb0bc 8ea913e3>;
        "track/add_013.trackbin" = <9d6bc1bd 271c24d0 0acecc92 1343f90c 5fde5fec>;
        "track/add_014.trackbin" = <dfa0e898 d5b831f4 bb01744c 7a8c8b2f db484f3c>;
        "track/add_015.trackbin" = <4fe8b1cc 93c9f0a2 ef2857c4 1aaac8b3 31153c8c>;
        "track/add_016.trackbin" = <f7f4f238 a6366e51 b6a48b5d 0cfc3588 3bc9c8d2>;
        "track/add_017.trackbin" = <6120dea7 6b273a0e 2ec7e5af 9b25dda6 6d94f9bd>;
        "track/add_018.trackbin" = <6bc7eb11 963c0e5b 319cfb9d f418f17b 0e0682c9>;
        "track/add_019.trackbin" = <e4543d75 d636518c cb8d03ff de8b9fb7 32ba9fc9>;
        "track/add_020.trackbin" = <6bf07cb0 29d8d727 6cd3b921 9f4fc60f 1a483caa>;
        "track/add_021.trackbin" = <d790092d cf99dec0 57e13c0f d5aff4e9 f7e62279>;
        "track/add_022.trackbin" = <da2284c3 b076bf32 e97ab755 44d97820 89ddc7f1>;
        "track/add_023.trackbin" = <c9e9a623 0145af70 69354d32 74fa01c8 da4e1e43>;
        "track/add_024.trackbin" = <e0a9ae17 152dec58 0f9b8fe4 d1d6e610 9e934ba4>;
        "track/add_025.trackbin" = <7b4d493f 8c64ec13 57b78c72 2e97d566 7dd4d215>;
        "track/add_026.trackbin" = <4fbf3a0d fe13688b 1d75020b bac56cf2 b66fdd07>;
        "track/add_027.trackbin" = <259c496f 2097c4e7 62fb2399 2dffb362 ed9b4f3c>;
        "track/add_028.trackbin" = <d3d84509 7b1707f7 f4050e12 209684bc 14edf4d8>;
        "track/add_029.trackbin" = <fe76f716 1a182acc 7f49fdfc 88ed7d02 e3d59f69>;
        "track/add_030.trackbin" = <874955b1 e81efdf4 8b2150fd bbaba6b2 be0d2197>;
        "track/add_031.trackbin" = <a092df82 5fe7255b f76ef931 dff1007d 5dc31720>;
        "track/add_032.trackbin" = <6d895fbf 65cfd4f8 47849243 074e8ea1 0726a94b>;
        "track/add_033.trackbin" = <cb7ab371 3ad14d94 3792dcff a4793d54 1effefdf>;
        "track/add_034.trackbin" = <0871a75a 0dc78d09 df3b26c8 3666af2b dffa4b96>;
        "track/add_035.trackbin" = <66d444f3 16e11b92 ab49e097 f1d06461 84b21fda>;
        "track/add_show01.trackbin" = <6be625cc 429ec91b c673ca31 536499c7 a6dfa6cf>;
        "track/add_show02.trackbin" = <74f4d9e1 b742242b 1d89f45f 056fd46d 901d3de5>;
        "track/add_show03.trackbin" = <84e86188 13e1d1b4 3e3471fc ba184c9e 0b62e9e3>;
        "track/add_show04.trackbin" = <b9783bda bc89eaf1 9cc874a3 4a83cf48 1ad6d729>;
        "track/add_show05.trackbin" = <1df6c6c9 c8897552 6f2c88e2 ca56b5c2 149915d8>;
        "track/add_show06.trackbin" = <d3438994 b6a6f03b 0dc5e647 d2091b2c 9f8f262e>;
        "track/add_show07.trackbin" = <9ea13f5f 2335af92 4b863667 79bfb93d a10e929c>;
        "track/add_show08.trackbin" = <9940a144 b2dc18a4 e6b05f6c 42ead891 12e41b5d>;
        "track/add_show09.trackbin" = <45bd9692 d1b822d5 9b3a7925 ada55b61 637ff501>;
        "track/add_show10.trackbin" = <d2558227 fedaf54e fe16fc8f c59bfe1f a85b183c>;
        "track/attack_hetun.trackbin" = <f215b18d 3c817b41 4c62ab0a 7fbf716a 4e42d66e>;
        "track/attack_hetun02.trackbin" = <8d76f20e afcf3780 3935074a b65b4501 c2f81b45>;
        "track/combo.trackbin" = <7eea26b3 c43f5c89 41a6c113 819f5629 8b17a547>;
        "track/combo02.trackbin" = <852b4e84 9b7b4699 25d404b0 bf036de1 6c163f16>;
        "track/combo03.trackbin" = <504dde88 0a3f6664 ae309d76 90bd812a 643e6fe7>;
        "track/deep_fish_01.trackbin" = <a6596210 670dbe77 c441a5eb 4e0fe787 e116f53c>;
        "track/finger_guessing_1.trackbin" = <732f8783 8185c1db 843d2c0f cbc6fef4 8f6e8f74>;
        "track/finger_guessing_2.trackbin" = <d37de33d ccda7cce d19b7797 6383feb2 f1089684>;
        "track/finger_guessing_3.trackbin" = <652a2a0f 1fe99046 3eeda37e 52b6e075 622372a2>;
        "track/gold_fever.trackbin" = <414d4260 d1ff7af9 70bcf3a2 e5feaede 877c556c>;
        "track/goldshark.trackbin" = <942ee0e6 362ecb47 c937636c fd642a0e 47d74dbb>;
        "track/goldshark02.trackbin" = <950ae899 5a7917f0 efe92d6b e3030998 53b9051c>;
        "track/goldshark03.trackbin" = <99106b8f b0734e4c 2d45b123 d4f0ca43 792490ae>;
        "track/goldshark04.trackbin" = <41cf9728 0fb32838 694110c5 d32f3b5d 697264c5>;
        "track/hide_fish.trackbin" = <a1764974 375cd9a8 8f11ff6d a6bb62ff f1c60df6>;
        "track/logo.trackbin" = <59a717d4 13f3ca70 fedd7598 ce3fa128 b43c4b0e>;
        "track/secret_box.trackbin" = <c90cd19b 8cc19f38 00b34a86 e9731bf0 19f63e94>;
        "track/secret_box02.trackbin" = <0ddbe9c3 9c8e4a6a 4cebb0a9 906692b0 7cd1417c>;
        "track/secret_box03a.trackbin" = <56558d58 939df996 36044dce f4e487a7 0c6b6e34>;
        "track/secret_box03b.trackbin" = <d82dfa85 e4c415bc abf9f93a 87527f97 38bab434>;
        "track/secret_box04.trackbin" = <6f83d340 d82a5b11 30a16d04 6afaabba 2bd5a3c8>;
        "track/secret_box05.trackbin" = <b03e6db0 3ca565a9 d904de36 b28da70d d0bf5add>;
        "track/secret_box06a.trackbin" = <9e757ee4 971549ee 622638a6 6d1aadd5 b65fdaf8>;
        "track/secret_box06b.trackbin" = <979615b6 6fcc2bb3 7a6c17cb cf1862ed d4a94519>;
        "track/sharktooth.trackbin" = <416d37fc 5d161de0 18cecd42 3824ac3a 994a8a83>;
        "track/sharktooth02.trackbin" = <75ce7a3c 564e64c2 9585aa41 ac0ab951 960b9c85>;
        "track/sharktooth03.trackbin" = <a12be31a 86eeb124 89f01865 86211517 4173bf15>;
        "track/sharktooth04.trackbin" = <df7e3f9d fe5a9869 dbec42ac 11c1b117 28426f21>;
        "track/slight_denglong011.trackbin" = <d2a7e48d ad5b96a7 fcc9414f 18cbedd3 9c4e6950>;
        "track/slight_denglong012.trackbin" = <d2a7e48d ad5b96a7 fcc9414f 18cbedd3 9c4e6950>;
        "track/slight_denglong013.trackbin" = <4e7dc059 6d14d037 36e7c1df 21bac011 5ddcb4c1>;
        "track/slight_denglong014.trackbin" = <e70cb90a e94fad7e 17d10229 9a800aca 7374ee2f>;
        "track/slight_denglong021.trackbin" = <262ec7ad e741b5cc 1d480f49 65c449f7 4301f39f>;
        "track/slight_denglong022.trackbin" = <81c15f1c 8fc7d550 bef73b68 b9697fe5 d1b3abab>;
        "track/slight_denglong023.trackbin" = <a69789d3 d82797a1 954f8b26 d9bf95b6 66d95718>;
        "track/slight_denglong024.trackbin" = <11d65851 ed28ba35 c9410049 aac6ceaa 7b47b3d4>;
        "track/slight_denglong025.trackbin" = <2f5bebb3 f5dba0af 5b80b96c f321d3eb dda8395b>;
        "track/slight_denglong026.trackbin" = <591dd61d fcd53c9f 6c7f4c23 cff75590 e98090c0>;
        "track/slight_denglong027.trackbin" = <2f5bebb3 f5dba0af 5b80b96c f321d3eb dda8395b>;
        "track/slight_denglong031.trackbin" = <9349d986 a08745ab a056c7b7 839b73de f51cb2fa>;
        "track/slight_denglong032.trackbin" = <5db209b4 53d52d7c ce33ea6c 97b7a848 68813384>;
        "track/slight_denglong033.trackbin" = <4cd98248 ab32a0d3 bc01410b 7b46a89f f8edc817>;
        "track/slight_denglong034.trackbin" = <9e4e8fdb abcffcbb a0fb5337 f46c77bf a1e2e266>;
        "track/start_show.trackbin" = <fe421823 79d8ce03 ea53dce2 d8bc10e0 28ff1130>;
        "track/start_show02.trackbin" = <017a3a0b b01895c4 b879dff5 3dd241e4 cf5db91f>;
        "track/start_show03.trackbin" = <3c0cc755 fe922ecf bce7221b efadd6c2 4222de66>;
        "track/start_show04.trackbin" = <dda46146 fe3be8ed 463efce2 7bc00888 4687f8ff>;
        "track/track01_1.trackbin" = <c0e9e158 9b643217 8da41796 fa238bcc b187df7f>;
        "track/track01_10.trackbin" = <9c46a1be 3b31260a 6781bd62 3e93c62b 83af8f72>;
        "track/track01_15.trackbin" = <de0d86f7 a1f051d6 cd07ccff 61d66d33 8e4fcbc6>;
        "track/track01_150.trackbin" = <511b7f54 efe975fe b857363a f5ef6013 8e27553b>;
        "track/track01_2.trackbin" = <b56a53f1 1da39f91 f6c8c4fb 8f88ad3d d3d2fe08>;
        "track/track01_20.trackbin" = <bc6ea1a7 d5bfcfb3 69436d23 830d7970 77444db8>;
        "track/track01_25.trackbin" = <c47f593a ee0820b2 28303ba7 55aa434e 7dfa3bc3>;
        "track/track01_30.trackbin" = <b6dff283 0b3b1306 d061347f 10afb8dd d52aeb00>;
        "track/track01_5.trackbin" = <9a614c1e 7ee3f35b e787da03 b1bfe53d dd1a5c22>;
        "track/track01_50.trackbin" = <e32afdef 96c8c730 42a71f9c d52bf9be 1b3e4d1e>;
        "track/track01_60.trackbin" = <d22897df 151cd9ef 0b3c66b3 552cc999 cf13dcbc>;
        "track/track01_70.trackbin" = <3960aa67 0c2f4721 2444cdd0 061804d0 cb1e9e65>;
        "track/track01_80.trackbin" = <361f29c9 8ae1721f 69e5d99a d14cebb6 1cad0b73>;
        "track/track01_90.trackbin" = <4b7a2a86 b0161a2d 7a6c9b63 a7ed1b5f c2fa8241>;
        "track/track01_douyu.trackbin" = <b50328ae e133ace6 873ab659 528a919c d0456593>;
        "track/track01_gaiputi.trackbin" = <22261ae7 ad13b724 3aefb563 e240778f 27b0917a>;
        "track/track01_huangjinsha.trackbin" = <7f3db45c 501cbfb4 5908c199 06e524a5 0eb9a1e3>;
        "track/track01_huangjinsha_2.trackbin" = <ecbe906c 84b003c4 b7d5fc1a 0cc3db53 0aa70504>;
        "track/track01_huangjinsha_3.trackbin" = <5c5a3c49 dc6bb1f0 0f62b540 22ae930f 2868fbe5>;
        "track/track01_huangjinsha_4.trackbin" = <5cb29e29 ee5c746e af20f3de 8f07eb34 6f9b7438>;
        "track/track01_huangjinsha_5.trackbin" = <46c018ff 80075ac7 bde18b2e 8045ded7 12f52bb9>;
        "track/track01_huangjinsha_6.trackbin" = <81391ed2 af93c041 893b53db 1192ec48 721de262>;
        "track/track01_hujing.trackbin" = <d7fe4f7e 6d3fe30f 5d13d662 99b232fe 44c7997a>;
        "track/track01_ice.trackbin" = <82186d4a ab5a9e1f dbcfe497 8926984a 93504c7f>;
        "track/track01_light.trackbin" = <b8d2c6ab 29085d81 d704e90a 0b220f6f 227a42f3>;
        "track/track01_red.trackbin" = <2247d208 95d49b8b e32406c1 6ca264eb 0fb20d07>;
        "track/track01_special.trackbin" = <cf0d7c9a 4f6c7faa 3f6d2a89 166123fc 70c0d866>;
        "track/track02_1.trackbin" = <c0e9e158 9b643217 8da41796 fa238bcc b187df7f>;
        "track/track02_10.trackbin" = <6236e272 ddce2edc a00597ae b54f8b16 d3fc8ddd>;
        "track/track02_15.trackbin" = <95762081 f1798aa3 68287c14 adf6e175 22a5568d>;
        "track/track02_150.trackbin" = <511b7f54 efe975fe b857363a f5ef6013 8e27553b>;
        "track/track02_2.trackbin" = <b9178853 82953208 ab335745 698734ad 399e3f69>;
        "track/track02_20.trackbin" = <bc6ea1a7 d5bfcfb3 69436d23 830d7970 77444db8>;
        "track/track02_240.trackbin" = <46e6afd8 90bd74e7 f60e3548 cfc93860 dcd22372>;
        "track/track02_25.trackbin" = <c47f593a ee0820b2 28303ba7 55aa434e 7dfa3bc3>;
        "track/track02_30.trackbin" = <b6dff283 0b3b1306 d061347f 10afb8dd d52aeb00>;
        "track/track02_5.trackbin" = <9a614c1e 7ee3f35b e787da03 b1bfe53d dd1a5c22>;
        "track/track02_50.trackbin" = <e32afdef 96c8c730 42a71f9c d52bf9be 1b3e4d1e>;
        "track/track02_60.trackbin" = <d22897df 151cd9ef 0b3c66b3 552cc999 cf13dcbc>;
        "track/track02_70.trackbin" = <3960aa67 0c2f4721 2444cdd0 061804d0 cb1e9e65>;
        "track/track02_80.trackbin" = <361f29c9 8ae1721f 69e5d99a d14cebb6 1cad0b73>;
        "track/track02_90.trackbin" = <aaa86273 828988fa eb426319 45e4fa75 90e8f3e3>;
        "track/track02_douyu.trackbin" = <25a28c15 db9ba3f4 e2c662a9 b45a10e5 1dadbe8e>;
        "track/track02_gaiputi.trackbin" = <22261ae7 ad13b724 3aefb563 e240778f 27b0917a>;
        "track/track02_huangjinsha.trackbin" = <e2d672b8 77a3d62c 7b0868f2 689069e3 dc0cb81b>;
        "track/track02_hujing.trackbin" = <d7fe4f7e 6d3fe30f 5d13d662 99b232fe 44c7997a>;
        "track/track02_ice.trackbin" = <82186d4a ab5a9e1f dbcfe497 8926984a 93504c7f>;
        "track/track02_light.trackbin" = <b8d2c6ab 29085d81 d704e90a 0b220f6f 227a42f3>;
        "track/track02_red.trackbin" = <2247d208 95d49b8b e32406c1 6ca264eb 0fb20d07>;
        "track/track02_special.trackbin" = <cf0d7c9a 4f6c7faa 3f6d2a89 166123fc 70c0d866>;
        "track/track03_1.trackbin" = <c1d0730f 1424bd1e c9f4c663 1cda49de 5622e927>;
        "track/track03_150.trackbin" = <27e7de42 3acc1812 01a69ae3 d8710f78 f65889d6>;
        "track/track03_2.trackbin" = <4f939672 1e0bdedb 66a35176 b814d726 ba3418b4>;
        "track/track03_3.trackbin" = <9173b4c9 e3f00d76 6d336070 fc16994e c59af71d>;
        "track/track03_4.trackbin" = <2a81209a 71c25735 f71047cc 2936a8f2 cdf5f0bb>;
        "track/track03_5.trackbin" = <024307d5 e941d800 81bc6e46 1bcb8c53 365bf6ef>;
        "track/track03_6.trackbin" = <afea15fd 331c4255 96bc9278 5f529e75 2fe34c4f>;
        "track/track03_7.trackbin" = <fc4d027b f3f95261 a5964117 998d47e5 ab5eaf44>;
        "track/track03_cheqiyu.trackbin" = <b9178853 82953208 ab335745 698734ad 399e3f69>;
        "track/track03_diaoyu.trackbin" = <4ab68196 3e80b922 cc6db172 375da49f 1398b6c6>;
        "track/track03_dinianyu.trackbin" = <c47f593a ee0820b2 28303ba7 55aa434e 7dfa3bc3>;
        "track/track03_douyu.trackbin" = <d15ab332 f3073c20 727d73e0 2b893948 525bdb36>;
        "track/track03_fangyu.trackbin" = <7c671927 ffbd7b7d 5cbb0efa 380f27a3 386f6883>;
        "track/track03_gaiputi.trackbin" = <a9397d8f df9459f1 0e199592 8984b8cf 1649a736>;
        "track/track03_haigui.trackbin" = <d4813eae 0a5f4651 8812532e 0ed7387b f8471c0c>;
        "track/track03_hetun.trackbin" = <b6dff283 0b3b1306 d061347f 10afb8dd d52aeb00>;
        "track/track03_huangjinsha.trackbin" = <e2d672b8 77a3d62c 7b0868f2 689069e3 dc0cb81b>;
        "track/track03_hujing.trackbin" = <d7fe4f7e 6d3fe30f 5d13d662 99b232fe 44c7997a>;
        "track/track03_huoyandouyu.trackbin" = <5125ac94 5b86d098 8bcd7da4 39580dfb 8164f327>;
        "track/track03_jianyu.trackbin" = <20adf8c2 e1bfe9a7 be843574 02cdd6da 164b7d13>;
        "track/track03_jinqiangyu.trackbin" = <45cd9f34 750ab1d3 b5efdd5a a681ff89 ab50dd83>;
        "track/track03_moguiyu.trackbin" = <886b7e68 bc86db17 f761fa3d 0355dd97 e397fef0>;
        "track/track03_shayu.trackbin" = <c79534bc 4b9d11df 3f0e4dc4 6407f6e1 73124a37>;
        "track/track03_shiziyu.trackbin" = <d22897df 151cd9ef 0b3c66b3 552cc999 cf13dcbc>;
        "track/track03_special.trackbin" = <cf0d7c9a 4f6c7faa 3f6d2a89 166123fc 70c0d866>;
        "track/track03_tianshiyu.trackbin" = <d94be08e 12a41773 1c61c33b 1f419a2d 9fe0091f>;
        "track/track03_xiaochouyu.trackbin" = <6236e272 ddce2edc a00597ae b54f8b16 d3fc8ddd>;
        "track/track03_yingzuiyu.trackbin" = <957a0a2a d5f16ea0 1864f748 04a70d2b 99a468d8>;
        "track/track04_1000.trackbin" = <cba6cf8b 6ca2bc49 76424b45 f7a22762 b295ee9b>;
        "track/track_01.trackbin" = <396ab052 66d7806d 316c719b d8023e3d ae18aa3b>;
        "track/yuzhen01.trackbin" = <ba617c92 300ce58e 67fd549e 626fbad8 ec24374b>;
        "track/yuzhen02.trackbin" = <805274b6 8fa42e32 2e3ba997 12842151 a438ab08>;
        "track/yuzhen03.trackbin" = <49d5df49 ed4e1d23 e0459e86 7052aef9 96b8621a>;
        "track/yuzhen04.trackbin" = <87570f4d 6b7ad257 d1d11874 95901050 035c032c>;
        "track/yuzhen05.trackbin" = <77a1f041 a12dcd35 e052441a 46bb7d62 aec7b439>;
        "track/yuzhen06.trackbin" = <25591e24 d9eb7685 957fff4c c025afbb 6a645b1a>;
        "track/yuzhen07.trackbin" = <c79cb8f3 671b63d8 0dda0b7d c9c9cfc7 fa67c3c3>;
        "track/yuzhen08.trackbin" = <9c672368 c4404343 31096a8c f4cfe90c 440e31fe>;
        "track/yuzhen09.trackbin" = <802eb079 df6bd331 687adf66 bcb47012 e4de31b4>;
        "track/yuzhen10.trackbin" = <70f728fb b019a210 1d8f7e19 fb22e854 1298fda6>;
        "track/yuzhen11.trackbin" = <17b85e84 e62c707b 9e6658de 88677a64 c258c64a>;
        "track/yuzhen12.trackbin" = <9bcaade1 090dff06 074840c6 e7b852aa 3fae0099>;
        "track/yuzhen13.trackbin" = <bf63148d 15d28669 8700cdc7 602fa943 96aebd46>;
        "track/yuzhen14.trackbin" = <8b9ce19c bf7d43be f8c1a1e6 ff649f7e 4c91e9ed>;
        "track/yuzhen15.trackbin" = <695fe58a 406e47d6 cb8974d4 b1956683 472f3dd1>;
        "track/yuzhen16.trackbin" = <e5d285d5 96fdf91f 4c060276 2a54b45c 3704b24b>;
        "track/yuzhen17.trackbin" = <fec6ba37 f0ab657a 88646cb7 66ed2210 96edc164>;
        "track/yuzhen18.trackbin" = <c409f1a0 fb361edf a8538078 f07b5783 8e91993f>;
        "track/yuzhen19.trackbin" = <aafb6e02 547155ec 1e50f4fc 2b537c8d 04939dba>;
        "track/yuzhen_red.trackbin" = <7b8ff663 83c4fa62 64088c8c 4529cbd9 3a2233d0>;
        "zh-Hans.lproj/ShareSDKLocalizable.strings" =         {
            hash = <f7d9266f 84e2c0b1 7f5564b3 3a41ef42 7816879f>;
            optional = 1;
        };
    };
    files2 =     {
        "3d_ios/common/debug.material" = <676dca5d 4e67772d 596b6913 d223a4a9 4b70538f>;
        "3d_ios/common/default.material" = <a62b88c1 8a1cf174 7daf8e4a 2172c5ae c482cd19>;
        "3d_ios/common/default.pvr" = <7b4638de 44f6d6eb 35f715b9 03f2b145 f304cda7>;
        "3d_ios/common/default_skin.material" = <fa0c7f4e 7b6726a0 5f0d17ab 7116ce17 c52ed764>;
        "3d_ios/common/particle/particle.material" = <bc9a2aed dc87a2a9 b4e24a64 f1401d59 f7b0dd5e>;
        "3d_ios/common/particle/particle_tex.material" = <5bec9cd3 139ba706 94474e3f 047dad54 2c0b4a2a>;
        "3d_ios/config/effect.config" = <28d8c800 b219e0c7 e5076fe1 dc9f87be 923e5b00>;
        "3d_ios/effect/caustics.pvr" = <6fc91016 1b1d505e 63b6247b 2f67c5bc d3634bc8>;
        "3d_ios/fish/beijingyu/beijingyu.cbx" = <86360562 fc2ca400 2f515742 d38d49b9 b6963716>;
        "3d_ios/fish/beijingyu/beijingyu.ckb" = <0a74a536 bc8d1eb3 7694ba29 d8de8797 70ab8c5e>;
        "3d_ios/fish/beijingyu/beijingyu.pvr" = <2d000108 4cb4d803 17fd626f 846d211e 97b96fd9>;
        "3d_ios/fish/bianfuyu/bianfuyu.cbx" = <295f4e7d b14a19d6 e55c5e1a cad9dadb b2d8ca78>;
        "3d_ios/fish/bianfuyu/bianfuyu.ckb" = <4100b360 ea1c7987 89644a56 654b1a8c 67615dd9>;
        "3d_ios/fish/bianfuyu/bianfuyu.pvr" = <892198d9 a1862cb0 dbeca346 d46c94a6 682cf404>;
        "3d_ios/fish/bianfuyu/bianfuyu_gold.pvr" = <e896cc3f 6f42cc0d 321e2f3a 95fb3e22 57230b9e>;
        "3d_ios/fish/cheqiyu/cheqiyu.cbx" = <8f369749 af20fb4b 34826ec8 c69179c3 68ac02fd>;
        "3d_ios/fish/cheqiyu/cheqiyu.ckb" = <22a38241 e56cc27f 40bb7d70 5ffff0fc 3364f9cc>;
        "3d_ios/fish/cheqiyu/cheqiyu.pvr" = <266b1534 49a54c9e df0fcaa5 bbbd9a49 0a6352f5>;
        "3d_ios/fish/denglongyu/denglongyu.cbx" = <3432173e a75aa639 48a3e395 79615d47 f3ab41a8>;
        "3d_ios/fish/denglongyu/denglongyu.ckb" = <4642d501 6b4d17cf 401cf6e9 14627288 ce391fae>;
        "3d_ios/fish/denglongyu/denglongyu.pvr" = <ad743f7b f94c4088 98c5fdaf d880e4c2 118c0245>;
        "3d_ios/fish/dianmanyu/dianmanyu.cbx" = <e881a2fb fc4bd597 1645968b 361bc127 ac51d0ce>;
        "3d_ios/fish/dianmanyu/dianmanyu.ckb" = <fc31b30e 4d84553d 372206df 12852e77 1cf79eb9>;
        "3d_ios/fish/dianmanyu/dianmanyu.pvr" = <4420b0bc 56243c03 0723c032 87083b7f 607dc1dc>;
        "3d_ios/fish/dianmanyu/shandian1024.png" = <f3d10427 bd4d854d ea42379d 0c22ce39 245c9998>;
        "3d_ios/fish/diaoyu/diaoyu.cbx" = <73404428 bd157c3a 606ae238 ce7e92c1 247fac05>;
        "3d_ios/fish/diaoyu/diaoyu.ckb" = <73b373bb 8675f37c ebd0892a 308f6b9b cbad84ea>;
        "3d_ios/fish/diaoyu/diaoyu.pvr" = <543d9d2f 45029d41 478f8da8 62854313 a674cf11>;
        "3d_ios/fish/dinianyu/dinianyu.cbx" = <baf19c62 286fdc72 f8b103b7 54e843f2 2f0e0765>;
        "3d_ios/fish/dinianyu/dinianyu.ckb" = <214465b0 5ef32768 97aa05db fa4bed1b 80ae0dec>;
        "3d_ios/fish/dinianyu/dinianyu.pvr" = <07e1efcb db722382 50c0ce42 9a075a68 e7b16e91>;
        "3d_ios/fish/douyu/douyu.cbx" = <14d9cdfc e6277d5f b183709e 24fffdc2 846f64e2>;
        "3d_ios/fish/douyu/douyu.ckb" = <69e1b654 357f6591 ddd28215 16eec871 de516f56>;
        "3d_ios/fish/douyu/douyu.pvr" = <a81be35e 5f1ad330 81b98c2f 98470743 893bc548>;
        "3d_ios/fish/douyu/douyu_huo.pvr" = <e4ca688b ffe6c888 67b8ad6e 2849e436 0807920d>;
        "3d_ios/fish/fangyu/fangyu.cbx" = <ca80df67 35e1cc78 4abd4e7c 3f223f8f 964b2784>;
        "3d_ios/fish/fangyu/fangyu.ckb" = <0fb4a3d6 ca97315b 6d2c0dfb b3bb79e0 441cdc9e>;
        "3d_ios/fish/fangyu/fangyu.pvr" = <4ccff637 3683fe19 f5d157f3 cd771545 da92d2ad>;
        "3d_ios/fish/gaiputi/gaiputi.cbx" = <8c2592af 437fa9a1 f1d5ef5a d4a7a16c 2d51e4de>;
        "3d_ios/fish/gaiputi/gaiputi.ckb" = <890b794c d4ac883d 70be6480 59654e88 21a7532f>;
        "3d_ios/fish/gaiputi/gaiputi.pvr" = <431b06d4 acfdbd52 a3a9baac 06777026 6f3f4e71>;
        "3d_ios/fish/haigui/haigui.cbx" = <97e073b5 77abb5e2 9f2f0669 552c81f6 41984185>;
        "3d_ios/fish/haigui/haigui.ckb" = <c77bdaf7 b588b0f0 b0b05d88 85f1f019 6f875ba1>;
        "3d_ios/fish/haigui/haigui.pvr" = <be48182b f72ad634 de1545eb e00e112a 29d685ae>;
        "3d_ios/fish/haigui/haigui_ice.pvr" = <b05392fa 655a235b e04e4915 a1523665 b5de7632>;
        "3d_ios/fish/hetun/hetun.cbx" = <d842c4fc 0a478f65 551c6d8d 95dcf22b 6a606551>;
        "3d_ios/fish/hetun/hetun.ckb" = <622836d4 b0b1a734 3fc7a4e9 130ef6e7 9ceeb298>;
        "3d_ios/fish/hetun/hetun.pvr" = <f3446519 9220d51e f24232c5 9ecbe733 e02a1520>;
        "3d_ios/fish/hujing/hujing.cbx" = <9ecaf23d 792cd616 0737a098 e5c17e8c 3624bfe4>;
        "3d_ios/fish/hujing/hujing.ckb" = <f01defbd 0369af9b 0f4696e6 8cd7a0b2 3c8b644b>;
        "3d_ios/fish/hujing/hujing.pvr" = <ac3b4d3e e18cc447 bb15d2b4 12b86443 6cc93085>;
        "3d_ios/fish/jianyu/jianyu.cbx" = <7faa5ecb 3e967744 b01e3402 98fa4d71 dada016f>;
        "3d_ios/fish/jianyu/jianyu.ckb" = <24185343 16dc6e8f 669403f7 9902706a 3bc1a381>;
        "3d_ios/fish/jianyu/jianyu.pvr" = <7d6b3461 6e2e9693 3da3b17f b12bf883 5c137793>;
        "3d_ios/fish/jinqiangyu/jinqiangyu.cbx" = <32a4d9ca 2f6c05b0 c74443ec 61ee8c4e 90e3a789>;
        "3d_ios/fish/jinqiangyu/jinqiangyu.ckb" = <d3416e0b 0f6bb2d8 2bfc0757 80d7cd4d ec34bc03>;
        "3d_ios/fish/jinqiangyu/jinqiangyu_gold.pvr" = <0cf7b755 338d7604 29745f46 021a364a c696d5bd>;
        "3d_ios/fish/jinqiangyu/jinqiangyu_green.pvr" = <77c6ae79 540f8bc1 f7d9ab5d cd37d3f2 f8fb1565>;
        "3d_ios/fish/jinqiangyu/jinqiangyu_red.pvr" = <c20b97ed 32923c47 8886e061 483700e7 602fddbe>;
        "3d_ios/fish/shayu/box.ckb" = <5ae3f5e4 937edc84 e98e6219 791198ce 3faedc9e>;
        "3d_ios/fish/shayu/shayu.cbx" = <bbbb7580 f97eb70f b777f5ac 4de6d02b c8bdba10>;
        "3d_ios/fish/shayu/shayu.ckb" = <1468af16 624f1b80 c1a75f04 ee3490a5 97a0b5be>;
        "3d_ios/fish/shayu/shayu.pvr" = <01e7dda4 5206e504 993358cd 787eb91d 4fc5ae2c>;
        "3d_ios/fish/shayu/shayu_box.pvr" = <36715a84 57b08f61 300dcee0 8dfa8297 95209da0>;
        "3d_ios/fish/shayu/shayu_j2.pvr" = <83932081 9e969d93 9de7a140 5c1e7747 1a6d0725>;
        "3d_ios/fish/shayuya/shayuya.cbx" = <82b84333 ecdfd15f c83c1b70 06366ea0 9c7b0bce>;
        "3d_ios/fish/shayuya/shayuya.ckb" = <341a5692 a84e4205 ccc92ef5 9209a3ff 4c738fe3>;
        "3d_ios/fish/shiziyu/shiziyu.cbx" = <fcc92712 99d27ad0 aaf65bfa 7abc30b3 703bc6d3>;
        "3d_ios/fish/shiziyu/shiziyu.ckb" = <b2b9e6b8 c106c673 39d96c19 cecbe6d5 b24b242a>;
        "3d_ios/fish/shiziyu/shiziyu.pvr" = <086cded0 864d8b63 b60ad7fd 68f603b1 10d7f8ab>;
        "3d_ios/fish/tianshiyu/tianshiyu.cbx" = <b7d8ae1a 18b2e847 042f4c0e 4358b07e 022fb6c0>;
        "3d_ios/fish/tianshiyu/tianshiyu.ckb" = <e14b870c 0e097c79 17ba0717 58f549ff db93ce63>;
        "3d_ios/fish/tianshiyu/tianshiyu.pvr" = <c26e707c 27b4ba95 c42e4a1d de3fc196 cc8a2893>;
        "3d_ios/fish/xiaochouyu/xiaochouyu.cbx" = <81ed3b79 b414d902 80c463ba 691fb17e 901403f6>;
        "3d_ios/fish/xiaochouyu/xiaochouyu.ckb" = <a782983e c9e0de0c 80565511 9146848c 39943d10>;
        "3d_ios/fish/xiaochouyu/xiaochouyu.pvr" = <17be4eac 8314b951 25f57a55 2db044f5 e555e59b>;
        "3d_ios/fish/xiaohuangyu/xiaohuangyu.cbx" = <de8a2f93 fd30af95 fc82515f 565e62f0 0ed661c0>;
        "3d_ios/fish/xiaohuangyu/xiaohuangyu.ckb" = <8ac29166 69031a25 4e8837ef ac3771dd 5aa63d3c>;
        "3d_ios/fish/xiaohuangyu/xiaohuangyu.pvr" = <a376956e da3bc0fd fd0f6cbd 3246a54f 919ab635>;
        "3d_ios/fish/yingzuiyu/yingzuiyu.cbx" = <1be5fbc9 fab41e14 d7fc457d 65ec1b44 9906f629>;
        "3d_ios/fish/yingzuiyu/yingzuiyu.ckb" = <d556f6ed 8fea2de0 3f5d1921 596c9a5d 55b65336>;
        "3d_ios/fish/yingzuiyu/yingzuiyu.pvr" = <67950f52 8f30903c f42fb6fd 42da498c 37dbc2c6>;
        "3d_ios/fish_low/beijingyu/beijingyu.cbx" = <86360562 fc2ca400 2f515742 d38d49b9 b6963716>;
        "3d_ios/fish_low/beijingyu/beijingyu.ckb" = <0a74a536 bc8d1eb3 7694ba29 d8de8797 70ab8c5e>;
        "3d_ios/fish_low/beijingyu/beijingyu.pvr" = <ab784066 844f99ab 6a106eda 2951b2e4 e37edc83>;
        "3d_ios/fish_low/bianfuyu/bianfuyu.cbx" = <cc0154b7 96106497 6fc7f7b8 d2d7caeb b2611b92>;
        "3d_ios/fish_low/bianfuyu/bianfuyu.ckb" = <8939f2bf f95a21c0 15b1f2fa 1b48dd3a 1e6ae264>;
        "3d_ios/fish_low/bianfuyu/bianfuyu.pvr" = <ce3d1044 40813e2a 55cd3d38 be0babac e3103983>;
        "3d_ios/fish_low/bianfuyu/bianfuyu_gold.pvr" = <ff6e487a 90136f52 499702be 56a90a3f e78901fc>;
        "3d_ios/fish_low/bianfuyu/bianfuyu_green.pvr" = <ca39bc29 18a55026 975bbe39 313829a5 33e1e000>;
        "3d_ios/fish_low/cheqiyu/cheqiyu.cbx" = <314de387 13bd04f8 858ea6f2 ecec5479 ce296d7c>;
        "3d_ios/fish_low/cheqiyu/cheqiyu.ckb" = <af3df271 ed3aa25c f728ace8 41130111 547fb2da>;
        "3d_ios/fish_low/cheqiyu/cheqiyu.pvr" = <88ec6169 7eec57ca 4c62fc86 93778712 ba4fe229>;
        "3d_ios/fish_low/denglongyu/denglongyu.cbx" = <a084af39 43f61d1e 6e24b6ef b8a4f409 f78b3f61>;
        "3d_ios/fish_low/denglongyu/denglongyu.ckb" = <062db9a8 f118a69e 63109e23 923d2363 9ed65a97>;
        "3d_ios/fish_low/denglongyu/denglongyu.pvr" = <01ce8472 c1a97851 f93b49bc ff1e45a0 2fa481bc>;
        "3d_ios/fish_low/dianmanyu/dianmanyu.cbx" = <fc907769 86354996 c7da4704 527b6454 37f53198>;
        "3d_ios/fish_low/dianmanyu/dianmanyu.ckb" = <2c19a2e0 bba081f8 307c1c4b df499dc1 deec398f>;
        "3d_ios/fish_low/dianmanyu/dianmanyu.pvr" = <48ad184d 25c49e27 78b72ca3 3d107659 f3a4d6f2>;
        "3d_ios/fish_low/dianmanyu/shandian1024.png" = <0040747f b29bf67b e14b5bdd d8062f44 18875356>;
        "3d_ios/fish_low/diaoyu/diaoyu.cbx" = <cee7cee6 4beaa00b dcd68de4 dcf318c3 4bbe0bd4>;
        "3d_ios/fish_low/diaoyu/diaoyu.ckb" = <6fe4f12d 3ac60096 092fd471 962e9288 4b863ed8>;
        "3d_ios/fish_low/diaoyu/diaoyu.pvr" = <111cf4a3 86f29bb9 6c63bee3 6e3a313f 2ff0732f>;
        "3d_ios/fish_low/dinianyu/dinianyu.cbx" = <84332d5b 909e374e 16c2dbde 9e3542f9 2ec04cf6>;
        "3d_ios/fish_low/dinianyu/dinianyu.ckb" = <60490765 721db631 5a421213 e1f422fb 3da960ef>;
        "3d_ios/fish_low/dinianyu/dinianyu.pvr" = <bc9da009 78c2703b ad4622de 3abacb98 b7a64214>;
        "3d_ios/fish_low/douyu/douyu.cbx" = <7833e330 efe15792 7341519b 3fa89722 a9a29027>;
        "3d_ios/fish_low/douyu/douyu.ckb" = <d7ef9d1a 067e3ed6 28510097 8381bfe2 439b5e3c>;
        "3d_ios/fish_low/douyu/douyu.pvr" = <ef8b0e6b b0eb6f68 4e6e7156 635d3ac8 ce448cb7>;
        "3d_ios/fish_low/douyu/douyu_huo.pvr" = <739182ef b0b674e8 91ca1a27 26b4d48e 8b800d96>;
        "3d_ios/fish_low/fangyu/fangyu.cbx" = <f9cd7216 1fa33ff9 143375c0 eeeb8089 d2e319bc>;
        "3d_ios/fish_low/fangyu/fangyu.ckb" = <6f98119c f3c45084 d5e0e2f1 50e36761 8bbcdd91>;
        "3d_ios/fish_low/fangyu/fangyu.pvr" = <68b0026c aee32e9e fd0ebd08 c52eac7e 8896f376>;
        "3d_ios/fish_low/gaiputi/gaiputi.cbx" = <500af12d 8b65c8cf 7b40bc48 2b92d649 25a0f4d6>;
        "3d_ios/fish_low/gaiputi/gaiputi.ckb" = <853006e9 eecac006 7ba0e881 99db3c3b dd810a5a>;
        "3d_ios/fish_low/gaiputi/gaiputi.pvr" = <d6612e0b b40fa6d9 8a3fcd2b 0228b041 63346277>;
        "3d_ios/fish_low/haigui/haigui.cbx" = <c2e7516b 542d8d4b 4ac110a3 eeeaae6e 2c204d13>;
        "3d_ios/fish_low/haigui/haigui.ckb" = <da818299 2d567b0f cb4fc869 f454235f 11d39503>;
        "3d_ios/fish_low/haigui/haigui.pvr" = <3a900de1 96e9698b a1aab11c 8228d085 1f2a7167>;
        "3d_ios/fish_low/haigui/haigui_ice.pvr" = <0151b3eb 2c9d2cb0 d4172447 1e5a8897 3f87ef19>;
        "3d_ios/fish_low/hetun/hetun.cbx" = <42f04368 f743eeeb 64a5ffc9 2f4132e6 01fdd429>;
        "3d_ios/fish_low/hetun/hetun.ckb" = <38d500a3 3adde7ef 6da70d16 d6218614 3e5c7107>;
        "3d_ios/fish_low/hetun/hetun.pvr" = <33e0ad70 301087a4 3c938c76 a2b61b6d d2d8716b>;
        "3d_ios/fish_low/hujing/hujing.cbx" = <9ecaf23d 792cd616 0737a098 e5c17e8c 3624bfe4>;
        "3d_ios/fish_low/hujing/hujing.ckb" = <f01defbd 0369af9b 0f4696e6 8cd7a0b2 3c8b644b>;
        "3d_ios/fish_low/hujing/hujing.pvr" = <20d61210 f2b8ac2f c5022e94 65c695da 01b9acef>;
        "3d_ios/fish_low/jianyu/jianyu.cbx" = <1ef0ad56 67d98fbb 186ce510 25d2cf83 068e9e0d>;
        "3d_ios/fish_low/jianyu/jianyu.ckb" = <02535801 f1442b34 712d3413 ef28ec73 6987bc91>;
        "3d_ios/fish_low/jianyu/jianyu.pvr" = <589e2a87 2972761b 185da25a ea58f31c bc63e03d>;
        "3d_ios/fish_low/jinqiangyu/jinqiangyu.cbx" = <0c5f4d66 6b891839 2a9b06e6 62d330e3 2ba061eb>;
        "3d_ios/fish_low/jinqiangyu/jinqiangyu.ckb" = <d25f56e0 0467a57a 848dff1f b1ea6ee8 11fad430>;
        "3d_ios/fish_low/jinqiangyu/jinqiangyu_gold.pvr" = <adf8ba8e 6e365cfa 3671682d 59fa2b82 908aaf6b>;
        "3d_ios/fish_low/jinqiangyu/jinqiangyu_green.pvr" = <6def8cd5 8187a342 d89b9dcf 8c531f5f 58565459>;
        "3d_ios/fish_low/jinqiangyu/jinqiangyu_red.pvr" = <c8d092b8 d83b3b48 74d29cc0 0631079e 1caa0318>;
        "3d_ios/fish_low/shayu/box.ckb" = <5ae3f5e4 937edc84 e98e6219 791198ce 3faedc9e>;
        "3d_ios/fish_low/shayu/shayu.cbx" = <8b4dda5d 807bd7ed e10bc118 6069eb52 b9e00864>;
        "3d_ios/fish_low/shayu/shayu.ckb" = <d2778409 d4424677 8bccf4be 16b30019 215f4e44>;
        "3d_ios/fish_low/shayu/shayu.pvr" = <7d071977 269156f7 40c7038b 83f8b652 2897aadd>;
        "3d_ios/fish_low/shayu/shayu_box.pvr" = <36715a84 57b08f61 300dcee0 8dfa8297 95209da0>;
        "3d_ios/fish_low/shayu/shayu_j2.pvr" = <0057eaa4 985964ce 3a6dbcf3 8930095e 8a62f97a>;
        "3d_ios/fish_low/shayuya/shayuya.cbx" = <ae87e56f 11a88d1c 478b8cf8 bad41704 a1f0458a>;
        "3d_ios/fish_low/shayuya/shayuya.ckb" = <81178565 509fe9f4 2636ca19 a60a697e f9e296ef>;
        "3d_ios/fish_low/shiziyu/shiziyu.cbx" = <f049d74e 9ade4654 1b39c2c3 71978714 7b13d650>;
        "3d_ios/fish_low/shiziyu/shiziyu.ckb" = <03c2f839 618a3cee 97d495b8 c7a77397 0d628163>;
        "3d_ios/fish_low/shiziyu/shiziyu.pvr" = <1a1f23a1 95f14352 6dd78c2d 97cfbb9f b3c9b088>;
        "3d_ios/fish_low/tianshiyu/tianshiyu.cbx" = <5c8e5ca5 ae0f86a7 9320c1b9 d8cf1ad9 810f99bc>;
        "3d_ios/fish_low/tianshiyu/tianshiyu.ckb" = <69b1b00e 2968814e b1148471 9bd6c498 52e22d5d>;
        "3d_ios/fish_low/tianshiyu/tianshiyu.pvr" = <42f49f04 78262caf 5bdfba29 92e08296 cdf0985f>;
        "3d_ios/fish_low/xiaochouyu/xiaochouyu.cbx" = <005e0bee 8cf71d69 4efddf6d dda2fafd aafee2d3>;
        "3d_ios/fish_low/xiaochouyu/xiaochouyu.ckb" = <bc5c60b5 6fd44cff 082060af 2c19ea98 ed3c7ab7>;
        "3d_ios/fish_low/xiaochouyu/xiaochouyu.pvr" = <c8bfe5a4 40dc3e55 132ba01c 0e2c16ee b7f88386>;
        "3d_ios/fish_low/xiaohuangyu/xiaohuangyu.cbx" = <5eecc0b4 9fa4e0f4 1fae05da 53d6c1e5 98fb1189>;
        "3d_ios/fish_low/xiaohuangyu/xiaohuangyu.ckb" = <62b163f4 533cc13a 15974123 5df206f3 5cb4a173>;
        "3d_ios/fish_low/xiaohuangyu/xiaohuangyu.pvr" = <6852be27 6c073564 eca5f122 d1e0d65a d9d32729>;
        "3d_ios/fish_low/yingzuiyu/yingzuiyu.cbx" = <475d48d2 e73a15b7 397ed13a 4847475f 7271304d>;
        "3d_ios/fish_low/yingzuiyu/yingzuiyu.ckb" = <934c1e12 1c98e06c 70f82e6c bbda3f41 d5d3f557>;
        "3d_ios/fish_low/yingzuiyu/yingzuiyu.pvr" = <0ebfa3a5 239c0047 fd719446 05ab205e 096de19d>;
        "3d_ios/material/background.material" = <4f57bc81 76a27bf8 130e3403 ea502065 45ba233a>;
        "3d_ios/material/base.material" = <df587125 6d01a236 7dcc68cf e67c26c7 1418e57d>;
        "3d_ios/material/base_no_anim_light.material" = <1d6ed03f 3103d89a 94c67f03 be13c293 0c3dc1b0>;
        "3d_ios/material/beijingyu.material" = <32d4bf98 1b0d6813 76cced8e bec269a3 13461c92>;
        "3d_ios/material/bublelight.material" = <a61f74a0 e7d37d6a 045f380b 6a3bfaeb 119eafde>;
        "3d_ios/material/bublewarp.material" = <243fbc9c e816fc83 2fb4ad35 b4e1ae9c 7502cd34>;
        "3d_ios/material/deng.material" = <a1581e7f 5cac6428 f7f6c9c1 a2d535bc 03f886a1>;
        "3d_ios/material/dian.material" = <fda79767 fdb3a1e8 ffcc09af 0b28f9e1 ddca7a3e>;
        "3d_ios/material/lightning.material" = <261fbb67 64bbfe3c 25674374 100c3db8 c381b173>;
        "3d_ios/material/lightning_cover.material" = <c98d0c9c 02d4b5fb 89feea5f 20bd0ad7 2e98ecb7>;
        "3d_ios/material/lightning_fever.material" = <69bcaee6 d48eb2e2 f3190f0e d20c6a21 b83986a2>;
        "3d_ios/material/lightning_red.material" = <2d2b4aaf eb5b7b6f 38e522e1 9e9162a8 5b769b93>;
        "3d_ios/material/ocean.material" = <712f92bf aa0ed1bf 4f82ce36 1680bdd7 5b466bf6>;
        "3d_ios/material/ocean_back.material" = <4486ffeb 712c49fc afd57ad0 45e919cf 0e47ec1e>;
        "3d_ios/material/ocean_front.material" = <a39cd1d0 e16cd6ae 90a2a177 f06c2911 371d1b93>;
        "3d_ios/material/ocean_line.material" = <e12ac98a a3b47e57 d04c9487 3d23e068 af41168b>;
        "3d_ios/material/shiziyu.material" = <902ac3da 27d0b0fc 49291c56 7d16f892 ff94a196>;
        "3d_ios/material/shuihua.material" = <44eef142 f75edea3 e4e52022 df4b3936 0689068c>;
        "3d_ios/material/sky_back.material" = <50f0b418 c85f6b11 199fdf50 0c97d1fa 297a427c>;
        "3d_ios/material/water_drop.material" = <f952af46 94bd53d2 d0c4715c e57b3e43 8fc6a2ab>;
        "3d_ios/material/ya.material" = <e653c64c 0867c076 32c5011f 9bd193d6 dc430071>;
        "3d_ios/particles/beijing_shanshoudiandian.particle" = <6a31bd64 a12e4609 4d776028 8584a476 e8515a5a>;
        "3d_ios/particles/beijingchenai01.particle" = <5c5374fd 072930ef 0438a9e8 05cd2fd2 fe7df8f1>;
        "3d_ios/particles/chenai.pvr" = <520f027e d73cce5a 2abdfd48 77899977 bcfa953c>;
        "3d_ios/particles/fever_guangban-1024.particle" = <a5762dc6 8ad31230 7b9c77be 51c31af7 11f694c6>;
        "3d_ios/particles/fever_guangban.particle" = <b26405ce 117f4d09 b022e1b5 d48681f6 28f6da35>;
        "3d_ios/particles/flare05_7.pvr" = <db96047c e918686d 21215ecb 4ed5fdde ea58965a>;
        "3d_ios/particles/guangban.pvr" = <2e0d2cfc d10a13ff 5d632e6a 3c689e6c 15df30c1>;
        "3d_ios/particles/qipao.particle" = <ba1cdaaa 8a754216 cd4ac6a7 ee489e16 aef930d4>;
        "3d_ios/particles/qipao.pvr" = <4b520f2a e223e9bb 5e2642ed e684cb7d 2b7f159d>;
        "3d_ios/particles/qipao_01.particle" = <353e6408 365b14c6 263279c5 096aec4c 450bc4d7>;
        "3d_ios/particles/qipao_02.particle" = <a9835ece 766eb212 c0cdc3f7 73397760 6defce81>;
        "3d_ios/particles/qipao_03.particle" = <b057c341 b9841d61 39923103 4fe7054f 36ec57fe>;
        "3d_ios/particles/qipao_03.pvr" = <edbbe7f9 621b71d5 d34d3086 bc8c1979 42570cb1>;
        "3d_ios/particles/shanshuodiandian01.pvr" = <1aa38d2b 90224d3c 5e4742cb db747cdc 778888e1>;
        "3d_ios/particles/shanshuodiandian02.pvr" = <c43d31b4 4aea4769 26c20738 2b09e25e e641413a>;
        "3d_ios/particles/yuanguang.png" = <9f6e8054 2d55fb9c 9c086be7 87e6d06d 62820013>;
        "3d_ios/posteffect/postprocess.material" = <c53b7f2c 0319a26f 6753eb31 67393aef b52e38c3>;
        "3d_ios/posteffect/postprocess_bloom.material" = <49a43ee2 8aff3be0 0b60ae91 429a4c6e da4f35ef>;
        "3d_ios/posteffect/postprocess_blur.material" = <4203d92f cad17521 e60efa3d 42099c95 f2d4c342>;
        "3d_ios/posteffect/postprocess_color.material" = <104a3460 01bcceca 62626846 f5750454 30b262fd>;
        "3d_ios/posteffect/postprocess_glow.material" = <61bde524 e4e6106e 0ebb3767 5b729ec3 9fe11af3>;
        "3d_ios/posteffect/postprocess_glow_and_point_warp.material" = <1470ed2b d176c4e2 9dc181b1 73a034ce 4835cd7a>;
        "3d_ios/posteffect/postprocess_outline.material" = <46acf144 3c776655 6226362f c24bef44 3812c813>;
        "3d_ios/posteffect/postprocess_scenechange.material" = <9f5d98ab cbeab8c0 b8b2d9cc b72dbe2e a8277b7d>;
        "3d_ios/posteffect/postprocess_scenecopy.material" = <723f464c 10416ecf 1a5540a1 4e40cc99 772778e2>;
        "3d_ios/posteffect/postprocess_spacewrap.material" = <80fe8a5d 2da92d85 1ce9e96c c2f5cebb 1e4001e8>;
        "3d_ios/posteffect/postprocess_vortex.material" = <3c6bc8af e596ba37 cdd4f05b f32d0a0d cf8791a7>;
        "3d_ios/scene/beike/beike.ckb" = <ed92e7f5 0d81458f 94051fb0 5772b8fb 9951f4b7>;
        "3d_ios/scene/beike/beike.pvr" = <a89008ed b98ed843 7d4e98ff 1702cb6c bc16f122>;
        "3d_ios/scene/bublelight/bublelight.ckb" = <c44d2355 0225ae3f d7659f37 7b5705d5 dc3d160f>;
        "3d_ios/scene/bublelight/bublelight01.png" = <d118242c 5510d85e 72acdbb1 004ef7d6 37c9e1df>;
        "3d_ios/scene/bublelight/bublelight02.png" = <7b93b426 ac191acb 48127fb4 121f38ec 5b74d649>;
        "3d_ios/scene/bublewarp/bublewarp.ckb" = <4bbd796a 6a094ab4 c673bb9d 69a4b7a7 bf44396f>;
        "3d_ios/scene/bublewarp/bublewarp.pvr" = <e0d969b9 300d3db2 2ff50d86 67a8742c e1cb50f3>;
        "3d_ios/scene/hua_02/hua_02.cbx" = <86360562 fc2ca400 2f515742 d38d49b9 b6963716>;
        "3d_ios/scene/hua_02/hua_02.ckb" = <329920f9 46b5bc05 4be7f6c2 e8dd4d4d fa44b989>;
        "3d_ios/scene/hua_02/hua_02.pvr" = <aff5ae34 e16be750 beb41709 f9d86d39 6c75b0d0>;
        "3d_ios/scene/lightning/lighting_cover.png" = <57ce4a64 9ebbce51 ff5148ff 969d4d32 18574491>;
        "3d_ios/scene/lightning/lightning.ckb" = <baf48a01 b6cac8a8 f158d44f 3b4584ea aa7d2d6a>;
        "3d_ios/scene/lightning/lightning.png" = <90b88449 98924d2a f3370658 9871fe5f fcb39c74>;
        "3d_ios/scene/lightning/lightning_fever.png" = <f2007a8c 0e68207f 0b10c51d 8c868b8b c136edc4>;
        "3d_ios/scene/lightning/lightning_red.png" = <5005b62a 8e030ffb 463a802a 13cdfef0 c79542ef>;
        "3d_ios/scene/pad_background/pad_background.ckb" = <fa03e89a 74beb75a dfbe02ad c9770262 26c3425a>;
        "3d_ios/scene/pad_background/pad_changjing07_beijing.jpg" = <f82ce325 7b047d3d 9ee0bac9 f576c23c c84c563d>;
        "3d_ios/scene/pad_background/pad_changjing08_beijing.jpg" = <5ce81b04 72c13b1c 2f36f7cc 97b689de 6d15e6f5>;
        "3d_ios/scene/pad_background/pad_changjing09_beijing.jpg" = <c4574154 572602fe f36ebb2e e32096e1 530f27ca>;
        "3d_ios/scene/phone_background/phone_background.ckb" = <601934dc cbcece87 3cd0da95 86651baa bb3c0448>;
        "3d_ios/scene/phone_background/phone_changjing07_beijing.jpg" = <477b3f4b e94f8d40 7b07c4de 54f0f9e8 add1c4eb>;
        "3d_ios/scene/phone_background/phone_changjing08_beijing.jpg" = <bc05662f 4b042901 989df601 e8517c2c 1ac881de>;
        "3d_ios/scene/phone_background/phone_changjing09_beijing.jpg" = <f2b6b327 1fd3e8f6 75a34afa 5f995214 dad28668>;
        "3d_ios/scene/start/ocean_back.jpg" = <1a665f7e ab6d04a1 bd73dd49 c3903d36 9d4380da>;
        "3d_ios/scene/start/ocean_front.tga" = <e91ea5a3 5e08bf60 1e391d7f b48ade55 e586248b>;
        "3d_ios/scene/start/ocean_line.png" = <b2fc3037 0633edd5 41cbf4e7 8d0f7edc 0dc1889d>;
        "3d_ios/scene/start/shuihua.ckb" = <f67a822f dac5086b 83088ed3 6072b178 6ae6d520>;
        "3d_ios/scene/start/shuihua.png" = <19c040c4 98b7a621 c3287030 e2fdffae 249ec250>;
        "3d_ios/scene/start/sky_back.jpg" = <dd940660 b661fda7 5c167286 90b396a0 3cc83504>;
        "3d_ios/scene/start/sky_back1.png" = <74c5bfcb b12d6e77 9fc0e061 4c1d1804 0037bb93>;
        "3d_ios/scene/start/sky_back2.png" = <70d9b1f1 9e613857 ceeca92b eae56e9b 6ea7b425>;
        "3d_ios/scene/start/sky_back3.png" = <d20b9e69 746857f1 14863f5e b7bac701 65ef1796>;
        "3d_ios/scene/start/sky_back4.png" = <c72a6368 cac516e2 dbc927a7 8a3afad7 6b478d1b>;
        "3d_ios/scene/start/water_drop.png" = <65d13c55 52c05d5a e83f0de0 12cfed75 db437ca6>;
        "3d_ios/scene/start/wave_normal.pvr" = <229bd12a 32c99666 742256a1 c0790f3d b52e4099>;
        "3d_ios/shaders/background.fsh" = <5d69c496 372f1f33 27e8823d 7bfa4bc9 4d2a2da1>;
        "3d_ios/shaders/background.vsh" = <c4daf186 3c72ec87 c4c35bbb 977a1958 094789e1>;
        "3d_ios/shaders/debug.fsh" = <fa56e9aa c0cb58f0 bd4dafe5 644fec5e d01eaa57>;
        "3d_ios/shaders/debug.vsh" = <5061d869 fef6b941 1cf0e146 2d66225e 76c8b68f>;
        "3d_ios/shaders/fish_basic.fsh" = <86691f90 d9ac376d be596c61 e8b9c38b 71a785ea>;
        "3d_ios/shaders/fish_basic.vsh" = <b01f03e0 3f7a5a2b 3f2f31ed b0d1e06d a3f51145>;
        "3d_ios/shaders/lighting_cover.fsh" = <5f1a8792 292564c5 87431b20 ed3fcd24 022a6e9e>;
        "3d_ios/shaders/lighting_cover.vsh" = <ced522a9 0676bd5c d0abbdd5 0c8e3aac 92419552>;
        "3d_ios/shaders/line_render.fsh" = <5d70fdd8 3c904b11 2ab5aae3 4b6408cc 2162aa5f>;
        "3d_ios/shaders/line_render.vsh" = <f052d5a6 2dfe6acb 5959e7e6 a93ce9af 3b59b732>;
        "3d_ios/shaders/ocean.fsh" = <8d2d552b 1bdba8af fa64e966 1964b440 b964164d>;
        "3d_ios/shaders/ocean.vsh" = <a31e88ae f1beaae0 880253a4 b16763e3 3a945c9f>;
        "3d_ios/shaders/ocean_front.fsh" = <0a3c8449 dec861f1 4467f687 cfbe37fd c7f137d4>;
        "3d_ios/shaders/ocean_front.vsh" = <cbc69141 e53e3722 1e21a5af 9f4c1971 241942e5>;
        "3d_ios/shaders/paopaoguang.fsh" = <ebbaee88 d984c412 c608b3f4 58a9c421 96c1cfe1>;
        "3d_ios/shaders/paopaoguang.vsh" = <8556937f 7d1c7bff a969ae5f 862e9777 4275ca26>;
        "3d_ios/shaders/paopaoniu.fsh" = <48619e01 f6b583a1 21928aa7 b3b36210 e789a125>;
        "3d_ios/shaders/paopaoniu.vsh" = <867349b7 7b41cad1 d2d1b380 9d63ea0e c191a8da>;
        "3d_ios/shaders/particle.fsh" = <70e2787a beb631fa d249b67e 1052665d 7db8ec9a>;
        "3d_ios/shaders/particle.vsh" = <77a1d591 ac92fb22 cd8635a6 19a1962e b49c0b3e>;
        "3d_ios/shaders/postprocess/postprocess.fsh" = <2c326dbb 896a7c24 5a13bb9d 2664f2b4 db033449>;
        "3d_ios/shaders/postprocess/postprocess.vsh" = <dfa221ef aaa77cae 7db57a61 35615545 871743a0>;
        "3d_ios/shaders/postprocess/postprocess_bloom.fsh" = <440df968 62233f83 ce8813b1 4f01be23 3582cd79>;
        "3d_ios/shaders/postprocess/postprocess_bloom_blur.fsh" = <ef2c537b 03a65d27 dd8606dc 1dd7b959 0b370a34>;
        "3d_ios/shaders/postprocess/postprocess_bloom_extract_bright.fsh" = <977101d7 4fadcc3b 83e87d24 744df36a e69ef239>;
        "3d_ios/shaders/postprocess/postprocess_blur.fsh" = <716d4ff9 263239f2 1af52007 0292cf5f b060ccf9>;
        "3d_ios/shaders/postprocess/postprocess_color.fsh" = <4674a5de b1c9a6d8 c3ddb155 0a7b4bc5 832d3b40>;
        "3d_ios/shaders/postprocess/postprocess_glow.fsh" = <c05eb36e 5ad1ebe2 4a5b8828 043d8c41 5ce73a46>;
        "3d_ios/shaders/postprocess/postprocess_outline.fsh" = <3238b95f b5ad87c5 68963932 5577ac9e a9257725>;
        "3d_ios/shaders/postprocess/postprocess_point_warp.vsh" = <ea86da91 d4b66215 6e878170 ed1e58ef 5f80b083>;
        "3d_ios/shaders/postprocess/postprocess_scene_change.fsh" = <41feab6e bebc2b11 8d934d6c 26f72b03 f85debd5>;
        "3d_ios/shaders/postprocess/postprocess_scene_copy.fsh" = <8494fde4 2c92c768 14a45b5d c4094169 665a7c25>;
        "3d_ios/shaders/postprocess/postprocess_vortex.vsh" = <21818ff9 b5a83396 66fd141a cf092706 69f2ead9>;
        "3d_ios/shaders/postprocess/postprocess_wrap.fsh" = <bc483614 2e63e9fa 7a17dd6e 26ca5431 b5da0899>;
        "3d_ios/shaders/postprocess/postprocess_wrap.vsh" = <1dd82c5d 4f48d6ac 6cdd8ec4 e1287e52 40aa7f28>;
        "3d_ios/shaders/skinned_general.h" = <9ab5cf88 2c9679a5 d58d76a9 0fb72d73 070488c8>;
        "3d_ios/shaders/sky_back.fsh" = <308c6827 67ce08d9 303289ce a3f6c05f 10839711>;
        "3d_ios/shaders/sky_back.vsh" = <c4daf186 3c72ec87 c4c35bbb 977a1958 094789e1>;
        "3d_ios/shaders/texture.fsh" = <3cec6ef8 ce9f0f47 0f64746b 6303d550 5a109f8c>;
        "3d_ios/shaders/texture.vsh" = <ffca60f6 3fd33f1d 32e9f173 212e8154 4707b260>;
        "3d_ios/shaders/textured.fsh" = <cce01110 77db470b 8558d170 2bfd0422 232e52d0>;
        "3d_ios/shaders/textured.vsh" = <1fd2d2bb 792ed078 7484d9eb 2a4eb9f3 6483da5c>;
        "3d_ios/shaders/uv_animation.fsh" = <8a94bb46 3db77b46 01166c1d 25e5cfcc 33acf5da>;
        "3d_ios/shaders/uv_animation.vsh" = <eb959d23 05927e3a 2b50edd5 a786efac 7e7b1678>;
        "3d_ios/shaders/water_drop.fsh" = <72545506 9d23ab95 877c3746 dc628dba b65e904b>;
        "3d_ios/shaders/water_drop.vsh" = <f6a52fd9 13b26374 870b60f2 25b031a0 f10af776>;
        "960x640/flash/achievement_light.plist" = <ace7fdf1 2a83a057 02b0c1de 742a2aa1 6e9a2f1c>;
        "960x640/flash/achievement_light.png" = <88e5e08f 501fa6dc 4ec945fd fc58ae0c 5a0a185e>;
        "960x640/flash/achievement_light.xml" = <6a71204c 2ed6b40e 5087d141 4e049303 5c0490cd>;
        "960x640/flash/achievement_max.plist" = <bdc28bb6 9493ab67 9ec57487 58954ff8 9e036755>;
        "960x640/flash/achievement_max.png" = <93d33aaa 7258f740 9e2c18ef bc764720 a14036fa>;
        "960x640/flash/achievement_max.xml" = <2347a79d 966b2d9b dda407c8 2554e4ee d4967dd7>;
        "960x640/flash/achievement_star.plist" = <02bc6158 06203f8b d2281174 97eee0a2 fc2d68f4>;
        "960x640/flash/achievement_star.png" = <6ab00b9b 7e7c3489 e5e2830c 99c29c9f c6b689ca>;
        "960x640/flash/achievement_star.xml" = <cbc692ca 5c30346c 88b6c832 3b5ab05d ef8ca615>;
        "960x640/flash/achievement_tip.plist" = <09cf5b8d 94ef77b8 0453cb84 ddc5eca4 00a58666>;
        "960x640/flash/achievement_tip.png" = <378ab800 3c0f03b0 449579d1 d8be0385 400281e1>;
        "960x640/flash/achievement_tip.xml" = <972016b3 a61f554c 6d531137 78b03888 0e55b787>;
        "960x640/flash/angle.plist" = <8f788a9b 96427394 16b9f9bc 8f4485a6 85d97ff2>;
        "960x640/flash/angle.png" = <aebe60d6 08b65fd0 9982c71a 6d3514e5 fbbe595a>;
        "960x640/flash/angle.xml" = <68877059 aa65132f df39cf82 e4102bdd 53be641c>;
        "960x640/flash/arrow.plist" = <f39a916a 788de2cd 64de30c0 526811a0 19b7ee1c>;
        "960x640/flash/arrow.png" = <fb563f14 753730bd f78bbd35 8b8ad57d 99875297>;
        "960x640/flash/arrow.xml" = <15ec2c68 5057261c a333cda8 c387c877 8c30c60d>;
        "960x640/flash/award_01.plist" = <87e314b1 46ec23f6 0ef26bef a0166c61 bb7e8587>;
        "960x640/flash/award_01.png" = <5c1b6a78 128f181b 150256b9 8d1bece9 31983e76>;
        "960x640/flash/award_01.xml" = <2aa9fb43 a1321cc4 fc299ee2 6964c65e b4eb5b0c>;
        "960x640/flash/award_02.plist" = <e09de10c d370388e 04d1b290 55b92902 6e9f04a9>;
        "960x640/flash/award_02.png" = <ca5df3ac 9c59b8b1 6a88bec9 cd3f991c d96be9ae>;
        "960x640/flash/award_02.xml" = <365629b6 23687a23 8c9eefcd df2feeb5 a396e325>;
        "960x640/flash/bar_coin.plist" = <376302b1 b4f18daa 301b4fc0 bd35312e 00e7505b>;
        "960x640/flash/bar_coin.png" = <e07d5de1 d8ddeb37 364f3d69 a17fb8e6 7746a6a0>;
        "960x640/flash/bar_coin.xml" = <9a7a20b7 c9b796c4 825abf3a 47f55ae7 fe13ee47>;
        "960x640/flash/bar_crystal.plist" = <8923c2c2 0de5cb26 5dce1b1a e0ee95c2 4358952b>;
        "960x640/flash/bar_crystal.png" = <7642fe43 e2b17f82 05d25f64 c083cf38 277cada5>;
        "960x640/flash/bar_crystal.xml" = <c32f9c54 e63e921b b1e4eb86 1e75477f 2af0d905>;
        "960x640/flash/bigwin.plist" = <51c8b95c 6b6102db 331db406 86bb8d7e 8d62d9b6>;
        "960x640/flash/bigwin.png" = <7431c44d b3836028 0d0b6ddd a20901d2 3e3a495e>;
        "960x640/flash/bigwin.xml" = <d8da446d b05acf09 ae7749ba 3a1d59be 2ebb97bf>;
        "960x640/flash/bomb.plist" = <1188e413 5ebbc465 208bbe3f f812bf06 190cb5b0>;
        "960x640/flash/bomb.png" = <687664f0 ce6d59e2 bc1cbd13 8204f34b 4d734c8a>;
        "960x640/flash/bomb.xml" = <292c3d6a df60928c 70e08d10 ecfee58b f6624f5d>;
        "960x640/flash/bullet.plist" = <9a0d12db 2f2a1100 42b79671 9b6dfd32 ff433271>;
        "960x640/flash/bullet.png" = <ad0bdfa9 9e98ba2d 71045c15 a5171fab 137686cc>;
        "960x640/flash/bullet.xml" = <cf296a6d 4eb886e9 3bfb62d2 bfdf55ba 257c361d>;
        "960x640/flash/bullet_bomb.plist" = <c8dc618a 7ece60c1 83ed1568 2b7a866e 5e1ad56a>;
        "960x640/flash/bullet_bomb.png" = <74c6d4e6 67cafda9 3ac70c79 3efc47f3 d966f9ae>;
        "960x640/flash/bullet_bomb.xml" = <7fce71ea c6adc2c8 3ea79d40 e3401068 f96f5c39>;
        "960x640/flash/bullet_circle.plist" = <9166b99c 33578133 cb0fd570 62c62652 f37ee23a>;
        "960x640/flash/bullet_circle.png" = <8844524a b56e0df8 19a3f58b ab157e3a a4a9ab06>;
        "960x640/flash/bullet_circle.xml" = <2881b8e0 ca304eb5 1335864e 56e52787 543c9b7f>;
        "960x640/flash/bullet_fire.plist" = <2e90bec5 cdbd541e 2c690d74 d744acb8 853c2a8c>;
        "960x640/flash/bullet_fire.png" = <3e1bdc79 10799090 f2121efb 5a584387 1b4af515>;
        "960x640/flash/bullet_fire.xml" = <945aab04 a09039ef 33a7e680 0848b200 6ddb24a0>;
        "960x640/flash/bullet_gas.plist" = <ccf977fd 819c14e6 7978e3d4 3e40c671 da28045a>;
        "960x640/flash/bullet_gas.png" = <a5020b8b 6b9fc939 eeefbe39 476bffb3 e3e7b26f>;
        "960x640/flash/bullet_gas.xml" = <98b92280 f52e80a4 aedaf483 acbe09ed 9bdce6b6>;
        "960x640/flash/bullet_harpoon.plist" = <7eff53a5 92dfc506 73d95b5a 7a49e68e 16e75132>;
        "960x640/flash/bullet_harpoon.png" = <0b7deaa7 2b9c462f 5150af98 3b019814 816beabc>;
        "960x640/flash/bullet_harpoon.xml" = <6e49600f aef3ee07 6a62ab3d fd5c4e01 ce5c9202>;
        "960x640/flash/bullet_moonlight.plist" = <525c478d a5906d14 75dad5e0 33cc9a6d a9107c55>;
        "960x640/flash/bullet_moonlight.png" = <0547a918 0eb550ab 77cd6a38 d4b5c404 15ad9aab>;
        "960x640/flash/bullet_moonlight.xml" = <1df6ff82 89390f41 c234bf10 2f6f3239 c6f7db17>;
        "960x640/flash/bullet_projectile.plist" = <45384188 1944351b 98d7591f 21566e2e 1ec6706b>;
        "960x640/flash/bullet_projectile.png" = <37f50ba8 8ad3a739 810813a5 abb7a86b 4ba95c4a>;
        "960x640/flash/bullet_projectile.xml" = <bc103225 9eb525b4 296da55a 9c37dff0 15e8d226>;
        "960x640/flash/bullet_rolling.plist" = <3aedea32 0c7da844 024ebe13 c7f91945 bbefed46>;
        "960x640/flash/bullet_rolling.png" = <0ea98f6b e9ea10e4 a6603b12 58567632 884c4ffe>;
        "960x640/flash/bullet_rolling.xml" = <61911065 f83186bf f3fabdd1 1a8a43bf acc0e95b>;
        "960x640/flash/bullet_shoulijjian.plist" = <7fef9be3 61af8e6a e646c57f f9800b09 828eb098>;
        "960x640/flash/bullet_shoulijjian.png" = <61a220d6 3f630f80 80e9ade1 a29d95c5 2356e387>;
        "960x640/flash/bullet_shoulijjian.xml" = <4994e5c3 3c8222cf fea7e37f f30eb7a8 7bd11a36>;
        "960x640/flash/choose_go.plist" = <298437c8 66a28367 f7475934 1a108e6b ead6898b>;
        "960x640/flash/choose_go.png" = <2e13ddb4 b4f1bdf1 daf33d7c 173aa89c a182a6c7>;
        "960x640/flash/choose_go.xml" = <e780edd1 e22fab0e 4369b2ba 6a78be82 408463e6>;
        "960x640/flash/cristal.plist" = <9e4e347b dc93f51c a59a06c4 2a19d575 7f9e725e>;
        "960x640/flash/cristal.png" = <ce5a3532 25dc2e88 318e5afc 5649763d 024b116e>;
        "960x640/flash/cristal.xml" = <aee8c825 691e97bc 0516cb7a ec6f26a6 f985b4c2>;
        "960x640/flash/ef_bomb.plist" = <36b0601f 1cfcf8ec bb02cee0 726ec036 c3e86eb8>;
        "960x640/flash/ef_bomb.png" = <d4020399 fb2fcbcc c729e2d9 493c91ec e2fef128>;
        "960x640/flash/ef_bomb.xml" = <c30f6558 c2ac423d b8fd70c8 26c1fe88 22f2504c>;
        "960x640/flash/ef_bursts_1.plist" = <33b8c48e 28cd0134 05abf92a 1aac3aa2 64315f55>;
        "960x640/flash/ef_bursts_1.png" = <fb05293e 4d27d4bd 683eea0d 87679906 082ecbce>;
        "960x640/flash/ef_bursts_1.xml" = <ddd4e21b 1c391405 217c94f0 1652980a 6e3f165e>;
        "960x640/flash/ef_bursts_2.plist" = <d08f4757 673d5d34 2f770dc8 0bc40816 041d5d5f>;
        "960x640/flash/ef_bursts_2.png" = <7ec32519 9283101b c587f9fc 5fb51496 c9711d3e>;
        "960x640/flash/ef_bursts_2.xml" = <342512b5 e9165cf1 577d1875 6930f27f a3f5a716>;
        "960x640/flash/ef_bursts_3.plist" = <7f9b5980 04918d1e 9d6c5073 bd3db1cf 32d53b04>;
        "960x640/flash/ef_bursts_3.png" = <9cdd8361 d9626bfb 0ca98219 a35e9c32 d4310214>;
        "960x640/flash/ef_bursts_3.xml" = <97f4266e 54c97d12 d1cd2380 9af3b364 6b05ab39>;
        "960x640/flash/ef_cannon_1.plist" = <92c94e1e ffd4c809 a0dfed68 2d03ee58 044f20e5>;
        "960x640/flash/ef_cannon_1.png" = <a68096e8 f9c84b84 5b749e95 63f41875 94980695>;
        "960x640/flash/ef_cannon_1.xml" = <46d195b2 f7c8027a 318c9b08 3c7668ca e9d557f3>;
        "960x640/flash/ef_circle.plist" = <a065ed70 033baa63 d0720899 3aba3ee3 4d43201d>;
        "960x640/flash/ef_circle.png" = <bc43509c 871aa58e 3a4914e4 19f3fd1e 5f02f169>;
        "960x640/flash/ef_circle.xml" = <4e2026d7 7a0ba71c 4c08937e f875513e 069dfddb>;
        "960x640/flash/ef_fire_1.plist" = <1440bbb0 c8c4721e fff79f11 abf82d89 d56da486>;
        "960x640/flash/ef_fire_1.png" = <b1276f5a 347e0916 95a62960 d9a1a492 884bb0b1>;
        "960x640/flash/ef_fire_1.xml" = <4ec924d6 2d2f4586 0bbe838c 9f4f06ba fb7e68fb>;
        "960x640/flash/ef_fire_2.plist" = <9bcc9bcf c739587d 60c8776c 149a4480 fdc99eec>;
        "960x640/flash/ef_fire_2.png" = <c75e0414 5509dc37 6f484c19 428c0585 68a0b49d>;
        "960x640/flash/ef_fire_2.xml" = <a75a9b6f b6f9dde2 18664344 1a49873b bc1bd2e3>;
        "960x640/flash/ef_gas.plist" = <0c770386 65aaa379 714c8904 c62d4c78 7de023c5>;
        "960x640/flash/ef_gas.png" = <fae3ae21 7e5915a9 6e3ee3c1 51cb21c2 6ee7363a>;
        "960x640/flash/ef_gas.xml" = <98a4adc1 dae20239 a18893a3 b4444116 e1b83820>;
        "960x640/flash/ef_harpoon_1.plist" = <6e3fa060 fdcfc3c9 a9262aff e2b14711 14369158>;
        "960x640/flash/ef_harpoon_1.png" = <f543c03c 7a6d495b d3793f78 d3ae77b4 0ccaf62c>;
        "960x640/flash/ef_harpoon_1.xml" = <98ede7dc 6026d840 917275a3 d401052b 006b2771>;
        "960x640/flash/ef_lighting.plist" = <b3d4c7da 532144c5 cc0e7757 5b9e5058 6bdee171>;
        "960x640/flash/ef_lighting.png" = <d438cda6 aa3ab1ca b8a7f511 83106373 0394523a>;
        "960x640/flash/ef_lighting.xml" = <e8e960d8 7d471efe 848bca39 cb882cf7 49ef51b8>;
        "960x640/flash/ef_lighting_1.plist" = <68e974e8 6ba18ef5 8ccf8116 46d0148e b0cb410f>;
        "960x640/flash/ef_lighting_1.png" = <8d572503 c94e2a16 7ddff16c 1fdc302d 3c9dc4cc>;
        "960x640/flash/ef_lighting_1.xml" = <a0c6d43c 102c69b6 a7792fbb bb8387b9 b7a82f56>;
        "960x640/flash/ef_moonlight.plist" = <6b55ac36 706ec66d 26ef48b2 a0bf7805 0e09da5c>;
        "960x640/flash/ef_moonlight.png" = <be0902bf 89d9495e 697c8219 6b32a48f aaed11f5>;
        "960x640/flash/ef_moonlight.xml" = <b7b46301 d2a04d89 fd0c484f 6a0320cf 0b52c0bc>;
        "960x640/flash/ef_projectile_1.plist" = <e22360c6 c1451d45 04e6da1c fa481a2b 28d31d0b>;
        "960x640/flash/ef_projectile_1.png" = <63ce4b02 66cacd22 4bea5646 b1486d38 ee732719>;
        "960x640/flash/ef_projectile_1.xml" = <e4db2bed fd6d1bca 878c972f 7634beaf 840a5b15>;
        "960x640/flash/ef_rolling.plist" = <4c7ba2b8 53e49263 481b0bc4 fc796316 fc344114>;
        "960x640/flash/ef_rolling.png" = <a0cdb40e 61b40ace 1417d60c ea1529b0 b66e035c>;
        "960x640/flash/ef_rolling.xml" = <56dba59a 7eb02ab3 367dfcf7 ee2769d9 973e923c>;
        "960x640/flash/finger.plist" = <5cd506a7 83a7334d abede1ba c0aa3c44 6a7466e9>;
        "960x640/flash/finger.png" = <7de5f813 2a6b1265 09a63f60 21b75629 a1d71c1d>;
        "960x640/flash/finger.xml" = <7f89307d b24651dc 2051b374 267afb14 79d4ad00>;
        "960x640/flash/firefish.plist" = <d2b73f91 fbac7791 47643a48 a046c552 f1643e00>;
        "960x640/flash/firefish.png" = <871cc7aa a44dd240 e171bbff 31075f1f 99b879f0>;
        "960x640/flash/firefish.xml" = <3a4d8bf1 525c9a43 d7a3c9b6 d86bde2c b1482c31>;
        "960x640/flash/fish_dead.plist" = <7543385c 693d07c4 45e9e97c 26312c19 f4206baf>;
        "960x640/flash/fish_dead.png" = <cafea2b0 54ba2bc7 73b1ef25 878489f6 125c2bc1>;
        "960x640/flash/fish_dead.xml" = <a9ba1bbf efc3f907 ac0969b0 5dcedd0f 7b86f3da>;
        "960x640/flash/fish_dead_1.plist" = <58854d04 472526f0 88b0ff7f 33fa0acb 0f2bafda>;
        "960x640/flash/fish_dead_1.png" = <5710183b 725d95a8 d607e90b 3bfe3169 f1bb1d91>;
        "960x640/flash/fish_dead_1.xml" = <7b6719bc e1c6e407 46007a82 bde24d71 5c63ceb2>;
        "960x640/flash/fish_dead_2.plist" = <ec1c696d 32d6d494 56ee83ca 94271c1e 10046ec2>;
        "960x640/flash/fish_dead_2.png" = <9b3ff4af 3583717f 54994d3c b7498b8c 7f0fd168>;
        "960x640/flash/fish_dead_2.xml" = <8649ef25 5d5fe0f5 cac76ca0 129d8780 0c8fa4d3>;
        "960x640/flash/fort.plist" = <7107648f 60e65284 847e0afd a2c5a3d6 8f268dab>;
        "960x640/flash/fort.png" = <ecb668c3 c1e8ad99 f2c028d3 f4497dbe c64d964d>;
        "960x640/flash/fort.xml" = <f95dc4db 7e5878a9 53cc9d41 caf89e1f e32559a4>;
        "960x640/flash/frozen.plist" = <fb9b58e8 ae4734a1 6a00ab19 0d5898be 1cf6eeae>;
        "960x640/flash/frozen.png" = <fdc4c325 6c623a8f 8efe03f7 07edd9d1 6e3f26dc>;
        "960x640/flash/frozen.xml" = <a4081048 79375602 5b42be1e 2494e5e8 ceea8481>;
        "960x640/flash/guessfinger_bubble.plist" = <a68fcf4c a4a689b6 68729572 2678903e 88dd001d>;
        "960x640/flash/guessfinger_bubble.png" = <c70a0119 73159c29 10bc5441 ed3fd3ce f60cd7c7>;
        "960x640/flash/guessfinger_bubble.xml" = <71f19ff3 8467c0fc c3389682 557c42dc 7aca6260>;
        "960x640/flash/hetun_explode.plist" = <3ea3c3f4 af43a48c 6898caa2 a6242258 887afc46>;
        "960x640/flash/hetun_explode.png" = <d52fbc9b 29052795 7448386c 195d68f8 2a9fbd3f>;
        "960x640/flash/hetun_explode.xml" = <77d3e442 baf3a029 3daa516f b621fdcd 95189823>;
        "960x640/flash/huangjinpao.plist" = <6874fd0a b753b3d8 f815e0ff dce02116 1d37be50>;
        "960x640/flash/huangjinpao.png" = <877a6043 a7efe34d 4e180485 f69bcc7d b4e2640e>;
        "960x640/flash/huangjinpao.xml" = <cd22dbcd 92698d9c 49d6dc6d 22452963 6a65d071>;
        "960x640/flash/huangjinpaodan.plist" = <93ae8b1c a951b6b3 dd566a44 b6e945cb 0b7a6eda>;
        "960x640/flash/huangjinpaodan.png" = <44e4fbb7 e77c14b7 460e0198 84845833 5412e907>;
        "960x640/flash/huangjinpaodan.xml" = <20e241db 5c947789 8715b8b1 6f3f5e0e 14f26978>;
        "960x640/flash/jinbi.plist" = <d30009ee 318834ae edb6716e f44210ff 30f6281d>;
        "960x640/flash/jinbi.png" = <bff1cb7b a54d41c5 8cf76e81 0cc20a5d 087d9a64>;
        "960x640/flash/jinbi.xml" = <5e3cc831 f8391aa4 3149150c c1c0d071 feddea7a>;
        "960x640/flash/jinbi_1.plist" = <73ca559f acabb869 0e1022bd bd44287e 15c733b6>;
        "960x640/flash/jinbi_1.png" = <bff1cb7b a54d41c5 8cf76e81 0cc20a5d 087d9a64>;
        "960x640/flash/jinbi_1.xml" = <499f4540 c95fd593 c6fe0c82 9b3e0b0d 3733820d>;
        "960x640/flash/lahuji_4.plist" = <c5fffe38 f3954b78 5d8b8ef8 2fb93f89 ddb3dd02>;
        "960x640/flash/lahuji_4.png" = <7e777174 e6e048ab c5265065 ffaefb74 7dfae3d2>;
        "960x640/flash/lahuji_4.xml" = <49c17b0d af7d95ac 766e8f9e df08c863 5ccf479a>;
        "960x640/flash/laohuji_jinbi_1.plist" = <6953f5e1 6d94b7d8 e3651f58 4da2818a 10a68f4c>;
        "960x640/flash/laohuji_jinbi_1.png" = <c464c2d6 8cb2661a d8431117 8838c2e8 4a5c45da>;
        "960x640/flash/laohuji_jinbi_1.xml" = <43073170 8643e6b0 cf385a7d 3d48ad1f 6fb166d7>;
        "960x640/flash/laohuji_jinbi_2.plist" = <5d5011c3 7e9e0157 d5893da7 e9c06ccf 178da99d>;
        "960x640/flash/laohuji_jinbi_2.png" = <ef8e175d de9bc286 70fa61a2 810ee5df aa94ec48>;
        "960x640/flash/laohuji_jinbi_2.xml" = <955b225a 8ef304c9 79f6f820 7f217a1e 7faff638>;
        "960x640/flash/laohuji_shoubing.plist" = <a550df01 d5af0a85 b6267be5 7962ca18 5e90bff9>;
        "960x640/flash/laohuji_shoubing.png" = <20b258fd 23be0abd a8ab67bd 1c4df301 3cca8eec>;
        "960x640/flash/laohuji_shoubing.xml" = <de841419 52521999 bfea3ca4 deec5c9f 57a9ae86>;
        "960x640/flash/levelup.plist" = <504c5c4f abc0abd0 6ed6c022 5768ac9a fcd9d9bc>;
        "960x640/flash/levelup.png" = <3f336774 7b7911dc 4a2af0f9 adf4c393 20fc6fa6>;
        "960x640/flash/levelup.xml" = <7cdf694d d19219b4 79ce57bb cbe017f6 a6e80648>;
        "960x640/flash/light_background.plist" = <da79dd09 5c35726e 992e059c 24184efd a034f850>;
        "960x640/flash/light_background.png" = <51f27688 ad0166ba 58c07541 d0bc4151 74c825f3>;
        "960x640/flash/light_background.xml" = <c6606f0d 6ffb6027 233ae927 5b3661b7 fcc1e965>;
        "960x640/flash/lightening.plist" = <68acb773 2edca92d a26441b9 092da3cb ca3ef43f>;
        "960x640/flash/lightening.png" = <070d1176 3832f130 256e7d68 97685f3a 9f89dcf2>;
        "960x640/flash/lightening.xml" = <48fc2294 57cea6df 6394581c d6ed9d7a e523c2ce>;
        "960x640/flash/mission_text.plist" = <7feccd43 18020d80 35ac94a9 2ae00374 50d26c5d>;
        "960x640/flash/mission_text.png" = <b07117ec 8219b150 a3be21f4 11735bfa f35e0ee7>;
        "960x640/flash/mission_text.xml" = <dc048914 37dc6d19 a6e483ed 7f2782d4 33330d9b>;
        "960x640/flash/mission_text_1.plist" = <34a2feef d58dba0c 53dbf2c4 bec8097a c13d9ef9>;
        "960x640/flash/mission_text_1.png" = <ad0b2bd2 06fa6742 2d482105 9c6421bf 329148af>;
        "960x640/flash/mission_text_1.xml" = <1d8ab118 027c303d c24ae8e5 c691fae3 81081ee2>;
        "960x640/flash/net.plist" = <8851d525 18c1d590 efc7353d 67e4f71c 97874de4>;
        "960x640/flash/net.png" = <1b388251 e150f9b1 4830630a 2d07fab0 6707f8f4>;
        "960x640/flash/net.xml" = <4059b746 36771f5b 0863183a eb59836a 3c9d9a7c>;
        "960x640/flash/new_record.plist" = <07ecdfbd 9442eec0 3607d6f2 a65a1478 736a4808>;
        "960x640/flash/new_record.png" = <f0ebaf18 0f068c1b d849e0eb 271c800c 78466613>;
        "960x640/flash/new_record.xml" = <21b4daab f9716211 20b784d1 e8b51e9e b67c9d5d>;
        "960x640/flash/pao_1.plist" = <8c1b4d6d 7097764e c7e79d2b 821e449f 3aa7b08f>;
        "960x640/flash/pao_1.png" = <c00282f3 115ca4eb bda2d2f4 2ea83435 13249feb>;
        "960x640/flash/pao_1.xml" = <3aae4524 b29a1794 0e02f065 59385234 872e8031>;
        "960x640/flash/pao_2.plist" = <3129ad64 860324ab 3fee1bbc 98eba4a8 37a349ca>;
        "960x640/flash/pao_2.png" = <726f7d9d e3a01d7e ec249c13 b37ea516 3dd98604>;
        "960x640/flash/pao_2.xml" = <7c2f93b2 072508dc 9a4305bd 60acac3d 2a6f607b>;
        "960x640/flash/pao_3.plist" = <ee3a07d5 7cf764ad edb92bb7 a3a82925 6a2f6a82>;
        "960x640/flash/pao_3.png" = <ac4a5fac 4d35aeeb b26a43bd 17cfda19 dcee74bf>;
        "960x640/flash/pao_3.xml" = <3180f743 52694b5a dbcefc4b 9580e92e 2825f71e>;
        "960x640/flash/radar.plist" = <6f7152cf dab8457e 99401d08 b0a6eefa e44efa9b>;
        "960x640/flash/radar.png" = <fe2d663f 196b86bb 49482816 4765563b 894290ca>;
        "960x640/flash/radar.xml" = <f504a397 36234034 488a23e3 3b115ef0 62da3522>;
        "960x640/flash/reward_get.plist" = <690099d8 ae99b846 337309cc 4b2f05b2 c96551aa>;
        "960x640/flash/reward_get.png" = <b017a8aa c3ce4c6a 8f0c824c 71397ba7 0d067e0d>;
        "960x640/flash/reward_get.xml" = <71dd530f 24ee22d3 ddee9ed5 5acb749e 64e68b9f>;
        "960x640/flash/shayuyayouxi_3.plist" = <ac5dc1e0 eeceb610 c5f9eef6 69eb6fcd d0d211ec>;
        "960x640/flash/shayuyayouxi_3.png" = <9d78868a f9792901 063c03bd 368d4217 971bb133>;
        "960x640/flash/shayuyayouxi_3.xml" = <2e50587c 26f7123e ee034bb0 a5ebde70 accba5ff>;
        "960x640/flash/slot_3.plist" = <d9f4b492 4c93a147 058bb994 8fcad6d7 599ee385>;
        "960x640/flash/slot_3.png" = <5d1b2d8b ae533753 f6a7a835 bab7d405 6db67c54>;
        "960x640/flash/slot_3.xml" = <e1cd3714 b9ab5fab 98817130 35731402 e9734d6e>;
        "960x640/flash/slot_arrow.plist" = <cfde1c23 8a96ec90 8488d7fd 66154d51 50ddd975>;
        "960x640/flash/slot_arrow.png" = <3f9905a4 3da0beb8 14cf169c 3e1a3862 b98b0fcc>;
        "960x640/flash/slot_arrow.xml" = <69c00121 642beb84 82178911 80d87b3e d35cb290>;
        "960x640/flash/slot_bg_light.plist" = <ed513cb0 8c8d608a 744f7f00 6570117a e235426e>;
        "960x640/flash/slot_bg_light.png" = <9cfb70ff e51eb39f 1da44050 502d6e97 bf80dced>;
        "960x640/flash/slot_bg_light.xml" = <afaf550b 10db4ea5 e0babcd9 b014705f bcdbcc77>;
        "960x640/flash/slot_light1.plist" = <e02ac01f 7ba8d349 8f8b71b6 3f18ae32 b75ff9b0>;
        "960x640/flash/slot_light1.png" = <7911dcdf 4cd29d09 20582976 9d331b88 00ba3879>;
        "960x640/flash/slot_light1.xml" = <feb8c7be cd62b36f 7c5a1ce6 deecfe82 e93c5de5>;
        "960x640/flash/slot_light2.plist" = <5d93a434 b7b40a75 831d23e4 ee274cc1 589214eb>;
        "960x640/flash/slot_light2.png" = <31ba94a2 a4502a7e c66ee14f e2fcc61b c634a570>;
        "960x640/flash/slot_light2.xml" = <5beda8da 44ec7c0e 02847d0d 47b7a684 6785c04b>;
        "960x640/flash/slot_light3.plist" = <e65a17b5 7c873b3d 40c7a8e8 f0636aa4 b6ea8c45>;
        "960x640/flash/slot_light3.png" = <2ecd2bb5 91e27eb1 25b31555 ea995cd2 b1d05b29>;
        "960x640/flash/slot_light3.xml" = <1c3ca0ab 3821581c e6f07c52 4a267885 1448db68>;
        "960x640/flash/star.plist" = <1fe74893 4949ae93 7d6185a3 f1fde442 1476cf5c>;
        "960x640/flash/star.png" = <a1b4d67c 585e52de 6f627823 243dd8bb ebaa717a>;
        "960x640/flash/star.xml" = <43e8f697 a1d0ae8e e8685d14 0576c8af 0b3ededf>;
        "960x640/flash/submarine1.plist" = <30f2af32 6423145b a4cfdd87 b69c5e01 44272a6d>;
        "960x640/flash/submarine1.png" = <9f9e4dd2 f55ecea9 beebca07 f3ab1acc 6a2c257f>;
        "960x640/flash/submarine1.xml" = <2cf6f2df bc9f7bb9 902607c6 ff039ab0 42dd6048>;
        "960x640/flash/submarine2.plist" = <e4388e8f fc5f483f 6ce80f7b 3dc7cca4 ddc6d57c>;
        "960x640/flash/submarine2.png" = <d33eb1fd 3e06c702 8e924b4a df1d5887 9e619952>;
        "960x640/flash/submarine2.xml" = <3428684c dc76325b 44bff60c 68b98e81 a934c6e8>;
        "960x640/flash/submarine3.plist" = <2c7b5eaf 1a050756 c1b7e0b4 77ec6bec b6be8da8>;
        "960x640/flash/submarine3.png" = <d65a858f 0d222476 5ef7613e 6d373f80 2f3af8c2>;
        "960x640/flash/submarine3.xml" = <e0f4afe9 3f123978 ae2d0cfa 60890290 9cecf07c>;
        "960x640/flash/submarine4.plist" = <719a329c 4957588f 9693ed7b 17ef33ca 190b0de9>;
        "960x640/flash/submarine4.png" = <06c09799 e280d78a 25b513e2 972a1c5a 3cc0a2ee>;
        "960x640/flash/submarine4.xml" = <f9510e48 14c16292 8cc25f1d a86ab6af 9f520260>;
        "960x640/flash/submarine_button_effect.plist" = <d71c4db6 cc4ee2dd 61a2297e 147e7e84 cfdecb23>;
        "960x640/flash/submarine_button_effect.png" = <1ec78087 55a77755 8184599b 9074fc13 d2d4d9aa>;
        "960x640/flash/submarine_button_effect.xml" = <65387b68 8c170d41 207eddf5 3783857c e4cac0aa>;
        "960x640/flash/tanhao_1.plist" = <22bbf9c8 798331c1 a62673c9 64073224 e3657b26>;
        "960x640/flash/tanhao_1.png" = <33f9ef77 f0239826 cba4c271 e05cf72d 44b147c9>;
        "960x640/flash/tanhao_1.xml" = <43f3b410 725213a3 b3288f51 039c6dfe 52de1f05>;
        "960x640/flash/tanhao_2.plist" = <289600c2 ecef1317 a0b23608 6f21e6a1 75af2fd1>;
        "960x640/flash/tanhao_2.png" = <0be123d1 dafc110a dc1363c4 cdeea260 5130c15f>;
        "960x640/flash/tanhao_2.xml" = <5acd88bc 57c3c6d2 96dad048 c8598bdc d96a3a90>;
        "960x640/flash/target.plist" = <8907190c 612da7bd f3475542 dbf5f4ea 59cfe689>;
        "960x640/flash/target.png" = <93c2aac1 c8838776 764a4ac1 f02eed3e d47fbedb>;
        "960x640/flash/target.xml" = <36db1d17 3da3b3a3 c14130ea deb459ea 1a421e85>;
        "960x640/flash/target_2.plist" = <45a87934 d746b282 03796e6e 15e8eb3f 3589e0e7>;
        "960x640/flash/target_2.png" = <39629e6a 1a74a892 07f06acc 6ecae79f df6e0a76>;
        "960x640/flash/target_2.xml" = <98d82320 97e4273f 3cb860b7 fb681cc0 0f790549>;
        "960x640/flash/treasure.plist" = <38b22202 94429169 160898cd 1c27c687 0402212f>;
        "960x640/flash/treasure.png" = <f1eded04 0cb14c1c a753b17c 0e6a5078 b73a408f>;
        "960x640/flash/treasure.xml" = <e572fc82 23bed3ce 3e98c417 f2740e68 63c92fbf>;
        "960x640/flash/vip.plist" = <eee5184c b5ea8c70 fd5fed84 c6bd7e13 b5f7bbb7>;
        "960x640/flash/vip.png" = <30262973 307b9589 d57aec9d 97dbf7a2 9dcdefda>;
        "960x640/flash/vip.xml" = <de33269e c50d1167 e73139ec 5ae30cd8 5ed1b1dd>;
        "960x640/image/angle.png" = <dc9bc1e8 d2f8e9b6 bfe7039f e1caee75 bc24429f>;
        "960x640/image/bar_light_move.png" = <3bcdd734 15f1f3c1 c12c477e 23df4a80 6c6297a6>;
        "960x640/image/bar_light_stop.png" = <a04a251e 715d17d7 61998b04 f12af412 63d34f26>;
        "960x640/image/blood.png" = <51a26ce2 5a53b4bc a3d66933 e0bed71a a157f8b1>;
        "960x640/image/card_fly.png" = <e51372da d337198c a725c753 9b278a04 a494f236>;
        "960x640/image/effect_res_gold1_003.png" = <7bee64b5 8e5da287 7bc7f48e 6c8791cf 1fe22578>;
        "960x640/image/event_weapon_flash.png" = <d8ba682c 6209c32a 41693b77 2556a8d3 e2d7a5f6>;
        "960x640/image/event_weapon_light.png" = <b47496f5 7b9db22b 1295ad4d 5d0ccec0 cc9169cf>;
        "960x640/image/fever.png" = <ccc207a6 47b0432e 774b5fc5 e27d392e 8e8986ed>;
        "960x640/image/fever_blue.png" = <0473ea0d bfd40a3b 547314af 8a059067 c977c54b>;
        "960x640/image/fever_light2.png" = <a080f534 c2b0596c 508c5290 e736df1e cc63e3a4>;
        "960x640/image/fever_light3.png" = <b1fb5cee d4d45f0f da9586c2 48956741 a74e271b>;
        "960x640/image/fever_line.png" = <83a66c2e d5fac771 2f3e9922 35e554b1 6c2eb1e2>;
        "960x640/image/feverbar_ready1.png" = <88279f3c 6f91a7dc 2467a772 4c906df3 51074e37>;
        "960x640/image/feverbar_ready2.png" = <b1b54e27 642edaf8 9990435d 7a5dd9fa 49b88077>;
        "960x640/image/feverbar_ready3.png" = <52ee6d52 787b1314 215e6737 1c2e18e5 7be818eb>;
        "960x640/image/jingbiguang.png" = <37cb4deb d4079198 fd5478b2 ba6f3fd6 95bd78b8>;
        "960x640/image/landscape.png" = <2fd862ef cf75f4bb 2083f901 46905ea1 b32b9697>;
        "960x640/image/mission_text_denglongyu.png" = <29551800 42c658d7 53a53270 3f8f1566 dc089227>;
        "960x640/image/mission_text_gameover.png" = <e1d58cc6 1c69066f c9bf020f b2220814 d502f1a3>;
        "960x640/image/mission_text_goldenshark.png" = <bb429cc0 d634dd5d 41c28fe2 deb78db5 cf09ff0e>;
        "960x640/image/mission_text_guessfinger.png" = <db7ff4a2 f836b53c fddca1cb f88b8d59 dd08f1b7>;
        "960x640/image/mission_text_hetun.png" = <ab9a5b0a aae50391 63ce55a6 bfbaf003 6fe9730c>;
        "960x640/image/mission_text_sharktooth.png" = <c83e5607 6ae1c967 2d2915fb 99b40b9a 9d1b4a73>;
        "960x640/image/mission_text_treasure.png" = <2c954229 f6695295 fb1390b8 f9797cf1 02d963a0>;
        "960x640/image/mission_text_yuchao.png" = <0877e180 5c0dbddd 0fcf293d 37f1f6a1 72cf013a>;
        "960x640/image/qiaoya.png" = <828278d1 739ab5ed c57f780c cf2ec6e4 1d746515>;
        "960x640/image/rado.png" = <dd52ebda ebcf2c01 524121b0 3838acd5 3f10285f>;
        "960x640/image/shark_chest.png" = <c1ed0510 298237f1 c5e4bae9 61ca9a19 b4df3de1>;
        "960x640/image/sprite_pos.png" = <edbbddf8 d99123a5 cccac43b 7bd85121 6db4327d>;
        "960x640/image/swirl_light_4.png" = <1f50f9a7 11776bad 3b681595 0629b48f 762cfb4d>;
        "960x640/image/timer_blue.png" = <fa58f082 0cb2b0f2 5fa90901 ed9fcead ca13d16a>;
        "960x640/image/timer_red.png" = <fafdc9ae c4d45e45 227938b6 794b50bd 737b8985>;
        "960x640/image/topbombred.png" = <559c3343 9de56345 35d5b861 95257552 35730d01>;
        "960x640/image/treasure_light_1.png" = <679227e3 31f1c406 84013a12 ec926789 c41a1990>;
        "960x640/image/treasure_light_2.png" = <1235cee9 70d7c475 5ec39635 8ecc91f2 3e44582f>;
        "960x640/ui/achievement_icon_0.png" = <63da48ce e41a10dc b7e60483 d4301907 a072f74e>;
        "960x640/ui/achievement_icon_1.png" = <a3ee2329 65271956 7d8810bc f85aeaa9 576ab3bc>;
        "960x640/ui/achievement_icon_10.png" = <a7851654 a86e4ca6 c68e451a 5f9c579a 3cde66ee>;
        "960x640/ui/achievement_icon_11.png" = <8aa9eade c14208bb 9f225a9f 9ef01ee2 143f03e1>;
        "960x640/ui/achievement_icon_12.png" = <f44478e8 fd59ec58 a8ee45d2 dc3ff75e 3e4f2272>;
        "960x640/ui/achievement_icon_13.png" = <325aafd7 e27ad6ac 99ca3a60 338c4d20 65571bc1>;
        "960x640/ui/achievement_icon_14.png" = <d2180360 b168c42a 90556c19 78f3f87f 4318436f>;
        "960x640/ui/achievement_icon_15.png" = <67432cbc 8e677873 60a0eea8 16fad0c9 8391fc27>;
        "960x640/ui/achievement_icon_16.png" = <c0e7c106 0f07261b 541484bf fa388fd7 d2ca2370>;
        "960x640/ui/achievement_icon_17.png" = <4392901e 225ca0f3 74393771 7ff66869 c0723444>;
        "960x640/ui/achievement_icon_18.png" = <38c8cd5f b7850a46 447166db eb4df14f ad9d6c62>;
        "960x640/ui/achievement_icon_19.png" = <f4904ea1 547aa191 fe6e2e91 44aa1531 7f6c680a>;
        "960x640/ui/achievement_icon_2.png" = <26f9bb98 bef9686b 4c795263 cecc7abb b1d5ffac>;
        "960x640/ui/achievement_icon_20.png" = <a73179b7 ee123c09 24395269 a53d7aee bc7e941d>;
        "960x640/ui/achievement_icon_21.png" = <d6b7bd60 9a25b147 6261de95 a43cc053 800448f7>;
        "960x640/ui/achievement_icon_22.png" = <33ee67a2 9408e0cf 96dd2780 11e1058b 0e826b0f>;
        "960x640/ui/achievement_icon_23.png" = <1de20284 170acd8a 2df08e90 2b538ae5 2ceaeb34>;
        "960x640/ui/achievement_icon_24.png" = <fec2a133 f6af1072 7e284704 d0a9b37a 282399ec>;
        "960x640/ui/achievement_icon_25.png" = <145d67e7 498de623 b07ef8eb 8aef4dfa 77fd7504>;
        "960x640/ui/achievement_icon_26.png" = <6a877cf4 4fa9ddf5 10c6e3ac 8ab0305b 84628222>;
        "960x640/ui/achievement_icon_27.png" = <c28cf7b2 4ea2f249 cd60fe0a ffdc2c93 165c7a3b>;
        "960x640/ui/achievement_icon_28.png" = <a9238d6e 183f15d4 ccd63777 14f16de3 e53ffe5e>;
        "960x640/ui/achievement_icon_29.png" = <abeaae1a c74c3d2d 220bead6 06e38623 e496d536>;
        "960x640/ui/achievement_icon_3.png" = <515fd154 3552bab6 79a7f22e ecc15648 ee81cc5a>;
        "960x640/ui/achievement_icon_30.png" = <f6f5a266 823898e0 20a1f7bf 4f5b75d3 c49fb1a1>;
        "960x640/ui/achievement_icon_31.png" = <c6cdcef6 51f691fa 90efc8cf 862a9239 8e66ef9d>;
        "960x640/ui/achievement_icon_32.png" = <537c4935 66ac00d7 2e864ad5 c453e349 564780d4>;
        "960x640/ui/achievement_icon_33.png" = <2257a7f6 89aadc24 58c7bb09 d65fd45b 5ff1bbf3>;
        "960x640/ui/achievement_icon_34.png" = <340502ab 45541a75 96d7c06e f37fe9d3 a1394887>;
        "960x640/ui/achievement_icon_35.png" = <e07844ce 6e01ba30 dde63c52 4c2b8024 a3d838de>;
        "960x640/ui/achievement_icon_36.png" = <475c0f19 ca442adb c6468e15 024eb4bb 714ec875>;
        "960x640/ui/achievement_icon_37.png" = <1e197450 d0276f8e 4637ffff b49a8e9e d0deceb8>;
        "960x640/ui/achievement_icon_38.png" = <c7d4ea55 068e04e1 d113de47 a660507d 5f77114e>;
        "960x640/ui/achievement_icon_39.png" = <7106e245 caca8cdb c3dba51d 0b86a83d 0f0861e7>;
        "960x640/ui/achievement_icon_4.png" = <313dd6b8 c7d81169 17b4bfcc 78dd1612 071ae504>;
        "960x640/ui/achievement_icon_40.png" = <54b143b7 5053731e 6c0b7c61 ce80d58a a965f431>;
        "960x640/ui/achievement_icon_41.png" = <907e7815 003737f3 f2fe2db1 75b3a03f cf418daf>;
        "960x640/ui/achievement_icon_42.png" = <df35f384 f2b9f169 9fe310aa 54b3f34e 67a92d2d>;
        "960x640/ui/achievement_icon_43.png" = <245c6757 d0606d89 a427790a d70936ce f19180bc>;
        "960x640/ui/achievement_icon_44.png" = <ad23bcb6 f0a35ff5 dbddc997 6f8f2a28 16c22ba5>;
        "960x640/ui/achievement_icon_45.png" = <3297198c d2dbba2d 09874a30 5437251b 889d164f>;
        "960x640/ui/achievement_icon_46.png" = <24b1061a 805ab688 3db6fd57 da2b0531 3d33f70a>;
        "960x640/ui/achievement_icon_47.png" = <64c9a755 4cb6ceb8 ac61583e e85815b9 7dcafdb2>;
        "960x640/ui/achievement_icon_48.png" = <b748b231 eafc609e 4a9b30d1 819707ce 0c91e149>;
        "960x640/ui/achievement_icon_49.png" = <015ad283 de2a3da0 20c39d26 5a103437 979eebc7>;
        "960x640/ui/achievement_icon_5.png" = <8fb54281 f4269d55 a60dd010 be8bec85 8baa547d>;
        "960x640/ui/achievement_icon_50.png" = <e2fd0c2c 6ed876bd 3821aea2 f54bf9b4 e91e129b>;
        "960x640/ui/achievement_icon_51.png" = <58201b73 71332af1 f8e42ff1 2ad63bb4 26c97c85>;
        "960x640/ui/achievement_icon_52.png" = <5eeda6be 1c08c97e 502d29f3 73ece6b1 0216f833>;
        "960x640/ui/achievement_icon_53.png" = <ad76a4e3 07dc31a8 5ca4ddf9 633a2157 cb92fd3b>;
        "960x640/ui/achievement_icon_54.png" = <893e0706 f6d203a5 4a2eafac 057de9a0 4e7d0a93>;
        "960x640/ui/achievement_icon_55.png" = <ab80e1bc dcf41159 359a9eb7 9cca2b20 2715de69>;
        "960x640/ui/achievement_icon_56.png" = <1efca115 8bfe62a8 30eb435e 532b800f c7654296>;
        "960x640/ui/achievement_icon_57.png" = <6d3ef66d 17233e0a 855df7dc 404e7f14 6729957a>;
        "960x640/ui/achievement_icon_6.png" = <9e7196c6 fc56e23f 90b0d3c0 86618034 72fce61b>;
        "960x640/ui/achievement_icon_7.png" = <829368fe d676b454 a19a51c3 180d0363 5523aa39>;
        "960x640/ui/achievement_icon_8.png" = <4fd71376 be8b90ea 0d677388 d87eceb5 5d377b10>;
        "960x640/ui/achievement_icon_9.png" = <c6617aaf 23144541 b0df819b 2b84e534 2dad9526>;
        "960x640/ui/achievements.json" = <263d50f2 54c3dcb1 7309d5ca 9a90f83e 2898a066>;
        "960x640/ui/achievements.plist" = <2dad2f66 b73a7d7a abbe26e2 54a0d2bb 8847629a>;
        "960x640/ui/achievements.png" = <51597da7 2a7fe4c2 3eb732ed 06b18ba8 d29a0bcf>;
        "960x640/ui/activity.plist" = <80c44fe1 cbdeda38 2465a8fd 74ecfc7d ddbaf160>;
        "960x640/ui/activity.png" = <40c365c9 53747f84 20348796 90bdd799 49e531ca>;
        "960x640/ui/atlas/atlas_blue.png" = <74198635 2d83fbdc 13c88e8d 1b8cf679 40933b50>;
        "960x640/ui/atlas/atlas_depth_blue.png" = <78436ec7 94b66ea5 3be313ab ac72d945 f58d459e>;
        "960x640/ui/atlas/atlas_gold.png" = <952704a5 08764b4e edc510e9 f6a24f6b 88cd4776>;
        "960x640/ui/atlas/atlas_gray.png" = <df346fa8 377b7404 43b6b243 9b546c65 773f23e6>;
        "960x640/ui/atlas/atlas_green.png" = <d1417587 faeb763f 526c2e4f bc977170 6a45427d>;
        "960x640/ui/atlas/atlas_he.png" = <4087a2d5 a2ccbebd 11648b14 1937587c f685eabf>;
        "960x640/ui/atlas/atlas_number_grey_2.png" = <4395da4d d105f573 59cfb9ee b60f3b1c 3f35fde1>;
        "960x640/ui/atlas/atlas_number_vip.png" = <834c8102 0778fdd5 c355fc2d fcfa5975 180fc798>;
        "960x640/ui/atlas/atlas_white.png" = <39414815 e63a67e6 2007fd8e 79fbf8b7 b5207ee0>;
        "960x640/ui/atlas/atlas_yellow.png" = <cd9cec6c 1d4627ed 6eaff4c0 c29ff543 23d4635f>;
        "960x640/ui/button.plist" = <10eae06e 62cb8d80 e28c20ea 51f243e0 d3ce5f25>;
        "960x640/ui/button.png" = <92b05ab6 b2b31265 28b5a88a 0dd0c82e 805a6cd9>;
        "960x640/ui/card.json" = <6c1282a6 aeb6cfe5 a774712f 07828e66 b10fc8f5>;
        "960x640/ui/choose_new.json" = <e21bb610 617482c5 34a4733d 5b0e486a 069b3519>;
        "960x640/ui/choose_new.plist" = <0e023f39 641cad87 f28df777 335b8dcb 7f046a3e>;
        "960x640/ui/choose_new.png" = <a835ac17 e69d02db 01ef847e cb02d873 2c4bd508>;
        "960x640/ui/common_icon.plist" = <10778132 61e30ae3 c48385f0 d476ca7f 003052e4>;
        "960x640/ui/common_icon.png" = <668a450a 587dcf34 784b3039 3bd7c9f5 7780a709>;
        "960x640/ui/common_image.plist" = <0dc7b43c c41247d5 3f3257a1 c29d0530 344502f9>;
        "960x640/ui/common_image.png" = <ec993faf 31bddea3 7726ec15 8649e320 34d30557>;
        "960x640/ui/confirm.json" = <9c214eb3 b6868a7b 19b77cfe c301c03d 577bbf33>;
        "960x640/ui/confirm_phone_num.json" = <6863926a 4a7e40a9 36189856 29783ac6 2f554294>;
        "960x640/ui/confirm_win.json" = <c44f96f6 40448d2f 144f0176 c05db437 e91550fc>;
        "960x640/ui/contact.json" = <1c3d5eb4 4f40d74a 473bc133 9b963fc9 395ca3a4>;
        "960x640/ui/dachinko.json" = <8c4a3366 c4d2d401 4a049939 b162624b 1528b37b>;
        "960x640/ui/dachinko.plist" = <83fc74cb c7b8c96e 91ae482a 093364f4 cb5d2b89>;
        "960x640/ui/dachinko.png" = <a02d2cc1 bafcb9d9 e1fe52cf 2793f557 0e2b3683>;
        "960x640/ui/dachinko_howtoplay.json" = <b96bad8b 71b81b28 ed28c454 5120546a 8dfa3cb1>;
        "960x640/ui/debug_panel.json" = <5ac4de40 6cbe036d 20bd1828 4c6e6354 c0e3cb07>;
        "960x640/ui/debug_panel.plist" = <43196605 8f93a952 04b362d5 3540a9b1 0555c4b7>;
        "960x640/ui/debug_panel.png" = <6166e6d9 9017f8d2 4c57129f 3e8b687f 86d7861f>;
        "960x640/ui/draw_card.json" = <ea293aec c58fc29b 1c0ec900 6b684dfe abfa6a26>;
        "960x640/ui/exchange.json" = <2f68ab13 e40f9205 13903b95 925fc233 dbeb70a4>;
        "960x640/ui/fish_data.json" = <8f21fc44 2074b399 30eb1eb1 c25ab037 5f958205>;
        "960x640/ui/fishwiki.json" = <df656066 03b9c6ef 9c50ab52 efaf1f1a 7b03f245>;
        "960x640/ui/fishwiki_1.json" = <0c707119 f4335d38 2a0cc699 10722486 f792024d>;
        "960x640/ui/friend_reward.json" = <dedf4c04 5692aa76 9935212c a2e427c4 709e5695>;
        "960x640/ui/friend_weapon.json" = <3f922894 7d4d297e ed4dd585 7d8ba339 19a09919>;
        "960x640/ui/gold_tips.json" = <cf5f05aa 0221afe4 27720488 471a2413 c2ebd0db>;
        "960x640/ui/guess_finger.json" = <d62bc1e4 ffd76eb3 f784b138 dfa9a199 0f6898e1>;
        "960x640/ui/guess_finger.plist" = <4bb423e0 c4000602 bae15220 e3935280 365da6e7>;
        "960x640/ui/guess_finger.png" = <10817c0b c5597273 3d463ad4 f2189539 72c63b51>;
        "960x640/ui/hintinfo.json" = <bcd968f1 8cac6c16 9845321c 01d75b63 2d810ad3>;
        "960x640/ui/introduce.json" = <0dcd2df3 07b57b90 067793c4 ca71ac0a a775e6de>;
        "960x640/ui/item.json" = <54b47f35 713b3fdd 82f76361 e976607b 024cc6db>;
        "960x640/ui/item_final_reward.json" = <61c70705 fb19ab98 ae203d82 068eeb25 5d9cbc52>;
        "960x640/ui/item_signin.json" = <fc643145 736ced26 366989dc 05c159fc 1175a42d>;
        "960x640/ui/levelup_new.json" = <fb094ac2 c247313b 60d3b9a6 e6ddefcb 23db86f6>;
        "960x640/ui/levelup_new.plist" = <c2181f56 d1d6209a 5b6d3ba5 fb8f8d86 4567f180>;
        "960x640/ui/levelup_new.png" = <855bac19 ef4c972f 9c366622 b98c47ed a6123088>;
        "960x640/ui/login_mission.json" = <9321b6d8 732d6b88 0991afb5 eeeb41bd 1f2a3089>;
        "960x640/ui/login_reward.json" = <3e67d2bf 26c2f96b d4f21186 f5fad6df 3e857045>;
        "960x640/ui/main_screen.json" = <e36a685b 6758587f 5c9ad567 2fe732e8 84856b8d>;
        "960x640/ui/main_screen.plist" = <8dd0edf4 ed0a78c3 abca7efc ffc55489 3a551d63>;
        "960x640/ui/main_screen.png" = <1a35d3cc 9ea163c4 2172d682 dae54d7c 6b649d4d>;
        "960x640/ui/mission.json" = <23ca8daa e2505ffd a022a29f 266cd936 e182b53e>;
        "960x640/ui/mission.plist" = <9c1f126c 4aa2a28d defea52e 1df27c53 d10a3296>;
        "960x640/ui/mission.png" = <5ee61bdd 34de5356 f68e7580 aec41817 2c8fa1c8>;
        "960x640/ui/mission_anycannon.png" = <ccc4a128 3091d611 53837099 2da80848 b5e91c25>;
        "960x640/ui/mission_anyfish.png" = <085c0fb7 20ce745d 9c558b3c baff9a04 4104d76d>;
        "960x640/ui/mission_cannon.png" = <0786a8bc 3ae5a038 f04da8c7 6400692e fad12786>;
        "960x640/ui/mission_cards.png" = <c32d9742 d9d5f9ef bc0f7c95 7062f8d7 06ccfcf8>;
        "960x640/ui/mission_chouka.png" = <668a2715 6c7e9e0c 4f6c7547 36283e0e 6a2cd3dd>;
        "960x640/ui/mission_coin.png" = <7d1c1539 7cd03a11 5286f74e f1fc3e5a aef0ae26>;
        "960x640/ui/mission_cristal.png" = <4dd5e817 848a4527 cac4a12b 97e8fdaf 260a721a>;
        "960x640/ui/mission_earn.png" = <827aa003 f20cf1b0 4455b475 5e74b6b1 60a3a38b>;
        "960x640/ui/mission_evaluate.png" = <1474a3d3 d5b86b4c c945f2bf 6d236c99 6cf7b769>;
        "960x640/ui/mission_exchange.png" = <64f2372e 4c6b7bc2 88c775c8 790aeeab 79b03b3f>;
        "960x640/ui/mission_fever.png" = <041d8b1f 5592f819 8ef81030 079f5e72 f02ae133>;
        "960x640/ui/mission_fishing.png" = <8c2c089f 574c913d 4ee97a9d 4aad6785 b6fcc845>;
        "960x640/ui/mission_fishtide.png" = <d9cd44b4 44c7c8db 7fde8505 f8515978 0250d0be>;
        "960x640/ui/mission_laohuji.png" = <a4dcb2b6 b7c8fe50 ec3b983c c8376b6b 9425bc7c>;
        "960x640/ui/mission_mini.png" = <a63587bc f031ad22 ac82f5ff 8e37ee19 af1b2dee>;
        "960x640/ui/mission_pay.png" = <341a1997 d0c25ab6 508a171f 07772654 1b10d198>;
        "960x640/ui/mission_photo.png" = <672c1f5d c3cc175e 4036562a fbd00036 4bd6d871>;
        "960x640/ui/mission_rate.png" = <1fcb2322 5a98abf2 ab6fcb2d c5e8f197 9cb6c127>;
        "960x640/ui/mission_scene.png" = <2dbc3e96 98b28bc3 90ac8d9d fabb39b7 e5fea742>;
        "960x640/ui/namelist.json" = <6a5e95c4 5948c335 71a29a0d 6012ece2 8aa5b7ca>;
        "960x640/ui/notice.json" = <e5ffc795 6bc20d47 61f96799 7b20d85c dc24b62e>;
        "960x640/ui/notice_image_02.png" = <96f02f52 cccf1938 0026d8fc 1332b68f 52124d73>;
        "960x640/ui/notice_item.json" = <5032d602 2ef42ba2 9445608c add8aaf2 b122da8f>;
        "960x640/ui/offline_reward.json" = <94edca50 17a7e6a6 96683db6 41118fa5 5c8c136e>;
        "960x640/ui/payments.json" = <d82811da 048e223c 4ec1b059 086190e7 3eb85874>;
        "960x640/ui/rank_data.json" = <d6fec711 38cc902c dd1be9d7 429b224f bb1675a9>;
        "960x640/ui/ranking.json" = <8100176f 91a9b960 bfa81dc3 306a6f1b 3f4baab2>;
        "960x640/ui/result.json" = <5726a50f 5a08785d 1fc4cdf3 3a22097b 716aa1a6>;
        "960x640/ui/reward.plist" = <507dd611 d3f05698 4e42d2be 0b17cdab bfd768d8>;
        "960x640/ui/reward.png" = <c235f78f 0d95485d bf6c70b9 c0398f5c 522f96bb>;
        "960x640/ui/reward_confirm.json" = <6ea30944 2b198d0e 6dc5b302 62353177 11096239>;
        "960x640/ui/reward_icon_1.png" = <e651cd2b 26c5c88b 95237156 3d4a30c5 cd3afedf>;
        "960x640/ui/reward_icon_2.png" = <52cdcb31 f2e628c1 2a04216a 71bb5fa6 68cc3985>;
        "960x640/ui/reward_icon_3.png" = <42ef27db 233d9932 c60673b3 626fd2e5 4a3623f8>;
        "960x640/ui/reward_icon_4.png" = <9587096d 65198715 7e1e5c04 f78ea5b1 b3b1cd40>;
        "960x640/ui/reward_icon_5.png" = <10eb4e2d b9650076 b2d6fef8 9e100b4c 11c13be8>;
        "960x640/ui/reward_icon_6.png" = <52bddb43 ba5ac0ef 28e611f0 02dbb0e6 35b1c6ce>;
        "960x640/ui/reward_icon_7.png" = <dbc65b99 e701560e e8159fb3 db763731 91e2c938>;
        "960x640/ui/reward_tip.json" = <fa9c80e6 2c0094fc 44093b16 8dd68318 4464be7a>;
        "960x640/ui/sale.plist" = <e2b93965 0680a5dd cba633da 8140a02f d8ee10d7>;
        "960x640/ui/sale.png" = <1291fa47 6b16cb60 59e7d6dd cb038545 b9f62f73>;
        "960x640/ui/sale_icon.json" = <2fe3bf74 6a4c8160 0afc2bd2 c9b77edf 8c014f6b>;
        "960x640/ui/sale_layer_locked_weapon.json" = <c74901f1 b5e62920 07240076 540fbac6 14df95a5>;
        "960x640/ui/sale_layer_locked_weapon_confirm.json" = <1e9dff01 4223a7cc bb620301 1f73aabc 08089410>;
        "960x640/ui/sale_panel2.json" = <f9af96fc 74edcab4 5ab4bf30 4c31da0f 7059ce83>;
        "960x640/ui/secrectbox.json" = <9afccd95 9e56f0a7 77466ddd 652b7b4a a627fa48>;
        "960x640/ui/setting.json" = <87d884d1 1c4e676b 4fb0e392 e90e4367 085dd16d>;
        "960x640/ui/setting.plist" = <10150e6c d042a9a4 785a1399 8d5dc4c4 e717cd10>;
        "960x640/ui/setting.png" = <f2f712ce b860a271 58d70d68 de73a638 ee3d8c25>;
        "960x640/ui/share.json" = <dc90cae1 92e41e12 3a0012c3 229f34f6 06287e59>;
        "960x640/ui/shark_teeth.json" = <950f83c9 7cc4da6c 4992e225 a2f1307c d6b889e0>;
        "960x640/ui/show_all.json" = <e109ad22 c6259525 e61ea478 1e7f8ba5 b1782afb>;
        "960x640/ui/start.plist" = <bf6866a9 267f69a7 a5dc426d ce6303e0 6d156077>;
        "960x640/ui/start.png" = <83a68e24 43ef1547 c177d05c 6f6bac37 f88f1eb3>;
        "960x640/ui/start_logo.json" = <ef01cb23 d26494df 40b5ad61 8ddb54a2 59d0f053>;
        "960x640/ui/start_menu.json" = <96d6b17e e96645d0 910fffe3 1a04f496 30fb1c6b>;
        "960x640/ui/store.json" = <e3eb17d4 e8a6c918 3e7bbf83 2f96e695 471291e8>;
        "960x640/ui/store.plist" = <1cfe64db 103262fd d21b0d9a 7f69d9ca 5b662e8a>;
        "960x640/ui/store.png" = <eb5da798 d3f9a0ed 596d918d fc1ba740 f92eaa1d>;
        "960x640/ui/submarine.json" = <6981f5da 80ea92a1 50ddd53a cbd939fc 86ba7b1e>;
        "960x640/ui/submarine.plist" = <4e8409c7 f8627578 ec34465d ee2bf68e 11606458>;
        "960x640/ui/submarine.png" = <3e729d61 4233ccdd f29c3cf7 5bede690 a84e606f>;
        "960x640/ui/tip_arrow.json" = <b082979f ef0de989 a1ab4982 624b945a 07a1c3d8>;
        "960x640/ui/tip_confirm.json" = <34bdb02a a0ad5fe3 a83cabd6 11fec71c de54ab91>;
        "960x640/ui/tip_general.json" = <ee888461 cec0aabf c7cfef44 c570355c 6d54972e>;
        "960x640/ui/tip_gold.json" = <6aa90a6b 36d0ef50 a0efdc43 412dbdec ebf9957c>;
        "960x640/ui/tip_panel.json" = <6f8a3439 88e941da 5b376a94 50cc9c3c 0012c5e7>;
        "960x640/ui/tip_panel_guess.json" = <ad8f02d0 a150f578 2270986d eb6034b8 39fbba9a>;
        "960x640/ui/user_guide_choose.json" = <a8090de2 bc62d611 029fd3b6 eb5fb556 c40b1a26>;
        "960x640/ui/vip_power.json" = <bc216212 51c121dc 734f2c23 9bb46fdd ea4ea086>;
        "AppIcon29x29.png" = <5a3bb2d5 cea58e40 c8dadb5b 58f2ad06 7ca407b5>;
        "AppIcon40x40@2x.png" = <ba39ea7b ca805a6a 7e20b27b 72977e86 921a247d>;
        "AppIcon40x40@2x~ipad.png" = <ba39ea7b ca805a6a 7e20b27b 72977e86 921a247d>;
        "AppIcon40x40~ipad.png" = <90b84887 f65441f9 0db4e3a0 b2680128 6f2bfac2>;
        "AppIcon50x50@2x~ipad.png" = <5bc8923d de167558 80f38883 95c8ef99 735e38f0>;
        "AppIcon50x50~ipad.png" = <899d758a a62b1004 d4769c4b 1288412d 101d6821>;
        "AppIcon57x57.png" = <916ed0ad f7c27981 32d141f1 84688bad bb92d473>;
        "AppIcon57x57@2x.png" = <0d2192d1 0ad30611 ba6cd9c2 39c2ab3f a48c1785>;
        "AppIcon60x60@2x.png" = <81189d9d 44e0b49f a10fd389 2dbd0aca d54f268a>;
        "AppIcon72x72@2x~ipad.png" = <8e872e8b 1fdbf071 05efec7f b79f067f bb5e0722>;
        "AppIcon72x72~ipad.png" = <e1d223f0 eff4c624 b70e7fa7 f56339bf 7383d39b>;
        "AppIcon76x76@2x~ipad.png" = <f2d27219 7c0f423b aab121fc bac5c9a5 c4e09e82>;
        "AppIcon76x76~ipad.png" = <e9be2d81 9c7687c8 e1d487c4 43b26b80 a23e17ca>;
        "ChanceAdRes.bundle/Root.plist" = <5656c74f 50cb8281 22f60148 3d0e814b a6d983f6>;
        "ChanceAdRes.bundle/cs_browser_back@2x.png" = <e8d054f4 7b28a22a 1e6eb35a 4455fd9f 69e7a6d7>;
        "ChanceAdRes.bundle/cs_browser_back_disabled@2x.png" = <a6837738 8dbf8839 636ed57a 030f9235 7f884ff5>;
        "ChanceAdRes.bundle/cs_browser_btnbg_hl@2x.png" = <676b370a cb5be9d5 d401e538 f849bc6a fec9c686>;
        "ChanceAdRes.bundle/cs_browser_btnbg_nor@2x.png" = <619fa50a 65774209 1097da75 851576d4 d64cf3cf>;
        "ChanceAdRes.bundle/cs_browser_cancelfullscreen@2x.png" = <bebfa56f fd96ae67 4b8bccea 577f97a3 5fcc1f97>;
        "ChanceAdRes.bundle/cs_browser_close@2x.png" = <1f91a1b9 c8c2e974 ecd13886 b0b91e46 d686fec6>;
        "ChanceAdRes.bundle/cs_browser_forward@2x.png" = <2c1ee075 6aa1c8cb 9aea4905 26acce2c bdba6f12>;
        "ChanceAdRes.bundle/cs_browser_forward_disabled@2x.png" = <f4c8bd30 442c50cf 02598ce0 05582351 b148207e>;
        "ChanceAdRes.bundle/cs_browser_fullscreen@2x.png" = <287e7203 2666abe4 ececa037 e833b63e a0105562>;
        "ChanceAdRes.bundle/cs_browser_refresh@2x.png" = <e32e3ffb 8ab28c55 88eea9f6 ec0341eb 8e3ac5d7>;
        "ChanceAdRes.bundle/cs_browser_share@2x.png" = <cd5407e0 e292f41a d25adc4a c1187cb0 197e9831>;
        "ChanceAdRes.bundle/cs_cocobear@2x.png" = <3e636c9e 559cb03c 5a7f520b 82fb7046 5d558da0>;
        "ChanceAdRes.bundle/cs_loading_0@2x.png" = <f1ff6f14 9fa2e327 8547eddf d37fc1c6 137091f7>;
        "ChanceAdRes.bundle/cs_loading_1@2x.png" = <25e98723 fe7366ed faa15064 364e08ff 4f4616db>;
        "ChanceAdRes.bundle/cs_loading_2@2x.png" = <b6ef345a 190f0162 f1959a43 18f6ffe5 ef851ae8>;
        "ChanceAdRes.bundle/cs_loading_3@2x.png" = <8c526032 b0cfcbfd 4e493662 35a3d213 264a595a>;
        "ChanceAdRes.bundle/cs_loading_4@2x.png" = <b6ef345a 190f0162 f1959a43 18f6ffe5 ef851ae8>;
        "ChanceAdRes.bundle/cs_loading_5@2x.png" = <25e98723 fe7366ed faa15064 364e08ff 4f4616db>;
        "ChanceAdRes.bundle/cs_reload_d@2x.png" = <440f5d61 72467efb 8ae59178 6f838a96 b2dc2dea>;
        "ChanceAdRes.bundle/cs_reload_n@2x.png" = <a80e0c83 bb8f7cf3 a2833c72 76654028 ad3d57e6>;
        "ChanceAdRes.bundle/csi_btn_close_normal@2x.png" = <0da2edcc 819b4c87 826ddb35 cbc90740 6f7f898a>;
        "ChanceAdRes.bundle/csi_btn_close_normal_iPad@2x.png" = <cedb2386 39a92ef9 af89553e e24bc675 4ba55ec5>;
        "ChanceAdRes.bundle/csm_btn_recom0@2x.png" = <c844a0c5 b4be6d13 2c66c924 be1d9bfc 5fea5227>;
        "ChanceAdRes.bundle/csm_btn_recom0@2x~ipad.png" = <481858b8 aa6ff637 356076f5 51df152b 4c90b84c>;
        "ChanceAdRes.bundle/csm_btn_recom10@2x.png" = <b793669e a70a29d8 57145cc0 b243fa35 bf68dc5f>;
        "ChanceAdRes.bundle/csm_btn_recom10@2x~ipad.png" = <b37a1fbd 4dd8588a ac05c24e c6c86c9b f2af0440>;
        "ChanceAdRes.bundle/csm_btn_recom11@2x.png" = <dab19677 41001a0a 7a94a7d2 a9b6a896 8dd45a1b>;
        "ChanceAdRes.bundle/csm_btn_recom11@2x~ipad.png" = <c3f61aca 588bb928 700d50e7 e68d8768 b463f59b>;
        "ChanceAdRes.bundle/csm_btn_recom1@2x.png" = <3697b9b7 f40b23bd 0e120530 19b27a64 a9224b35>;
        "ChanceAdRes.bundle/csm_btn_recom1@2x~ipad.png" = <287a8959 14ac8110 e609cbf4 ed0cf87c d2092b36>;
        "ChanceAdRes.bundle/csm_btn_recom2@2x.png" = <62544e88 b532522d c7e392a1 743160d3 80f27254>;
        "ChanceAdRes.bundle/csm_btn_recom2@2x~ipad.png" = <5e16a91e 5c668f44 d9ad62be 9e0bb881 5096f0dd>;
        "ChanceAdRes.bundle/csm_btn_recom3@2x.png" = <fbeb8bcc f6e1b374 9fb77f41 f85fa3de 88899efa>;
        "ChanceAdRes.bundle/csm_btn_recom3@2x~ipad.png" = <e689c255 7105213a 96ae7598 8ec189c8 03b972df>;
        "ChanceAdRes.bundle/csm_btn_recom4@2x.png" = <75780b03 90fb3939 770bcc75 97b25969 e98fe73f>;
        "ChanceAdRes.bundle/csm_btn_recom4@2x~ipad.png" = <bba5e652 efdec782 427f2be6 186fc539 64cd131a>;
        "ChanceAdRes.bundle/csm_btn_recom5@2x.png" = <621ec429 950b1a07 75120f7b 53cef79f 0050bcb6>;
        "ChanceAdRes.bundle/csm_btn_recom5@2x~ipad.png" = <9354eef2 26141f33 de83fe41 ef2e0592 183eb505>;
        "ChanceAdRes.bundle/csm_btn_recom6@2x.png" = <2d002ed9 3c912920 fe08b3de fa9194bb 93fa131b>;
        "ChanceAdRes.bundle/csm_btn_recom6@2x~ipad.png" = <7108717f 72eac824 3a933bef 47691fa5 ef49c405>;
        "ChanceAdRes.bundle/csm_btn_recom7@2x.png" = <a74f5666 c3484567 66bfe224 521064a2 c4ccdaad>;
        "ChanceAdRes.bundle/csm_btn_recom7@2x~ipad.png" = <56cd25c1 c7f54fff 46de6787 faf5e768 e6885b56>;
        "ChanceAdRes.bundle/csm_btn_recom8@2x.png" = <45b6d198 0b9cdd59 1391c5e2 259bc061 0ef682b9>;
        "ChanceAdRes.bundle/csm_btn_recom8@2x~ipad.png" = <efef7355 2ad54d92 ebcb8e61 3a471634 6698ba91>;
        "ChanceAdRes.bundle/csm_btn_recom9@2x.png" = <21118dfe 5cbd5e66 4dd0bd3e ac669d82 b26d3da1>;
        "ChanceAdRes.bundle/csm_btn_recom9@2x~ipad.png" = <3bc628a5 30416dcb 3eb38f72 250f7428 14b80dcc>;
        "ChanceAdRes.bundle/csnm_arrow@2x.png" = <c885b08e 6aaa22e0 06b08470 b0140fbe 153f923b>;
        "ChanceAdRes.bundle/csnm_focusView_default@2x.png" = <078a13ec c28b0620 35a2f58f 6a93187a 97ab5aa3>;
        "ChanceAdRes.bundle/csnm_nav_back@2x.png" = <d5ddd224 f164a41a 5610d50b 9c66efa6 f3358a34>;
        "ChanceAdRes.bundle/csnm_nav_back_ipad@2x.png" = <e74cbbea 6eb6d885 3efac2e2 931f2931 8784e2e3>;
        "ChanceAdRes.bundle/csnm_nav_back_sel@2x.png" = <e439d864 15aadde9 38ce5765 73700b28 5c0ddb87>;
        "ChanceAdRes.bundle/csnm_nav_back_sel_ipad@2x.png" = <406b6249 abad2a32 5bd33dc5 23ded3ab 82fe7aec>;
        "ChanceAdRes.bundle/csnm_nav_coco@2x.png" = <20fc7489 63eff479 2d38a47f 39b72539 32dc9c35>;
        "ChanceAdRes.bundle/csnm_sectionView_clickMore@2x.png" = <0a8b8ddd bd87096c 409886e3 11f6f99f fcae18ed>;
        "ChanceAdRes.bundle/csnm_sectionView_more@2x.png" = <0b7cd058 bc62008d 34b9ad58 153db4bc 984bed21>;
        "ChanceAdRes.bundle/csnm_sectionView_more_ipad@2x.png" = <52c0a720 ba51c7a4 9b2409ca 4bca06fa b3c7b125>;
        "ChanceAdRes.bundle/csnm_sectionView_more_sel@2x.png" = <dea437b7 170bf17e b65cbbb1 c9ed26fd 40694af7>;
        "ChanceAdRes.bundle/csnm_sectionView_more_sel_ipad@2x.png" = <d6d0d9d5 71b461ee 72877a3e 11a3cc3c b3e396e0>;
        "ChanceAdRes.bundle/csnm_singleView_charge@2x.png" = <366e163d 4e3cbcf2 41736050 ad1a58df 2b26f39e>;
        "ChanceAdRes.bundle/csnm_singleView_charge_ipad@2x.png" = <033d73f0 d31a9aae 314b375e 82c6dcd7 4daf233b>;
        "ChanceAdRes.bundle/csnm_singleView_charge_sel@2x.png" = <f1c79c27 5bfc4242 a460b4c4 d6829461 2278e5bc>;
        "ChanceAdRes.bundle/csnm_singleView_charge_sel_ipad@2x.png" = <a03d68ec bf4dd5c3 0c78ac13 67f92fdc 708280e9>;
        "ChanceAdRes.bundle/csnm_singleView_free@2x.png" = <d6297aa9 0d69eeb3 14b25ad3 2ccd3e69 3160327c>;
        "ChanceAdRes.bundle/csnm_singleView_free_ipad@2x.png" = <78e808b3 3e1fca45 8fbbb5b4 974322ab 887dc6f8>;
        "ChanceAdRes.bundle/csnm_singleView_free_sel@2x.png" = <a48950fc 2051484e 0f6b7dea 02acc324 e9427c5a>;
        "ChanceAdRes.bundle/csnm_singleView_free_sel_ipad@2x.png" = <c8fd73c2 3579df68 6db61dbf 503576b9 d9ee8df4>;
        "ChanceAdRes.bundle/csnm_singleView_iconDefault@2x.png" = <9184f47a a3abee19 da518a48 3e636949 1b78529e>;
        "ChanceAdRes.bundle/csnm_singleView_iconDefault_ipad@2x.png" = <c16c905d f66e6807 c52aa6ec 5e933ec5 e8c4e9bb>;
        "ChanceAdRes.bundle/csnm_singleView_icon_leftConer@2x.png" = <2cfcdc0f 16ed88f8 3c0d9d78 a1bf26e4 344b04a6>;
        "ChanceAdRes.bundle/csnm_singleView_icon_leftConer_ipad@2x.png" = <28ba57d4 3fb2a23b 5a243e4d 5126a381 e155560e>;
        "ChanceAdRes.bundle/csnm_singleView_limitFree@2x.png" = <ef6af672 235742c1 97bb1222 b2c2a27b c0d294f8>;
        "ChanceAdRes.bundle/csnm_singleView_limitFree_ipad@2x.png" = <e4c6c3c4 ba242615 3c495e88 4e6a24ca 002bb5dc>;
        "ChanceAdRes.bundle/csnm_singleView_limitFree_sel@2x.png" = <59c7fa46 9d849d87 9ab16843 757ae52a 65e90b24>;
        "ChanceAdRes.bundle/csnm_singleView_limitFree_sel_ipad@2x.png" = <7f2f87e6 dc49b467 eba85f2d 28fc5d69 8fd1d093>;
        "ChanceAdRes.bundle/csnmg_appinfofolder@2x.png" = <dd1aed88 7f10c1f6 10dab88a 4ff76126 c2a58192>;
        "ChanceAdRes.bundle/csnmg_appinfofolder_ipad@2x.png" = <bf89c380 3a5116b6 f61e53dd c10cbabf 6609bbf4>;
        "ChanceAdRes.bundle/csnmg_appinfoicon@2x.png" = <1ac44d14 bb88f503 bb874099 583b9f3a 31e1d54f>;
        "ChanceAdRes.bundle/csnmg_downloadapp@2x.png" = <ba7f993d a91f6cb2 0f654376 c51ca38e 7a582952>;
        "ChanceAdRes.bundle/en.lproj/Root.strings" =         {
            hash = <b78298f4 6878b888 8ebfb567 1ed46b28 4b8f458f>;
            optional = 1;
        };
        "FishingJoy3_iphone_zh_cn.plist" = <d53a1d35 dd7b7f5c 5e136264 571236cb e21cb731>;
        "LaunchImage-568h@2x.png" = <cb9776fc a17794eb e8648f91 74d1ebd2 92492d33>;
        "LaunchImage.png" = <65da0ee6 2d8c033e 7e392667 e4adef75 a2562534>;
        "LaunchImage@2x.png" = <12191829 142ccd84 98af384f d768d3a2 5380ccb3>;
        "Resource.bundle/ActionSheet/ActionSheetBG.png" = <92391720 89c6d49e 490c1e70 0eaea273 68471e35>;
        "Resource.bundle/ActionSheet/ActionSheetBG@2x.png" = <2aaa5a1d 02689eda 854bd9b5 72c4b12c ef1a83fc>;
        "Resource.bundle/ActionSheet/ActionSheetCancelButtonBG.png" = <5b05bf52 c4adc3eb 6893fc10 1fc0b5cf 7c070d1a>;
        "Resource.bundle/ActionSheet/ActionSheetCancelButtonBG@2x.png" = <4ed91d4b 8262b5fb 394ac91e 3c5ddd3a 56a76240>;
        "Resource.bundle/ActionSheet/action-black-button-selected.png" = <c5991c7f e98091d3 07a8d03a 1b6d016c 3758e700>;
        "Resource.bundle/ActionSheet/action-black-button-selected@2x.png" = <33a1e3cf 23f21a70 2fea1c66 06843749 d218bef4>;
        "Resource.bundle/ActionSheet/action-black-button.png" = <c3236bb3 28194266 5b48bf0c a2e9135f 7ac98f6d>;
        "Resource.bundle/ActionSheet/action-black-button@2x.png" = <6ba4ee35 17bfacb3 70a7a7dd d8795489 a05944c4>;
        "Resource.bundle/ActionSheet/action-gray-button-selected.png" = <5905b128 80b38b3f 0b3a7390 a19a6036 bd038bfe>;
        "Resource.bundle/ActionSheet/action-gray-button-selected@2x.png" = <98a27241 5979e1ca a4a7c7e3 ac19b9a3 36675825>;
        "Resource.bundle/ActionSheet/action-gray-button.png" = <ae6db988 10152173 22d01849 9e2fa9db 896e358f>;
        "Resource.bundle/ActionSheet/action-gray-button@2x.png" = <89715b1b 05fab80c 8065e8b6 01a8c8e3 35630570>;
        "Resource.bundle/ActionSheet/action-red-button-selected.png" = <3ebd12f5 4a37b236 1060296d ecc3f682 2b0cc7bd>;
        "Resource.bundle/ActionSheet/action-red-button-selected@2x.png" = <8e6e401b 20045ad4 3975e466 f27ad371 94a22ad6>;
        "Resource.bundle/ActionSheet/action-red-button.png" = <a51a6916 72c371c9 6b057e8c a9d686ce 09814017>;
        "Resource.bundle/ActionSheet/action-red-button@2x.png" = <af26cb56 3aab1514 15cec4b2 7065abea 09f8516a>;
        "Resource.bundle/ActionSheet/action-sheet-panel.png" = <e9c8c82f 6d296baf 83f94a28 6412cdd1 b2d61cb4>;
        "Resource.bundle/ActionSheet/action-sheet-panel@2x.png" = <310309c3 df6d30c3 966f9f26 ba8d620d d792fe5c>;
        "Resource.bundle/AuthView/CheckBoxSelectedSkin.png" = <0b429194 6fc46756 c2177d9f 471041b0 9abfffc4>;
        "Resource.bundle/AuthView/CheckBoxSelectedSkin@2x.png" = <cebbabe1 ad6d40c9 06d5cd91 0a27f96b 69c1b463>;
        "Resource.bundle/AuthView/CheckBoxUpSkin.png" = <757d5580 51f6efb2 c075cd57 bd61c61f e35e84fe>;
        "Resource.bundle/AuthView/CheckBoxUpSkin@2x.png" = <90c16741 1c6056fd 64c258ef f0624181 6fd17a16>;
        "Resource.bundle/AuthView/CloseButton.png" = <67474cdd 8aa03353 c4e2ad00 8790125d 6908c2af>;
        "Resource.bundle/AuthView/CloseButton@2x.png" = <ab1aa879 e939fd92 f2b49351 f510cc1d eeee9256>;
        "Resource.bundle/AuthView/NavigationBG.png" = <2d8ae6ad 6bc04f24 6acec297 49d88bf8 081a2c7e>;
        "Resource.bundle/AuthView/NavigationBG@2x.png" = <67045a2e 87db3ed4 e4a44908 9602bae5 9b893c36>;
        "Resource.bundle/AuthView/PowerBy.png" = <832746cb 4f032143 ec766dfc 6f5aab19 eb7e4ffb>;
        "Resource.bundle/AuthView/PowerBy@2x.png" = <6285e480 bde77451 a7f2825b d0ca9033 53d35dd1>;
        "Resource.bundle/AuthView/ToolbarBG.png" = <bb09d254 adc17b1e 7f46dbf1 484d930b d93f6d9f>;
        "Resource.bundle/AuthView/ToolbarBG@2x.png" = <5ee6c42f a4a298d5 c782e2a2 29c06169 f29806fb>;
        "Resource.bundle/Common/AuthViewCloseButton.png" = <35a8e5df 2aec3f0b 35052239 d0590556 f91b8010>;
        "Resource.bundle/Common/AuthViewCloseButton@2x.png" = <36779356 fe74001c f5432bfa 87269d12 2259b237>;
        "Resource.bundle/Common/NavigationButtonBG.png" = <7e3bdb84 ac6b33af 4e0ee380 f278bb25 2f000a83>;
        "Resource.bundle/Common/NavigationButtonBG@2x.png" = <50db03b0 127c41ab 8c8ada34 844309d2 87002884>;
        "Resource.bundle/Common_Landscape/NavigationButtonBG.png" = <cd9189e2 9902dfac 8fb024dd 4465be5c 8bb99856>;
        "Resource.bundle/Common_Landscape/NavigationButtonBG@2x.png" = <da8b5051 a0adee40 b21b5893 8e157cc8 ae767ca4>;
        "Resource.bundle/Icon/sns_icon_1.png" = <e29f4975 376ea644 1bff0b05 f67e9926 e0a62a3b>;
        "Resource.bundle/Icon/sns_icon_10.png" = <0dc67a95 83b86604 6a29728b 0ccc22d4 e6d4d236>;
        "Resource.bundle/Icon/sns_icon_10@2x.png" = <fe29d435 89281a5c b7b3cdb3 937220f2 c84ec51d>;
        "Resource.bundle/Icon/sns_icon_10_s.png" = <ed97397d 5426642b cc0a4638 8fb8c57e b572c352>;
        "Resource.bundle/Icon/sns_icon_10_s@2x.png" = <a54a9bfd 4b5eae23 96575702 8e74d2ce 768ddc87>;
        "Resource.bundle/Icon/sns_icon_11.png" = <2e024fb9 56437612 c19433a6 0df06080 756c278c>;
        "Resource.bundle/Icon/sns_icon_11@2x.png" = <af95bc5c f776a5bb e53518e5 76aca815 13642a29>;
        "Resource.bundle/Icon/sns_icon_11_s.png" = <2d4cbed4 61c59c52 91690191 f32e3b94 f2300a3e>;
        "Resource.bundle/Icon/sns_icon_11_s@2x.png" = <b5e1e2c6 0624c221 95326553 b0a90413 cd38fde0>;
        "Resource.bundle/Icon/sns_icon_12.png" = <82d30020 7fd16f80 0ab830bc 9d6b328a b3977dfa>;
        "Resource.bundle/Icon/sns_icon_12@2x.png" = <26c0326c 6e8311d5 c978daa4 89c3d4c1 a6aa8661>;
        "Resource.bundle/Icon/sns_icon_13.png" = <3515eaf8 25775149 afda6b1b 7ea07fc4 41db997c>;
        "Resource.bundle/Icon/sns_icon_13@2x.png" = <559a47d2 ed54cf7c cef1b4f0 3799ad42 ea3f1bec>;
        "Resource.bundle/Icon/sns_icon_14.png" = <e3cb0922 392061bf 292ded84 48d2d70c fa9d8a90>;
        "Resource.bundle/Icon/sns_icon_14@2x.png" = <f1bb159e 7862d7d7 d4b0f42c 0bfca0db 320127ea>;
        "Resource.bundle/Icon/sns_icon_15.png" = <9dcc100a 4a0c8ef7 9ed90672 1e2440ff 5f24ac66>;
        "Resource.bundle/Icon/sns_icon_15@2x.png" = <cab2847c 245518a5 3610ae00 b476b9b1 bbecd158>;
        "Resource.bundle/Icon/sns_icon_16.png" = <a432d0d0 d72ff694 5d3baeb5 3959fece 895d01fa>;
        "Resource.bundle/Icon/sns_icon_16@2x.png" = <c29d2609 81b281cb 48f1e4ac 86495124 b77fd04b>;
        "Resource.bundle/Icon/sns_icon_17.png" = <7b5dfd4c 5183a095 84655aa7 dde85b50 413e762a>;
        "Resource.bundle/Icon/sns_icon_17@2x.png" = <d9cf2c7a 2a61616b bf3aa2e2 42e7dd74 21f44f06>;
        "Resource.bundle/Icon/sns_icon_18.png" = <015f9a9a 372e6871 782ce8bf 14786db5 ef2bf9d9>;
        "Resource.bundle/Icon/sns_icon_18@2x.png" = <15e79663 2320bb22 5f47768d 839a9437 289fe2bf>;
        "Resource.bundle/Icon/sns_icon_19.png" = <c085d04d 897a92bd 70d60a0e 1ec46b41 a9faa102>;
        "Resource.bundle/Icon/sns_icon_19@2x.png" = <38a1b573 7b94f09f 223b0ef4 47af4822 bc43a0ee>;
        "Resource.bundle/Icon/sns_icon_1@2x.png" = <237a7fed 901fe283 cc87d25b 3f1601c5 edb8d583>;
        "Resource.bundle/Icon/sns_icon_1_s.png" = <de1d7a87 f89ec4fc 609e3f62 98040d70 59f3b855>;
        "Resource.bundle/Icon/sns_icon_1_s@2x.png" = <409d38c9 37f1cfb6 3bb6f10d f7e8a9c8 ea09feeb>;
        "Resource.bundle/Icon/sns_icon_2.png" = <5f591cee 1c7e5e87 c4dda98d 4c313155 0e7c502e>;
        "Resource.bundle/Icon/sns_icon_20.png" = <e1175ef6 3586b7f4 647afb15 45a9814f b64b4073>;
        "Resource.bundle/Icon/sns_icon_20@2x.png" = <3f343068 423033e8 f46634ce 3ff9dfdb f7277b6e>;
        "Resource.bundle/Icon/sns_icon_21.png" = <09d9213f af25511b ba3966b7 a2fee75c a0e77666>;
        "Resource.bundle/Icon/sns_icon_21@2x.png" = <e271dee1 5d007032 8cc9ce12 3bfbeddb 44753b10>;
        "Resource.bundle/Icon/sns_icon_22.png" = <6d8d8dc7 42535b3b 3ee60d1e 659307b7 4c8303f1>;
        "Resource.bundle/Icon/sns_icon_22@2x.png" = <addb98aa ba1028cb 7ba147bd 138e50d0 817680d9>;
        "Resource.bundle/Icon/sns_icon_23.png" = <93478f8e 20327b38 bae1e04d 326ccafa d5e91229>;
        "Resource.bundle/Icon/sns_icon_23@2x.png" = <2313debe dcf0597c bb8fef48 6f8a571a b85832e9>;
        "Resource.bundle/Icon/sns_icon_24.png" = <5357e722 0e1692b1 196c03ff 38cc6d4f ebb0e3c0>;
        "Resource.bundle/Icon/sns_icon_24@2x.png" = <a976b463 50156a2d 006e93aa 5bcca8ba c28c0058>;
        "Resource.bundle/Icon/sns_icon_25.png" = <0e01c4ed 10f52922 048edbe6 434c738a 51b86831>;
        "Resource.bundle/Icon/sns_icon_25@2x.png" = <eb815066 fa664946 8cc7a255 a1c5dbca 79f8b9f4>;
        "Resource.bundle/Icon/sns_icon_26.png" = <7f917614 9dcafe35 f88b8d3c 86de8e1a 5e6afeba>;
        "Resource.bundle/Icon/sns_icon_26@2x.png" = <680ab78d eb4eb169 9cbe1645 39f26eee f7c7497b>;
        "Resource.bundle/Icon/sns_icon_27.png" = <e1d23d7b e051d595 95d2aa82 04c969d2 e0b2b1f0>;
        "Resource.bundle/Icon/sns_icon_27@2x.png" = <a46c9e6f 5a0a9c2d e1f2e1c4 866e42c2 716b3f7a>;
        "Resource.bundle/Icon/sns_icon_28.png" = <4c437d78 6d1f6c6c 5c1c7b7f 85269702 1da665e8>;
        "Resource.bundle/Icon/sns_icon_28@2x.png" = <b249d93f 81df3e0b de48744b 4eeb6d2c 1045136a>;
        "Resource.bundle/Icon/sns_icon_28_s.png" = <ea2ff5aa 79051120 feab1b4e ec9713dc da2dcc5b>;
        "Resource.bundle/Icon/sns_icon_28_s@2x.png" = <34077fba ef2d8013 a5186f1a 6547dfcd b1069fb0>;
        "Resource.bundle/Icon/sns_icon_2@2x.png" = <ec0dc479 2d96adc2 2265c085 f5e3350f 6afb7867>;
        "Resource.bundle/Icon/sns_icon_2_s.png" = <6dfa0879 ea3f3ab8 ff986717 8d38dd77 03512d43>;
        "Resource.bundle/Icon/sns_icon_2_s@2x.png" = <73418567 49e2ba88 e7397a69 4005764f 8debd7d2>;
        "Resource.bundle/Icon/sns_icon_3.png" = <b8219899 0c1218f6 356ccb5f d6ff1199 3f67bcab>;
        "Resource.bundle/Icon/sns_icon_30.png" = <b1f131a0 84bcbe71 432afe59 18bb56da 37b7df28>;
        "Resource.bundle/Icon/sns_icon_30@2x.png" = <437b815f 7b7ef1e8 6342ecd3 8e40419f 66ae696f>;
        "Resource.bundle/Icon/sns_icon_34.png" = <c8209104 050836b9 767782b1 5eb8b618 5eb584d9>;
        "Resource.bundle/Icon/sns_icon_34@2x.png" = <0842d8f0 9eae71d0 5e705314 71064473 8aad79bd>;
        "Resource.bundle/Icon/sns_icon_35.png" = <d250b316 6026ae67 4af6bd6c a4f60ff1 854c846c>;
        "Resource.bundle/Icon/sns_icon_35@2x.png" = <0131e00e d2b8e2e5 e20ee0c1 d0928c67 7516327b>;
        "Resource.bundle/Icon/sns_icon_36.png" = <9a83ca9d 43778880 c8696ea4 ca85a8a4 e295cb23>;
        "Resource.bundle/Icon/sns_icon_36@2x.png" = <11cfef8a 7155db59 3223f3ed 45a1d05a 45f3a62c>;
        "Resource.bundle/Icon/sns_icon_37.png" = <5f0c2fba c8003cb3 60f16424 7df4ae30 23c24d2b>;
        "Resource.bundle/Icon/sns_icon_37@2x.png" = <0bdc50e0 2b1c7adc 98310083 30633b2d 3ed1207c>;
        "Resource.bundle/Icon/sns_icon_38.png" = <91428389 85b675e6 552aba49 977588d2 ddbd4a08>;
        "Resource.bundle/Icon/sns_icon_38@2x.png" = <b463b649 ff29620f 59bb4695 bbd28e93 6804c45c>;
        "Resource.bundle/Icon/sns_icon_39.png" = <6796beac 1bab1073 ad20d521 ad3673c9 cd2f16c8>;
        "Resource.bundle/Icon/sns_icon_39@2x.png" = <057c8be2 852817c1 9b9e317b e818d682 95f1131c>;
        "Resource.bundle/Icon/sns_icon_3@2x.png" = <bcf23ea8 bad5c1b4 4c211bfc a276eae4 5c7b09a1>;
        "Resource.bundle/Icon/sns_icon_3_s.png" = <8436eeb1 4b8c5e41 f634ee66 2ba85239 6c6d630c>;
        "Resource.bundle/Icon/sns_icon_3_s@2x.png" = <0559a6d1 44406f85 50864876 9190cf30 37c84781>;
        "Resource.bundle/Icon/sns_icon_4.png" = <3c2a3e03 cd56fa1a 08d0c806 b42156a1 0c5c5ed2>;
        "Resource.bundle/Icon/sns_icon_41.png" = <796846d7 dca086d3 6d62cb2c bf53235d 877acbe4>;
        "Resource.bundle/Icon/sns_icon_41@2x.png" = <affcb792 72658580 f3c0a282 30954d89 ae82debc>;
        "Resource.bundle/Icon/sns_icon_42.png" = <259bd815 44f4e54a 7f21a220 58ad9f96 65d1cec2>;
        "Resource.bundle/Icon/sns_icon_42@2x.png" = <a16772da 05ea2ae2 213c2e99 3c628732 23d879ae>;
        "Resource.bundle/Icon/sns_icon_43.png" = <360efccf b71f132c 109894ca 61a2b8a2 84d1c890>;
        "Resource.bundle/Icon/sns_icon_43@2x.png" = <f42a23e0 78f4b60e 5117e55c 38649b33 dbd37460>;
        "Resource.bundle/Icon/sns_icon_44.png" = <0c9ccd03 9e35c80f 09f1cccd 255972d2 bf97dd40>;
        "Resource.bundle/Icon/sns_icon_44@2x.png" = <9a204c0a 719c821e 6d969448 13c56bc9 d338696a>;
        "Resource.bundle/Icon/sns_icon_45.png" = <42a4abc2 ee3c9c98 59b2009b 0d6e47d5 ecce65bd>;
        "Resource.bundle/Icon/sns_icon_45@2x.png" = <56c8a003 54a7644b 2ab4da83 cc2838f6 f2631038>;
        "Resource.bundle/Icon/sns_icon_4@2x.png" = <9f591520 9bf84918 2e44e7c9 f6d1d5d6 04be2991>;
        "Resource.bundle/Icon/sns_icon_4_s.png" = <30d77296 4f9254f3 6c3e3268 e66a0c92 7c1bb6b2>;
        "Resource.bundle/Icon/sns_icon_4_s@2x.png" = <39fe5214 aee80718 347e0a78 35cb77a5 89b131a8>;
        "Resource.bundle/Icon/sns_icon_5.png" = <7af14e7a 3f840ad3 f83654e8 d7d8c802 bd828f09>;
        "Resource.bundle/Icon/sns_icon_5@2x.png" = <0b75a42f c94bd0a9 014b8c62 d111bb4e b2f60b82>;
        "Resource.bundle/Icon/sns_icon_5_s.png" = <6a4b7c0e 61a94dcf c71aedcc 91d5c8d9 8da5cafc>;
        "Resource.bundle/Icon/sns_icon_5_s@2x.png" = <611c4fe9 3ceb02d1 fa7d234c 77d3c7aa 99722bb3>;
        "Resource.bundle/Icon/sns_icon_6.png" = <bbc7a0c2 6f6512c2 f76a8c18 02ff53b1 4f1dc6f1>;
        "Resource.bundle/Icon/sns_icon_6@2x.png" = <3693db7f 8eb6f987 8e6b956a 04e3247a b1d58d24>;
        "Resource.bundle/Icon/sns_icon_6_s.png" = <86baff92 ee2ff1fa c686bbda 8b508f3b 500bbd83>;
        "Resource.bundle/Icon/sns_icon_6_s@2x.png" = <647e548a ee5eaf72 16465da4 52cf5e45 64a0a330>;
        "Resource.bundle/Icon/sns_icon_7.png" = <7d2d8adf 120696d0 b745a85f a3519960 34b43c7e>;
        "Resource.bundle/Icon/sns_icon_7@2x.png" = <81ae8fd0 05cf0209 450abb20 0ad7155d 625025de>;
        "Resource.bundle/Icon/sns_icon_7_s.png" = <b43cfbfb 820dbf4f 70535810 b5c4ebe2 92e66d87>;
        "Resource.bundle/Icon/sns_icon_7_s@2x.png" = <064cfcea 5ced40aa 03c7703d 0f93dea9 836c434d>;
        "Resource.bundle/Icon/sns_icon_8.png" = <f80b6348 67d45b65 93b580a9 c8834b4e 9f5c32dd>;
        "Resource.bundle/Icon/sns_icon_8@2x.png" = <717d7716 ee69c262 f1e3e59a 4605c689 67cd90f9>;
        "Resource.bundle/Icon/sns_icon_8_s.png" = <8c786e81 8120a662 f758e9db 28ecc4aa 3611c1a6>;
        "Resource.bundle/Icon/sns_icon_8_s@2x.png" = <8c2846ed ef8e5db0 465f8c95 dfe9ded9 e8950cf0>;
        "Resource.bundle/Icon/sns_icon_9.png" = <5edc4c7d a510ca99 ab029236 77ca1b4a f34f6ab0>;
        "Resource.bundle/Icon/sns_icon_9@2x.png" = <4464c9f1 d1d23d9e 7bcdbda7 f5702c37 7f29547d>;
        "Resource.bundle/Icon_7/sns_icon_1.png" = <63967996 437b10e4 46f12d58 2a9ef92a 497cf95e>;
        "Resource.bundle/Icon_7/sns_icon_10.png" = <d8ba816d 5d619d96 69aa5aff c607ed26 cb6cf7f9>;
        "Resource.bundle/Icon_7/sns_icon_10@2x.png" = <b9db7112 b3c7ad80 fa975ad6 0980ea66 6074de5c>;
        "Resource.bundle/Icon_7/sns_icon_11.png" = <c51bf185 4af8e12d 7bc7df96 a0f95d3d 923a9655>;
        "Resource.bundle/Icon_7/sns_icon_11@2x.png" = <37e12f02 952f5935 285cc7a2 7de0a03d 593e9fd4>;
        "Resource.bundle/Icon_7/sns_icon_12.png" = <410aef5a 015fe302 4bfd227b 86fafe8c 57a128c9>;
        "Resource.bundle/Icon_7/sns_icon_12@2x.png" = <6f7d7760 b9873fab 234158a5 dfe84b4f e4d344ee>;
        "Resource.bundle/Icon_7/sns_icon_13.png" = <ad76d6bd 172bd5fd 899a516d 46e308a9 47f133f3>;
        "Resource.bundle/Icon_7/sns_icon_13@2x.png" = <523d6cfb 7aeb382b 9f98fa89 f9a3ff8d 0f5f8e41>;
        "Resource.bundle/Icon_7/sns_icon_14.png" = <51ce9de2 b25b1265 c8559964 9c3d0c1e 71038d27>;
        "Resource.bundle/Icon_7/sns_icon_14@2x.png" = <6cbc77b5 18b0687c ae948d36 a8bfcde9 138dc348>;
        "Resource.bundle/Icon_7/sns_icon_15.png" = <b93fae16 03e65907 d44851f0 e6c288ec b0732a05>;
        "Resource.bundle/Icon_7/sns_icon_15@2x.png" = <4073ad96 edc7d6ab 5bb2b462 0b5c0166 6a83b03f>;
        "Resource.bundle/Icon_7/sns_icon_16.png" = <ae3ce6a8 fe709315 0335c132 f61415c7 b416bf18>;
        "Resource.bundle/Icon_7/sns_icon_16@2x.png" = <c66ca192 bb51825e 65ba9a64 2e07de96 732681c0>;
        "Resource.bundle/Icon_7/sns_icon_17.png" = <2806dbb9 459ada48 04bc4c66 ca85887e 1b48cbd4>;
        "Resource.bundle/Icon_7/sns_icon_17@2x.png" = <eca1d0dd 1a6226ee dbcf5d02 195f1cfb aad8f5be>;
        "Resource.bundle/Icon_7/sns_icon_18.png" = <467e04d8 5948c838 59fa2351 9cecdad1 32246321>;
        "Resource.bundle/Icon_7/sns_icon_18@2x.png" = <6c1e63d4 dcd09d79 c5052320 8f81a261 9469c78d>;
        "Resource.bundle/Icon_7/sns_icon_19.png" = <c12442d7 39346591 f4b1fbba bd11651d 00d9db5f>;
        "Resource.bundle/Icon_7/sns_icon_19@2x.png" = <049eb988 25d43a8f 852ee855 0049d5b9 99320509>;
        "Resource.bundle/Icon_7/sns_icon_1@2x.png" = <5cc9260f 230252de cf95e88e e633aa62 1ff321b1>;
        "Resource.bundle/Icon_7/sns_icon_2.png" = <e4d0d06b f22d96f6 c97c73da 3901f57d 353f6c14>;
        "Resource.bundle/Icon_7/sns_icon_20.png" = <0e5bbf1c 1163aec0 2496910e 5528d149 a36e5686>;
        "Resource.bundle/Icon_7/sns_icon_20@2x.png" = <0b365aee f8d0cae9 f6374bd1 379e3354 57037ac9>;
        "Resource.bundle/Icon_7/sns_icon_21.png" = <136bae54 7a1a965e 2cbf971b 7297c5f9 9fb21162>;
        "Resource.bundle/Icon_7/sns_icon_21@2x.png" = <ea4282ea e7d1a51e 29184842 00d82068 579d5281>;
        "Resource.bundle/Icon_7/sns_icon_22.png" = <194165f5 054bc406 e4771f7d bbc60fe2 0e26c481>;
        "Resource.bundle/Icon_7/sns_icon_22@2x.png" = <71d6fb2d b118d978 9b968f9f 17064586 86ae5d10>;
        "Resource.bundle/Icon_7/sns_icon_23.png" = <51d613ec cbc42ae2 af1a18f5 d3781f1d 187c7256>;
        "Resource.bundle/Icon_7/sns_icon_23@2x.png" = <0a56d66e 35e78e75 e6741834 460a0f5c 942cde8e>;
        "Resource.bundle/Icon_7/sns_icon_24.png" = <632de99c 6dda6290 b123a0a5 592b32a3 243552a0>;
        "Resource.bundle/Icon_7/sns_icon_24@2x.png" = <eccec2f7 50d65524 00ee6be6 b2955df2 cd84e15b>;
        "Resource.bundle/Icon_7/sns_icon_25.png" = <93bd9570 89dbf583 39429ec1 11614670 c540f714>;
        "Resource.bundle/Icon_7/sns_icon_25@2x.png" = <67c9846f c758f416 2b85ba8b ef26647e 92e1e8b6>;
        "Resource.bundle/Icon_7/sns_icon_26.png" = <242ed62d 9fd1180e 63a1daed ed762417 00e1eb63>;
        "Resource.bundle/Icon_7/sns_icon_26@2x.png" = <73ad895b f7e68c03 962b8fa6 613328f7 ce038bc8>;
        "Resource.bundle/Icon_7/sns_icon_27.png" = <760eab88 9e4807b3 c12deac6 e1133406 46995809>;
        "Resource.bundle/Icon_7/sns_icon_27@2x.png" = <52fd754a cef14249 5f2dd69f 0ecd0a01 13cf2f93>;
        "Resource.bundle/Icon_7/sns_icon_28.png" = <43143449 e85433ae f6c83646 338103a0 bdcac15d>;
        "Resource.bundle/Icon_7/sns_icon_28@2x.png" = <41ff0908 4364c0fa d990bcf7 6e9ee36e 593b6247>;
        "Resource.bundle/Icon_7/sns_icon_2@2x.png" = <ec87611a 7a3d7fcf 519cc612 27b52b8e e7331e15>;
        "Resource.bundle/Icon_7/sns_icon_3.png" = <e19f1981 befd3d17 b91456e0 d8d11127 48879696>;
        "Resource.bundle/Icon_7/sns_icon_30.png" = <1780a3ee 1c33acf7 538dbf67 d153714a 54de59a2>;
        "Resource.bundle/Icon_7/sns_icon_30@2x.png" = <e76e2461 b99d70cb 85dbbe43 8584cf6d 41a61ed0>;
        "Resource.bundle/Icon_7/sns_icon_34.png" = <a287a033 692807b3 e4e7c8e3 7f94a63e 3511301e>;
        "Resource.bundle/Icon_7/sns_icon_34@2x.png" = <4cc6dd0b 7e709154 60f7e594 03d3ec69 713d3554>;
        "Resource.bundle/Icon_7/sns_icon_35.png" = <7b7e911a c1cf0127 bcdbaa94 f293c15c 56ef8396>;
        "Resource.bundle/Icon_7/sns_icon_35@2x.png" = <0b57f407 22b49399 15961a37 727dc50c 55589ff8>;
        "Resource.bundle/Icon_7/sns_icon_36.png" = <a24ce948 37ac74a4 3ec90e53 365e0ea9 b7082be7>;
        "Resource.bundle/Icon_7/sns_icon_36@2x.png" = <cbb88f21 a35d4b9e 1cd2e2d1 76811c25 338e0d08>;
        "Resource.bundle/Icon_7/sns_icon_37.png" = <a9a73fa2 93661fc8 0abdefea f3da8ab6 77661035>;
        "Resource.bundle/Icon_7/sns_icon_37@2x.png" = <bc17f356 581248df d3bdefb0 6cb34870 06eed5c4>;
        "Resource.bundle/Icon_7/sns_icon_38.png" = <d43f807a 094b2669 9b7c69d6 daaace86 4583553f>;
        "Resource.bundle/Icon_7/sns_icon_38@2x.png" = <0a810f95 a50c9886 2da81e4f 7d61ac33 caffe65a>;
        "Resource.bundle/Icon_7/sns_icon_39.png" = <ef28caf5 8fe37522 d65868c2 c7aa8292 d520189e>;
        "Resource.bundle/Icon_7/sns_icon_39@2x.png" = <0d856a8f f6c38353 daec29f7 3d552f71 aaa42711>;
        "Resource.bundle/Icon_7/sns_icon_3@2x.png" = <fe29bbdd d038687a af75ab86 6a694f4d 262b9725>;
        "Resource.bundle/Icon_7/sns_icon_4.png" = <75314313 a4375542 5e241266 b745af25 3baa8fa9>;
        "Resource.bundle/Icon_7/sns_icon_41.png" = <c5447fbc 94e1d3cc 6d266198 342344f0 b3655827>;
        "Resource.bundle/Icon_7/sns_icon_41@2x.png" = <674ac810 89f0855f 90073de0 c209be90 d98b2f45>;
        "Resource.bundle/Icon_7/sns_icon_42.png" = <5c8fd089 679e497f 5674d783 3cca968c 6c77550d>;
        "Resource.bundle/Icon_7/sns_icon_42@2x.png" = <2aafca8b 55124283 86a7e924 019c50b9 68d37d06>;
        "Resource.bundle/Icon_7/sns_icon_43.png" = <ae9cda2d ae854193 500500c7 50b174de d2d65ec4>;
        "Resource.bundle/Icon_7/sns_icon_43@2x.png" = <c14bd56a b00c730f 01df9d81 d79596c7 cac3e002>;
        "Resource.bundle/Icon_7/sns_icon_44.png" = <c042805c f2851a1f 4ee5e61b 1991f177 cf5f70d2>;
        "Resource.bundle/Icon_7/sns_icon_44@2x.png" = <86c08893 d8e0ecaf cce8152f b8907139 5cbb6b8c>;
        "Resource.bundle/Icon_7/sns_icon_45.png" = <d8c4ccc3 f8211608 f647091c b31c1ceb 20fe90bd>;
        "Resource.bundle/Icon_7/sns_icon_45@2x.png" = <97937c49 3176fa7d 2526fe4e 122caa04 d34fce74>;
        "Resource.bundle/Icon_7/sns_icon_4@2x.png" = <5a670c02 dfb99f59 7e41488b 6aeb4dd6 b55fa8c6>;
        "Resource.bundle/Icon_7/sns_icon_5.png" = <19d285ed 4dcdd79e 6d5a8593 7b92c294 13286fee>;
        "Resource.bundle/Icon_7/sns_icon_5@2x.png" = <1958086b f9a7b36b ed02dec9 68cfd3a1 a0605f8c>;
        "Resource.bundle/Icon_7/sns_icon_6.png" = <d55dbb7c 26a8a772 e9b7bd94 729811f2 d52ff67a>;
        "Resource.bundle/Icon_7/sns_icon_6@2x.png" = <115fabbc 02d38e2e c3ef8cd2 676a0798 c43713e1>;
        "Resource.bundle/Icon_7/sns_icon_7.png" = <a20500dc e3e64d54 c3e33353 a8f99ce8 b5b65652>;
        "Resource.bundle/Icon_7/sns_icon_7@2x.png" = <b984c62a 9c046de5 6106a0f5 e1b07dfe 455caeb1>;
        "Resource.bundle/Icon_7/sns_icon_8.png" = <7733944d 3a6674de 77a4bfec c502e03c 9ae003bd>;
        "Resource.bundle/Icon_7/sns_icon_8@2x.png" = <084eb5c3 787c9163 44943a64 dba1a50b c315e527>;
        "Resource.bundle/Icon_7/sns_icon_9.png" = <be848ba3 fa4d9139 ec176082 c9a3c884 c792ba1b>;
        "Resource.bundle/Icon_7/sns_icon_9@2x.png" = <a74212e5 2f38d21c 7b202622 088cb673 074d452e>;
        "SC_Info/Manifest.plist" = <1ba0b852 51419725 2a59b111 896dd036 6db1bbc5>;
        "ShareSDKFlatShareViewUI.bundle/en.lproj/ShareSDKFlatShareViewUI.strings" =         {
            hash = <4131f256 e8bed891 fdfcf457 fe602a20 d2f63119>;
            optional = 1;
        };
        "ShareSDKFlatShareViewUI.bundle/line.gif" = <ab4e80cf e76767a3 066187cf d1c992c1 0d26eef3>;
        "ShareSDKFlatShareViewUI.bundle/line@2x.gif" = <0367ff2d 46a259e6 e308f0f1 b84f4d7f 0f2e7bc2>;
        "ShareSDKFlatShareViewUI.bundle/zh-Hans.lproj/ShareSDKFlatShareViewUI.strings" =         {
            hash = <f0d5bc46 0d6e2647 466805cc 06da33c6 f8dfb46a>;
            optional = 1;
        };
        "data_table/achievement_achievement.bin" = <288a313d 1cc92d85 0863cc50 aebd6819 7113762a>;
        "data_table/achievement_detail_type.bin" = <f83d5360 77cd427a 6bc08c0c e5589f57 352b99ac>;
        "data_table/achievement_mission_common.bin" = <fc9a2830 0985d03e e730a826 2c240b3d d48c7aaf>;
        "data_table/achievement_mission_daily.bin" = <760e90c6 2d6cb3bd 1bc5fade 15d11bfe b4eeb363>;
        "data_table/achievement_mission_special.bin" = <c3c01c26 8df48bcf 8fb17198 2860828d f9f478d9>;
        "data_table/achievement_var_type.bin" = <e1ca3ece c8af3f80 e5f48365 3a8f3ce4 aa44855b>;
        "data_table/activity_activity_push.bin" = <854a63d9 c87ebdca 79c818bb 7ac2e4af 62fd459b>;
        "data_table/activity_box.bin" = <2dcc9500 d626f1a3 592c4bc4 06780e83 d1102f8f>;
        "data_table/activity_card.bin" = <a909baed ca8e35e6 44901d07 03eff036 dee06ac0>;
        "data_table/activity_card_exchange.bin" = <a47600a8 a7a00b50 09de551c a5c69d13 7ca33959>;
        "data_table/activity_card_probility.bin" = <b9a5405e 2784537d 4b590b87 9ac4a2fc 2f1708a5>;
        "data_table/activity_channel_reward.bin" = <371dd3ba 6cfd3349 2fd08a5a e4ed7dad 3e1d4e94>;
        "data_table/activity_land.bin" = <5a7228ba 36e79aa1 91e3dc6c 956bf50c e4819897>;
        "data_table/activity_local_reward.bin" = <186903d7 313568e7 519f0bd3 2201a4e6 82c746ec>;
        "data_table/activity_lottery_config.bin" = <60424d9b 19c85613 bc3986c6 48693bfd 55f26afa>;
        "data_table/activity_phone_number.bin" = <cd783450 2f1be0ea ba733638 b0a71f02 cfa825b5>;
        "data_table/activity_point_probility.bin" = <a783e42e 995f4c9d 45dda38d 4d5d0dc9 5c5e3ade>;
        "data_table/activity_recharge.bin" = <e0fb9b0b b244e8a8 0ff0945f 95ad772c e7fc25b0>;
        "data_table/activity_reward_weight.bin" = <ce56ecdd 01dc187f 9dbd4a6b 9f910015 febbaacc>;
        "data_table/activity_time_bonus.bin" = <21c43eca 14c97ab7 8b5d27ff 83d78b4c a4983d9f>;
        "data_table/ani2dconfig_ani_weapon.bin" = <ec6cf5ba 8d0429a7 33053a49 cc92b989 008ac006>;
        "data_table/bonus_bonus.bin" = <1cf3cb69 1e862e85 c664f643 3dcd5420 ea0af515>;
        "data_table/config_fish_line.bin" = <8c24088c 8a7484f8 aaa7c336 8b0e58de a0f20cfd>;
        "data_table/config_function.bin" = <17d00377 6ee26b8d 288e944e cc9169b4 1151b4b7>;
        "data_table/config_lv.bin" = <9db09ea7 6f0ff163 e5b0a15c c3548bd0 5f688310>;
        "data_table/config_magnification.bin" = <dc9b6d29 d0d610e5 3fb5d5aa 93fb8c78 c8f33632>;
        "data_table/config_player.bin" = <6753edb9 b29f0434 7892daf8 0704008e ab1f5869>;
        "data_table/config_rule.bin" = <bffea17f 7dd34d8c bd56cca9 d061806d 0a6943c8>;
        "data_table/config_setting.bin" = <fd906e1c 4d1d7190 4dbbbcc5 ad51ab42 2c4a5ce4>;
        "data_table/config_special_tide.bin" = <456224cd 9249e7c7 ad3ba476 053b9d87 f0d51300>;
        "data_table/config_strike_probability.bin" = <ded3990e dd0d4ca3 95c5b945 33fd66a4 d27b279b>;
        "data_table/config_track.bin" = <1f206766 80ecf418 9c22c319 bf212d71 7b352ca0>;
        "data_table/config_vip_level.bin" = <f8f67716 25f5801e 92eaac0f ce612978 3cfcb202>;
        "data_table/config_weapon.bin" = <bfa1feb6 50d6f5f0 4673f706 ce9f7221 9a4557fe>;
        "data_table/config_weapon_change.bin" = <77c5c953 651c2412 50e97647 0f6f8bfd 0995ae03>;
        "data_table/correction_correction.bin" = <378ed8c0 4d8e5764 aa88181f 48909b7a 6aec2a87>;
        "data_table/correction_hit_reward.bin" = <689745f6 18471596 3d70d3bc f1031003 c890d75a>;
        "data_table/fish_action_high.bin" = <af56d9f8 4bb27721 b2c76799 ae2eeec6 b1d8398e>;
        "data_table/fish_action_low.bin" = <3c12b9fd fe59037f ce7653bd 8cdc46f6 e11cbf64>;
        "data_table/fish_fish.bin" = <730ef987 012843b5 910e4da3 e1e2abb3 a3d5b4c0>;
        "data_table/gaming_cost_job.bin" = <1812c04c 3f91009d 5de5610a 595230c0 e6f36172>;
        "data_table/gaming_event_reward.bin" = <c044ba18 d71b1ecd bf83d9b5 1c6f43ec f29034ba>;
        "data_table/gaming_gaming.bin" = <3f50a9bc 153fb0df 56d30a24 a7c54528 4030c73f>;
        "data_table/gaming_golden_shark.bin" = <53fbe335 9525ae9f e3918ec7 d130be0a 30418b0f>;
        "data_table/gaming_golden_shark_discription.bin" = <f9c01522 74259bf1 d3cb9dba ff65bb5e f8c5e7d5>;
        "data_table/gaming_golden_shark_percent.bin" = <4b3770cd 36c3a203 2acdba13 a14e3521 7778cf1e>;
        "data_table/gaming_guessfinger_random.bin" = <6f1f6127 5e0985ca ccc74544 1f75996d 73a06812>;
        "data_table/gaming_guessfinger_type.bin" = <1e801d0d 38e4c125 0bfb87ef 3f5bc327 05755868>;
        "data_table/gaming_guessfinger_value.bin" = <56e464c1 f7040fc1 e4d4378f 99c048ef 846118b0>;
        "data_table/gaming_hetun.bin" = <5bc72a5c f5d58098 371ef93a c1ae94d7 a18e789f>;
        "data_table/gaming_job_gold_range.bin" = <bcd15a5b e8e4a1f4 fc8122e4 603ab5cf 0bb9d9c8>;
        "data_table/gaming_job_reward.bin" = <fcd924e0 574e1d87 de8e8b01 26cf1a0d 09ca9a47>;
        "data_table/gaming_login.bin" = <0c165865 b9e2fc99 7c7bd1d2 ae9009aa 03ab8c43>;
        "data_table/gaming_login_cost.bin" = <741b4d5e d2abd09f 5863aad8 05375ce5 f8a9f83f>;
        "data_table/gaming_login_discription.bin" = <53b19993 9748c74c 8acca44e 76eb4a89 a30a9eed>;
        "data_table/gaming_login_reaward.bin" = <699ed3dd fa48f88b cc3db368 f1cbff96 48c01492>;
        "data_table/gaming_login_reaward_correct.bin" = <77aa1211 539e685d b4085565 4921af67 6267656a>;
        "data_table/gaming_pachinko.bin" = <5f62fbd6 383c0f7e 50fa457f e8a21905 0f129b0e>;
        "data_table/gaming_pachinko_config.bin" = <a1987674 0901c4b8 80c3d0d5 2d0f2edc 8bf3aadb>;
        "data_table/gaming_pachinko_discription.bin" = <a2083fda ad997ba5 7a17dc00 a3a80aee 576d73d0>;
        "data_table/gaming_shark_tooth.bin" = <9679c269 f32cc60b 287c0a66 c25146a9 39264236>;
        "data_table/gaming_sign_in_base.bin" = <d477d859 107b6bd6 9d7909b2 1fe2cce0 ba244e1f>;
        "data_table/gaming_sign_in_config.bin" = <391698cc 7c4bcf02 af7d3c58 8c92529c dc57bafe>;
        "data_table/gaming_slignt.bin" = <605244b7 345e55d5 81e3994c 306714e6 07486556>;
        "data_table/gaming_treasure.bin" = <86f7d7a8 3a2b5aa3 15c97dd5 e111a4f0 0d95f30a>;
        "data_table/gaming_trigger_config.bin" = <1e82e236 38055e7a ee152b56 e622609d 10308308>;
        "data_table/gaming_trigger_job.bin" = <fdd3156e 8e506ba6 a24354c1 77a56b29 de246e49>;
        "data_table/log_event_log_event.bin" = <229afbea 68334f50 5df5c8b2 625f0528 de18f21b>;
        "data_table/mission_card_mission.bin" = <3a63611d 25442ded 8c66b284 4ab85b22 e0ec3b86>;
        "data_table/mission_fish_gather.bin" = <d7eb6263 0dd56857 3368ce5a cd5cc0e2 a1de30b2>;
        "data_table/mission_mission_detail.bin" = <3dd49aaa 500a2208 5ed7d932 979ed2a4 28b6f05f>;
        "data_table/mission_mission_fall.bin" = <c27ea65c faadccb4 49cae65f e275fb8a 33a1b4d1>;
        "data_table/mission_mission_stage.bin" = <c7ad7903 f0482ef5 927eb2f0 12f1b565 b2d8c679>;
        "data_table/mission_mission_type.bin" = <e2471f24 b9f60248 55af8dab a9b8f67c 62bb7892>;
        "data_table/music_music.bin" = <2e5ee023 a599a50b 92ab031f bcf9a3ca 641ee0ca>;
        "data_table/notice_notice_android.bin" = <1d63b4cd 9321970a 4962bab3 52744383 181631d3>;
        "data_table/notice_notice_ios.bin" = <8993ba79 8d29b505 8be52c03 d6913ae7 55b9083e>;
        "data_table/notice_web_notice.bin" = <3bffa690 63e2dc53 f3674168 04e42e8c 0091a2be>;
        "data_table/particle_particle_2d.bin" = <b94e7d61 ea5b953f 332e8a0d a03d9964 d32d4a97>;
        "data_table/particle_particle_3d.bin" = <7cb0544f c07103ce cfaf6cc2 b253780c cf7eaef5>;
        "data_table/sale_goods.bin" = <f0c84643 2594f852 3dfed090 d48867e4 eccad47c>;
        "data_table/sale_sale_android_cn.bin" = <2ca9d002 85e4ba27 5bb53627 9e2e44ba 5dc8c11f>;
        "data_table/sale_sale_ipad_cn.bin" = <3538109d 0c952ef9 9c0be61c c1493b0c 12947a6c>;
        "data_table/sale_sale_ipad_en.bin" = <680c5fb6 ace072ce 6891a86e e3164c6b 05bc951d>;
        "data_table/sale_sale_iphone_cn.bin" = <2ba81875 99265387 ef3feed6 20586c62 1ab88ca6>;
        "data_table/sale_sale_iphone_en.bin" = <fa73abc9 bdfbbfb2 6afe90ea d1ecd004 dec7b169>;
        "data_table/sale_sale_jailbreak_cn.bin" = <0208f441 6ccade9a fba70c97 44b1a735 0846a5d5>;
        "data_table/scene_fishfarm.bin" = <4913e314 2436bf90 1f1c2a9d 5e3f2978 85273ac3>;
        "data_table/scene_map.bin" = <3701f2fe f28deecf f025dcca 864ef1a4 2567de19>;
        "data_table/scene_shuicao.bin" = <67607c06 e03fa5e9 a0e02253 d6d9229f 11842762>;
        "data_table/store_crystal_android_cn.bin" = <02c659e5 d399c139 2afcfd0c a49320f2 4a7f3608>;
        "data_table/store_crystal_ipad_cn.bin" = <38414c06 84e5c38e c18af3ac 27d03aa1 33ed64ef>;
        "data_table/store_crystal_ipad_en.bin" = <8bec0302 4fa5a474 ad4ce0d8 da17b902 7a53f4d4>;
        "data_table/store_crystal_iphone_cn.bin" = <e80de12b ceee10f5 e089d9d2 1a447a49 a72cd628>;
        "data_table/store_crystal_iphone_en.bin" = <713fc080 4ffabd4a 4cb76d09 f7a1f282 d55fdddd>;
        "data_table/store_crystal_jailbreak_cn.bin" = <94c492fa f5fdecfa a8cd4c22 9d30e4c9 89d31f51>;
        "data_table/store_gold_android_cn.bin" = <bfe32c3b 0b07cf50 1b37ef1e 12e68aea 45c674c6>;
        "data_table/store_gold_ipad_cn.bin" = <f7b796c7 1ce71102 affa1c99 a9ce9bd5 17986c2f>;
        "data_table/store_gold_ipad_en.bin" = <fb8fb1d2 8c566d92 e4a34eee 6fdc7ea6 bd96193d>;
        "data_table/store_gold_iphone_cn.bin" = <24edb28b 2d2439e7 b390c9f8 800a2671 20a89f4a>;
        "data_table/store_gold_iphone_en.bin" = <3d0c56e3 da047e82 b6524afe 77fd2e7f 35440b86>;
        "data_table/store_gold_jailbreak_cn.bin" = <e16b9ccf a8da6792 9c8b93ec cd4a9d51 7c75998d>;
        "data_table/store_submarine_android_cn.bin" = <a90dbbd7 78d8561d 83236354 4d396a2f 16d64ede>;
        "data_table/store_submarine_ipad_cn.bin" = <09cb63a2 fa5794cf e8361e71 1a6ecd48 172b2dc5>;
        "data_table/store_submarine_ipad_en.bin" = <40b61d50 54650a49 9ef2df1d 3c48df56 45ec3e69>;
        "data_table/store_submarine_iphone_cn.bin" = <7e5cc5fc 7cd054bd 2ce5cbc6 30350aa8 5b2ca8c5>;
        "data_table/store_submarine_iphone_en.bin" = <d8282a9f ef522ede be2031d2 d569ccf0 c30dac54>;
        "data_table/store_submarine_jailbreak_cn.bin" = <948fff15 60e10cbb 53405287 fb2d851c 3e1f2cd6>;
        "data_table/store_vip_android_cn.bin" = <1837ba47 6aab1b85 2cd6983b a4eb7825 f6ed703f>;
        "data_table/store_vip_ipad_cn.bin" = <e7a59ad0 3b93c7f2 b27e062d 1412c496 c2ee108d>;
        "data_table/store_vip_ipad_en.bin" = <f141181a c778bf38 1364ee50 394fdf6e eedb52a9>;
        "data_table/store_vip_iphone_cn.bin" = <bab4d978 d2f35eb9 91c7b41a 420090d8 5a5c0c45>;
        "data_table/store_vip_iphone_en.bin" = <f141181a c778bf38 1364ee50 394fdf6e eedb52a9>;
        "data_table/store_vip_jailbreak_cn.bin" = <a716dc11 96e5858e ef5b0a41 7d070381 cd257de6>;
        "data_table/store_weapon_android_cn.bin" = <c8b38b7e 7099dd63 5f1793c8 4e058387 b51a9422>;
        "data_table/store_weapon_ipad_cn.bin" = <82b5ab34 392636e0 a9d70c61 eb57b00c 4cb4c253>;
        "data_table/store_weapon_ipad_en.bin" = <ec41ec79 61087dcb 31e0666d cb993d7f 782645c7>;
        "data_table/store_weapon_iphone_cn.bin" = <23bf4374 736ec289 05c2f58f e4619dd3 8e5412f2>;
        "data_table/store_weapon_iphone_en.bin" = <886248b3 59de31af 50fb797c 854d1f12 3550b327>;
        "data_table/store_weapon_jailbreak_cn.bin" = <8d146fa1 ddcfb70b c78cb449 bc34908f 52b4d4b1>;
        "data_table/string_string.bin" = <c19103d9 03088144 28027df3 8251a821 554ab353>;
        "data_table/submarine_skill_info.bin" = <debc3993 91308fdc 0a5e79b7 f1c8252f 81b68fbf>;
        "data_table/submarine_submarine_info.bin" = <f9a290b6 c9bdbd21 a83db119 bdd89e99 0cd41363>;
        "data_table/tariff_mall.bin" = <cc3983fe 489c9d20 18216886 d45b236c 860b6c67>;
        "data_table/tariff_sales_promotion.bin" = <f7852c56 1fc6b55c d4842295 37f5856a 7ae892cb>;
        "data_table/time_event_axis_event.bin" = <d7a40a1c cf7309c7 fd0dbbf1 5fe1e444 97e0b3a4>;
        "data_table/time_event_event.bin" = <57efb275 74de662b 144c6121 93df88a5 11c95520>;
        "data_table/time_event_quartz.bin" = <82e22867 cb55790e c617dab1 f993df1b 709329b6>;
        "data_table/time_event_time_control.bin" = <4c47969d bc5f827a 0747754a 2eb92448 42940a7f>;
        "data_table/tip_tip.bin" = <a21fa823 301424af 8b70cf98 ee46941b 8e335f3f>;
        "en.lproj/ShareSDKLocalizable.strings" =         {
            hash = <192d091c da7351a7 cc4d0725 9220f4c4 fbe477ef>;
            optional = 1;
        };
        "font/fj3.ttf" = <c664c796 725ef305 fb514361 82aa1cf4 836d5031>;
        "map/changjing_05.sce" = <9642366d c3afcb4c 86b1d904 0ca5c5e2 557244f5>;
        "map/changjing_07.sce" = <82c387a2 a385822d e95b560f a7ac01a8 cdf8df3c>;
        "map/changjing_08.sce" = <1b95677d 1d0c97c8 5e07c766 43576d97 b7eed983>;
        "map/changjing_09.sce" = <201be84e bd429c69 e4c17753 acaf9ef2 02761c72>;
        "music/bgm_boss.mp3" = <d10abccd aa115007 4c3547c9 ae37f4d7 05b736c8>;
        "music/bgm_fever.mp3" = <14087110 b51e9ae8 c09109a7 37bafef9 93f20625>;
        "music/bgm_minigame.mp3" = <d1a0db20 8c3bcd04 6dd6e042 8ca5809c ee48a5e3>;
        "music/bgm_opening.mp3" = <f0d47fa4 b313a865 73e5f6ed 2cf0e9cd fc67a7e7>;
        "music/bgm_scene1.mp3" = <c76d4234 29f15f29 5869f7b7 2ef78ca5 c2e56781>;
        "music/bgm_scene2.mp3" = <38463825 bc993c10 01d05dc4 5b8753e2 c6f7682f>;
        "music/bgm_scene3.mp3" = <71f71f19 b9173769 d4c2874d a8187246 cb53e797>;
        "music/bgm_select.mp3" = <847830bc 6ab28229 f8bcb658 68f49355 428d48f0>;
        "music/bgm_slot.mp3" = <a9a68da9 e5edf8a4 cbc7768f 3535991d 52e0ec4d>;
        "music/sfx_attention.mp3" = <ae94ea69 2eb5293f 9447a5e7 0ccd3b57 ac1855b0>;
        "music/sfx_babbleburst.mp3" = <b1469925 9cecfeea e9bc86f5 ab808549 42c8904f>;
        "music/sfx_blackhole.mp3" = <691cec9d 85b7595d 14f01d88 db399f4b 8e0e801f>;
        "music/sfx_box_appear.mp3" = <890c97d1 c7ed3048 aa87eab3 a09d5a31 0f37f67d>;
        "music/sfx_box_coin.mp3" = <4bcc705c 2519de7f 156b2e2e 1e76e249 b851309a>;
        "music/sfx_box_fish.mp3" = <c6b50cd6 90a35320 d141fe1c 1c9fd47d 4a9a9d47>;
        "music/sfx_boy_wow1.mp3" = <3772881f 6492df3e 60bb38be 903cf96a ea56f513>;
        "music/sfx_boy_wow2.mp3" = <540ffc98 ce3020dc a049e6ed 8056312f 8ef34b85>;
        "music/sfx_bubble_appear.mp3" = <bcdd987c 25cacdc8 510501d7 1e3a66e8 5a61d20b>;
        "music/sfx_bubble_broken.mp3" = <27b3055a 158ab118 ac405dde 20d19207 82b4a8ee>;
        "music/sfx_bursts.mp3" = <5d669abd 284d1d6f 2a3be296 5c60df56 b7eb2852>;
        "music/sfx_bursts_fever.mp3" = <aac4ccb7 2330db1a 6b560655 6807e11a 085cc9b1>;
        "music/sfx_button.mp3" = <6d67698b f0ab18a6 1070172e 223514d5 ba793764>;
        "music/sfx_camera.mp3" = <7b053a8a d3376f26 00fc86c6 280a25b4 6ffd96b8>;
        "music/sfx_cannon.mp3" = <ca0747d0 8d44cf00 bcb3a009 b5c647cd 7f3d55b6>;
        "music/sfx_cannon_fever.mp3" = <412decc7 bbf0dedf 55ac5534 16a5be45 62de6cd5>;
        "music/sfx_card_click.mp3" = <23dc1cee e2aef28a cf14ef3e c34d407c c26d2f8c>;
        "music/sfx_card_open.mp3" = <33ef029d eafd84aa f94f80f8 4b5f3bf1 81050957>;
        "music/sfx_circle.mp3" = <3bc66fe5 9743b5c9 fcc575e9 fe0752c9 180d6658>;
        "music/sfx_circle_fever.mp3" = <820e3dd6 6df8f926 f997380d 283aa124 1b21fdaf>;
        "music/sfx_coin.mp3" = <344f6be4 063730be c9099c7c 6e5436b0 593f72ae>;
        "music/sfx_coins.mp3" = <bd711168 8f6c8e77 79a257bf 02333414 aa078bfc>;
        "music/sfx_collect.mp3" = <874fcd6e 9750f268 ace28901 da6786d4 9a76852f>;
        "music/sfx_cristal_collect.mp3" = <fe8913eb 0740eedb 6e9f206b 7ef3582a eeff11cd>;
        "music/sfx_event.mp3" = <34f101ed 281a3aff 58776f9e 486a03e2 64bfd2ec>;
        "music/sfx_fever_apply.mp3" = <314c69b3 fa3d794f 61de0feb e1da5162 62641e1a>;
        "music/sfx_fire.mp3" = <63a596c1 283a920c b45e3282 094651da 31b23e23>;
        "music/sfx_fish_talk_1.mp3" = <eab26656 16b53d6d 0638c11f e693669c bdaec4fd>;
        "music/sfx_fish_talk_2.mp3" = <aadfb7ef c1612fb2 79074b91 d0c96310 65609fee>;
        "music/sfx_fish_talk_3.mp3" = <5af0e022 6b045088 3aefbc16 b834eba9 bb2140b4>;
        "music/sfx_flash.mp3" = <1438b659 3ba152b9 23d5adbb 67ab1ad6 4c3ed87f>;
        "music/sfx_goldweapon.mp3" = <61ef078e ccd889e5 a229061f d9c0c860 33016c9f>;
        "music/sfx_guess.mp3" = <9a456269 3631df8c 69e2df9f d8f6b66e 89f71dca>;
        "music/sfx_guess_lose.mp3" = <131cc4a2 df79462d baccf143 de09cc5c 52db639e>;
        "music/sfx_guess_win.mp3" = <4984e962 bc67d080 25039e65 a5c6158d fc7cff92>;
        "music/sfx_guess_win_gold.mp3" = <54284e5b d5e830c2 7d89c5b3 16c1a0f4 88fc3e6e>;
        "music/sfx_harpoon.mp3" = <1fb39ab0 9ebab1e4 e2cc864a 94376ace 074f87d4>;
        "music/sfx_harpoon_fever.mp3" = <a4f9faa7 18fa66a9 4b85e02e 5a580e15 36ffad40>;
        "music/sfx_levelup.mp3" = <3a3ed35f 4bffc450 25749c58 bd2b4e89 0cc21cd1>;
        "music/sfx_needmore.mp3" = <49eb966e 9de86c59 d55de5ea 026d7fb4 1bb9934c>;
        "music/sfx_net.mp3" = <74a893d8 ad9650d5 73d5a71f 07ef4ec5 c9d82269>;
        "music/sfx_open_card.mp3" = <8c574a68 455e97ae 2f817314 0761b39a 5072303f>;
        "music/sfx_rank_up.mp3" = <db620e1b b2a930a6 8c3f1ef0 6d93250f 9360bf5a>;
        "music/sfx_sharktooth_correct.mp3" = <281846e4 a6e4b1f2 31f84687 80009a76 12c15b1d>;
        "music/sfx_sharktooth_open.mp3" = <140534b9 ca5d24be b6b94f28 fdcd91c9 db03d6b8>;
        "music/sfx_sharktooth_wrong.mp3" = <71a836bd 33a01d73 7363590d 0b7d6220 bf5e53cc>;
        "music/sfx_shoalhint.mp3" = <211a219e c0f0dc65 ffb318cd f2e1bdc6 20d78d8d>;
        "music/sfx_slot_appear.mp3" = <3df4bd5a 20271d3f 8f587d1c e362934d 3de579d0>;
        "music/sfx_slot_push.mp3" = <44fe206d 29e7cf70 1712d18c 03237412 57b066a1>;
        "music/sfx_slot_rolling.mp3" = <ed631bfe 941d8fb0 f7452c95 3ede7c17 74b27fe3>;
        "music/sfx_slot_stop.mp3" = <57dfdae9 d8fee02f 32ada43a fda44d89 4cbd551f>;
        "music/sfx_slot_x2.mp3" = <44d38b09 f576db96 5624280c d3c9eb24 67d4edd7>;
        "music/sfx_supersoul.mp3" = <ca9a9070 6b3479bb b8412b47 23de31c0 21a17d69>;
        "music/sfx_superweapon.mp3" = <1f0c1f40 063bf224 e1270cac 35b4b176 8a43f8f9>;
        "music/sfx_superweapon_fever.mp3" = <7a17d7fe c5b9e5d7 68bdb627 9313509b c9484e38>;
        "music/sfx_switch.mp3" = <6c3ae56a c2d769f1 bb85c95a 4fb8a23c e7fec554>;
        "music/sfx_thunder.mp3" = <e0c0652e 048cab1c 00ef4b8b 09bf4f13 59a155a1>;
        "music/sfx_thunder_fever.mp3" = <ae152a2a a8571614 035f2076 f01eb794 1075b2ea>;
        "music/sfx_window_close.mp3" = <05064e7e ee414f8a 661ff4f7 a7460a3a edafa298>;
        "music/sfx_window_open.mp3" = <e2dcc742 a3b7307d 7afeea69 8fecbbb9 2888213b>;
        "particle/achievement_star.plist" = <2baa0729 2df51525 57fa0c9b 268a9061 ed698a8b>;
        "particle/achievement_star.png" = <ba9d4169 fca8ddea 8e9bbae8 121b2622 b45a23f2>;
        "particle/bao_li_zi_fen.plist" = <34b956c8 a0741753 3bcc43cb c5620315 8e8f21b6>;
        "particle/bao_li_zi_fen.png" = <6dae82ac edd283d7 f3a2ddda 401768d4 0d5a6d3e>;
        "particle/bao_li_zi_green.plist" = <20013901 b43ba5be 437bbe49 dd329dd1 42df5291>;
        "particle/bao_li_zi_green.png" = <d4113a86 ce53b4ad 8abed0df 9014e892 9199b6d9>;
        "particle/bao_li_zi_lan.plist" = <e3db5c16 a8794c12 29fcac04 a9a3d1e5 f2d6dbdf>;
        "particle/bao_li_zi_lan.png" = <6283d036 fafb52f4 f2c7489c 432f0b09 6d1e0924>;
        "particle/bao_li_zi_red.plist" = <56457588 856dfe53 683b9e37 65576ee0 c17d34a8>;
        "particle/bao_li_zi_red.png" = <34f4f52c 0442808e b89d1c16 3513e2d6 8f1a5c3b>;
        "particle/baoxiangbao.plist" = <bc84ce55 ad89ac62 da90f730 e295b3bb 05ff2dc3>;
        "particle/baoxiangbao.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/baoxiangshan.plist" = <0c1db644 64cfaab5 ec51d421 6e5dedf9 99468fa5>;
        "particle/baozha01.plist" = <b2797a0c a8538a24 05fb731a 8fb8f0ad 62d0e304>;
        "particle/baozha01.png" = <fea870b0 5a80538c e5a48fae 6a363b29 7ff6def7>;
        "particle/baozha02.plist" = <39545ddb ec1ca050 f9f6dd79 e15c3bd6 df274001>;
        "particle/baozha02.png" = <4eae1af4 c33e8250 2cf71f2d 09163595 d47c2b3c>;
        "particle/bar_get.plist" = <504dafd2 86baacda 945c546e 04fe475d 350cc3da>;
        "particle/bar_get.png" = <002ad3c2 f37b100b f25fafd1 375e77d3 d1c12d9a>;
        "particle/bigwin_blowout_1.plist" = <7c23c41e 95b1a737 8e4f11e7 2f53fefe f9677844>;
        "particle/bigwin_blowout_1.png" = <416db3da c07639ef 8d0a97d2 7103209a 29a674cc>;
        "particle/bigwin_blowout_2.plist" = <65e723c0 a50e56d8 0f4e203f 671a0998 d1ac53fb>;
        "particle/bigwin_blowout_2.png" = <416db3da c07639ef 8d0a97d2 7103209a 29a674cc>;
        "particle/bomb1.plist" = <b8d9b50a 33c7004d e2ffb879 257717be 6e432a9d>;
        "particle/bomb1.png" = <e97374a1 69495350 d7341d02 7dab4ef6 7f3e01b6>;
        "particle/bomb2.plist" = <514485d6 e3741449 0bfb1268 c823d4d3 ab21cfc3>;
        "particle/bomb2.png" = <ec6dc3cb 8b591408 f843ddcd 85a92fa7 4eac0af4>;
        "particle/bombparticle_sashuolizi.png" = <d45fbf40 f4b6e86d 48f6e72f ec65ce91 a20d3986>;
        "particle/bombparticle_shanshuolizi.plist" = <996153ea f259e779 c5112529 03103fea a107bd6a>;
        "particle/bombparticle_xslz.plist" = <e4e18a89 2d04dd6b d36fb535 324be1ed 18d2b8cd>;
        "particle/bombparticle_xslz.png" = <d45fbf40 f4b6e86d 48f6e72f ec65ce91 a20d3986>;
        "particle/bombparticle_xxbz.plist" = <f45cfc69 6260f15d f8866ae8 f2cc0500 6ab18956>;
        "particle/bombparticle_xxbz.png" = <4eae1af4 c33e8250 2cf71f2d 09163595 d47c2b3c>;
        "particle/bombparticle_zhxslz.plist" = <ca87e4dd 61155ea2 adad6097 89e8ac2f be366cfd>;
        "particle/bombparticle_zhxslz.png" = <6a853286 33a91544 d50dc9d4 3630a81f 53a967dc>;
        "particle/bujingcha.plist" = <54323007 820af679 5da81ee9 11e991ab 30e17f6f>;
        "particle/bujingcha.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/bullet_circle.plist" = <64782530 1d25f563 20a34ea1 bf66a27c ecc9a9ef>;
        "particle/bullet_circle.png" = <fea870b0 5a80538c e5a48fae 6a363b29 7ff6def7>;
        "particle/bullet_fire_01.plist" = <29e17a83 ca02388c f9c087bc 67bbfd9e 28f0a57f>;
        "particle/bullet_fire_01.png" = <8cda6e42 815bf4a7 d133858d f3243783 8c6d1169>;
        "particle/bullet_fire_02.plist" = <9cff0c0c df60772d 1d612729 780192a5 87b7beec>;
        "particle/bullet_fire_02.png" = <8b35959c f526df73 8c22ee02 f65c87d4 ade3df83>;
        "particle/card_1.plist" = <1b93b061 9758412a b67259d7 0f71fabc 67282a3b>;
        "particle/card_1.png" = <8cda6e42 815bf4a7 d133858d f3243783 8c6d1169>;
        "particle/card_2.plist" = <faba4e91 07761201 a8508552 3fd30a1c 6e6eb833>;
        "particle/card_2.png" = <331811e5 a4d2f9d7 9da2fcba 1c91d68c 004566dd>;
        "particle/combo.plist" = <10e3d5cc fea7a954 f61bdf28 c0ba9d90 8fd04662>;
        "particle/combo.png" = <eece2938 a78584a1 6911a17f b2287faf 840ac564>;
        "particle/cristal_tail.plist" = <71b9ac69 612bc346 19261ae5 7aa3450f a27e181c>;
        "particle/cristal_tail.png" = <6075ef47 e7e2dfb6 914ee0f5 58d4b554 443c3dd5>;
        "particle/dalizi.plist" = <82ebf086 b33cf42c 7491eac3 3c6db22f e4128781>;
        "particle/dalizi.png" = <fbec9d1a ee7edc27 89478412 9a855f92 cb5850be>;
        "particle/datuowei.plist" = <d08aec65 d2d73dba c2ecabef 45180f88 75d579db>;
        "particle/datuowei.png" = <774cea68 5150489e eae0e9b6 7e5bfbdd 318bcf93>;
        "particle/denglongyu1.plist" = <f08a38e1 dd08e10d d961bf19 0d565cb4 838498aa>;
        "particle/denglongyu1.png" = <84e382e9 0ef4212c 290ac8ef 24dc16a0 516d5556>;
        "particle/denglongyu2.plist" = <d6bc6dc4 a7af0225 9d711ac5 9a41d77a 6cb7ce7b>;
        "particle/denglongyu2.png" = <cb1b5f6a dc74e2e5 7af74c8d e064db2c 79f33deb>;
        "particle/door_close.plist" = <eddfe2cb d2064138 2e8f5ab6 8fac28ff b0230b4b>;
        "particle/door_close.png" = <e97374a1 69495350 d7341d02 7dab4ef6 7f3e01b6>;
        "particle/event_weapon.plist" = <85affd2f 3d9fab06 6e13f5d0 c79e999c 83734e8e>;
        "particle/event_weapon.png" = <87494852 02c1f295 bacab7ee 1caf1523 2982bbb5>;
        "particle/exploding.plist" = <a949fd8e 2187d654 8cf0dd83 4b7a7d9a 6b0495a2>;
        "particle/exploding.png" = <3a279d7e 534b7a6d f1bf7fa7 62339326 f563559e>;
        "particle/fever_ready.plist" = <685e10c7 01f26a9d 17f653fe ededb563 e945c63b>;
        "particle/fever_ready.png" = <331811e5 a4d2f9d7 9da2fcba 1c91d68c 004566dd>;
        "particle/fire02.plist" = <9cff0c0c df60772d 1d612729 780192a5 87b7beec>;
        "particle/fire02.png" = <8b35959c f526df73 8c22ee02 f65c87d4 ade3df83>;
        "particle/fire02_2.plist" = <29e17a83 ca02388c f9c087bc 67bbfd9e 28f0a57f>;
        "particle/fire02_2.png" = <8cda6e42 815bf4a7 d133858d f3243783 8c6d1169>;
        "particle/firefish_trail1.plist" = <a370e228 21e4d1c9 af697f44 aa3f30a1 9bba9840>;
        "particle/firefish_trail1.png" = <ce11ebc6 c74bb8b3 ead079ee 1a93653c 93f4bc8b>;
        "particle/firefish_trail2.plist" = <d6e7d450 a6fe6528 d4749203 3c2f0d0d 6baac366>;
        "particle/firefish_trail2.png" = <8cda6e42 815bf4a7 d133858d f3243783 8c6d1169>;
        "particle/fish_disappear.plist" = <c1877a86 05194e76 721aa4f4 ed9af5ed ba286a05>;
        "particle/fish_disappear.png" = <5cca50a8 b4916314 4c346c3a 278bff97 e469bc2f>;
        "particle/frozen.plist" = <ce6bfbc1 53a56e60 4abf9e99 e730cda8 d7e47260>;
        "particle/frozen.png" = <54332a39 99b12d57 40485edb 62ae77ef 058da342>;
        "particle/gaibao.plist" = <905c8596 d3b9a8b0 3a38b429 cb624736 aca5353f>;
        "particle/gaibao.png" = <0efad1e5 6aac21be d6770a2c 05e0a4ec 32d66025>;
        "particle/gas_disappear.plist" = <0691eb6d 4cb93b25 62a97a79 a1f4cf10 4fe16b75>;
        "particle/gas_disappear.png" = <774cea68 5150489e eae0e9b6 7e5bfbdd 318bcf93>;
        "particle/gas_explode.plist" = <e2c82c9e d554e4a4 7909f815 cbbc1db3 b441ac56>;
        "particle/gas_explode.png" = <25805ecd ea590adc a28391c9 1ece4313 c70a9300>;
        "particle/gas_loopbubble.plist" = <c32702a8 5c53d286 28093707 c752c65a 8ec5df14>;
        "particle/gas_loopbubble.png" = <77a09390 5bb5ed2e 535fc43f d4aa51d6 9cf85e7d>;
        "particle/gas_loopsmoke.plist" = <bd71d304 07e7ce16 ff16d031 b8aed988 9126f5b9>;
        "particle/gas_loopsmoke.png" = <3a279d7e 534b7a6d f1bf7fa7 62339326 f563559e>;
        "particle/gas_smoke.plist" = <b2f9ef8b 8c5d54f3 c89b9b16 5c02ce1d 1473016a>;
        "particle/gas_smoke.png" = <3a279d7e 534b7a6d f1bf7fa7 62339326 f563559e>;
        "particle/haigui_ice.plist" = <7c49e695 02cee292 e3c1c8cd 0c98d3e7 b723ffa0>;
        "particle/haigui_ice.png" = <774cea68 5150489e eae0e9b6 7e5bfbdd 318bcf93>;
        "particle/harpoon_explode1.plist" = <e1ef5859 5046572e e4ec6ac1 9ff8ee79 fc330c8a>;
        "particle/harpoon_explode1.png" = <d615cb73 854e54b3 c05b671a 9a2ed973 85ccf347>;
        "particle/harpoon_explode2.plist" = <59d92514 90642cf0 ebc59bc8 4318eef0 5181dd2a>;
        "particle/harpoon_explode2.png" = <25805ecd ea590adc a28391c9 1ece4313 c70a9300>;
        "particle/harpoon_standby.plist" = <e8e3ed1d 445498b0 02ea1ef7 c2caae08 67246a72>;
        "particle/harpoon_standby.png" = <a8ca8635 165db92a aa6c3455 e519888e f97122f7>;
        "particle/hetun_crash.plist" = <8203fb38 5bf76903 71ab2adb 6434cbe7 1ff58abd>;
        "particle/hetun_crash.png" = <c2ce95e1 252b3266 7547c41c 2e0c8f9b a7d1816e>;
        "particle/hetun_crash_blue.plist" = <3ca1dd96 28814d5d aa23d8e7 becc7757 1c41b0fa>;
        "particle/hetun_crash_blue.png" = <c52a31e9 946958f7 3b5cbf34 a5994802 27d3e2fa>;
        "particle/hetun_dead.plist" = <1bf598bd 46e66d64 bea088af 7b24e6f4 1607ef36>;
        "particle/hetun_dead.png" = <c2ce95e1 252b3266 7547c41c 2e0c8f9b a7d1816e>;
        "particle/hetun_disappear.plist" = <d7eeadaa 42871ef1 f90b9cd3 65304c61 c68d5d6f>;
        "particle/hetun_disappear.png" = <c52a31e9 946958f7 3b5cbf34 a5994802 27d3e2fa>;
        "particle/huangjinpao.plist" = <00d0d4b5 86f7cb66 170bad48 96e05195 f242e9d9>;
        "particle/huangjinpao.png" = <362903eb d523b3b5 98d28c16 a8bd1ec0 8ee6d9c9>;
        "particle/jbtwlz.plist" = <3b177d91 119d6cb5 00d34484 93a2e334 62605ab8>;
        "particle/jbtwlz.png" = <67032fe2 e47fe639 88ffe6fb 75d31353 1563310f>;
        "particle/jinbitubiao.plist" = <d7675d9d cc4ae4d0 08c3795c 8ccc1770 5cb92573>;
        "particle/jinbitubiao.png" = <ae5cbf10 4a43dbdf 8345eba8 d9138c0a 80e5cd04>;
        "particle/jizhonglizi.plist" = <00ba7e55 4a78aade 0290760a ffaf2998 e4f8a534>;
        "particle/jizhonglizi.png" = <fbec9d1a ee7edc27 89478412 9a855f92 cb5850be>;
        "particle/levelup_bubble.plist" = <efc347c5 9ed06999 88a3548c 835c914f fd4004e4>;
        "particle/levelup_bubble.png" = <d4e5ff5a 27248f47 5b9b2343 50b7f91d 027046ed>;
        "particle/levelup_button.plist" = <b552cd08 f9b726d7 357b16a3 e465f9ab 6222a7ec>;
        "particle/levelup_button.png" = <a8c46e55 f365e075 a93b16fb 1cc54566 e22a74f3>;
        "particle/pao1.plist" = <5662614c 5898e323 9ac8bbdd 82fe6cbc 696acb1f>;
        "particle/pao1.png" = <3a279d7e 534b7a6d f1bf7fa7 62339326 f563559e>;
        "particle/pao1_fever.plist" = <b0b5c0ac f33d8303 53205988 832f1731 684bbb86>;
        "particle/pao1_fever.png" = <d615cb73 854e54b3 c05b671a 9a2ed973 85ccf347>;
        "particle/pao2.plist" = <f2cbf7e3 f8139a62 899d2020 f85c31f3 e656cd74>;
        "particle/pao2.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/paopaoxiangshang.plist" = <aecf45bc 128ee4f3 d12d8868 e8c5bf74 99aee3c1>;
        "particle/paopaoxiangshang.png" = <36d04658 2dd27fc0 4fb51719 48b8a845 4fa78679>;
        "particle/qianshuitingqipao.plist" = <c0cdf4d6 6b729037 633e82d4 d91b1205 aadaf256>;
        "particle/qianshuitingqipao.png" = <553199b1 3f1abc11 775bfcdc eda5a674 590364f0>;
        "particle/qiaoya1.plist" = <e50da7eb 9aefd405 e82c429a 2c41aeae de075fba>;
        "particle/qiaoya1.png" = <543f2849 47a2339d 2a4b36ff 9c4ef71f 4ebcf7c3>;
        "particle/quanpin.plist" = <8b542d22 f748f36d a708d01c 709c75f3 9d59b2e0>;
        "particle/quanpin.png" = <2a0c7978 09abe2f5 77c04ff8 9e65b715 e9668faa>;
        "particle/shan_li_zi.plist" = <5cb7bcf7 68650b53 b8dda856 390fc7a8 0cdd1610>;
        "particle/shan_li_zi.png" = <eee617f4 c24b8dfc 2db17d4e d22b3718 f5d151c2>;
        "particle/shoulijian.plist" = <49937550 c56d6ed3 51835e83 a0dea8a2 a60d1562>;
        "particle/shoulijian.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/shousuo.plist" = <85c23f42 b1bff4d7 74104700 36bd8143 5bada725>;
        "particle/shousuo.png" = <3a279d7e 534b7a6d f1bf7fa7 62339326 f563559e>;
        "particle/shuxianlizi01.plist" = <20cfd748 018c46c4 705da861 d4a803d6 4c964200>;
        "particle/shuxianlizi01.png" = <fea870b0 5a80538c e5a48fae 6a363b29 7ff6def7>;
        "particle/shuxianlizi02.plist" = <d0f6dd6a 7c5d716b 9815e96f ed7f0a2e a73b17d4>;
        "particle/shuxianlizi02.png" = <82987ca2 a990ad4d 9dfd8fb0 65a9e90e 0ed4e43f>;
        "particle/sjlizicaise.plist" = <7d55ca31 55deef78 23fb0acd b64955b1 2f267aad>;
        "particle/sjlizicaise.png" = <a250c77e 6dfa2d01 f78f99a1 7bf3b04a 702b1d2f>;
        "particle/sjlizipaopao.plist" = <888d0f4b fd462d7b 8e6aa71c a883dc33 32ae6ca2>;
        "particle/sjlizipaopao.png" = <d4e5ff5a 27248f47 5b9b2343 50b7f91d 027046ed>;
        "particle/sjlizixingguang.plist" = <8a1d5295 78bb0668 88eed5e8 6f8ffa2a 82f360c8>;
        "particle/sjlizixingguang.png" = <15c53d63 cce55461 f0c8afbe 44a2f0a0 2027fc33>;
        "particle/sjlizixingxing.plist" = <2220044e 2041d2ed f15e4e58 79181517 4bc923d7>;
        "particle/sjlizixingxing.png" = <9cbddcaa 15c2828c 5565c7e8 7f5ff75d a4cdd25f>;
        "particle/sjliziyuandian.plist" = <a056c013 66b75f63 dd663416 c0eb2a7c cf938a03>;
        "particle/sjliziyuandian.png" = <bf80cc63 67f6b61a c046ab3f a0e9b070 b010f9e7>;
        "particle/slot_coin02.plist" = <0264e336 5d15a8c9 a6dbd72e b8f6cb34 0fdd66f2>;
        "particle/slot_coin02.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/slot_paper01.plist" = <2c916534 d7e6b038 d64b9f12 cc707898 1bd9019e>;
        "particle/slot_paper01.png" = <7fbeadcf 18b79ebd 05e1d682 7e54488f 70f8fa4a>;
        "particle/slot_paper02.plist" = <4a865778 730d84f5 79029300 e88c4ee2 e00e6b08>;
        "particle/slot_paper02.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/standard_circle.plist" = <3a30ef46 3cb65f50 48f12ae5 ba6ba127 6be726fd>;
        "particle/standard_circle.png" = <8ffa3814 d90de1a3 cd29d7c9 f0813442 b66d4270>;
        "particle/star.plist" = <a695f157 33f94566 8038a25f 6f4b882f b68abf84>;
        "particle/star.png" = <2801bc6d 388d4d58 24879fa2 8d80eac1 72b29bc0>;
        "particle/star1.plist" = <e8d415bc 843d04a8 0de590bb ff74606f 07613b36>;
        "particle/star1.png" = <4d2f0904 9b8b68d8 c4a5628f 8b1e4806 76a813ff>;
        "particle/star2.plist" = <6bdeb02d abbaf6b0 1d4f2a12 3f163c46 467f046f>;
        "particle/star2.png" = <ba9d4169 fca8ddea 8e9bbae8 121b2622 b45a23f2>;
        "particle/star2_icon.plist" = <c5030737 2c86c1c8 a1c23160 dad16e9c 1eb82fe2>;
        "particle/star2_icon.png" = <67032fe2 e47fe639 88ffe6fb 75d31353 1563310f>;
        "particle/star_fanpai.plist" = <6ac75dfe 1f21a47f 7bdd6b0a 5b7b4faa e2ad39fb>;
        "particle/star_fanpai.png" = <8cb440f1 e9d3d44a 5ac00097 f9399c17 1a7d87b3>;
        "particle/star_icon.plist" = <ce56f6cc bb107db9 12195224 c96ebf10 37def000>;
        "particle/star_icon.png" = <67032fe2 e47fe639 88ffe6fb 75d31353 1563310f>;
        "particle/submarine_change.plist" = <d741cad2 875d2b36 a1e3280c f38633b5 0667d0f6>;
        "particle/submarine_change.png" = <87494852 02c1f295 bacab7ee 1caf1523 2982bbb5>;
        "particle/tbblue.plist" = <b3ceacdd faa68e8b df8f9cfd 25bee4ea 85c87959>;
        "particle/tbblue.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/tbgreen.plist" = <43838b36 04144c84 99229112 0ecc8a61 1eab8d38>;
        "particle/tbgreen.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/tborange.plist" = <9fce9447 df13aaa0 7cd4e366 9615e326 11261f8c>;
        "particle/tborange.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/tbyellow.plist" = <a753eb25 caf668dd 70952eb3 79033f27 b56ca71e>;
        "particle/tbyellow.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/trigertip.plist" = <22710715 9b79eaf0 717a8e70 2c733f47 3d9f4d06>;
        "particle/trigertip.png" = <b6e9426e d1736424 25061f6e 6da9f303 e5059f58>;
        "particle/tuowei.plist" = <10864114 92391506 60ab2c3a 05ec4e9c b471a62a>;
        "particle/tuowei.png" = <002ad3c2 f37b100b f25fafd1 375e77d3 d1c12d9a>;
        "particle/xialuo_li_zi.plist" = <989cd2ee 0c0c64d2 3b386913 38ef150b 236bd6e2>;
        "particle/xialuo_li_zi.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/xiaolizi.plist" = <2cc2923c 9422176f 02885145 029c8fb8 9f2d7ab4>;
        "particle/xiaolizi.png" = <fbec9d1a ee7edc27 89478412 9a855f92 cb5850be>;
        "particle/xingxinglizi.plist" = <f8ce8c24 1140c009 da3d8d6e 42090574 72f5987c>;
        "particle/xingxinglizi.png" = <fea870b0 5a80538c e5a48fae 6a363b29 7ff6def7>;
        "particle/xingxinglizi_loop.plist" = <58721e87 c69c5873 aa02b8fc 3295f19d e5df19d4>;
        "particle/xuehualz.plist" = <dc7b690c 74768c6e d3028c7d 7658ea83 ba72fdc3>;
        "particle/xuehualz.png" = <d615cb73 854e54b3 c05b671a 9a2ed973 85ccf347>;
        "particle/yanhua.plist" = <a9126d03 5245924b 26504bbc 58f1475c 6ccd7101>;
        "particle/yanhua.png" = <002ad3c2 f37b100b f25fafd1 375e77d3 d1c12d9a>;
        "particle/yanhua_huang.plist" = <a80454ea b3322ca0 edbfa6c5 ee6758f9 c9b57361>;
        "particle/yanhua_huang.png" = <6169df92 8ac94d48 9b008829 e937e89e 39696789>;
        "particle/yanhua_l.plist" = <b508d418 0af82a51 cab32fa8 9fd31675 de214a90>;
        "particle/yanhua_l.png" = <b924c74d 6c42b147 c6617f93 5541e6ba 66111862>;
        "particle/yanhua_lan.plist" = <86c8177c 466e753a 2fbd556f c4719a49 91145c86>;
        "particle/yanhua_lan.png" = <b924c74d 6c42b147 c6617f93 5541e6ba 66111862>;
        "particle/yanhua_zi.plist" = <918c94e5 397a930e be7e2d37 b30bc2f1 56a9509a>;
        "particle/yanhua_zi.png" = <6169df92 8ac94d48 9b008829 e937e89e 39696789>;
        "particle/yihaopao.plist" = <b1d93339 8ad19952 de1def0d befdafe7 92c758af>;
        "particle/yihaopao.png" = <5e4cc87c a8e47f61 08894c07 d19610c6 5ac8e47e>;
        "particle/yuchaoshuipaotexiao.plist" = <92256e8f ee61cbc4 8e3ba6eb 84a7b608 48e391dd>;
        "particle/yuchaoshuipaotexiao.png" = <cf0af7dc 478b33f2 20519d9e 0c706f71 16ad0077>;
        "track/add_001.trackbin" = <372f4a1e dccf2178 05733a0a 7cf47dcc 87daa038>;
        "track/add_002.trackbin" = <49daab8c 4c8f70b1 eabe8931 79e3d12d f383af41>;
        "track/add_003.trackbin" = <995050f6 1375ce1c 48f6b6ff 0ebe179c 50bb5855>;
        "track/add_004.trackbin" = <eadda143 4bfb55e5 673d3f72 81bbe7c0 1eccae7c>;
        "track/add_005.trackbin" = <f80c5b0a 0277273a 4293876d 6ee7ff30 8049bfcf>;
        "track/add_006.trackbin" = <610da345 5935f2bf ce3a5002 d8e9d832 82fde8a4>;
        "track/add_007.trackbin" = <ffbd0a06 c93f38cc 94cbc7ec 2810dc6c 3654938b>;
        "track/add_008.trackbin" = <a5594e5f 7112e79c 28ed9f65 9b57459e 4c390285>;
        "track/add_009.trackbin" = <5a496100 8792e864 0e903034 739df274 f0fb8bf2>;
        "track/add_010.trackbin" = <2a5ae9df 1cb92786 f303e7a7 e0804b2c 2f456e22>;
        "track/add_011.trackbin" = <4faa7e36 64828958 8973d858 78c743ee c190b3ef>;
        "track/add_012.trackbin" = <6b639add 61d223c7 a988e0f9 9f2eb0bc 8ea913e3>;
        "track/add_013.trackbin" = <9d6bc1bd 271c24d0 0acecc92 1343f90c 5fde5fec>;
        "track/add_014.trackbin" = <dfa0e898 d5b831f4 bb01744c 7a8c8b2f db484f3c>;
        "track/add_015.trackbin" = <4fe8b1cc 93c9f0a2 ef2857c4 1aaac8b3 31153c8c>;
        "track/add_016.trackbin" = <f7f4f238 a6366e51 b6a48b5d 0cfc3588 3bc9c8d2>;
        "track/add_017.trackbin" = <6120dea7 6b273a0e 2ec7e5af 9b25dda6 6d94f9bd>;
        "track/add_018.trackbin" = <6bc7eb11 963c0e5b 319cfb9d f418f17b 0e0682c9>;
        "track/add_019.trackbin" = <e4543d75 d636518c cb8d03ff de8b9fb7 32ba9fc9>;
        "track/add_020.trackbin" = <6bf07cb0 29d8d727 6cd3b921 9f4fc60f 1a483caa>;
        "track/add_021.trackbin" = <d790092d cf99dec0 57e13c0f d5aff4e9 f7e62279>;
        "track/add_022.trackbin" = <da2284c3 b076bf32 e97ab755 44d97820 89ddc7f1>;
        "track/add_023.trackbin" = <c9e9a623 0145af70 69354d32 74fa01c8 da4e1e43>;
        "track/add_024.trackbin" = <e0a9ae17 152dec58 0f9b8fe4 d1d6e610 9e934ba4>;
        "track/add_025.trackbin" = <7b4d493f 8c64ec13 57b78c72 2e97d566 7dd4d215>;
        "track/add_026.trackbin" = <4fbf3a0d fe13688b 1d75020b bac56cf2 b66fdd07>;
        "track/add_027.trackbin" = <259c496f 2097c4e7 62fb2399 2dffb362 ed9b4f3c>;
        "track/add_028.trackbin" = <d3d84509 7b1707f7 f4050e12 209684bc 14edf4d8>;
        "track/add_029.trackbin" = <fe76f716 1a182acc 7f49fdfc 88ed7d02 e3d59f69>;
        "track/add_030.trackbin" = <874955b1 e81efdf4 8b2150fd bbaba6b2 be0d2197>;
        "track/add_031.trackbin" = <a092df82 5fe7255b f76ef931 dff1007d 5dc31720>;
        "track/add_032.trackbin" = <6d895fbf 65cfd4f8 47849243 074e8ea1 0726a94b>;
        "track/add_033.trackbin" = <cb7ab371 3ad14d94 3792dcff a4793d54 1effefdf>;
        "track/add_034.trackbin" = <0871a75a 0dc78d09 df3b26c8 3666af2b dffa4b96>;
        "track/add_035.trackbin" = <66d444f3 16e11b92 ab49e097 f1d06461 84b21fda>;
        "track/add_show01.trackbin" = <6be625cc 429ec91b c673ca31 536499c7 a6dfa6cf>;
        "track/add_show02.trackbin" = <74f4d9e1 b742242b 1d89f45f 056fd46d 901d3de5>;
        "track/add_show03.trackbin" = <84e86188 13e1d1b4 3e3471fc ba184c9e 0b62e9e3>;
        "track/add_show04.trackbin" = <b9783bda bc89eaf1 9cc874a3 4a83cf48 1ad6d729>;
        "track/add_show05.trackbin" = <1df6c6c9 c8897552 6f2c88e2 ca56b5c2 149915d8>;
        "track/add_show06.trackbin" = <d3438994 b6a6f03b 0dc5e647 d2091b2c 9f8f262e>;
        "track/add_show07.trackbin" = <9ea13f5f 2335af92 4b863667 79bfb93d a10e929c>;
        "track/add_show08.trackbin" = <9940a144 b2dc18a4 e6b05f6c 42ead891 12e41b5d>;
        "track/add_show09.trackbin" = <45bd9692 d1b822d5 9b3a7925 ada55b61 637ff501>;
        "track/add_show10.trackbin" = <d2558227 fedaf54e fe16fc8f c59bfe1f a85b183c>;
        "track/attack_hetun.trackbin" = <f215b18d 3c817b41 4c62ab0a 7fbf716a 4e42d66e>;
        "track/attack_hetun02.trackbin" = <8d76f20e afcf3780 3935074a b65b4501 c2f81b45>;
        "track/combo.trackbin" = <7eea26b3 c43f5c89 41a6c113 819f5629 8b17a547>;
        "track/combo02.trackbin" = <852b4e84 9b7b4699 25d404b0 bf036de1 6c163f16>;
        "track/combo03.trackbin" = <504dde88 0a3f6664 ae309d76 90bd812a 643e6fe7>;
        "track/deep_fish_01.trackbin" = <a6596210 670dbe77 c441a5eb 4e0fe787 e116f53c>;
        "track/finger_guessing_1.trackbin" = <732f8783 8185c1db 843d2c0f cbc6fef4 8f6e8f74>;
        "track/finger_guessing_2.trackbin" = <d37de33d ccda7cce d19b7797 6383feb2 f1089684>;
        "track/finger_guessing_3.trackbin" = <652a2a0f 1fe99046 3eeda37e 52b6e075 622372a2>;
        "track/gold_fever.trackbin" = <414d4260 d1ff7af9 70bcf3a2 e5feaede 877c556c>;
        "track/goldshark.trackbin" = <942ee0e6 362ecb47 c937636c fd642a0e 47d74dbb>;
        "track/goldshark02.trackbin" = <950ae899 5a7917f0 efe92d6b e3030998 53b9051c>;
        "track/goldshark03.trackbin" = <99106b8f b0734e4c 2d45b123 d4f0ca43 792490ae>;
        "track/goldshark04.trackbin" = <41cf9728 0fb32838 694110c5 d32f3b5d 697264c5>;
        "track/hide_fish.trackbin" = <a1764974 375cd9a8 8f11ff6d a6bb62ff f1c60df6>;
        "track/logo.trackbin" = <59a717d4 13f3ca70 fedd7598 ce3fa128 b43c4b0e>;
        "track/secret_box.trackbin" = <c90cd19b 8cc19f38 00b34a86 e9731bf0 19f63e94>;
        "track/secret_box02.trackbin" = <0ddbe9c3 9c8e4a6a 4cebb0a9 906692b0 7cd1417c>;
        "track/secret_box03a.trackbin" = <56558d58 939df996 36044dce f4e487a7 0c6b6e34>;
        "track/secret_box03b.trackbin" = <d82dfa85 e4c415bc abf9f93a 87527f97 38bab434>;
        "track/secret_box04.trackbin" = <6f83d340 d82a5b11 30a16d04 6afaabba 2bd5a3c8>;
        "track/secret_box05.trackbin" = <b03e6db0 3ca565a9 d904de36 b28da70d d0bf5add>;
        "track/secret_box06a.trackbin" = <9e757ee4 971549ee 622638a6 6d1aadd5 b65fdaf8>;
        "track/secret_box06b.trackbin" = <979615b6 6fcc2bb3 7a6c17cb cf1862ed d4a94519>;
        "track/sharktooth.trackbin" = <416d37fc 5d161de0 18cecd42 3824ac3a 994a8a83>;
        "track/sharktooth02.trackbin" = <75ce7a3c 564e64c2 9585aa41 ac0ab951 960b9c85>;
        "track/sharktooth03.trackbin" = <a12be31a 86eeb124 89f01865 86211517 4173bf15>;
        "track/sharktooth04.trackbin" = <df7e3f9d fe5a9869 dbec42ac 11c1b117 28426f21>;
        "track/slight_denglong011.trackbin" = <d2a7e48d ad5b96a7 fcc9414f 18cbedd3 9c4e6950>;
        "track/slight_denglong012.trackbin" = <d2a7e48d ad5b96a7 fcc9414f 18cbedd3 9c4e6950>;
        "track/slight_denglong013.trackbin" = <4e7dc059 6d14d037 36e7c1df 21bac011 5ddcb4c1>;
        "track/slight_denglong014.trackbin" = <e70cb90a e94fad7e 17d10229 9a800aca 7374ee2f>;
        "track/slight_denglong021.trackbin" = <262ec7ad e741b5cc 1d480f49 65c449f7 4301f39f>;
        "track/slight_denglong022.trackbin" = <81c15f1c 8fc7d550 bef73b68 b9697fe5 d1b3abab>;
        "track/slight_denglong023.trackbin" = <a69789d3 d82797a1 954f8b26 d9bf95b6 66d95718>;
        "track/slight_denglong024.trackbin" = <11d65851 ed28ba35 c9410049 aac6ceaa 7b47b3d4>;
        "track/slight_denglong025.trackbin" = <2f5bebb3 f5dba0af 5b80b96c f321d3eb dda8395b>;
        "track/slight_denglong026.trackbin" = <591dd61d fcd53c9f 6c7f4c23 cff75590 e98090c0>;
        "track/slight_denglong027.trackbin" = <2f5bebb3 f5dba0af 5b80b96c f321d3eb dda8395b>;
        "track/slight_denglong031.trackbin" = <9349d986 a08745ab a056c7b7 839b73de f51cb2fa>;
        "track/slight_denglong032.trackbin" = <5db209b4 53d52d7c ce33ea6c 97b7a848 68813384>;
        "track/slight_denglong033.trackbin" = <4cd98248 ab32a0d3 bc01410b 7b46a89f f8edc817>;
        "track/slight_denglong034.trackbin" = <9e4e8fdb abcffcbb a0fb5337 f46c77bf a1e2e266>;
        "track/start_show.trackbin" = <fe421823 79d8ce03 ea53dce2 d8bc10e0 28ff1130>;
        "track/start_show02.trackbin" = <017a3a0b b01895c4 b879dff5 3dd241e4 cf5db91f>;
        "track/start_show03.trackbin" = <3c0cc755 fe922ecf bce7221b efadd6c2 4222de66>;
        "track/start_show04.trackbin" = <dda46146 fe3be8ed 463efce2 7bc00888 4687f8ff>;
        "track/track01_1.trackbin" = <c0e9e158 9b643217 8da41796 fa238bcc b187df7f>;
        "track/track01_10.trackbin" = <9c46a1be 3b31260a 6781bd62 3e93c62b 83af8f72>;
        "track/track01_15.trackbin" = <de0d86f7 a1f051d6 cd07ccff 61d66d33 8e4fcbc6>;
        "track/track01_150.trackbin" = <511b7f54 efe975fe b857363a f5ef6013 8e27553b>;
        "track/track01_2.trackbin" = <b56a53f1 1da39f91 f6c8c4fb 8f88ad3d d3d2fe08>;
        "track/track01_20.trackbin" = <bc6ea1a7 d5bfcfb3 69436d23 830d7970 77444db8>;
        "track/track01_25.trackbin" = <c47f593a ee0820b2 28303ba7 55aa434e 7dfa3bc3>;
        "track/track01_30.trackbin" = <b6dff283 0b3b1306 d061347f 10afb8dd d52aeb00>;
        "track/track01_5.trackbin" = <9a614c1e 7ee3f35b e787da03 b1bfe53d dd1a5c22>;
        "track/track01_50.trackbin" = <e32afdef 96c8c730 42a71f9c d52bf9be 1b3e4d1e>;
        "track/track01_60.trackbin" = <d22897df 151cd9ef 0b3c66b3 552cc999 cf13dcbc>;
        "track/track01_70.trackbin" = <3960aa67 0c2f4721 2444cdd0 061804d0 cb1e9e65>;
        "track/track01_80.trackbin" = <361f29c9 8ae1721f 69e5d99a d14cebb6 1cad0b73>;
        "track/track01_90.trackbin" = <4b7a2a86 b0161a2d 7a6c9b63 a7ed1b5f c2fa8241>;
        "track/track01_douyu.trackbin" = <b50328ae e133ace6 873ab659 528a919c d0456593>;
        "track/track01_gaiputi.trackbin" = <22261ae7 ad13b724 3aefb563 e240778f 27b0917a>;
        "track/track01_huangjinsha.trackbin" = <7f3db45c 501cbfb4 5908c199 06e524a5 0eb9a1e3>;
        "track/track01_huangjinsha_2.trackbin" = <ecbe906c 84b003c4 b7d5fc1a 0cc3db53 0aa70504>;
        "track/track01_huangjinsha_3.trackbin" = <5c5a3c49 dc6bb1f0 0f62b540 22ae930f 2868fbe5>;
        "track/track01_huangjinsha_4.trackbin" = <5cb29e29 ee5c746e af20f3de 8f07eb34 6f9b7438>;
        "track/track01_huangjinsha_5.trackbin" = <46c018ff 80075ac7 bde18b2e 8045ded7 12f52bb9>;
        "track/track01_huangjinsha_6.trackbin" = <81391ed2 af93c041 893b53db 1192ec48 721de262>;
        "track/track01_hujing.trackbin" = <d7fe4f7e 6d3fe30f 5d13d662 99b232fe 44c7997a>;
        "track/track01_ice.trackbin" = <82186d4a ab5a9e1f dbcfe497 8926984a 93504c7f>;
        "track/track01_light.trackbin" = <b8d2c6ab 29085d81 d704e90a 0b220f6f 227a42f3>;
        "track/track01_red.trackbin" = <2247d208 95d49b8b e32406c1 6ca264eb 0fb20d07>;
        "track/track01_special.trackbin" = <cf0d7c9a 4f6c7faa 3f6d2a89 166123fc 70c0d866>;
        "track/track02_1.trackbin" = <c0e9e158 9b643217 8da41796 fa238bcc b187df7f>;
        "track/track02_10.trackbin" = <6236e272 ddce2edc a00597ae b54f8b16 d3fc8ddd>;
        "track/track02_15.trackbin" = <95762081 f1798aa3 68287c14 adf6e175 22a5568d>;
        "track/track02_150.trackbin" = <511b7f54 efe975fe b857363a f5ef6013 8e27553b>;
        "track/track02_2.trackbin" = <b9178853 82953208 ab335745 698734ad 399e3f69>;
        "track/track02_20.trackbin" = <bc6ea1a7 d5bfcfb3 69436d23 830d7970 77444db8>;
        "track/track02_240.trackbin" = <46e6afd8 90bd74e7 f60e3548 cfc93860 dcd22372>;
        "track/track02_25.trackbin" = <c47f593a ee0820b2 28303ba7 55aa434e 7dfa3bc3>;
        "track/track02_30.trackbin" = <b6dff283 0b3b1306 d061347f 10afb8dd d52aeb00>;
        "track/track02_5.trackbin" = <9a614c1e 7ee3f35b e787da03 b1bfe53d dd1a5c22>;
        "track/track02_50.trackbin" = <e32afdef 96c8c730 42a71f9c d52bf9be 1b3e4d1e>;
        "track/track02_60.trackbin" = <d22897df 151cd9ef 0b3c66b3 552cc999 cf13dcbc>;
        "track/track02_70.trackbin" = <3960aa67 0c2f4721 2444cdd0 061804d0 cb1e9e65>;
        "track/track02_80.trackbin" = <361f29c9 8ae1721f 69e5d99a d14cebb6 1cad0b73>;
        "track/track02_90.trackbin" = <aaa86273 828988fa eb426319 45e4fa75 90e8f3e3>;
        "track/track02_douyu.trackbin" = <25a28c15 db9ba3f4 e2c662a9 b45a10e5 1dadbe8e>;
        "track/track02_gaiputi.trackbin" = <22261ae7 ad13b724 3aefb563 e240778f 27b0917a>;
        "track/track02_huangjinsha.trackbin" = <e2d672b8 77a3d62c 7b0868f2 689069e3 dc0cb81b>;
        "track/track02_hujing.trackbin" = <d7fe4f7e 6d3fe30f 5d13d662 99b232fe 44c7997a>;
        "track/track02_ice.trackbin" = <82186d4a ab5a9e1f dbcfe497 8926984a 93504c7f>;
        "track/track02_light.trackbin" = <b8d2c6ab 29085d81 d704e90a 0b220f6f 227a42f3>;
        "track/track02_red.trackbin" = <2247d208 95d49b8b e32406c1 6ca264eb 0fb20d07>;
        "track/track02_special.trackbin" = <cf0d7c9a 4f6c7faa 3f6d2a89 166123fc 70c0d866>;
        "track/track03_1.trackbin" = <c1d0730f 1424bd1e c9f4c663 1cda49de 5622e927>;
        "track/track03_150.trackbin" = <27e7de42 3acc1812 01a69ae3 d8710f78 f65889d6>;
        "track/track03_2.trackbin" = <4f939672 1e0bdedb 66a35176 b814d726 ba3418b4>;
        "track/track03_3.trackbin" = <9173b4c9 e3f00d76 6d336070 fc16994e c59af71d>;
        "track/track03_4.trackbin" = <2a81209a 71c25735 f71047cc 2936a8f2 cdf5f0bb>;
        "track/track03_5.trackbin" = <024307d5 e941d800 81bc6e46 1bcb8c53 365bf6ef>;
        "track/track03_6.trackbin" = <afea15fd 331c4255 96bc9278 5f529e75 2fe34c4f>;
        "track/track03_7.trackbin" = <fc4d027b f3f95261 a5964117 998d47e5 ab5eaf44>;
        "track/track03_cheqiyu.trackbin" = <b9178853 82953208 ab335745 698734ad 399e3f69>;
        "track/track03_diaoyu.trackbin" = <4ab68196 3e80b922 cc6db172 375da49f 1398b6c6>;
        "track/track03_dinianyu.trackbin" = <c47f593a ee0820b2 28303ba7 55aa434e 7dfa3bc3>;
        "track/track03_douyu.trackbin" = <d15ab332 f3073c20 727d73e0 2b893948 525bdb36>;
        "track/track03_fangyu.trackbin" = <7c671927 ffbd7b7d 5cbb0efa 380f27a3 386f6883>;
        "track/track03_gaiputi.trackbin" = <a9397d8f df9459f1 0e199592 8984b8cf 1649a736>;
        "track/track03_haigui.trackbin" = <d4813eae 0a5f4651 8812532e 0ed7387b f8471c0c>;
        "track/track03_hetun.trackbin" = <b6dff283 0b3b1306 d061347f 10afb8dd d52aeb00>;
        "track/track03_huangjinsha.trackbin" = <e2d672b8 77a3d62c 7b0868f2 689069e3 dc0cb81b>;
        "track/track03_hujing.trackbin" = <d7fe4f7e 6d3fe30f 5d13d662 99b232fe 44c7997a>;
        "track/track03_huoyandouyu.trackbin" = <5125ac94 5b86d098 8bcd7da4 39580dfb 8164f327>;
        "track/track03_jianyu.trackbin" = <20adf8c2 e1bfe9a7 be843574 02cdd6da 164b7d13>;
        "track/track03_jinqiangyu.trackbin" = <45cd9f34 750ab1d3 b5efdd5a a681ff89 ab50dd83>;
        "track/track03_moguiyu.trackbin" = <886b7e68 bc86db17 f761fa3d 0355dd97 e397fef0>;
        "track/track03_shayu.trackbin" = <c79534bc 4b9d11df 3f0e4dc4 6407f6e1 73124a37>;
        "track/track03_shiziyu.trackbin" = <d22897df 151cd9ef 0b3c66b3 552cc999 cf13dcbc>;
        "track/track03_special.trackbin" = <cf0d7c9a 4f6c7faa 3f6d2a89 166123fc 70c0d866>;
        "track/track03_tianshiyu.trackbin" = <d94be08e 12a41773 1c61c33b 1f419a2d 9fe0091f>;
        "track/track03_xiaochouyu.trackbin" = <6236e272 ddce2edc a00597ae b54f8b16 d3fc8ddd>;
        "track/track03_yingzuiyu.trackbin" = <957a0a2a d5f16ea0 1864f748 04a70d2b 99a468d8>;
        "track/track04_1000.trackbin" = <cba6cf8b 6ca2bc49 76424b45 f7a22762 b295ee9b>;
        "track/track_01.trackbin" = <396ab052 66d7806d 316c719b d8023e3d ae18aa3b>;
        "track/yuzhen01.trackbin" = <ba617c92 300ce58e 67fd549e 626fbad8 ec24374b>;
        "track/yuzhen02.trackbin" = <805274b6 8fa42e32 2e3ba997 12842151 a438ab08>;
        "track/yuzhen03.trackbin" = <49d5df49 ed4e1d23 e0459e86 7052aef9 96b8621a>;
        "track/yuzhen04.trackbin" = <87570f4d 6b7ad257 d1d11874 95901050 035c032c>;
        "track/yuzhen05.trackbin" = <77a1f041 a12dcd35 e052441a 46bb7d62 aec7b439>;
        "track/yuzhen06.trackbin" = <25591e24 d9eb7685 957fff4c c025afbb 6a645b1a>;
        "track/yuzhen07.trackbin" = <c79cb8f3 671b63d8 0dda0b7d c9c9cfc7 fa67c3c3>;
        "track/yuzhen08.trackbin" = <9c672368 c4404343 31096a8c f4cfe90c 440e31fe>;
        "track/yuzhen09.trackbin" = <802eb079 df6bd331 687adf66 bcb47012 e4de31b4>;
        "track/yuzhen10.trackbin" = <70f728fb b019a210 1d8f7e19 fb22e854 1298fda6>;
        "track/yuzhen11.trackbin" = <17b85e84 e62c707b 9e6658de 88677a64 c258c64a>;
        "track/yuzhen12.trackbin" = <9bcaade1 090dff06 074840c6 e7b852aa 3fae0099>;
        "track/yuzhen13.trackbin" = <bf63148d 15d28669 8700cdc7 602fa943 96aebd46>;
        "track/yuzhen14.trackbin" = <8b9ce19c bf7d43be f8c1a1e6 ff649f7e 4c91e9ed>;
        "track/yuzhen15.trackbin" = <695fe58a 406e47d6 cb8974d4 b1956683 472f3dd1>;
        "track/yuzhen16.trackbin" = <e5d285d5 96fdf91f 4c060276 2a54b45c 3704b24b>;
        "track/yuzhen17.trackbin" = <fec6ba37 f0ab657a 88646cb7 66ed2210 96edc164>;
        "track/yuzhen18.trackbin" = <c409f1a0 fb361edf a8538078 f07b5783 8e91993f>;
        "track/yuzhen19.trackbin" = <aafb6e02 547155ec 1e50f4fc 2b537c8d 04939dba>;
        "track/yuzhen_red.trackbin" = <7b8ff663 83c4fa62 64088c8c 4529cbd9 3a2233d0>;
        "zh-Hans.lproj/ShareSDKLocalizable.strings" =         {
            hash = <f7d9266f 84e2c0b1 7f5564b3 3a41ef42 7816879f>;
            optional = 1;
        };
    };
    rules =     {
        "^" = 1;
        "^(Frameworks/[^/]+\\.framework/|PlugIns/[^/]+\\.appex/|())SC_Info/[^/]+\\.(sinf|supf|supp)$" =         {
            omit = 1;
            weight = 10000;
        };
        "^.*\\.lproj/" =         {
            optional = 1;
            weight = 1000;
        };
        "^.*\\.lproj/locversion.plist$" =         {
            omit = 1;
            weight = 1100;
        };
        "^version.plist$" = 1;
    };
    rules2 =     {
        ".*\\.dSYM($|/)" =         {
            weight = 11;
        };
        "^" =         {
            weight = 20;
        };
        "^(.*/)?\\.DS_Store$" =         {
            omit = 1;
            weight = 2000;
        };
        "^(Frameworks/[^/]+\\.framework/|PlugIns/[^/]+\\.appex/|())SC_Info/[^/]+\\.(sinf|supf|supp)$" =         {
            omit = 1;
            weight = 10000;
        };
        "^(Frameworks|SharedFrameworks|PlugIns|Plug-ins|XPCServices|Helpers|MacOS|Library/(Automator|Spotlight|LoginItems))/" =         {
            nested = 1;
            weight = 10;
        };
        "^.*" = 1;
        "^.*\\.lproj/" =         {
            optional = 1;
            weight = 1000;
        };
        "^.*\\.lproj/locversion.plist$" =         {
            omit = 1;
            weight = 1100;
        };
        "^Info\\.plist$" =         {
            omit = 1;
            weight = 20;
        };
        "^PkgInfo$" =         {
            omit = 1;
            weight = 20;
        };
        "^[^/]+$" =         {
            nested = 1;
            weight = 10;
        };
        "^embedded\\.provisionprofile$" =         {
            weight = 20;
        };
        "^version\\.plist$" =         {
            weight = 20;
        };
    };
}
~~~~~~~~~~~~~
\section Plist171 ShareSDKLocalizable.strings 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/en.lproj/ShareSDKLocalizable.strings
[ ->] {
    "ACCOUNT_VIEW_OK_BUTTON" = OK;
    "ACCOUNT_VIEW_TIPS_SECTION_TITLE" = "Authorization fails, please re-authorization";
    "ALTER_VIEW_I_KNOWN_BUTTON" = "I know";
    "ALTER_VIEW_TITLE" = Message;
    "AUTH_VIEW_CANCEL_BUTTON" = Cancel;
    "AUTH_VIEW_FOLLOW_ME" = "Follow Us";
    "AUTH_VIEW_TITLE_1" = "Sina Weibo";
    "AUTH_VIEW_TITLE_10" = Facebook;
    "AUTH_VIEW_TITLE_11" = Twitter;
    "AUTH_VIEW_TITLE_12" = Evernote;
    "AUTH_VIEW_TITLE_14" = "Google+";
    "AUTH_VIEW_TITLE_15" = Instagram;
    "AUTH_VIEW_TITLE_16" = LinkedIn;
    "AUTH_VIEW_TITLE_17" = Tumblr;
    "AUTH_VIEW_TITLE_2" = "Tencent Weibo";
    "AUTH_VIEW_TITLE_25" = Instapaper;
    "AUTH_VIEW_TITLE_26" = Pocket;
    "AUTH_VIEW_TITLE_27" = "YouDao Notes";
    "AUTH_VIEW_TITLE_28" = "Soho Kan";
    "AUTH_VIEW_TITLE_3" = "Soho Weibo";
    "AUTH_VIEW_TITLE_34" = Flickr;
    "AUTH_VIEW_TITLE_35" = Dropbox;
    "AUTH_VIEW_TITLE_36" = VKontakte;
    "AUTH_VIEW_TITLE_4" = "Netease weibo";
    "AUTH_VIEW_TITLE_41" = Mingdao;
    "AUTH_VIEW_TITLE_5" = Douban;
    "AUTH_VIEW_TITLE_6" = QZone;
    "AUTH_VIEW_TITLE_7" = Renren;
    "AUTH_VIEW_TITLE_8" = Kaixin;
    "AWARD_BACK_ITEM_TITLE" = Close;
    "ERROR_CODE_LINE_INVALID_UIPASTBOARD" = "Invalid UIPasteboard object";
    "ERROR_DESC_API_NOT_SUPPORT" = "This api is not supported";
    "ERROR_DESC_DROPBOX_UPLOAD_FILE_FAIL" = "Failed to upload file";
    "ERROR_DESC_FACEBOOK_CANCEL_ADD_FRIEND" = "Cancel Add Friend";
    "ERROR_DESC_GOOGLE_PLUS_SEND_FAIL" = "Sharing Failed";
    "ERROR_DESC_INSTAGRAM_NOT_INSTALL" = "Instagram is not installed";
    "ERROR_DESC_INSTAGRAM_SEND_FAIL" = "Failed to send";
    "ERROR_DESC_INVALID_PARAM" = "Invalid parameter";
    "ERROR_DESC_INVALID_PLATFORM" = "Invalid platform or the platform is not initialized!";
    "ERROR_DESC_LINE_NOT_INSTALL" = "Line is not installed";
    "ERROR_DESC_LINE_NOT_SUPPORTED_FOR_IOS7_PLUS" = "Line platforms do not support sharing pictures over the iOS7";
    "ERROR_DESC_LINE_SEND_FAIL" = "Failed to send";
    "ERROR_DESC_MAIL_NOT_SUPPORT" = "The device does not support e-mail feature or have not yet set up a mail account";
    "ERROR_DESC_NOT_CONNECT" = "Not yet integrated the platform";
    "ERROR_DESC_NO_NETWORK" = "Device is not connected to the network";
    "ERROR_DESC_PINTEREST_NOT_INSTALL" = "Pinterest is not installed";
    "ERROR_DESC_PINTEREST_NOT_INTEGRATED" = "Not yet integrated Pinterest SDK";
    "ERROR_DESC_PINTEREST_NOT_SET_URL_SCHEME" = "Pinterest's URL Scheme has not been set";
    "ERROR_DESC_PRINT_NOT_SUPPORT" = "The device does not allow for printing";
    "ERROR_DESC_QQ_API_IS_NOT_SUPPORT" = "The current version does not support this api of QQ";
    "ERROR_DESC_QQ_NOT_INSTALLED" = "QQ is not installed";
    "ERROR_DESC_QQ_NOT_INTEGRATED" = "Not yet integrated QQ SDK";
    "ERROR_DESC_QQ_NOT_SET_URL_SCHEME" = "QQ's URL Scheme has not been set";
    "ERROR_DESC_QQ_SEND_FAIL" = "Failed to send";
    "ERROR_DESC_QQ_UNKNOWN_MEDIA_TYPE" = "Unknown media type";
    "ERROR_DESC_SINA_WEIBO_IS_NOT_SUPPORT" = "The current version does not support this api of Sina Weibo";
    "ERROR_DESC_SINA_WEIBO_NOT_INSTALL" = "Sina Weibo is not installed";
    "ERROR_DESC_SINA_WEIBO_NOT_INTEGRATED" = "Not yet integrated Sina Weibo SDK";
    "ERROR_DESC_SINA_WEIBO_SEND_FAIL" = "Failed to send";
    "ERROR_DESC_SMS_NOT_SUPPORT" = "The device does not support SMS feature";
    "ERROR_DESC_SYS_VER_LOW_THAN" = "This feature does not support the iOS %@ following system version";
    "ERROR_DESC_TWITTER_GET_FRIENDS_API_DEPRECATED" = "This method is deprecated, use getFriendsWithPage method instead";
    "ERROR_DESC_UNAUTH" = "Not authorized";
    "ERROR_DESC_UNKNOWN" = "Unknown error";
    "ERROR_DESC_UNREGISTER" = "SDK has not been initialized";
    "ERROR_DESC_WEIXIN_API_IS_NOT_SUPPORT" = "The current version does not support this api of WeChat";
    "ERROR_DESC_WEIXIN_NOT_INSTALL" = "WeChat is not installed";
    "ERROR_DESC_WEIXIN_NOT_INTEGRATED" = "Not yet integrated WeChat SDK";
    "ERROR_DESC_WEIXIN_NOT_SET_URL_SCHEME" = "WeChat's URL Scheme has not been set";
    "ERROR_DESC_WEIXIN_SEND_REQUEST_ERROR" = "WeChat OpenApi Request failed";
    "ERROR_DESC_WEIXIN_UNKNOWN_MEDIA_TYPE" = "Unknown media type";
    "ERROR_DESC_WHATSAPP_NOT_INSTALL" = "WhatsApp is not installed";
    "ERROR_DESC_WHATSAPP_SEND_FAIL" = "Failed to send";
    "ERROR_DESC_YIXIN_API_IS_NOT_SUPPORT" = "The current version does not support this api of YiXin";
    "ERROR_DESC_YIXIN_NOT_INSTALL" = "YiXin is not installed";
    "ERROR_DESC_YIXIN_NOT_INTEGRATED" = "Not yet integrated YiXin SDK";
    "ERROR_DESC_YIXIN_NOT_SET_URL_SCHEME" = "YiXin's URL Scheme has not been set";
    "ERROR_DESC_YIXIN_SEND_REQUEST_ERROR" = "YiXin OpenApi Request failed";
    "ERROR_DESC_YIXIN_UNKNOWN_MEDIA_TYPE" = "Unknown media type";
    "FRIENDS_VIEW_CANCEL_BUTTON" = Cancel;
    "FRIENDS_VIEW_RECENT_SECTION_TITLE" = Recent;
    "FRIENDS_VIEW_SEARCHBAR_PLACEHOLDER" = "Search...";
    "FRIENDS_VIEW_SEARCH_RESULT_SECTION_TITLE" = Results;
    "FRIENDS_VIEW_TITLE" = Contact;
    "MSG_DEVICE_ALBUM_NOT_SUPPORT" = "The device does not support the album feature";
    "MSG_DEVICE_CAMERA_NOT_SUPPORT" = "The device does not support the camera feature";
    "MSG_SENDING" = "sending...";
    "MSG_SHARE_FAIL" = "%@ Sharing Failed! %@";
    "MSG_SHARE_FAIL_2" = "Sharing Failed!";
    "MSG_SHARE_SUCCESS" = "%@ share success";
    "PRINT_JOB_NAME" = Share;
    "QQ_MSG_SEND_FAIL" = "Failed to send QQ:%@";
    "QQ_MSG_SEND_SUCCESS" = "QQ messages sent successfully";
    "SHARE_ACTION_SHEET_CANCEL_BUTTON" = Cancel;
    "SHARE_VIEW_ACTIONSHEET_CANCEL_ITEM" = Cancel;
    "SHARE_VIEW_ACTIONSHEET_CLEAR_CONTENT_ITEM" = Clear;
    "SHARE_VIEW_ACTIONSHEET_OK_ITEM" = OK;
    "SHARE_VIEW_ACTIONSHEET_REMOVE_PHOTO_ITEM" = Delete;
    "SHARE_VIEW_ACTIONSHEET_SELECT_PHOTO_ITEM" = "Select photo...";
    "SHARE_VIEW_ACTIONSHEET_TAKE_PHOTO_ITEM" = "Take photo...";
    "SHARE_VIEW_ACTIONSHEET_VIEW_PHOTO_ITEM" = View;
    "SHARE_VIEW_AUTH_TITLE" = "Bind %@";
    "SHARE_VIEW_CANCEL_BUTTON" = Cancel;
    "SHARE_VIEW_DEF_PIC_PUBLISH_CONTENT" = "\U56fe\U7247\U5206\U4eab";
    "SHARE_VIEW_MSG_INVALID_CLIENTS" = "Please select the target platform";
    "SHARE_VIEW_MSG_INVALID_CONTENT" = "Please input share content";
    "SHARE_VIEW_ONE_KEY_LIST_TITLE" = "Share to";
    "SHARE_VIEW_PUBLISH_BUTTON" = Publish;
    "SHARE_VIEW_TITLE" = Share;
    "SHARE_VIEW_TOPIC" = Topic;
    "ShareType_1" = "Sina Weibo";
    "ShareType_10" = Facebook;
    "ShareType_11" = Twitter;
    "ShareType_12" = Evernote;
    "ShareType_13" = Foursquare;
    "ShareType_14" = "Google+";
    "ShareType_15" = Instagram;
    "ShareType_16" = LinkedIn;
    "ShareType_17" = Tumblr;
    "ShareType_18" = Mail;
    "ShareType_19" = SMS;
    "ShareType_2" = "Tencent Weibo";
    "ShareType_20" = Print;
    "ShareType_21" = Copy;
    "ShareType_22" = "Wechat contacts";
    "ShareType_23" = "Wechat moments";
    "ShareType_24" = QQ;
    "ShareType_25" = Instapaper;
    "ShareType_26" = Pocket;
    "ShareType_27" = "YouDao Notes";
    "ShareType_28" = "Soho Kan";
    "ShareType_3" = "Soho Weibo";
    "ShareType_30" = Pinterest;
    "ShareType_34" = Flickr;
    "ShareType_35" = Dropbox;
    "ShareType_36" = VKontakte;
    "ShareType_37" = "Wechat Favorites";
    "ShareType_38" = "Yixin Contacts";
    "ShareType_39" = "Yixin moments";
    "ShareType_4" = "Netease weibo";
    "ShareType_41" = Mingdao;
    "ShareType_42" = Line;
    "ShareType_43" = WhatsApp;
    "ShareType_5" = Douban;
    "ShareType_6" = QZone;
    "ShareType_7" = Renren;
    "ShareType_8" = Kaixin;
    "ShareType_9" = Pengyou;
    "WEIXIN_MSG_SEND_FAIL" = "Fail to send %@: %@";
    "WEIXIN_MSG_SEND_SUCCESS" = "Send %@ Success!";
    "WEIXIN_SESSION_NAME" = "WeChat Session";
    "WEIXIN_TIMELINE_NAME" = "WeChat Timeline";
}
~~~~~~~~~~~~~
\section Plist172 achievement_star.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/achievement_star.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 771;
    blendFuncSource = 1;
    duration = 0.300000011920929;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 41.71052551269531;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 66;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.5;
    particleLifespanVariance = 0.3199999928474426;
    radialAccelVariance = 0;
    radialAcceleration = -495.2713928222656;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 15;
    sourcePositionVariancey = 15;
    sourcePositionx = 826.6871337890625;
    sourcePositiony = 1192.80615234375;
    speed = 392.9378967285156;
    speedVariance = 14.44284534454346;
    startColorAlpha = 0;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 56.6315803527832;
    startParticleSizeVariance = 102.2631607055664;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "achievement_star.png";
}
~~~~~~~~~~~~~
\section Plist173 bao_li_zi_fen.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bao_li_zi_fen.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.05000000074505806;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0.02043269202113152;
    finishParticleSize = 50.39473724365234;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -300;
    maxParticles = 60;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.7627466917037964;
    radialAccelVariance = 0;
    radialAcceleration = -855.26318359375;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 90;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 45;
    sourcePositionVariancey = 45;
    sourcePositionx = 1308.990356445312;
    sourcePositiony = 893.3877563476562;
    speed = 858.9124145507812;
    speedVariance = 101.0999145507812;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 158.0526275634766;
    startParticleSizeVariance = 255;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bao_li_zi_fen.png";
}
~~~~~~~~~~~~~
\section Plist174 bao_li_zi_green.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bao_li_zi_green.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.05000000074505806;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0.02043269202113152;
    finishParticleSize = 50.39473724365234;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -300;
    maxParticles = 60;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.7627466917037964;
    radialAccelVariance = 0;
    radialAcceleration = -855.26318359375;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 90;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 45;
    sourcePositionVariancey = 45;
    sourcePositionx = 1607.739013671875;
    sourcePositiony = 1167.673461914062;
    speed = 858.9124145507812;
    speedVariance = 101.0999145507812;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 158.0526275634766;
    startParticleSizeVariance = 255;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bao_li_zi_green.png";
}
~~~~~~~~~~~~~
\section Plist175 bao_li_zi_lan.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bao_li_zi_lan.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.05000000074505806;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0.02043269202113152;
    finishParticleSize = 50.39473724365234;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -300;
    maxParticles = 60;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.7627466917037964;
    radialAccelVariance = 0;
    radialAcceleration = -855.26318359375;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 90;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 45;
    sourcePositionVariancey = 45;
    sourcePositionx = 1607.739013671875;
    sourcePositiony = 1167.673461914062;
    speed = 858.9124145507812;
    speedVariance = 101.0999145507812;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 158.0526275634766;
    startParticleSizeVariance = 255;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bao_li_zi_lan.png";
}
~~~~~~~~~~~~~
\section Plist176 bao_li_zi_red.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bao_li_zi_red.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.05000000074505806;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0.02043269202113152;
    finishParticleSize = 50.39473724365234;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -300;
    maxParticles = 60;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.7627466917037964;
    radialAccelVariance = 0;
    radialAcceleration = -855.26318359375;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 90;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 45;
    sourcePositionVariancey = 45;
    sourcePositionx = 1607.739013671875;
    sourcePositiony = 1167.673461914062;
    speed = 858.9124145507812;
    speedVariance = 101.0999145507812;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 158.0526275634766;
    startParticleSizeVariance = 255;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bao_li_zi_red.png";
}
~~~~~~~~~~~~~
\section Plist177 baoxiangbao.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/baoxiangbao.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0.2000000029802322;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 0.755859375;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 21;
    maxRadius = 480;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.500781238079071;
    particleLifespanVariance = 0;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 60;
    sourcePositionVariancey = 60;
    sourcePositionx = 841.9961547851562;
    sourcePositiony = 1406.142822265625;
    speed = 42.24917602539062;
    speedVariance = 15.57360172271729;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 116.9473648071289;
    startParticleSizeVariance = 78.81578826904297;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "baoxiangbao.png";
}
~~~~~~~~~~~~~
\section Plist178 baoxiangshan.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/baoxiangshan.plist
[ ->] {
    angle = 184.9299926757812;
    angleVariance = 96.16000366210938;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 2;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 3;
    maxRadius = 480;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.4204358458518982;
    particleLifespanVariance = 0.3150699138641357;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 3;
    rotationEndVariance = 8;
    rotationStart = 2;
    rotationStartVariance = 5;
    sourcePositionVariancex = 210.7890625;
    sourcePositionVariancey = 249.0263214111328;
    sourcePositionx = 1069.4970703125;
    sourcePositiony = 768.153076171875;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 0;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 373.2105407714844;
    startParticleSizeVariance = 0;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "baoxiangbao.png";
}
~~~~~~~~~~~~~
\section Plist179 baozha01.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/baozha01.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0.2000000029802322;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0.5;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 252.8684234619141;
    finishParticleSizeVariance = 74.10526275634766;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 15;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 1.298943161964417;
    radialAccelVariance = 0;
    radialAcceleration = -249.5246734619141;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 10;
    sourcePositionVariancey = 10;
    sourcePositionx = 1024.092163085938;
    sourcePositiony = 796.0101928710938;
    speed = 326.5316467285156;
    speedVariance = 0;
    startColorAlpha = 0.7337239384651184;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 52.6315803527832;
    startParticleSizeVariance = 15;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "baozha01.png";
}
~~~~~~~~~~~~~
\section Plist180 baozha02.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/baozha02.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 771;
    blendFuncSource = 771;
    duration = 0.2000000029802322;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 0.4900000095367432;
    finishColorGreen = 0.6700000166893005;
    finishColorRed = 0.6800000071525574;
    finishColorVarianceAlpha = 0.5;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 1.394736886024475;
    finishParticleSizeVariance = 40.81578826904297;
    gravityx = 0;
    gravityy = -5;
    maxParticles = 15;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 1.168943166732788;
    radialAccelVariance = 0;
    radialAcceleration = -249.5246734619141;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = -134.3338775634766;
    rotationStartVariance = -250.1644744873047;
    sourcePositionVariancex = 10;
    sourcePositionVariancey = 10;
    sourcePositionx = 1024.092163085938;
    sourcePositiony = 796.0101928710938;
    speed = 241.5201416015625;
    speedVariance = 0;
    startColorAlpha = 0;
    startColorBlue = 0.5299999713897705;
    startColorGreen = 0.7200000286102295;
    startColorRed = 0.7099999785423279;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 73.36842346191406;
    startParticleSizeVariance = 15;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "baozha02.png";
}
~~~~~~~~~~~~~
\section Plist181 bar_get.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bar_get.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0.09999999403953552;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 0.5201823115348816;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -525.493408203125;
    maxParticles = 120;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.3184415996074677;
    particleLifespanVariance = 0.487911194562912;
    radialAccelVariance = 0;
    radialAcceleration = -211.7598724365234;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 1038.8330078125;
    sourcePositiony = 671.1122436523438;
    speed = 360.9378967285156;
    speedVariance = 164.9876708984375;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0.005468749906867743;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 148.9736785888672;
    startParticleSizeVariance = 64.10526275634766;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bar_get.png";
}
~~~~~~~~~~~~~
\section Plist182 bigwin_blowout_1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bigwin_blowout_1.plist
[ ->] {
    angle = 90;
    angleVariance = 30.64144706726074;
    blendFuncDestination = 771;
    blendFuncSource = 1;
    duration = 0.300000011920929;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.8999999761581421;
    finishColorGreen = 0.8999999761581421;
    finishColorRed = 0.8999999761581421;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 87.86842346191406;
    finishParticleSizeVariance = 149.8421020507812;
    gravityx = 0;
    gravityy = -1924.03369140625;
    maxParticles = 216;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 2.759046077728271;
    particleLifespanVariance = 0;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = -239.8026275634766;
    rotationEndVariance = 585.444091796875;
    rotationStart = 815.625;
    rotationStartVariance = -727.1792602539062;
    sourcePositionVariancex = 268.1640625;
    sourcePositionVariancey = 74;
    sourcePositionx = 1039.508666992188;
    sourcePositiony = 1070.265258789062;
    speed = 1000;
    speedVariance = 420.2816467285156;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 93.97368621826172;
    startParticleSizeVariance = 81.76316070556641;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bigwin_blowout_1.png";
}
~~~~~~~~~~~~~
\section Plist183 bigwin_blowout_2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bigwin_blowout_2.plist
[ ->] {
    angle = 93.18256378173828;
    angleVariance = 26.47820663452148;
    blendFuncDestination = 771;
    blendFuncSource = 1;
    duration = 0.5;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.8999999761581421;
    finishColorGreen = 0.8999999761581421;
    finishColorRed = 0.8999999761581421;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 80.84210205078125;
    finishParticleSizeVariance = 90.84210205078125;
    gravityx = 0;
    gravityy = -1005.962158203125;
    maxParticles = 120;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 1.792763113975525;
    particleLifespanVariance = 0;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 72.53289794921875;
    rotationEndVariance = 61.4309196472168;
    rotationStart = 815.625;
    rotationStartVariance = 232.03125;
    sourcePositionVariancex = 214.109375;
    sourcePositionVariancey = 74;
    sourcePositionx = 1055.508666992188;
    sourcePositiony = 954;
    speed = 730.4173583984375;
    speedVariance = 420.2816467285156;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 116.4736862182617;
    startParticleSizeVariance = 112.4210510253906;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bigwin_blowout_2.png";
}
~~~~~~~~~~~~~
\section Plist184 bomb1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bomb1.plist
[ ->] {
    angle = 70.16000366210938;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 0.7062174677848816;
    finishColorRed = 1;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 1;
    finishColorVarianceGreen = 1;
    finishColorVarianceRed = 1;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -911.5953979492188;
    maxParticles = 10;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.4000000059604645;
    particleLifespanVariance = 0.164473682641983;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 15;
    rotationStartVariance = 180;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 944.3992309570312;
    sourcePositiony = 848.3571166992188;
    speed = 381.4761657714844;
    speedVariance = 166.0670166015625;
    startColorAlpha = 1;
    startColorBlue = 0.1979166716337204;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0.496874988079071;
    startColorVarianceGreen = 0.5278124809265137;
    startColorVarianceRed = 1;
    startParticleSize = 246.5;
    startParticleSizeVariance = 208.3947296142578;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bomb1.png";
}
~~~~~~~~~~~~~
\section Plist185 bomb2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bomb2.plist
[ ->] {
    angle = 70.16000366210938;
    angleVariance = 180.4600067138672;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 0.7062174677848816;
    finishColorRed = 1;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 1;
    finishColorVarianceGreen = 1;
    finishColorVarianceRed = 1;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 5;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.1000000014901161;
    particleLifespanVariance = 0.164473682641983;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 15;
    rotationStartVariance = 180;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 944.3992309570312;
    sourcePositiony = 848.3571166992188;
    speed = 48.7253303527832;
    speedVariance = 30.99300956726074;
    startColorAlpha = 1;
    startColorBlue = 0.1979166716337204;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 1;
    startColorVarianceGreen = 1;
    startColorVarianceRed = 1;
    startParticleSize = 82.28947448730469;
    startParticleSizeVariance = 201.3421020507812;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bomb2.png";
}
~~~~~~~~~~~~~
\section Plist186 bombparticle_shanshuolizi.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bombparticle_shanshuolizi.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 772;
    blendFuncSource = 1;
    duration = 1.700000047683716;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 0.7174479365348816;
    finishColorRed = 0.45654296875;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 2;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 65;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.5;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 202.4299926757812;
    rotationStartVariance = 0;
    sourcePositionVariancex = 240;
    sourcePositionVariancey = 215.6052703857422;
    sourcePositionx = 761.4697265625;
    sourcePositiony = 1018.212097167969;
    speed = 40;
    speedVariance = 9;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 0.5966796875;
    startColorRed = 0.56591796875;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 3;
    startParticleSizeVariance = 100;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bombparticle_xslz.png";
}
~~~~~~~~~~~~~
\section Plist187 bombparticle_xslz.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bombparticle_xslz.plist
[ ->] {
    angle = 283.5599975585938;
    angleVariance = 184.9299926757812;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 7;
    finishParticleSizeVariance = 0;
    gravityx = 248.977783203125;
    gravityy = 1061.780395507812;
    maxParticles = 45;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.0534375011920929;
    particleLifespanVariance = 0.385875791311264;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 227.6875;
    sourcePositionVariancey = 130;
    sourcePositionx = 756.1363525390625;
    sourcePositiony = 1007.909118652344;
    speed = 7;
    speedVariance = 86.29728698730469;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 51;
    startParticleSizeVariance = 35;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bombparticle_xslz.png";
}
~~~~~~~~~~~~~
\section Plist188 bombparticle_xxbz.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bombparticle_xxbz.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.2000000029802322;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 0.4900000095367432;
    finishColorGreen = 0.6700000166893005;
    finishColorRed = 0.6800000071525574;
    finishColorVarianceAlpha = 0.5;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 1.394736886024475;
    finishParticleSizeVariance = 40.81578826904297;
    gravityx = 0;
    gravityy = -5;
    maxParticles = 64;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.5;
    particleLifespanVariance = 0.5689431667327881;
    radialAccelVariance = 0;
    radialAcceleration = -249.5246734619141;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = -134.3338775634766;
    rotationStartVariance = -250.1644744873047;
    sourcePositionVariancex = 10;
    sourcePositionVariancey = 10;
    sourcePositionx = 1022.924255371094;
    sourcePositiony = 1171.5;
    speed = 241.5201416015625;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 0.5299999713897705;
    startColorGreen = 0.7200000286102295;
    startColorRed = 0.7099999785423279;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 128.3684234619141;
    startParticleSizeVariance = 15;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bombparticle_xxbz.png";
}
~~~~~~~~~~~~~
\section Plist189 bombparticle_zhxslz.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bombparticle_zhxslz.plist
[ ->] {
    angle = 239.1799926757812;
    angleVariance = 115.8899993896484;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.1500000059604645;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.9100000262260437;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 1.315789461135864;
    finishParticleSizeVariance = 0;
    gravityx = 45;
    gravityy = 89;
    maxParticles = 478;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.8891858458518982;
    particleLifespanVariance = 1.486315846443176;
    radialAccelVariance = 65;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 183;
    sourcePositionVariancey = -64;
    sourcePositionx = 771.3030395507812;
    sourcePositiony = 1045.0302734375;
    speed = 57.85896301269531;
    speedVariance = 45;
    startColorAlpha = 0.800000011920929;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 45;
    startParticleSizeVariance = 86.89473724365234;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bombparticle_zhxslz.png";
}
~~~~~~~~~~~~~
\section Plist190 bujingcha.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bujingcha.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 775;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 0.30029296875;
    finishColorRed = 0;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 90;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.05000000074505806;
    particleLifespanVariance = 0.6345394849777222;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 30;
    sourcePositionVariancey = 25;
    sourcePositionx = 1453.451049804688;
    sourcePositiony = 433.8367309570312;
    speed = 26.72697448730469;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 0.2086588591337204;
    startColorGreen = 1;
    startColorRed = 0.412109375;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0.1700000017881393;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 11.60526275634766;
    startParticleSizeVariance = 20.07894706726074;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bujingcha.png";
}
~~~~~~~~~~~~~
\section Plist191 bullet_circle.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bullet_circle.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 772;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 0.7174479365348816;
    finishColorRed = 0.5387369990348816;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 2;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 80;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.5;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 202.4299926757812;
    rotationStartVariance = 0;
    sourcePositionVariancex = 30;
    sourcePositionVariancey = 25;
    sourcePositionx = 1194.840698242188;
    sourcePositiony = 543.505126953125;
    speed = 128;
    speedVariance = 64;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 0.6552734375;
    startColorRed = 0.6017252802848816;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 180;
    startParticleSizeVariance = 32;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "bullet_circle.png";
}
~~~~~~~~~~~~~
\section Plist192 bullet_fire_01.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bullet_fire_01.plist
[ ->] {
    angle = 90;
    angleVariance = 6;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 2;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 36.15789413452148;
    gravityx = 0;
    gravityy = -951.069091796875;
    maxParticles = 80;
    maxRadius = 60.63000106811523;
    maxRadiusVariance = 0;
    minRadius = 3.160000085830688;
    particleLifespan = 0.8136307597160339;
    particleLifespanVariance = 0.1099999994039536;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 360;
    rotatePerSecondVariance = 125.5299987792969;
    rotationEnd = 93.62664794921875;
    rotationEndVariance = 134.3338775634766;
    rotationStart = -51.88651275634766;
    rotationStartVariance = 699.79443359375;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 3;
    sourcePositionx = 1024.629516601562;
    sourcePositiony = 213.7193908691406;
    speed = 1000;
    speedVariance = 939.1961059570312;
    startColorAlpha = 0.5771484375;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0.3251562416553497;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 29.52631568908691;
    startParticleSizeVariance = 118.2368392944336;
    tangentialAccelVariance = 678.36181640625;
    tangentialAcceleration = 0;
    textureFileName = "fire02_2.png";
}
~~~~~~~~~~~~~
\section Plist193 bullet_fire_02.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/bullet_fire_02.plist
[ ->] {
    angle = 90;
    angleVariance = 6;
    blendFuncDestination = 771;
    blendFuncSource = 1;
    duration = 1.5;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 0.5052083134651184;
    finishColorGreen = 0.5458984375;
    finishColorRed = 0.7721354365348816;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 288.3947448730469;
    finishParticleSizeVariance = 147.3947296142578;
    gravityx = 0;
    gravityy = 3000;
    maxParticles = 90;
    maxRadius = 60.63000106811523;
    maxRadiusVariance = 0;
    minRadius = 3.160000085830688;
    particleLifespan = 0.550000011920929;
    particleLifespanVariance = 0.009999999776482582;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 360;
    rotatePerSecondVariance = 125.5299987792969;
    rotationEnd = 93.62664794921875;
    rotationEndVariance = 134.3338775634766;
    rotationStart = -53.88651275634766;
    rotationStartVariance = 699.79443359375;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 2;
    sourcePositionx = 1024.092163085938;
    sourcePositiony = 208.2857208251953;
    speed = 1000;
    speedVariance = 807.0518188476562;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 0.27587890625;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 76.42105102539062;
    startParticleSizeVariance = 23.44736862182617;
    tangentialAccelVariance = 10;
    tangentialAcceleration = 0;
    textureFileName = "fire02.png";
}
~~~~~~~~~~~~~
\section Plist194 card_1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/card_1.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0.2800000011920929;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 0.9638671875;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 20;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 50;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.4280999898910522;
    radialAccelVariance = 0;
    radialAcceleration = 10;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 180;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 350.5078125;
    sourcePositionVariancey = 420.7631530761719;
    sourcePositionx = 1537.3359375;
    sourcePositiony = 750.6428833007812;
    speed = 195.7899932861328;
    speedVariance = 59.20999908447266;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 75.26316070556641;
    startParticleSizeVariance = 20.38999938964844;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "card_1.png";
}
~~~~~~~~~~~~~
\section Plist195 card_2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/card_2.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0.2000000029802322;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 2;
    finishParticleSizeVariance = 29.65789413452148;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 80;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.01999999955296516;
    particleLifespanVariance = 0.7691200375556946;
    radialAccelVariance = 0;
    radialAcceleration = -111.8421020507812;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 121.7516479492188;
    rotationStart = 0;
    rotationStartVariance = 311.225341796875;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 1532.560424804688;
    sourcePositiony = 786.0153198242188;
    speed = 452.2286071777344;
    speedVariance = 303.0670166015625;
    startColorAlpha = 0.20263671875;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0.3929687440395355;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 43.23683929443359;
    startParticleSizeVariance = 122.6052627563477;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "card_2.png";
}
~~~~~~~~~~~~~
\section Plist196 combo.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/combo.plist
[ ->] {
    angle = 90;
    angleVariance = 0;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0.3499999940395355;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 0.05000000074505806;
    finishColorRed = 0;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 10;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 60;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 1;
    particleLifespanVariance = 0;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 42;
    sourcePositionVariancey = 114;
    sourcePositionx = 511.9232177734375;
    sourcePositiony = 323.7321472167969;
    speed = 334.1385803222656;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 0.6435546875;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 127.7105255126953;
    startParticleSizeVariance = 10;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "combo.png";
}
~~~~~~~~~~~~~
\section Plist197 cristal_tail.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/cristal_tail.plist
[ ->] {
    angle = 0;
    angleVariance = 0;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 0.5341796875;
    finishColorRed = 1;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 6;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 80;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.009999998845160007;
    particleLifespanVariance = 0.9040912985801697;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 225.6924285888672;
    rotationStartVariance = 30.34539413452148;
    sourcePositionVariancex = 15;
    sourcePositionVariancey = 15;
    sourcePositionx = 1142.940551757812;
    sourcePositiony = 269.7398071289062;
    speed = 0;
    speedVariance = 46.8190803527832;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 0.4928385317325592;
    startColorRed = 1;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 0;
    startParticleSizeVariance = 112.5;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "cristal_tail.png";
}
~~~~~~~~~~~~~
\section Plist198 dalizi.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/dalizi.plist
[ ->] {
    angle = 90;
    angleVariance = 0;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 0;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 5;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.1500000059604645;
    particleLifespanVariance = 0.5;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 276.6640625;
    sourcePositionVariancey = 253.8157958984375;
    sourcePositionx = 994.5182495117188;
    sourcePositiony = 822.8571166992188;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 0;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 100;
    startParticleSizeVariance = 50;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "dalizi.png";
}
~~~~~~~~~~~~~
\section Plist199 datuowei.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/datuowei.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 772;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.6466471552848816;
    finishColorGreen = 0.6927083134651184;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0.580078125;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 2;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 100;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 1.171414494514465;
    particleLifespanVariance = 1;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 202.4299926757812;
    rotationStartVariance = 0;
    sourcePositionVariancex = 30;
    sourcePositionVariancey = 30;
    sourcePositionx = 1162.825317382812;
    sourcePositiony = 367.0101928710938;
    speed = 40;
    speedVariance = 9;
    startColorAlpha = 0.6940104365348816;
    startColorBlue = 0;
    startColorGreen = 0.7701823115348816;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0.5931249856948853;
    startParticleSize = 120;
    startParticleSizeVariance = 50;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "datuowei.png";
}
~~~~~~~~~~~~~
\section Plist200 denglongyu1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/denglongyu1.plist
[ ->] {
    angle = 162.5049285888672;
    angleVariance = 7.993421077728271;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 0.4085286557674408;
    finishColorBlue = 0.67529296875;
    finishColorGreen = 1;
    finishColorRed = 0.28125;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 1;
    finishColorVarianceRed = 0;
    finishParticleSize = 6;
    finishParticleSizeVariance = 12;
    gravityx = 0;
    gravityy = -32.75246810913086;
    maxParticles = 12;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.300000011920929;
    particleLifespanVariance = 0.4853000044822693;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = -15.54276275634766;
    rotationStartVariance = 57.36019897460938;
    sourcePositionVariancex = 60;
    sourcePositionVariancey = 60;
    sourcePositionx = 1635.669921875;
    sourcePositiony = 899.8163452148438;
    speed = 0;
    speedVariance = 52.93996810913086;
    startColorAlpha = 1;
    startColorBlue = 0.73583984375;
    startColorGreen = 0.8836262822151184;
    startColorRed = 0.3859049379825592;
    startColorVarianceAlpha = 0.4834375083446503;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 1;
    startColorVarianceRed = 0;
    startParticleSize = 80.36842346191406;
    startParticleSizeVariance = 33.60526275634766;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "denglongyu1.png";
}
~~~~~~~~~~~~~
\section Plist201 denglongyu2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/denglongyu2.plist
[ ->] {
    angle = 162.5049285888672;
    angleVariance = 7.993421077728271;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 0.4085286557674408;
    finishColorBlue = 0.67529296875;
    finishColorGreen = 1;
    finishColorRed = 0.28125;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 1;
    finishColorVarianceRed = 0;
    finishParticleSize = 6;
    finishParticleSizeVariance = 12;
    gravityx = 0;
    gravityy = -32.75246810913086;
    maxParticles = 50;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.3400000333786011;
    particleLifespanVariance = 0.345300018787384;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = -15.54276275634766;
    rotationStartVariance = 57.36019897460938;
    sourcePositionVariancex = 52;
    sourcePositionVariancey = 51.6315803527832;
    sourcePositionx = 895.401123046875;
    sourcePositiony = 886.85205078125;
    speed = 0;
    speedVariance = 52.93996810913086;
    startColorAlpha = 1;
    startColorBlue = 0.73583984375;
    startColorGreen = 0.8836262822151184;
    startColorRed = 0.3859049379825592;
    startColorVarianceAlpha = 0.4834375083446503;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 1;
    startColorVarianceRed = 0;
    startParticleSize = 31.86842155456543;
    startParticleSizeVariance = 67.10526275634766;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "denglongyu2.png";
}
~~~~~~~~~~~~~
\section Plist202 door_close.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/door_close.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0.1000000014901161;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 0;
    finishColorGreen = 0;
    finishColorRed = 1;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 130;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.2996422648429871;
    particleLifespanVariance = 0.2404440939426422;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 169.4901275634766;
    sourcePositionVariancex = 267.3671875;
    sourcePositionVariancey = 5;
    sourcePositionx = 1272.537475585938;
    sourcePositiony = 771.35205078125;
    speed = 180.3791046142578;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 0;
    startColorGreen = 0.7568359375;
    startColorRed = 1;
    startColorVarianceAlpha = 0.005468749906867743;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 293.1842041015625;
    startParticleSizeVariance = 164.0789489746094;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "door_close.png";
}
~~~~~~~~~~~~~
\section Plist203 event_weapon.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/event_weapon.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 772;
    blendFuncSource = 770;
    duration = 0.300000011920929;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 60;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.5;
    particleLifespanVariance = 0.3199999928474426;
    radialAccelVariance = 0;
    radialAcceleration = -661.3898315429688;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 15;
    sourcePositionVariancey = 15;
    sourcePositionx = 1022.310913085938;
    sourcePositiony = 77.69387817382812;
    speed = 548.6739501953125;
    speedVariance = 9;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 70;
    startParticleSizeVariance = 115.9473648071289;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "event_weapon.png";
}
~~~~~~~~~~~~~
\section Plist204 exploding.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/exploding.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0.2;
    emissionRate = 128;
    emitterType = 0;
    finishColorAlpha = 0.8392157;
    finishColorBlue = 0;
    finishColorGreen = 0;
    finishColorRed = 0;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 13.59;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 100;
    maxRadius = 256;
    maxRadiusVariance = 0;
    minRadius = 0;
    minRadiusVariance = 0;
    particleLifespan = 1.3;
    particleLifespanVariance = 0.2;
    radialAccelVariance = 0;
    radialAcceleration = -2048;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 0;
    sourcePositiony = 0;
    speed = 512;
    speedVariance = 1;
    startColorAlpha = 1;
    startColorBlue = 0.3568628;
    startColorGreen = 0.5568628;
    startColorRed = 0.8862745;
    startColorVarianceAlpha = 0.4980392;
    startColorVarianceBlue = 0.2;
    startColorVarianceGreen = 0.2;
    startColorVarianceRed = 0.2;
    startParticleSize = 256.11;
    startParticleSizeVariance = 16.32;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 1024;
    textureFileName = "exploding.png";
}
~~~~~~~~~~~~~
\section Plist205 fever_ready.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/fever_ready.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 1;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 1.539999961853027;
    gravityy = 0.699999988079071;
    maxParticles = 50;
    maxRadius = 102.2105255126953;
    maxRadiusVariance = 158.4210510253906;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.7915295958518982;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 50;
    sourcePositionVariancey = 50;
    sourcePositionx = 1025.02880859375;
    sourcePositiony = 70.79081726074219;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 0;
    startParticleSizeVariance = 46.60526275634766;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "fever_ready.png";
}
~~~~~~~~~~~~~
\section Plist206 fire02.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/fire02.plist
[ ->] {
    angle = 90;
    angleVariance = 6;
    blendFuncDestination = 771;
    blendFuncSource = 1;
    duration = 1.5;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 0.5052083134651184;
    finishColorGreen = 0.5458984375;
    finishColorRed = 0.7721354365348816;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 288.3947448730469;
    finishParticleSizeVariance = 147.3947296142578;
    gravityx = 0;
    gravityy = 3000;
    maxParticles = 90;
    maxRadius = 60.63000106811523;
    maxRadiusVariance = 0;
    minRadius = 3.160000085830688;
    particleLifespan = 0.550000011920929;
    particleLifespanVariance = 0.009999999776482582;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 360;
    rotatePerSecondVariance = 125.5299987792969;
    rotationEnd = 93.62664794921875;
    rotationEndVariance = 134.3338775634766;
    rotationStart = -53.88651275634766;
    rotationStartVariance = 699.79443359375;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 2;
    sourcePositionx = 1024.092163085938;
    sourcePositiony = 208.2857208251953;
    speed = 1000;
    speedVariance = 807.0518188476562;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 0.27587890625;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 76.42105102539062;
    startParticleSizeVariance = 23.44736862182617;
    tangentialAccelVariance = 10;
    tangentialAcceleration = 0;
    textureFileName = "fire02.png";
}
~~~~~~~~~~~~~
\section Plist207 fire02_2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/fire02_2.plist
[ ->] {
    angle = 90;
    angleVariance = 6;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 2;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 36.15789413452148;
    gravityx = 0;
    gravityy = -951.069091796875;
    maxParticles = 80;
    maxRadius = 60.63000106811523;
    maxRadiusVariance = 0;
    minRadius = 3.160000085830688;
    particleLifespan = 0.8136307597160339;
    particleLifespanVariance = 0.1099999994039536;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 360;
    rotatePerSecondVariance = 125.5299987792969;
    rotationEnd = 93.62664794921875;
    rotationEndVariance = 134.3338775634766;
    rotationStart = -51.88651275634766;
    rotationStartVariance = 699.79443359375;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 3;
    sourcePositionx = 1024.629516601562;
    sourcePositiony = 213.7193908691406;
    speed = 1000;
    speedVariance = 939.1961059570312;
    startColorAlpha = 0.5771484375;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0.3251562416553497;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 29.52631568908691;
    startParticleSizeVariance = 118.2368392944336;
    tangentialAccelVariance = 678.36181640625;
    tangentialAcceleration = 0;
    textureFileName = "fire02_2.png";
}
~~~~~~~~~~~~~
\section Plist208 firefish_trail1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/firefish_trail1.plist
[ ->] {
    angle = 360;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 0.6590169072151184;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 45.84210586547852;
    finishParticleSizeVariance = 53.97368240356445;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 60;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.4087418019771576;
    particleLifespanVariance = 0.04934210702776909;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 60;
    rotationStart = 0;
    rotationStartVariance = 80;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 886.0345458984375;
    sourcePositiony = 959.005126953125;
    speed = 186.7290344238281;
    speedVariance = 189.3503265380859;
    startColorAlpha = 1;
    startColorBlue = 0.17333984375;
    startColorGreen = 0.6090494990348816;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 173.2105255126953;
    startParticleSizeVariance = 255;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "firefish_trail1.png";
}
~~~~~~~~~~~~~
\section Plist209 firefish_trail2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/firefish_trail2.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 80;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.3854852020740509;
    particleLifespanVariance = 0.06938733905553818;
    radialAccelVariance = 0;
    radialAcceleration = -351.1513061523438;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 168.0098724365234;
    rotationStartVariance = 0;
    sourcePositionVariancex = 30;
    sourcePositionVariancey = 30;
    sourcePositionx = 828.9750366210938;
    sourcePositiony = 617.005126953125;
    speed = 335.1167907714844;
    speedVariance = 85.11513519287109;
    startColorAlpha = 0.521484375;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 1.63157844543457;
    startParticleSizeVariance = 130.1842041015625;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "firefish_trail2.png";
}
~~~~~~~~~~~~~
\section Plist210 fish_disappear.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/fish_disappear.plist
[ ->] {
    angle = 0;
    angleVariance = 0;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 9.251645088195801;
    maxParticles = 28;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.1516241729259491;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 89.6640625;
    sourcePositionVariancey = 85;
    sourcePositionx = 1034.318603515625;
    sourcePositiony = 1112.1123046875;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 29.94736862182617;
    startParticleSizeVariance = 92.26316070556641;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "fish_disappear.png";
}
~~~~~~~~~~~~~
\section Plist211 frozen.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/frozen.plist
[ ->] {
    angle = 90.22203826904297;
    angleVariance = 0;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 1;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0.4884314835071564;
    finishParticleSize = 0;
    finishParticleSizeVariance = 14.60526275634766;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 80;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.2000000029802322;
    particleLifespanVariance = 0.699999988079071;
    radialAccelVariance = 25.18503379821777;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 1083.765625;
    sourcePositionVariancey = 670.5;
    sourcePositionx = 1010.564270019531;
    sourcePositiony = 785.3724365234375;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 1;
    startColorVarianceGreen = 1;
    startColorVarianceRed = 1;
    startParticleSize = 10;
    startParticleSizeVariance = 18.44736862182617;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "frozen.png";
}
~~~~~~~~~~~~~
\section Plist212 gaibao.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/gaibao.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 771;
    blendFuncSource = 1;
    duration = 0.1000000014901161;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.37109375;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0.5842848420143127;
    finishColorVarianceGreen = 0.7193509340286255;
    finishColorVarianceRed = 1;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 80;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.9822162985801697;
    radialAccelVariance = 0;
    radialAcceleration = -229.8519744873047;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 168.0098724365234;
    rotationStartVariance = 0;
    sourcePositionVariancex = 5;
    sourcePositionVariancey = 5;
    sourcePositionx = 785.3333129882812;
    sourcePositiony = 1099.227294921875;
    speed = 265.4572448730469;
    speedVariance = 27.03536224365234;
    startColorAlpha = 1;
    startColorBlue = 0.3352864682674408;
    startColorGreen = 0.8102213740348816;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0.5535937547683716;
    startColorVarianceGreen = 0.8701562285423279;
    startColorVarianceRed = 1;
    startParticleSize = 38.81578826904297;
    startParticleSizeVariance = 82.5;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "gaibao.png";
}
~~~~~~~~~~~~~
\section Plist213 gas_disappear.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/gas_disappear.plist
[ ->] {
    angle = 0;
    angleVariance = 0;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0.300000011920929;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 438.2195739746094;
    maxParticles = 20;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.5895353555679321;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 120;
    sourcePositionVariancey = 120;
    sourcePositionx = 1029.143920898438;
    sourcePositiony = 798.6122436523438;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 75;
    startParticleSizeVariance = 64.84210205078125;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "gas_disappear.png";
}
~~~~~~~~~~~~~
\section Plist214 gas_explode.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/gas_explode.plist
[ ->] {
    angle = 360;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 0.6015625;
    finishColorRed = 0;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 254;
    finishParticleSizeVariance = 60;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 100;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.8862746357917786;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 8;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 793.757568359375;
    sourcePositiony = 1044.302978515625;
    speed = 139.4428405761719;
    speedVariance = 14.1858549118042;
    startColorAlpha = 0.02999999932944775;
    startColorBlue = 0.953125;
    startColorGreen = 0.4991861879825592;
    startColorRed = 0;
    startColorVarianceAlpha = 0.5;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 177.5;
    startParticleSizeVariance = 59.84210586547852;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "gas_explode.png";
}
~~~~~~~~~~~~~
\section Plist215 gas_loopbubble.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/gas_loopbubble.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 0.1106770858168602;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 20;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.1500000059604645;
    particleLifespanVariance = 0.699999988079071;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 30;
    sourcePositionVariancey = 30;
    sourcePositionx = 1257.090209960938;
    sourcePositiony = 584.2193603515625;
    speed = 26.72697448730469;
    speedVariance = 0;
    startColorAlpha = 0.3743489682674408;
    startColorBlue = 0.7161458134651184;
    startColorGreen = 0.5149739384651184;
    startColorRed = 1;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 121.184211730957;
    startParticleSizeVariance = 57.73684310913086;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "gas_loopbubble.png";
}
~~~~~~~~~~~~~
\section Plist216 gas_loopsmoke.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/gas_loopsmoke.plist
[ ->] {
    angle = 360;
    angleVariance = 360;
    blendFuncDestination = 771;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 0;
    finishColorGreen = 0;
    finishColorRed = 0.0738932266831398;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0.3405949473381042;
    finishParticleSize = 61.81578826904297;
    finishParticleSizeVariance = 442.2894592285156;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 100;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.177323192358017;
    particleLifespanVariance = 1.056743383407593;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 3600;
    rotationEndVariance = 3600;
    rotationStart = 0;
    rotationStartVariance = 1734.868408203125;
    sourcePositionVariancex = 20;
    sourcePositionVariancey = 20;
    sourcePositionx = 747.1324462890625;
    sourcePositiony = 902.4642944335938;
    speed = 39.47368240356445;
    speedVariance = 0;
    startColorAlpha = 0.17724609375;
    startColorBlue = 1;
    startColorGreen = 0;
    startColorRed = 0.5821940302848816;
    startColorVarianceAlpha = 0.06640625;
    startColorVarianceBlue = 0.425000011920929;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0.6695312261581421;
    startParticleSize = 49.68421173095703;
    startParticleSizeVariance = 51.42105102539062;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "gas_loopsmoke.png";
}
~~~~~~~~~~~~~
\section Plist217 gas_smoke.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/gas_smoke.plist
[ ->] {
    angle = 360;
    angleVariance = 360;
    blendFuncDestination = 771;
    blendFuncSource = 770;
    duration = 0.009999999776482582;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 0.2254231721162796;
    finishColorGreen = 0;
    finishColorRed = 0.4375;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0.2770432829856873;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0.4397535920143127;
    finishParticleSize = 109.9473648071289;
    finishParticleSizeVariance = 201.6578979492188;
    gravityx = 1.149999976158142;
    gravityy = 1.580000042915344;
    maxParticles = 64;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.5;
    particleLifespanVariance = 0.2;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 3600;
    rotationEndVariance = 3600;
    rotationStart = 0;
    rotationStartVariance = 1734.868408203125;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 993.02880859375;
    sourcePositiony = 989.6326293945312;
    speed = 138.7232666015625;
    speedVariance = 40.60444259643555;
    startColorAlpha = 0.0862630233168602;
    startColorBlue = 1;
    startColorGreen = 0;
    startColorRed = 0.2373046875;
    startColorVarianceAlpha = 0.1150000020861626;
    startColorVarianceBlue = 0.8029687404632568;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0.7053124904632568;
    startParticleSize = 34.11000061035156;
    startParticleSizeVariance = 72.42105102539062;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "gas_smoke.png";
}
~~~~~~~~~~~~~
\section Plist218 haigui_ice.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/haigui_ice.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 40;
    maxRadius = 66.31999969482422;
    maxRadiusVariance = 60;
    minRadius = 41.04999923706055;
    particleLifespan = 0.2266653031110764;
    particleLifespanVariance = 0.5417351722717285;
    radialAccelVariance = 0;
    radialAcceleration = -144.6340484619141;
    rotatePerSecond = 45;
    rotatePerSecondVariance = 125.5299987792969;
    rotationEnd = 0;
    rotationEndVariance = 4.440789699554443;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 80;
    sourcePositionVariancey = 80;
    sourcePositionx = 1583.96923828125;
    sourcePositiony = 741.8112182617188;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0.1943749934434891;
    startColorVarianceRed = 1;
    startParticleSize = 50.18421173095703;
    startParticleSizeVariance = 88.81578826904297;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "haigui_ice.png";
}
~~~~~~~~~~~~~
\section Plist219 harpoon_explode1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/harpoon_explode1.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0.800000011920929;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 0.6500651240348816;
    finishColorRed = 0.8933919072151184;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 6;
    maxRadius = 66.31999969482422;
    maxRadiusVariance = 60;
    minRadius = 41.04999923706055;
    particleLifespan = 0.2266653031110764;
    particleLifespanVariance = 0.05000000074505806;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 45;
    rotatePerSecondVariance = 125.5299987792969;
    rotationEnd = 0;
    rotationEndVariance = 4.440789699554443;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 109.1875;
    sourcePositionVariancey = 109;
    sourcePositionx = 1036.97509765625;
    sourcePositiony = 798.0458984375;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 0.7194010615348816;
    startColorRed = 0.7482096552848816;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 35.05263137817383;
    startParticleSizeVariance = 88.81578826904297;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "harpoon_explode1.png";
}
~~~~~~~~~~~~~
\section Plist220 harpoon_explode2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/harpoon_explode2.plist
[ ->] {
    angle = 360;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 0.6015625;
    finishColorRed = 0;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 254;
    finishParticleSizeVariance = 60;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 100;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.8862746357917786;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 793.757568359375;
    sourcePositiony = 1044.302978515625;
    speed = 139.4428405761719;
    speedVariance = 14.1858549118042;
    startColorAlpha = 0.02999999932944775;
    startColorBlue = 0.953125;
    startColorGreen = 0.4991861879825592;
    startColorRed = 0;
    startColorVarianceAlpha = 0.5;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 177.5;
    startParticleSizeVariance = 59.84210586547852;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "harpoon_explode2.png";
}
~~~~~~~~~~~~~
\section Plist221 harpoon_standby.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/harpoon_standby.plist
[ ->] {
    angle = 90;
    angleVariance = 10;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 140.9333953857422;
    maxParticles = 20;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.362870067358017;
    particleLifespanVariance = 0.6260279417037964;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 20.46875;
    sourcePositionVariancey = -64;
    sourcePositionx = 517.4510498046875;
    sourcePositiony = 496.3775634765625;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 10.71052646636963;
    startParticleSizeVariance = 3.736842155456543;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "harpoon_standby.png";
}
~~~~~~~~~~~~~
\section Plist222 hetun_crash.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/hetun_crash.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.300000011920929;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 0;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 70;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.3499999940395355;
    particleLifespanVariance = 0.2599999904632568;
    radialAccelVariance = 0;
    radialAcceleration = -495.2713928222656;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 15;
    sourcePositionVariancey = 15;
    sourcePositionx = 794.0908813476562;
    sourcePositiony = 337.9696960449219;
    speed = 520;
    speedVariance = 14.44284534454346;
    startColorAlpha = 1;
    startColorBlue = 0;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0.5767187476158142;
    startColorVarianceBlue = 0.7678124904632568;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 9.315788269042969;
    startParticleSizeVariance = 31.18421173095703;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "hetun_crash.png";
}
~~~~~~~~~~~~~
\section Plist223 hetun_crash_blue.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/hetun_crash_blue.plist
[ ->] {
    angle = 101.0094604492188;
    angleVariance = 184.9299926757812;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 0.0730794295668602;
    finishColorRed = 0;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 248.977783203125;
    gravityy = 416.6324157714844;
    maxParticles = 79;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.4096052646636963;
    particleLifespanVariance = 0.2000000029802322;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 154.34375;
    sourcePositionVariancey = 65;
    sourcePositionx = 737.48486328125;
    sourcePositiony = 304.8939514160156;
    speed = 0;
    speedVariance = 230.5201416015625;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 0.2825520932674408;
    startColorRed = 0.208984375;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 1;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0.7231249809265137;
    startParticleSize = 22;
    startParticleSizeVariance = 78.86842346191406;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "hetun_crash_blue.png";
}
~~~~~~~~~~~~~
\section Plist224 hetun_dead.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/hetun_dead.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.300000011920929;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 0.3992513120174408;
    finishColorRed = 0;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 70;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.3499999940395355;
    particleLifespanVariance = 0.2599999904632568;
    radialAccelVariance = 0;
    radialAcceleration = -495.2713928222656;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 15;
    sourcePositionVariancey = 15;
    sourcePositionx = 1340.575805664062;
    sourcePositiony = 971.494873046875;
    speed = 520;
    speedVariance = 14.44284534454346;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 0;
    startColorVarianceAlpha = 0.5767187476158142;
    startColorVarianceBlue = 0.7678124904632568;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 21.31578826904297;
    startParticleSizeVariance = 31.18421173095703;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "hetun_dead.png";
}
~~~~~~~~~~~~~
\section Plist225 hetun_disappear.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/hetun_disappear.plist
[ ->] {
    angle = 283.5599975585938;
    angleVariance = 184.9299926757812;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 7;
    finishParticleSizeVariance = 0;
    gravityx = 248.977783203125;
    gravityy = 416.6324157714844;
    maxParticles = 193;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.6300740242004395;
    particleLifespanVariance = 0.2000000029802322;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 120;
    sourcePositionVariancey = 65;
    sourcePositionx = 1010.579650878906;
    sourcePositiony = 47.15816497802734;
    speed = 0;
    speedVariance = 86.29728698730469;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 17;
    startParticleSizeVariance = 35;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "hetun_disappear.png";
}
~~~~~~~~~~~~~
\section Plist226 huangjinpao.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/huangjinpao.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 6;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 35;
    maxRadius = 66.31999969482422;
    maxRadiusVariance = 60;
    minRadius = 41.04999923706055;
    particleLifespan = 0.06666530668735504;
    particleLifespanVariance = 0.2917351722717285;
    radialAccelVariance = 0;
    radialAcceleration = -35.36184310913086;
    rotatePerSecond = 45;
    rotatePerSecondVariance = 125.5299987792969;
    rotationEnd = 0;
    rotationEndVariance = 4.440789699554443;
    rotationStart = 4;
    rotationStartVariance = 0;
    sourcePositionVariancex = 26;
    sourcePositionVariancey = 26;
    sourcePositionx = 552.6141967773438;
    sourcePositiony = 1194.336791992188;
    speed = 6.990131378173828;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 3.105262756347656;
    startParticleSizeVariance = 55.5;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "huangjinpao.png";
}
~~~~~~~~~~~~~
\section Plist227 jbtwlz.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/jbtwlz.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 10;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.1947985142469406;
    particleLifespanVariance = 0.1000000014901161;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 26;
    sourcePositionVariancey = 26;
    sourcePositionx = 189.9731292724609;
    sourcePositiony = -45.88775634765625;
    speed = 98.47861480712891;
    speedVariance = 113.8980255126953;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 20;
    startParticleSizeVariance = 67.68421173095703;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "jbtwlz.png";
}
~~~~~~~~~~~~~
\section Plist228 jinbitubiao.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/jinbitubiao.plist
[ ->] {
    angle = 75.41941070556641;
    angleVariance = 0;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 6;
    maxRadius = 480;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.3381990194320679;
    particleLifespanVariance = 0.7976973652839661;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 50;
    sourcePositionVariancey = 50;
    sourcePositionx = 397.0211181640625;
    sourcePositiony = 62.69387817382812;
    speed = 0;
    speedVariance = 0.7400000095367432;
    startColorAlpha = 1;
    startColorBlue = 0.3487955629825592;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 25.8157901763916;
    startParticleSizeVariance = 25.8157901763916;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "jinbitubiao.png";
}
~~~~~~~~~~~~~
\section Plist229 jizhonglizi.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/jizhonglizi.plist
[ ->] {
    angle = 360;
    angleVariance = 172.8947296142578;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.2000000029802322;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 0.7262369990348816;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 100;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.4000000059604645;
    particleLifespanVariance = 0.5;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 82.24712371826172;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 113.9802627563477;
    rotationStartVariance = 0;
    sourcePositionVariancex = 41.8515625;
    sourcePositionVariancey = 22.21052551269531;
    sourcePositionx = 994.5182495117188;
    sourcePositiony = 822.8571166992188;
    speed = 280;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 0;
    startColorGreen = 0;
    startColorRed = 1;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 233.2894744873047;
    startParticleSizeVariance = 50;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "jizhonglizi.png";
}
~~~~~~~~~~~~~
\section Plist230 levelup_bubble.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/levelup_bubble.plist
[ ->] {
    angle = 90.22203826904297;
    angleVariance = 0;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 29.21052551269531;
    finishParticleSizeVariance = 145.5;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 90;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 5.087376594543457;
    particleLifespanVariance = 2.392578125;
    radialAccelVariance = 25.18503379821777;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 1083.765625;
    sourcePositionVariancey = 100;
    sourcePositionx = 1030.495239257812;
    sourcePositiony = 43.54591751098633;
    speed = 407.9461364746094;
    speedVariance = 274.0028686523438;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 39.18421173095703;
    startParticleSizeVariance = 191.6052703857422;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "levelup_bubble.png";
}
~~~~~~~~~~~~~
\section Plist231 levelup_button.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/levelup_button.plist
[ ->] {
    angle = 360;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0.2000000029802322;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.5;
    finishColorGreen = 0.5;
    finishColorRed = 0.5;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 2;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 25;
    maxRadius = 480;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.2917064130306244;
    particleLifespanVariance = 0.4832319319248199;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 3;
    rotationEndVariance = 8;
    rotationStart = 2;
    rotationStartVariance = 5;
    sourcePositionVariancex = 100;
    sourcePositionVariancey = 20;
    sourcePositionx = 1662.203491210938;
    sourcePositiony = 1096.94384765625;
    speed = 250;
    speedVariance = 0.7400000095367432;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 197.3421020507812;
    startParticleSizeVariance = 53.3684196472168;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "levelup_button.png";
}
~~~~~~~~~~~~~
\section Plist232 pao1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/pao1.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 772;
    blendFuncSource = 1;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.88916015625;
    finishColorGreen = 0.74365234375;
    finishColorRed = 0.1959635466337204;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 2;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 80;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.800000011920929;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 30;
    rotationStartVariance = 0;
    sourcePositionVariancex = 10;
    sourcePositionVariancey = 10;
    sourcePositionx = 768.6448974609375;
    sourcePositiony = 220.4234619140625;
    speed = 25;
    speedVariance = 9;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 0.5447590947151184;
    startColorRed = 0.3260091245174408;
    startColorVarianceAlpha = 0.5389062762260437;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0.6432812213897705;
    startColorVarianceRed = 0.5256249904632568;
    startParticleSize = 5;
    startParticleSizeVariance = 10;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "pao1.png";
}
~~~~~~~~~~~~~
\section Plist233 pao1_fever.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/pao1_fever.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 772;
    blendFuncSource = 1;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.5291340947151184;
    finishColorGreen = 1;
    finishColorRed = 0.6331380009651184;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 2;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 60;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 1;
    particleLifespanVariance = 0.5000000596046448;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 30;
    rotationStartVariance = 0;
    sourcePositionVariancex = 15;
    sourcePositionVariancey = 15;
    sourcePositionx = 1147.900146484375;
    sourcePositiony = 615.35205078125;
    speed = 32;
    speedVariance = 16;
    startColorAlpha = 0.5509440302848816;
    startColorBlue = 1;
    startColorGreen = 0.5960286259651184;
    startColorRed = 0;
    startColorVarianceAlpha = 0.5389062762260437;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 32;
    startParticleSizeVariance = 8;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "pao1_fever.png";
}
~~~~~~~~~~~~~
\section Plist234 pao2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/pao2.plist
[ ->] {
    angle = 70.16000366210938;
    angleVariance = 180.4600067138672;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 0.7099609375;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 26;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.119999997317791;
    particleLifespanVariance = 0.4000000059604645;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 30;
    sourcePositionVariancey = 30;
    sourcePositionx = 1257.443359375;
    sourcePositiony = 828.8571166992188;
    speed = 31.76398086547852;
    speedVariance = 30.99300956726074;
    startColorAlpha = 1;
    startColorBlue = 0.3113606870174408;
    startColorGreen = 0.8302409052848816;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 80.81578922271729;
    startParticleSizeVariance = 32.39473724365234;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "pao2.png";
}
~~~~~~~~~~~~~
\section Plist235 paopaoxiangshang.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/paopaoxiangshang.plist
[ ->] {
    angle = 239.1799926757812;
    angleVariance = 115.8899993896484;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0.1000000014901161;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.9100000262260437;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 1.315789461135864;
    finishParticleSizeVariance = 0;
    gravityx = 20;
    gravityy = 45;
    maxParticles = 53;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.2000000029802322;
    particleLifespanVariance = 1.776315808296204;
    radialAccelVariance = 65;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 70;
    sourcePositionVariancey = -64;
    sourcePositionx = 1162.272583007812;
    sourcePositiony = 1064.770385742188;
    speed = 65;
    speedVariance = 45;
    startColorAlpha = 0.800000011920929;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 120;
    startParticleSizeVariance = 86.89473724365234;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "paopaoxiangshang.png";
}
~~~~~~~~~~~~~
\section Plist236 qianshuitingqipao.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/qianshuitingqipao.plist
[ ->] {
    angle = 162.5049285888672;
    angleVariance = 7.993421077728271;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 1;
    emitterType = 0;
    finishColorAlpha = 0.4085286557674408;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 6;
    finishParticleSizeVariance = 12;
    gravityx = 0;
    gravityy = -32.75246810913086;
    maxParticles = 12;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.7000000476837158;
    particleLifespanVariance = 0.5353000164031982;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 2;
    sourcePositionVariancey = 3;
    sourcePositionx = 391.6161193847656;
    sourcePositiony = 1432.928588867188;
    speed = 186;
    speedVariance = 105.9313354492188;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 27.6842098236084;
    startParticleSizeVariance = 33.60526275634766;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "qianshuitingqipao.png";
}
~~~~~~~~~~~~~
\section Plist237 qiaoya1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/qiaoya1.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.06000001356005669;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -400;
    maxParticles = 273;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 1.132298469543457;
    particleLifespanVariance = 0;
    radialAccelVariance = -100.0731964111328;
    radialAcceleration = -159.6883239746094;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 441.3973083496094;
    sourcePositiony = 1171.622436523438;
    speed = 231.0855255126953;
    speedVariance = 35.2590446472168;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 100.3684234619141;
    startParticleSizeVariance = 7;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "qiaoya1.png";
}
~~~~~~~~~~~~~
\section Plist238 quanpin.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/quanpin.plist
[ ->] {
    angle = 90.22203826904297;
    angleVariance = 0;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 1;
    finishColorVarianceGreen = 1;
    finishColorVarianceRed = 1;
    finishParticleSize = 0;
    finishParticleSizeVariance = 14.60526275634766;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 90;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.2000000029802322;
    particleLifespanVariance = 0.2999999821186066;
    radialAccelVariance = 25.18503379821777;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 1083.765625;
    sourcePositionVariancey = 670.5;
    sourcePositionx = 1010.564270019531;
    sourcePositiony = 785.3724365234375;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 1;
    startColorVarianceGreen = 1;
    startColorVarianceRed = 1;
    startParticleSize = 10;
    startParticleSizeVariance = 31.44736862182617;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "quanpin.png";
}
~~~~~~~~~~~~~
\section Plist239 shan_li_zi.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/shan_li_zi.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 1;
    finishColorVarianceGreen = 1;
    finishColorVarianceRed = 1;
    finishParticleSize = 3;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 65;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.6275699138641357;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 1189.75;
    sourcePositionVariancey = 886.7105102539062;
    sourcePositionx = 1025.965454101562;
    sourcePositiony = 826.7755126953125;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 47.18421173095703;
    startParticleSizeVariance = 15;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "shan_li_zi.png";
}
~~~~~~~~~~~~~
\section Plist240 shoulijian.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/shoulijian.plist
[ ->] {
    angle = 70.16000366210938;
    angleVariance = 180.4600067138672;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.6051432490348816;
    finishColorGreen = 0;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 32;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.2000000029802322;
    particleLifespanVariance = 0.6000000238418579;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 30;
    sourcePositionVariancey = 40;
    sourcePositionx = 1106.871459960938;
    sourcePositiony = 728.7398071289062;
    speed = 31.76398086547852;
    speedVariance = 30.99300956726074;
    startColorAlpha = 1;
    startColorBlue = 0;
    startColorGreen = 0.51708984375;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 80.81578922271729;
    startParticleSizeVariance = 37.44736862182617;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "shoulijian.png";
}
~~~~~~~~~~~~~
\section Plist241 shousuo.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/shousuo.plist
[ ->] {
    angle = 360;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0.1000000014901161;
    emitterType = 1;
    finishColorAlpha = 0.8399999737739563;
    finishColorBlue = 0;
    finishColorGreen = 0;
    finishColorRed = 0;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 13.59000015258789;
    finishParticleSizeVariance = 0;
    gravityx = 1.149999976158142;
    gravityy = 1.580000042915344;
    maxParticles = 809;
    maxRadius = 150;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 1.118399977684021;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 183.1531524658203;
    sourcePositiony = 277.770263671875;
    speed = 243.4199981689453;
    speedVariance = 1;
    startColorAlpha = 1;
    startColorBlue = 0.3600000143051147;
    startColorGreen = 0.5600000023841858;
    startColorRed = 0.8899999856948853;
    startColorVarianceAlpha = 0.5;
    startColorVarianceBlue = 0.2000000029802322;
    startColorVarianceGreen = 0.2000000029802322;
    startColorVarianceRed = 0.2000000029802322;
    startParticleSize = 30;
    startParticleSizeVariance = 10;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "shousuo.png";
}
~~~~~~~~~~~~~
\section Plist242 shuxianlizi01.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/shuxianlizi01.plist
[ ->] {
    angle = 90;
    angleVariance = 10;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 36;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.3993626534938812;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 20;
    sourcePositionVariancey = 116;
    sourcePositionx = 959.0479736328125;
    sourcePositiony = 561.551025390625;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 73.39473724365234;
    startParticleSizeVariance = 78.60526275634766;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "shuxianlizi01.png";
}
~~~~~~~~~~~~~
\section Plist243 shuxianlizi02.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/shuxianlizi02.plist
[ ->] {
    angle = 90;
    angleVariance = 10;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 16;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.3993626534938812;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 20;
    sourcePositionVariancey = 116;
    sourcePositionx = 959.0479736328125;
    sourcePositiony = 561.551025390625;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 6;
    startParticleSizeVariance = 33.76315689086914;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "shuxianlizi02.png";
}
~~~~~~~~~~~~~
\section Plist244 sjlizicaise.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/sjlizicaise.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 0;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0.3473557829856873;
    finishColorVarianceRed = 1;
    finishParticleSize = 30;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 1546.566650390625;
    maxParticles = 16;
    maxRadius = 66.31999969482422;
    maxRadiusVariance = 60;
    minRadius = 41.04999923706055;
    particleLifespan = 0.1099999994039536;
    particleLifespanVariance = 0.1599999964237213;
    radialAccelVariance = -135.4851989746094;
    radialAcceleration = 0;
    rotatePerSecond = 45;
    rotatePerSecondVariance = 125.5299987792969;
    rotationEnd = 360;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 994.9140625;
    sourcePositionVariancey = 1390.9736328125;
    sourcePositionx = 980.376220703125;
    sourcePositiony = 52.8979606628418;
    speed = 126;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 0;
    startColorGreen = 0;
    startColorRed = 0;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0.6721875071525574;
    startColorVarianceRed = 1;
    startParticleSize = 132.1842041015625;
    startParticleSizeVariance = 24.28947448730469;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "sjlizicaise.png";
}
~~~~~~~~~~~~~
\section Plist245 sjlizipaopao.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/sjlizipaopao.plist
[ ->] {
    angle = 0;
    angleVariance = 0;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 0;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0.3473557829856873;
    finishColorVarianceRed = 1;
    finishParticleSize = 197.0263214111328;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 618.6266479492188;
    maxParticles = 18;
    maxRadius = 66.31999969482422;
    maxRadiusVariance = 60;
    minRadius = 41.04999923706055;
    particleLifespan = 1;
    particleLifespanVariance = 0.800000011920929;
    radialAccelVariance = 494.9629821777344;
    radialAcceleration = 0;
    rotatePerSecond = 45;
    rotatePerSecondVariance = 125.5299987792969;
    rotationEnd = 15;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 1494.5546875;
    sourcePositionVariancey = 1390.9736328125;
    sourcePositionx = 980.376220703125;
    sourcePositiony = 52.8979606628418;
    speed = 85.16652679443359;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 0;
    startColorGreen = 0;
    startColorRed = 0;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0.6721875071525574;
    startColorVarianceRed = 1;
    startParticleSize = 286;
    startParticleSizeVariance = 150.4210510253906;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "sjlizipaopao.png";
}
~~~~~~~~~~~~~
\section Plist246 sjlizixingguang.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/sjlizixingguang.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 2;
    maxRadius = 66.31999969482422;
    maxRadiusVariance = 60;
    minRadius = 41.04999923706055;
    particleLifespan = 0.4600000083446503;
    particleLifespanVariance = 0.05000000074505806;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 45;
    rotatePerSecondVariance = 125.5299987792969;
    rotationEnd = 10;
    rotationEndVariance = 4.440789699554443;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 515.59375;
    sourcePositionVariancey = 497.7368469238281;
    sourcePositionx = 1020.468322753906;
    sourcePositiony = 1098.352905273438;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 200;
    startParticleSizeVariance = 0;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "sjlizixingguang.png";
}
~~~~~~~~~~~~~
\section Plist247 sjlizixingxing.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/sjlizixingxing.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.05999999865889549;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.54638671875;
    finishColorGreen = 0.4500325620174408;
    finishColorRed = 0.3094075620174408;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 1;
    finishColorVarianceGreen = 1;
    finishColorVarianceRed = 1;
    finishParticleSize = 165.6842041015625;
    finishParticleSizeVariance = 133.5263214111328;
    gravityx = 0;
    gravityy = -410.7730407714844;
    maxParticles = 22;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.7599999904632568;
    radialAccelVariance = 0;
    radialAcceleration = 44.51069259643555;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 90;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 1021.159301757812;
    sourcePositiony = 1115.8623046875;
    speed = 962.5308227539062;
    speedVariance = 107.9872512817383;
    startColorAlpha = 0.7130534052848816;
    startColorBlue = 0.44384765625;
    startColorGreen = 0.3902994692325592;
    startColorRed = 0.22607421875;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 1;
    startColorVarianceGreen = 1;
    startColorVarianceRed = 1;
    startParticleSize = 66.94736480712891;
    startParticleSizeVariance = 173.5789489746094;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "sjlizixingxing.png";
}
~~~~~~~~~~~~~
\section Plist248 sjliziyuandian.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/sjliziyuandian.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 3;
    maxRadius = 66.31999969482422;
    maxRadiusVariance = 60;
    minRadius = 41.04999923706055;
    particleLifespan = 0.5919243097305298;
    particleLifespanVariance = 0.05000000074505806;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 45;
    rotatePerSecondVariance = 125.5299987792969;
    rotationEnd = 0;
    rotationEndVariance = 4.440789699554443;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 639.109375;
    sourcePositionVariancey = 497.7368469238281;
    sourcePositionx = 1020.468322753906;
    sourcePositiony = 1182.872436523438;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 86.76316070556641;
    startParticleSizeVariance = 0;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "sjliziyuandian.png";
}
~~~~~~~~~~~~~
\section Plist249 slot_coin02.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/slot_coin02.plist
[ ->] {
    angle = 90;
    angleVariance = 21.31999969482422;
    blendFuncDestination = 771;
    blendFuncSource = 1;
    duration = 0.300000011920929;
    emitterType = 0;
    finishColorAlpha = 0.58544921875;
    finishColorBlue = 0.8999999761581421;
    finishColorGreen = 0.8999999761581421;
    finishColorRed = 0.8999999761581421;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 70.60526275634766;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -2097.964599609375;
    maxParticles = 34;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.9498355388641357;
    particleLifespanVariance = 0.6116365194320679;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 10;
    rotationEndVariance = 300;
    rotationStart = 236.8399963378906;
    rotationStartVariance = 0;
    sourcePositionVariancex = 34;
    sourcePositionVariancey = 74;
    sourcePositionx = 1536.598876953125;
    sourcePositiony = 1121.709228515625;
    speed = 1000;
    speedVariance = 420.2816467285156;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 30;
    startParticleSizeVariance = 86.02631378173828;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "slot_coin02.png";
}
~~~~~~~~~~~~~
\section Plist250 slot_paper01.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/slot_paper01.plist
[ ->] {
    angle = 37.91323852539062;
    angleVariance = 0;
    blendFuncDestination = 771;
    blendFuncSource = 1;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 0;
    finishColorRed = 1;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 170.8947296142578;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -400;
    maxParticles = 15;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.9225945472717285;
    particleLifespanVariance = 1.920744299888611;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 938.48681640625;
    rotationEndVariance = -923.3141479492188;
    rotationStart = 236.8399963378906;
    rotationStartVariance = 0;
    sourcePositionVariancex = 1361.875;
    sourcePositionVariancey = 74;
    sourcePositionx = 903.5239868164062;
    sourcePositiony = 1446.826538085938;
    speed = 10.12541103363037;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 0;
    startColorRed = 1;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0.05906249955296516;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 9.552631378173828;
    startParticleSizeVariance = 15.21052646636963;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "slot_paper01.png";
}
~~~~~~~~~~~~~
\section Plist251 slot_paper02.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/slot_paper02.plist
[ ->] {
    angle = 37.91323852539062;
    angleVariance = 0;
    blendFuncDestination = 771;
    blendFuncSource = 1;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 0;
    finishColorRed = 1;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 58.89473724365234;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -400;
    maxParticles = 10;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.5329975485801697;
    particleLifespanVariance = 0.9251644611358643;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 938.48681640625;
    rotationEndVariance = -923;
    rotationStart = 236.8399963378906;
    rotationStartVariance = 0;
    sourcePositionVariancex = 214.375;
    sourcePositionVariancey = 74;
    sourcePositionx = 666.5796508789062;
    sourcePositiony = 1194.642822265625;
    speed = 10.12541103363037;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 0;
    startColorRed = 0.0550130195915699;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0.05906249955296516;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 5;
    startParticleSizeVariance = 15.21052646636963;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "slot_paper02.png";
}
~~~~~~~~~~~~~
\section Plist252 standard_circle.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/standard_circle.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 771;
    blendFuncSource = 770;
    duration = -1;
    emissionRate = 256;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 64;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 1000;
    maxRadius = 0;
    maxRadiusVariance = 0;
    minRadius = 0;
    minRadiusVariance = 0;
    particleLifespan = 0.3;
    particleLifespanVariance = 0;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 0;
    sourcePositiony = 0;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 196;
    startParticleSizeVariance = 0;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "standard_circle.png";
}
~~~~~~~~~~~~~
\section Plist253 star.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/star.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 771;
    blendFuncSource = 1;
    duration = 0.300000011920929;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 0.7433268427848816;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 60;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.5;
    particleLifespanVariance = 0.3199999928474426;
    radialAccelVariance = 0;
    radialAcceleration = -495.2713928222656;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 15;
    sourcePositionVariancey = 15;
    sourcePositionx = 515.1514892578125;
    sourcePositiony = 571.8257446289062;
    speed = 392.9378967285156;
    speedVariance = 14.44284534454346;
    startColorAlpha = 1;
    startColorBlue = 0.6466471552848816;
    startColorGreen = 0.923828125;
    startColorRed = 1;
    startColorVarianceAlpha = 0.5767187476158142;
    startColorVarianceBlue = 0.7674999833106995;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 32.6315803527832;
    startParticleSizeVariance = 102.2631607055664;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "star.png";
}
~~~~~~~~~~~~~
\section Plist254 star1.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/star1.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 771;
    blendFuncSource = 1;
    duration = 0.300000011920929;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 41.71052551269531;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 66;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.5;
    particleLifespanVariance = 0.3199999928474426;
    radialAccelVariance = 0;
    radialAcceleration = -495.2713928222656;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 15;
    sourcePositionVariancey = 15;
    sourcePositionx = 826.6871337890625;
    sourcePositiony = 1192.80615234375;
    speed = 392.9378967285156;
    speedVariance = 14.44284534454346;
    startColorAlpha = 0;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 56.6315803527832;
    startParticleSizeVariance = 102.2631607055664;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "star1.png";
}
~~~~~~~~~~~~~
\section Plist255 star2.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/star2.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = 0.05000000074505806;
    emitterType = 0;
    finishColorAlpha = 0.7062174677848816;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0.02043269202113152;
    finishParticleSize = 129;
    finishParticleSizeVariance = 181.5526275634766;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 48;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.03999999910593033;
    particleLifespanVariance = 0.7427467107772827;
    radialAccelVariance = 0;
    radialAcceleration = -603.1044311523438;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 18;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 1029.312866210938;
    sourcePositiony = 676.5612182617188;
    speed = 858.9124145507812;
    speedVariance = 108.7068252563477;
    startColorAlpha = 0.57666015625;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 139.3947296142578;
    startParticleSizeVariance = 248.2631530761719;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "star2.png";
}
~~~~~~~~~~~~~
\section Plist256 star2_icon.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/star2_icon.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 1.539999961853027;
    gravityy = 435.2853698730469;
    maxParticles = 10;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.09000000357627869;
    particleLifespanVariance = 0.4430509805679321;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 85;
    sourcePositionVariancey = 60;
    sourcePositionx = 1004.08447265625;
    sourcePositiony = 1160.816284179688;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0.58203125;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 15.68421173095703;
    startParticleSizeVariance = 69.42105102539062;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "star2_icon.png";
}
~~~~~~~~~~~~~
\section Plist257 star_fanpai.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/star_fanpai.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 1.539999961853027;
    gravityy = 0;
    maxParticles = 15;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.1400000005960464;
    particleLifespanVariance = 0.4630509912967682;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 85;
    sourcePositionVariancey = 133.3947296142578;
    sourcePositionx = 173.0461730957031;
    sourcePositiony = 277.5945739746094;
    speed = 105.8285369873047;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0.58203125;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 62.68421173095703;
    startParticleSizeVariance = 105.6052627563477;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "star_fanpai.png";
}
~~~~~~~~~~~~~
\section Plist258 star_icon.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/star_icon.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 1.539999961853027;
    gravityy = 435.2853698730469;
    maxParticles = 10;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.09000000357627869;
    particleLifespanVariance = 0.4430509805679321;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 85;
    sourcePositionVariancey = 60;
    sourcePositionx = 1004.08447265625;
    sourcePositiony = 1160.816284179688;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0.58203125;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 15.68421173095703;
    startParticleSizeVariance = 69.42105102539062;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "star_icon.png";
}
~~~~~~~~~~~~~
\section Plist259 submarine_change.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/submarine_change.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 0.5201823115348816;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 10;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.4096422791481018;
    particleLifespanVariance = 0.487911194562912;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 158.859375;
    sourcePositionVariancey = 65;
    sourcePositionx = 504.9827270507812;
    sourcePositiony = 1426.423461914062;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0.005468749906867743;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 64.81578826904297;
    startParticleSizeVariance = 60.42105102539062;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "submarine_change.png";
}
~~~~~~~~~~~~~
\section Plist260 tbblue.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/tbblue.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 772;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.2835286557674408;
    finishColorGreen = 0.70849609375;
    finishColorRed = 0;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 2;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 80;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 1;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 202.4299926757812;
    rotationStartVariance = 0;
    sourcePositionVariancex = 15;
    sourcePositionVariancey = 15;
    sourcePositionx = 235.2399291992188;
    sourcePositiony = 190.5382690429688;
    speed = 40;
    speedVariance = 9;
    startColorAlpha = 1;
    startColorBlue = 0.3904622495174408;
    startColorGreen = 0.6785481572151184;
    startColorRed = 0;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 70;
    startParticleSizeVariance = 50;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "tbblue.png";
}
~~~~~~~~~~~~~
\section Plist261 tbgreen.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/tbgreen.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 772;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 1;
    finishColorRed = 0;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 2;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 80;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 1;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 202.4299926757812;
    rotationStartVariance = 0;
    sourcePositionVariancex = 15;
    sourcePositionVariancey = 15;
    sourcePositionx = 355.9232177734375;
    sourcePositiony = 351.8571472167969;
    speed = 40;
    speedVariance = 9;
    startColorAlpha = 1;
    startColorBlue = 0.0369466133415699;
    startColorGreen = 1;
    startColorRed = 0.1809895783662796;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 70;
    startParticleSizeVariance = 50;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "tbgreen.png";
}
~~~~~~~~~~~~~
\section Plist262 tborange.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/tborange.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 772;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 0.2320963591337204;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 2;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 80;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 1;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 202.4299926757812;
    rotationStartVariance = 0;
    sourcePositionVariancex = 15;
    sourcePositionVariancey = 15;
    sourcePositionx = 460.2533569335938;
    sourcePositiony = 97.30867004394531;
    speed = 40;
    speedVariance = 9;
    startColorAlpha = 1;
    startColorBlue = 0.3904622495174408;
    startColorGreen = 0.6785481572151184;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 70;
    startParticleSizeVariance = 50;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "tborange.png";
}
~~~~~~~~~~~~~
\section Plist263 tbyellow.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/tbyellow.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 772;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 0.7177734375;
    finishColorRed = 0.9375;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 2;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 80;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 1;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 202.4299926757812;
    rotationStartVariance = 0;
    sourcePositionVariancex = 15;
    sourcePositionVariancey = 15;
    sourcePositionx = 475.0787048339844;
    sourcePositiony = 126.5280609130859;
    speed = 40;
    speedVariance = 9;
    startColorAlpha = 1;
    startColorBlue = 0.05615234375;
    startColorGreen = 1;
    startColorRed = 0.7976887822151184;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 70;
    startParticleSizeVariance = 50;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "tbyellow.png";
}
~~~~~~~~~~~~~
\section Plist264 trigertip.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/trigertip.plist
[ ->] {
    angle = 70.16000366210938;
    angleVariance = 180.4600067138672;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 375.9251708984375;
    maxParticles = 100;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.2000000029802322;
    particleLifespanVariance = 0.6000000238418579;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 832;
    sourcePositionVariancey = 109.1052627563477;
    sourcePositionx = 832;
    sourcePositiony = 790.2703857421875;
    speed = 31.76398086547852;
    speedVariance = 30.99300956726074;
    startColorAlpha = 0.7980143427848816;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 10.81578922271729;
    startParticleSizeVariance = 37.44736862182617;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "trigertip.png";
}
~~~~~~~~~~~~~
\section Plist265 tuowei.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/tuowei.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 90;
    maxRadius = 480;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.3381990194320679;
    particleLifespanVariance = 1.19140625;
    radialAccelVariance = 0;
    radialAcceleration = -207.6480255126953;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 100;
    sourcePositionVariancex = 0;
    sourcePositionVariancey = 0;
    sourcePositionx = 987.9002075195312;
    sourcePositiony = 685.9285888671875;
    speed = 188.7849578857422;
    speedVariance = 101.613899230957;
    startColorAlpha = 1;
    startColorBlue = 0;
    startColorGreen = 0.8284505009651184;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 58.47368240356445;
    startParticleSizeVariance = 69.86842346191406;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "tuowei.png";
}
~~~~~~~~~~~~~
\section Plist266 xialuo_li_zi.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/xialuo_li_zi.plist
[ ->] {
    angle = 273;
    angleVariance = 27.1200008392334;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 0.0660807266831398;
    finishColorBlue = 0.9100000262260437;
    finishColorGreen = 1;
    finishColorRed = 0.9100000262260437;
    finishColorVarianceAlpha = 0.239999994635582;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 5.868421077728271;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -150;
    maxParticles = 50;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 1;
    particleLifespanVariance = 2.806332349777222;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 1300.9140625;
    sourcePositionVariancey = 669.4736938476562;
    sourcePositionx = 1029.84375;
    sourcePositiony = 971.0294189453125;
    speed = 95.03495025634766;
    speedVariance = 77.25123596191406;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 10;
    startParticleSizeVariance = 148.1052703857422;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "xialuo_li_zi.png";
}
~~~~~~~~~~~~~
\section Plist267 xiaolizi.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/xiaolizi.plist
[ ->] {
    angle = 90;
    angleVariance = 0;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 0;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 90;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.2373999953269958;
    particleLifespanVariance = 0.5199999809265137;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 276.6640625;
    sourcePositionVariancey = 253.8157958984375;
    sourcePositionx = 994.5182495117188;
    sourcePositiony = 822.8571166992188;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 0;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 6;
    startParticleSizeVariance = 20;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "xiaolizi.png";
}
~~~~~~~~~~~~~
\section Plist268 xingxinglizi.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/xingxinglizi.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 20;
    maxRadius = 66.31999969482422;
    maxRadiusVariance = 60;
    minRadius = 41.04999923706055;
    particleLifespan = 0.5119243264198303;
    particleLifespanVariance = 0.05000000074505806;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 45;
    rotatePerSecondVariance = 125.5299987792969;
    rotationEnd = 0;
    rotationEndVariance = 4.440789699554443;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 639.109375;
    sourcePositionVariancey = 350.2894592285156;
    sourcePositionx = 1003.792724609375;
    sourcePositiony = 640.1173706054688;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 75.28947448730469;
    startParticleSizeVariance = 166.3684234619141;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "xingxinglizi.png";
}
~~~~~~~~~~~~~
\section Plist269 xingxinglizi_loop.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/xingxinglizi_loop.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0.5;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 15;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 40;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.300000011920929;
    particleLifespanVariance = 0.599535346031189;
    radialAccelVariance = 0;
    radialAcceleration = -581.3117065429688;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 10;
    sourcePositionVariancey = 10;
    sourcePositionx = 695.7696533203125;
    sourcePositiony = 1030.530639648438;
    speed = 390.522216796875;
    speedVariance = 55.25288009643555;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 150;
    startParticleSizeVariance = 64.86842346191406;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "xingxinglizi.png";
}
~~~~~~~~~~~~~
\section Plist270 xuehualz.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/xuehualz.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.6748046875;
    finishColorGreen = 0.462890625;
    finishColorRed = 0.4905599057674408;
    finishColorVarianceAlpha = 1;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 30;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.3600000143051147;
    particleLifespanVariance = 0.3600000143051147;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 5;
    sourcePositionVariancey = 5;
    sourcePositionx = 1297.059448242188;
    sourcePositiony = 534.5203857421875;
    speed = 78.43338775634766;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 0.3904622495174408;
    startColorVarianceAlpha = 1;
    startColorVarianceBlue = 0.425000011920929;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 48.78947448730469;
    startParticleSizeVariance = 12.36842060089111;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "xuehualz.png";
}
~~~~~~~~~~~~~
\section Plist271 yanhua.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/yanhua.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.05000000074505806;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.58349609375;
    finishColorGreen = 0.4833984375;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0.02043269202113152;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -107.3190765380859;
    maxParticles = 60;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.8932976722717285;
    radialAccelVariance = 0;
    radialAcceleration = -450;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 90;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 45;
    sourcePositionVariancey = 45;
    sourcePositionx = 638.3262939453125;
    sourcePositiony = 952.7296142578125;
    speed = 450;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 0.4192708432674408;
    startColorGreen = 0.5987955927848816;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 146.4736785888672;
    startParticleSizeVariance = 131.3684234619141;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "yanhua.png";
}
~~~~~~~~~~~~~
\section Plist272 yanhua_huang.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/yanhua_huang.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.1099999994039536;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0;
    finishColorGreen = 0.4833984375;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0.02043269202113152;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -417.865966796875;
    maxParticles = 60;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.8932976722717285;
    radialAccelVariance = 0;
    radialAcceleration = -450;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 90;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 45;
    sourcePositionVariancey = 45;
    sourcePositionx = 1094.2802734375;
    sourcePositiony = 862.1785888671875;
    speed = 450;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 0.0393880195915699;
    startColorGreen = 0.5987955927848816;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 173.1315765380859;
    startParticleSizeVariance = 115.9736862182617;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "yanhua_huang.png";
}
~~~~~~~~~~~~~
\section Plist273 yanhua_l.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/yanhua_l.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.05000000074505806;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.29150390625;
    finishColorGreen = 0.0843098983168602;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0.02043269202113152;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -426.8092041015625;
    maxParticles = 60;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.8932976722717285;
    radialAccelVariance = 0;
    radialAcceleration = -450;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 90;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 45;
    sourcePositionVariancey = 45;
    sourcePositionx = 1115.255249023438;
    sourcePositiony = 820.8826293945312;
    speed = 450;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 0.5901692509651184;
    startColorGreen = 0.7140299677848816;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 117.6052627563477;
    startParticleSizeVariance = 115.684211730957;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "yanhua_l.png";
}
~~~~~~~~~~~~~
\section Plist274 yanhua_lan.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/yanhua_lan.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.05000000074505806;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 0.9070637822151184;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0.02043269202113152;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -469.9835510253906;
    maxParticles = 60;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.8932976722717285;
    radialAccelVariance = 0;
    radialAcceleration = -450;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 90;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 45;
    sourcePositionVariancey = 45;
    sourcePositionx = 1089.827270507812;
    sourcePositiony = 769.698974609375;
    speed = 450;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 0.95947265625;
    startColorRed = 0;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 220;
    startParticleSizeVariance = 131.3684234619141;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "yanhua_lan.png";
}
~~~~~~~~~~~~~
\section Plist275 yanhua_zi.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/yanhua_zi.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 1;
    duration = 0.05000000074505806;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0.02043269202113152;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = -546.1554565429688;
    maxParticles = 60;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0;
    particleLifespanVariance = 0.8932976722717285;
    radialAccelVariance = 0;
    radialAcceleration = -450;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 90;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 45;
    sourcePositionVariancey = 45;
    sourcePositionx = 451.7312927246094;
    sourcePositiony = 874.5612182617188;
    speed = 450;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 211;
    startParticleSizeVariance = 131.3684234619141;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "yanhua_zi.png";
}
~~~~~~~~~~~~~
\section Plist276 yihaopao.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/yihaopao.plist
[ ->] {
    angle = 0;
    angleVariance = 360;
    blendFuncDestination = 1;
    blendFuncSource = 775;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 0;
    finishColorBlue = 1;
    finishColorGreen = 1;
    finishColorRed = 0;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 0;
    finishParticleSizeVariance = 0;
    gravityx = 0;
    gravityy = 0;
    maxParticles = 50;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.05000000074505806;
    particleLifespanVariance = 0.5345394611358643;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 30;
    sourcePositionVariancey = 25;
    sourcePositionx = 476.6065368652344;
    sourcePositiony = 472.7755126953125;
    speed = 26.72697448730469;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 0.2086588591337204;
    startColorGreen = 1;
    startColorRed = 0.412109375;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0.1700000017881393;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 11.60526275634766;
    startParticleSizeVariance = 52.73684310913086;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "yihaopao.png";
}
~~~~~~~~~~~~~
\section Plist277 yuchaoshuipaotexiao.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/particle/yuchaoshuipaotexiao.plist
[ ->] {
    angle = 239.1799926757812;
    angleVariance = 115.8899993896484;
    blendFuncDestination = 1;
    blendFuncSource = 770;
    duration = -1;
    emitterType = 0;
    finishColorAlpha = 1;
    finishColorBlue = 0.9100000262260437;
    finishColorGreen = 1;
    finishColorRed = 1;
    finishColorVarianceAlpha = 0;
    finishColorVarianceBlue = 0;
    finishColorVarianceGreen = 0;
    finishColorVarianceRed = 0;
    finishParticleSize = 1.315789461135864;
    finishParticleSizeVariance = 0;
    gravityx = 7;
    gravityy = 350.3289489746094;
    maxParticles = 136;
    maxRadius = 100;
    maxRadiusVariance = 0;
    minRadius = 0;
    particleLifespan = 0.7170024514198303;
    particleLifespanVariance = 4.67110013961792;
    radialAccelVariance = 0;
    radialAcceleration = 0;
    rotatePerSecond = 0;
    rotatePerSecondVariance = 0;
    rotationEnd = 0;
    rotationEndVariance = 0;
    rotationStart = 0;
    rotationStartVariance = 0;
    sourcePositionVariancex = 863;
    sourcePositionVariancey = -64;
    sourcePositionx = 538.8400268554688;
    sourcePositiony = 29.69000053405762;
    speed = 0;
    speedVariance = 0;
    startColorAlpha = 1;
    startColorBlue = 1;
    startColorGreen = 1;
    startColorRed = 1;
    startColorVarianceAlpha = 0;
    startColorVarianceBlue = 0;
    startColorVarianceGreen = 0;
    startColorVarianceRed = 0;
    startParticleSize = 41.5;
    startParticleSizeVariance = 215.2894744873047;
    tangentialAccelVariance = 0;
    tangentialAcceleration = 0;
    textureFileName = "yuchaoshuipaotexiao.png";
}
~~~~~~~~~~~~~
\section Plist278 ShareSDKLocalizable.strings 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/FF3.app/zh-Hans.lproj/ShareSDKLocalizable.strings
[ ->] {
    "ACCOUNT_VIEW_OK_BUTTON" = "\U786e\U5b9a";
    "ACCOUNT_VIEW_TIPS_SECTION_TITLE" = "\U6388\U6743\U5931\U6548\Uff0c\U8bf7\U91cd\U65b0\U6388\U6743";
    "ALTER_VIEW_I_KNOWN_BUTTON" = "\U77e5\U9053\U4e86";
    "ALTER_VIEW_TITLE" = "\U63d0\U793a";
    "AUTH_VIEW_CANCEL_BUTTON" = "\U53d6\U6d88";
    "AUTH_VIEW_FOLLOW_ME" = "\U5173\U6ce8\U6211\U4eec\U7684\U5fae\U535a";
    "AUTH_VIEW_TITLE_1" = "\U65b0\U6d6a\U5fae\U535a";
    "AUTH_VIEW_TITLE_10" = Facebook;
    "AUTH_VIEW_TITLE_11" = Twitter;
    "AUTH_VIEW_TITLE_12" = "\U5370\U8c61\U7b14\U8bb0";
    "AUTH_VIEW_TITLE_14" = "Google+";
    "AUTH_VIEW_TITLE_15" = Instagram;
    "AUTH_VIEW_TITLE_16" = LinkedIn;
    "AUTH_VIEW_TITLE_17" = Tumblr;
    "AUTH_VIEW_TITLE_2" = "\U817e\U8baf\U5fae\U535a";
    "AUTH_VIEW_TITLE_22" = WeChat;
    "AUTH_VIEW_TITLE_25" = Instapaper;
    "AUTH_VIEW_TITLE_26" = Pocket;
    "AUTH_VIEW_TITLE_27" = "\U6709\U9053\U4e91\U7b14\U8bb0";
    "AUTH_VIEW_TITLE_28" = "\U641c\U72d0\U968f\U8eab\U770b";
    "AUTH_VIEW_TITLE_3" = "\U641c\U72d0\U5fae\U535a";
    "AUTH_VIEW_TITLE_34" = Flickr;
    "AUTH_VIEW_TITLE_35" = Dropbox;
    "AUTH_VIEW_TITLE_36" = VKontakte;
    "AUTH_VIEW_TITLE_4" = "\U7f51\U6613\U5fae\U535a";
    "AUTH_VIEW_TITLE_41" = "\U660e\U9053";
    "AUTH_VIEW_TITLE_5" = "\U8c46\U74e3";
    "AUTH_VIEW_TITLE_6" = "QQ\U7a7a\U95f4";
    "AUTH_VIEW_TITLE_7" = "\U4eba\U4eba\U7f51";
    "AUTH_VIEW_TITLE_8" = "\U5f00\U5fc3\U7f51";
    "AWARD_BACK_ITEM_TITLE" = "\U5173\U95ed";
    "ERROR_CODE_LINE_INVALID_UIPASTBOARD" = "\U65e0\U6548UIPasteboard\U5bf9\U8c61";
    "ERROR_DESC_API_NOT_SUPPORT" = "\U4e0d\U652f\U6301\U6b64\U529f\U80fd";
    "ERROR_DESC_DROPBOX_UPLOAD_FILE_FAIL" = "\U4e0a\U4f20\U6587\U4ef6\U5931\U8d25";
    "ERROR_DESC_FACEBOOK_CANCEL_ADD_FRIEND" = "\U53d6\U6d88\U6dfb\U52a0\U597d\U53cb";
    "ERROR_DESC_GOOGLE_PLUS_SEND_FAIL" = "\U5206\U4eab\U5931\U8d25!";
    "ERROR_DESC_INSTAGRAM_NOT_INSTALL" = "\U5c1a\U672a\U5b89\U88c5Instagram\U5ba2\U6237\U7aef";
    "ERROR_DESC_INSTAGRAM_SEND_FAIL" = "\U53d1\U9001Instagram\U5185\U5bb9\U5931\U8d25";
    "ERROR_DESC_INVALID_PARAM" = "\U4f20\U5165\U53c2\U6570\U65e0\U6548";
    "ERROR_DESC_INVALID_PLATFORM" = "\U65e0\U6548\U7684\U5e73\U53f0\U6216\U8005\U5c1a\U672a\U521d\U59cb\U5316\U8be5\U5e73\U53f0!";
    "ERROR_DESC_KAKAOSTORY_NOT_INSTALL" = "\U5c1a\U672a\U5b89\U88c5KakaoStory\U5ba2\U6237\U7aef";
    "ERROR_DESC_KAKAOSTORY_SEND_FAIL" = "\U5206\U4eabKakaoStory\U5185\U5bb9\U5931\U8d25";
    "ERROR_DESC_KAKAOTALK_NOT_INSTALL" = "\U5c1a\U672a\U5b89\U88c5KakaoTalk\U5ba2\U6237\U7aef";
    "ERROR_DESC_KAKAOTALK_SEND_FAIL" = "\U5206\U4eabKakaoTalk\U5185\U5bb9\U5931\U8d25";
    "ERROR_DESC_LINE_NOT_INSTALL" = "\U5c1a\U672a\U5b89\U88c5Line\U5ba2\U6237\U7aef";
    "ERROR_DESC_LINE_NOT_SUPPORTED_FOR_IOS7_PLUS" = "Line\U5e73\U53f0\U6682\U65f6\U4e0d\U652f\U6301\U5728iOS7\U4ee5\U4e0a\U5206\U4eab\U56fe\U7247";
    "ERROR_DESC_LINE_SEND_FAIL" = "\U5206\U4eabLine\U5185\U5bb9\U5931\U8d25";
    "ERROR_DESC_MAIL_NOT_SUPPORT" = "\U8be5\U8bbe\U5907\U4e0d\U652f\U6301\U90ae\U4ef6\U5206\U4eab\U6216\U5c1a\U672a\U8bbe\U7f6e\U90ae\U4ef6\U5e10\U53f7";
    "ERROR_DESC_NOT_CONNECT" = "\U5c1a\U672a\U96c6\U6210\U8be5\U5e73\U53f0!";
    "ERROR_DESC_NO_NETWORK" = "\U8bbe\U5907\U5c1a\U672a\U8fde\U63a5\U7f51\U7edc!";
    "ERROR_DESC_PINTEREST_NOT_INSTALL" = "\U5c1a\U672a\U5b89\U88c5Pinterest";
    "ERROR_DESC_PINTEREST_NOT_INTEGRATED" = "\U5c1a\U672a\U96c6\U6210Pinterest";
    "ERROR_DESC_PINTEREST_NOT_SET_URL_SCHEME" = "\U5c1a\U672a\U914d\U7f6ePinterest URL Scheme";
    "ERROR_DESC_PRINT_NOT_SUPPORT" = "\U8be5\U8bbe\U5907\U4e0d\U5141\U8bb8\U8fdb\U884c\U6253\U5370";
    "ERROR_DESC_QQ_API_IS_NOT_SUPPORT" = "\U5f53\U524dQQ\U7248\U672c\U4e0d\U652f\U6301\U8be5\U529f\U80fd";
    "ERROR_DESC_QQ_NOT_INSTALLED" = "\U5c1a\U672a\U5b89\U88c5QQ";
    "ERROR_DESC_QQ_NOT_INTEGRATED" = "\U5c1a\U672a\U96c6\U6210QQ\U63a5\U53e3";
    "ERROR_DESC_QQ_NOT_SET_URL_SCHEME" = "\U5c1a\U672a\U8bbe\U7f6eQQ\U7684URL Scheme";
    "ERROR_DESC_QQ_SEND_FAIL" = "\U53d1\U9001\U5931\U8d25";
    "ERROR_DESC_QQ_UNKNOWN_MEDIA_TYPE" = "\U672a\U77e5\U7684\U6d88\U606f\U53d1\U9001\U7c7b\U578b";
    "ERROR_DESC_SINA_WEIBO_IS_NOT_SUPPORT" = "\U5f53\U524d\U65b0\U6d6a\U5fae\U535a\U5ba2\U6237\U7aef\U7248\U672c\U4e0d\U652f\U6301\U8be5\U529f\U80fd";
    "ERROR_DESC_SINA_WEIBO_NOT_INSTALL" = "\U5c1a\U672a\U5b89\U88c5\U65b0\U6d6a\U5fae\U535a\U5ba2\U6237\U7aef";
    "ERROR_DESC_SINA_WEIBO_NOT_INTEGRATED" = "\U5c1a\U672a\U96c6\U6210\U65b0\U6d6a\U5fae\U535a\U5206\U4eab\U63a5\U53e3";
    "ERROR_DESC_SINA_WEIBO_SEND_FAIL" = "\U53d1\U9001\U5931\U8d25";
    "ERROR_DESC_SMS_NOT_SUPPORT" = "\U8be5\U8bbe\U5907\U4e0d\U652f\U6301\U77ed\U4fe1\U5206\U4eab";
    "ERROR_DESC_SYS_VER_LOW_THAN" = "\U8be5\U529f\U80fd\U4e0d\U652f\U6301iOS %@\U4ee5\U4e0b\U7cfb\U7edf\U7248\U672c";
    "ERROR_DESC_TWITTER_GET_FRIENDS_API_DEPRECATED" = "\U6b64\U65b9\U6cd5\U5df2\U8fc7\U65f6\Uff0c\U8bf7\U4f7f\U7528getFriendsWithPage\U65b9\U6cd5\U4ee3\U66ff";
    "ERROR_DESC_UNAUTH" = "\U5c1a\U672a\U6388\U6743";
    "ERROR_DESC_UNKNOWN" = "\U672a\U77e5\U9519\U8bef";
    "ERROR_DESC_UNREGISTER" = "\U5c1a\U672a\U521d\U59cb\U5316SDK";
    "ERROR_DESC_WEIXIN_API_IS_NOT_SUPPORT" = "\U5f53\U524d\U5fae\U4fe1\U7248\U672c\U4e0d\U652f\U6301\U8be5\U529f\U80fd";
    "ERROR_DESC_WEIXIN_NOT_INSTALL" = "\U5c1a\U672a\U5b89\U88c5\U5fae\U4fe1";
    "ERROR_DESC_WEIXIN_NOT_INTEGRATED" = "\U5c1a\U672a\U96c6\U6210\U5fae\U4fe1\U63a5\U53e3";
    "ERROR_DESC_WEIXIN_NOT_SET_URL_SCHEME" = "\U5c1a\U672a\U8bbe\U7f6e\U5fae\U4fe1\U7684URL Scheme";
    "ERROR_DESC_WEIXIN_SEND_REQUEST_ERROR" = "\U8bf7\U6c42\U5fae\U4fe1OpenApi\U5931\U8d25";
    "ERROR_DESC_WEIXIN_UNKNOWN_MEDIA_TYPE" = "\U672a\U77e5\U7684\U6d88\U606f\U53d1\U9001\U7c7b\U578b";
    "ERROR_DESC_WHATSAPP_NOT_INSTALL" = "\U5c1a\U672a\U5b89\U88c5WhatsApp\U5ba2\U6237\U7aef";
    "ERROR_DESC_WHATSAPP_SEND_FAIL" = "\U5206\U4eabWhatsApp\U5185\U5bb9\U5931\U8d25";
    "ERROR_DESC_YIXIN_API_IS_NOT_SUPPORT" = "\U5f53\U524d\U6613\U4fe1\U7248\U672c\U4e0d\U652f\U6301\U8be5\U529f\U80fd";
    "ERROR_DESC_YIXIN_NOT_INSTALL" = "\U5c1a\U672a\U5b89\U88c5\U6613\U4fe1";
    "ERROR_DESC_YIXIN_NOT_INTEGRATED" = "\U5c1a\U672a\U96c6\U6210\U6613\U4fe1\U63a5\U53e3";
    "ERROR_DESC_YIXIN_NOT_SET_URL_SCHEME" = "\U5c1a\U672a\U8bbe\U7f6e\U6613\U4fe1\U7684URL Scheme";
    "ERROR_DESC_YIXIN_SEND_REQUEST_ERROR" = "\U8bf7\U6c42\U6613\U4fe1OpenApi\U5931\U8d25";
    "ERROR_DESC_YIXIN_UNKNOWN_MEDIA_TYPE" = "\U672a\U77e5\U7684\U6d88\U606f\U53d1\U9001\U7c7b\U578b";
    "FRIENDS_VIEW_CANCEL_BUTTON" = "\U53d6\U6d88";
    "FRIENDS_VIEW_RECENT_SECTION_TITLE" = "\U6700\U8fd1\U8054\U7cfb\U4eba";
    "FRIENDS_VIEW_SEARCHBAR_PLACEHOLDER" = "\U5728\U8054\U7cfb\U4eba\U4e2d\U641c\U7d22...";
    "FRIENDS_VIEW_SEARCH_RESULT_SECTION_TITLE" = "\U641c\U7d22\U7ed3\U679c";
    "FRIENDS_VIEW_TITLE" = "\U8054\U7cfb\U4eba";
    "MSG_DEVICE_ALBUM_NOT_SUPPORT" = "\U8be5\U8bbe\U5907\U4e0d\U652f\U6301\U76f8\U518c\U529f\U80fd";
    "MSG_DEVICE_CAMERA_NOT_SUPPORT" = "\U8be5\U8bbe\U5907\U4e0d\U652f\U6301\U62cd\U7167\U529f\U80fd";
    "MSG_SENDING" = "\U53d1\U9001\U4e2d...";
    "MSG_SHARE_FAIL" = "%@\U5206\U4eab\U5931\U8d25!%@";
    "MSG_SHARE_FAIL_2" = "\U5206\U4eab\U5931\U8d25!";
    "MSG_SHARE_SUCCESS" = "%@\U5206\U4eab\U6210\U529f!";
    "PRINT_JOB_NAME" = "\U5185\U5bb9\U5206\U4eab";
    "QQ_MSG_SEND_FAIL" = "\U53d1\U9001QQ\U6d88\U606f\U5931\U8d25:%@";
    "QQ_MSG_SEND_SUCCESS" = "\U53d1\U9001QQ\U6d88\U606f\U6210\U529f";
    "SHARE_ACTION_SHEET_CANCEL_BUTTON" = "\U53d6\U6d88";
    "SHARE_VIEW_ACTIONSHEET_CANCEL_ITEM" = "\U53d6\U6d88";
    "SHARE_VIEW_ACTIONSHEET_CLEAR_CONTENT_ITEM" = "\U6e05\U7a7a\U5185\U5bb9";
    "SHARE_VIEW_ACTIONSHEET_OK_ITEM" = "\U786e\U5b9a";
    "SHARE_VIEW_ACTIONSHEET_REMOVE_PHOTO_ITEM" = "\U5220\U9664\U7167\U7247";
    "SHARE_VIEW_ACTIONSHEET_SELECT_PHOTO_ITEM" = "\U9009\U53d6\U7167\U7247...";
    "SHARE_VIEW_ACTIONSHEET_TAKE_PHOTO_ITEM" = "\U62cd\U7167...";
    "SHARE_VIEW_ACTIONSHEET_VIEW_PHOTO_ITEM" = "\U89c2\U770b\U7167\U7247";
    "SHARE_VIEW_AUTH_TITLE" = "\U7ed1\U5b9a%@";
    "SHARE_VIEW_CANCEL_BUTTON" = "\U53d6\U6d88";
    "SHARE_VIEW_DEF_PIC_PUBLISH_CONTENT" = "\U56fe\U7247\U5206\U4eab";
    "SHARE_VIEW_MSG_INVALID_CLIENTS" = "\U8bf7\U9009\U62e9\U5206\U4eab\U7684\U76ee\U6807\U5e73\U53f0";
    "SHARE_VIEW_MSG_INVALID_CONTENT" = "\U8bf7\U8f93\U5165\U5206\U4eab\U5185\U5bb9";
    "SHARE_VIEW_ONE_KEY_LIST_TITLE" = "\U5206\U4eab\U5230";
    "SHARE_VIEW_PUBLISH_BUTTON" = "\U53d1\U8868";
    "SHARE_VIEW_TITLE" = "\U5185\U5bb9\U5206\U4eab";
    "SHARE_VIEW_TOPIC" = "\U8bdd\U9898";
    "ShareType_1" = "\U65b0\U6d6a\U5fae\U535a";
    "ShareType_10" = Facebook;
    "ShareType_11" = Twitter;
    "ShareType_12" = "\U5370\U8c61\U7b14\U8bb0";
    "ShareType_13" = Foursquare;
    "ShareType_14" = "Google+";
    "ShareType_15" = Instagram;
    "ShareType_16" = LinkedIn;
    "ShareType_17" = Tumblr;
    "ShareType_18" = "\U90ae\U4ef6";
    "ShareType_19" = "\U77ed\U4fe1";
    "ShareType_2" = "\U817e\U8baf\U5fae\U535a";
    "ShareType_20" = "\U6253\U5370";
    "ShareType_21" = "\U62f7\U8d1d";
    "ShareType_22" = "\U5fae\U4fe1\U597d\U53cb";
    "ShareType_23" = "\U5fae\U4fe1\U670b\U53cb\U5708";
    "ShareType_24" = QQ;
    "ShareType_25" = Instapaper;
    "ShareType_26" = Pocket;
    "ShareType_27" = "\U6709\U9053\U4e91\U7b14\U8bb0";
    "ShareType_28" = "\U641c\U72d0\U968f\U8eab\U770b";
    "ShareType_3" = "\U641c\U72d0\U5fae\U535a";
    "ShareType_30" = Pinterest;
    "ShareType_34" = Flickr;
    "ShareType_35" = Dropbox;
    "ShareType_36" = VKontakte;
    "ShareType_37" = "\U5fae\U4fe1\U6536\U85cf";
    "ShareType_38" = "\U6613\U4fe1\U597d\U53cb";
    "ShareType_39" = "\U6613\U4fe1\U670b\U53cb\U5708";
    "ShareType_4" = "\U7f51\U6613\U5fae\U535a";
    "ShareType_41" = "\U660e\U9053";
    "ShareType_42" = Line;
    "ShareType_43" = WhatsApp;
    "ShareType_44" = KakaoTalk;
    "ShareType_45" = KakaoStory;
    "ShareType_5" = "\U8c46\U74e3";
    "ShareType_6" = "QQ\U7a7a\U95f4";
    "ShareType_7" = "\U4eba\U4eba\U7f51";
    "ShareType_8" = "\U5f00\U5fc3\U7f51";
    "ShareType_9" = "\U670b\U53cb\U7f51";
    "WEIXIN_MSG_SEND_FAIL" = "\U53d1\U9001%@\U5931\U8d25:%@";
    "WEIXIN_MSG_SEND_SUCCESS" = "\U53d1\U9001%@\U6210\U529f!";
    "WEIXIN_SESSION_NAME" = "\U5fae\U4fe1\U597d\U53cb";
    "WEIXIN_TIMELINE_NAME" = "\U5fae\U4fe1\U670b\U53cb\U5708";
}
~~~~~~~~~~~~~
\section Plist288 .talkingdata_cpa 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/Library/Caches/.talkingdata_cpa
[ ->] {
    "_events" =     (
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421549922750&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421553205645&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421553856733&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421569206291&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Launch;
            urlString = "http://a.appcpa.net/d?tp=_launch&ts=1421569206527&lastDuration=926&interval=18356&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421569206527&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421554895298&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421569384657&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Launch;
            urlString = "http://a.appcpa.net/d?tp=_launch&ts=1421569385248&lastDuration=-18357&interval=18535&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421569385248&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421583785011&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421511784317&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421598184483&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421612583793&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421626983731&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421641385685&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421655783944&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421670184455&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421684586310&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421698983751&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421713383885&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421727784196&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421742183803&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421756583806&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421770983065&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421785383638&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421799783717&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421814184020&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421828583916&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421842983902&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421857383915&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421871786254&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421886183676&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421900943912&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421915344111&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421929744084&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421944144174&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421958544020&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421972943570&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421987343997&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422001743940&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421555853130&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421570224296&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422003123358&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422017523431&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422031923497&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422046323925&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422060723809&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422075123111&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422089523782&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422103924148&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422118324136&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422132664009&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422147064330&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422161463251&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422175864067&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422190264266&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422204664595&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422219064258&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422233464165&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422247864386&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422262264668&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422276664680&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422291064102&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422305464211&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422319863661&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422334264064&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422348664608&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422363064744&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422377465229&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422377482459&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Launch;
            urlString = "http://a.appcpa.net/d?tp=_launch&ts=1422377482895&lastDuration=-18535&interval=826633&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422377482895&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422392224135&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422406624132&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422421023983&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422435423973&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422449824576&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422464223820&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422478624083&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422493024390&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422507423886&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422521823892&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422536226924&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422550624221&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422565024083&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422579424017&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422593823974&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422608224609&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422622624008&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422637024236&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422651424243&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422665824538&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422680224025&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422694624166&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422709024131&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422723426249&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Launch;
            urlString = "http://a.appcpa.net/d?tp=_launch&ts=1422723426564&lastDuration=-826633&interval=1172576&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422723426564&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421573339846&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Launch;
            urlString = "http://a.appcpa.net/d?tp=_launch&ts=1421573340113&lastDuration=-1172577&interval=22490&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421573340113&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422851344116&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422865744130&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422880143724&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422894543933&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422908942269&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422923343777&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422937744483&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422952143687&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422966543896&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422980943610&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1422995344078&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423009744092&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423024143639&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423038543779&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423052944053&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423067350701&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423081743142&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423096144019&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423110544434&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423124943803&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423139344002&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423153744321&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423168144276&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423182544133&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423196943662&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423211343042&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423225743984&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423240150237&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423254544115&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423268944036&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423283343789&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423297744230&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423312143910&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1423326543138&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=46000&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        },
                {
            eventName = Activiate;
            urlString = "http://a.appcpa.net/s?tp=active&it=1421484929000&me=30365065216&s=1e3c47dab97d4ce1b3e9b5b0fd2e99e8&ch=500026&at=1421573910299&v=2.1.0.16&pt=2&os=7.1.2&mr=Apple&dt=iPhone3,1&o=\U4e2d\U56fd\U79fb\U52a8&nt=GPRS&mn=&mc=02:00:00:00:00:00&did=\U201c\U4e1d\U74dc&\U51ac\U74dc\U201d\U7684 iPhone&tdid=DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE&tdudid=1|2199023255552_2199023255552_2199023255552|ed36a9d7364d469c2cb57305a50abbd792406a9a|D6DF663A-E025-41F1-B6A9-783998034F07|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE|";
        }
    );
    "_isInitialized" = 0;
    "_sessionEndTime" = 1421550849612;
    "_sessionStartTime" = 1421573340314;
}
~~~~~~~~~~~~~
\section Plist289 .talkingdata_udid 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/Library/Caches/.talkingdata_udid
[ ->] {
    TalkingDataUDID = "DA60D8A2-0E81-432B-A8D3-E6C03CB55DAE";
}
~~~~~~~~~~~~~
\section Plist290 com.punchbox.fishingjoy3.plist 
~~~~~~~~~~~~~{.xml}
FILE: /tmp/iNalyzer5_9ec45fef/Payload/ClientFiles/Library/Preferences/com.punchbox.fishingjoy3.plist
[ ->] {
    CurrentUser = fishingjoy3;
    "FAAEBB11E09623FC48C25818DE37D397_history" =     (
                {
            eventId = "coco_start";
            timestamp = 1421549922413;
        },
                {
            eventId = "coco_start";
            timestamp = 1421569206119;
        },
                {
            eventId = "coco_start";
            timestamp = 1421569384526;
        },
                {
            eventId = "coco_start";
            timestamp = 1422377482277;
        },
                {
            eventId = "coco_start";
            timestamp = 1422723426047;
        },
                {
            eventId = "coco_start";
            timestamp = 1421573339609;
        }
    );
    HavePlayedGame = 1;
    OpenUDID =     {
        OpenUDID = ed36a9d7364d469c2cb57305a50abbd792406a9a;
        "OpenUDID_appUID" = "12430DB4-B4C6-45FE-9BC0-ACCB5E9315C0";
        "OpenUDID_createdTS" = 2015-01-18 02:58:41 +0000;
        "OpenUDID_slot" = "org.OpenUDID.slot.0";
    };
    WebDatabaseDirectory = "/var/mobile/Applications/4725D0E3-FCEC-4F95-932A-A436AC9CA8E1/Library/Caches";
    WebKitDiskImageCacheSavedCacheDirectory = "";
    WebKitLocalStorageDatabasePathPreferenceKey = "/var/mobile/Applications/4725D0E3-FCEC-4F95-932A-A436AC9CA8E1/Library/Caches";
    WebKitOfflineWebApplicationCacheEnabled = 1;
    WebKitShrinksStandaloneImagesToFit = 1;
    "_iat_download__" = 1;
    "_iat_map__" =     {
        download = 1;
    };
    badgeValue = 2;
    "com.inmobi._iat_list__" = <62706c69 73743030 d4010203 04050826 27542474 6f705824 6f626a65 63747358 24766572 73696f6e 59246172 63686976 6572d106 0754726f 6f748001 a7090a10 18191a21 55246e75 6c6cd20b 0c0d0e56 24636c61 73735a4e 532e6f62 6a656374 738006a1 0f8002d4 11120b13 14151615 5f10105f 6961745f 676f616c 5f6e616d 655f5f5f 10105f69 61745f77 6169745f 74696d65 5f5f5f10 125f6961 745f7265 7472795f 636f756e 745f5f80 03800480 05800458 646f776e 6c6f6164 1000d21b 1c1d2058 24636c61 73736573 5a24636c 6173736e 616d65a2 1e1f5a49 4d476f61 6c446174 61584e53 4f626a65 63745a49 4d476f61 6c446174 61d21b1c 2225a323 241f5e4e 534d7574 61626c65 41727261 79574e53 41727261 795e4e53 4d757461 626c6541 72726179 12000186 a05f100f 4e534b65 79656441 72636869 76657200 08001100 16001f00 28003200 35003a00 3c004400 4a004f00 56006100 63006500 67007000 83009600 ab00ad00 af00b100 b300bc00 be00c300 cc00d700 da00e500 ee00f900 fe010201 11011901 28012d00 00000000 00020100 00000000 00002800 00000000 00000000 00000000 00013f>;
    "com.inmobi.app.webview.ua" = "Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Mobile/11D257";
    "com.inmobi.metricManager.configs.API" = <62706c69 73743030 d4010203 0405081f 20542474 6f705824 6f626a65 63747358 24766572 73696f6e 59246172 63686976 6572d106 0754726f 6f748001 a6090a15 16171855 246e756c 6cd50b0c 0d0e0f10 11121314 5e6d6574 7269634d 61784974 656d735f 10116d65 74726963 4e657874 52657175 65737459 6d657472 69635552 4c5f1013 6d657472 69634475 6d705468 72657368 6f6c6456 24636c61 73738003 80048000 80028005 100a1103 e823411a 5e000000 0000d219 1a1b1e58 24636c61 73736573 5a24636c 6173736e 616d65a2 1c1d5f10 0f494d4d 65747269 63436f6e 66696773 584e534f 626a6563 745f100f 494d4d65 74726963 436f6e66 69677312 000186a0 5f100f4e 534b6579 65644172 63686976 65720008 00110016 001f0028 00320035 003a003c 00430049 00540063 00770081 0097009e 00a000a2 00a400a6 00a800aa 00ad00b6 00bb00c4 00cf00d2 00e400ed 00ff0104 00000000 00000201 00000000 00000021 00000000 00000000 00000000 00000116>;
    "com.inmobi.metricManager.timestamp.API" = 2015-01-18 02:58:42 +0000;
    "inmobi.sdkversion" = "4.5.1";
    lastOfflineBonusTime = 18693120998736;
    "ltvp_ft_day" = 735636;
    "ltvp_ft_ever" = 0;
    "ltvp_ft_month" = 24170;
    "ltvp_ft_user" = 1;
    "ltvp_ft_week" = 105091;
    sM = 7ea8fe4c4127a53ceca3e93eb042bcd6;
    timeNewMoreGame = 1421573910.550022;
}
~~~~~~~~~~~~~
\n
