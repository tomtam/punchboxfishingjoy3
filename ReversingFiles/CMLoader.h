/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "CMEventDispatcher.h"
#import "NSURLConnectionDelegate.h"
#import "NSURLConnectionDataDelegate.h"

@class NSMutableData, NSData, NSURLResponse, NSString, NSURLConnection;

@interface CMLoader : CMEventDispatcher <NSURLConnectionDelegate, NSURLConnectionDataDelegate> {
	int _state;
	int _sourceType;
	NSURLConnection* _conn;
	NSMutableData* _receiveData;
	NSURLResponse* _response;
	NSString* _key;
	int _contentLength;
}
@property(readonly, assign, nonatomic) NSString* key;	// @synthesize=_key
@property(readonly, assign, nonatomic) NSData* data;	// @synthesize=_receiveData
@property(readonly, assign, nonatomic) int sourceType;	// @synthesize=_sourceType
@property(readonly, assign, nonatomic) int state;	// @synthesize=_state
 -(id)key;
 -(id)data;
 -(int)state;
 -(int)sourceType;
-(void)connectionDidFinishLoading:(id)connection;
-(void)connection:(id)connection didReceiveResponse:(id)response;
-(void)connection:(id)connection didReceiveData:(id)data;
-(void)connection:(id)connection didFailWithError:(id)error;
-(void)releaseRequestObject;
-(void)loadByCache:(id)cache;
-(void)loadByPath:(id)path;
-(void)loadByUrl:(id)url;
-(void)dealloc;
-(id)initWithKey:(id)key;
@end

