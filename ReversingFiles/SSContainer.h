/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "ISSContainer.h"
#import </libobjc.A.h>
#import "捕鱼达人3-Structs.h"

@class UIViewController, SSiPadViewController;

@interface SSContainer : NSObject <ISSContainer> {
	UIViewController* _iPhoneViewController;
	SSiPadViewController* _iPadViewController;
}
@property(retain, nonatomic) SSiPadViewController* iPadViewController;	// @synthesize=_iPadViewController
@property(retain, nonatomic) UIViewController* iPhoneViewController;	// @synthesize=_iPhoneViewController
 -(void)setIPadViewController:(id)controller;
 -(id)iPadViewController;
 -(void)setIPhoneViewController:(id)controller;
-(void)setIPadContainerWithBarButtonItem:(id)barButtonItem arrowDirect:(unsigned)direct;
-(void)setIPadContainerWithView:(id)view rect:(CGRect)rect arrowDirect:(unsigned)direct;
-(void)setIPadContainerWithView:(id)view arrowDirect:(unsigned)direct;
-(void)setIPhoneContainerWithViewController:(id)viewController;
 -(id)iPhoneViewController;
-(void)dealloc;
@end

