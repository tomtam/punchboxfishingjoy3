/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>
#import "UIWebViewDelegate.h"
#import "UIAlertViewDelegate.h"

@class UIWebView, UIToolbar, UIBarButtonItem, NSString, UIView;


@interface UIWebLoader : NSObject <UIWebViewDelegate, UIAlertViewDelegate> {
	UIView* myView;
	UIWebView* myWebView;
	UIToolbar* myToolBar;
	UIBarButtonItem* myBackButton;
}
@property(readonly, copy) NSString* debugDescription;
@property(readonly, copy) NSString* description;
@property(readonly, assign) Class superclass;
@property(readonly, assign) unsigned hash;
-(void)webView:(id)view didFailLoadWithError:(id)error;
-(void)webViewDidFinishLoad:(id)webView;
-(void)webViewDidStartLoad:(id)webView;
-(void)backClicked:(id)clicked;
-(void)loadWebSiteOnUIWebView:(const char*)view;
-(void)dealloc;
-(id)init;
@end

