/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>


@interface MMApiRegister : NSObject {
}
+(BOOL)registerApp:(id)app withDescription:(id)description;
+(BOOL)registerApp:(id)app;
+(id)buildAppDictionary:(id)dictionary oldAppDictionary:(id)dictionary2;
+(id)getIconString;
+(id)getRegisterDictionary:(id)dictionary;
+(id)getRegisterPasteboardName;
@end

