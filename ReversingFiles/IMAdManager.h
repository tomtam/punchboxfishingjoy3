/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "IMAdScreenDismissDelegate.h"
#import "IMWebViewProcessDelegate.h"
#import "IMMediaManagerDelegate.h"
#import "SKStoreProductViewControllerDelegate.h"
#import "MFMailComposeViewControllerDelegate.h"
#import "MFMessageComposeViewControllerDelegate.h"
#import "UIImagePickerControllerDelegate.h"
#import "IMAsncyPingDelegate.h"
#import "EKEventEditViewDelegate.h"
#import "捕鱼达人3-Structs.h"
#import "IMActivityViewScreenDelegate.h"
#import "IMVideoCacheManagerDelegate.h"
#import </libobjc.A.h>
#import "UINavigationControllerDelegate.h"

@class IMActivityViewController, AVAudioRecorder, UIView, IMAsyncPingRequest, NSString, NSTimer, IMAdCloseButton, UIImage, IMVideoCacheManager, IMOrientationProperties, IMResizeProperties, IMExpandProperties, IMEmbeddedWebViewController, IMSKStoreProductViewController, IMMediaManager;
@protocol IMAdManagerDelegate;

@interface IMAdManager : NSObject <IMAdScreenDismissDelegate, IMWebViewProcessDelegate, IMMediaManagerDelegate, SKStoreProductViewControllerDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, IMAsncyPingDelegate, EKEventEditViewDelegate, IMActivityViewScreenDelegate, IMVideoCacheManagerDelegate> {
	id<IMAdManagerDelegate> adManagerDelegate;
	UIView* adView;
	IMAdCloseButton* graphicsAdCloseButton;
	IMAdCloseButton* customAdCloseButton;
	IMExpandProperties* expProperties;
	IMResizeProperties* resizeProps;
	IMOrientationProperties* orientationProps;
	int currentState;
	int previousState;
	float initialVolume;
	UIImage* tempImage;
	BOOL isExpandedWithURL;
	IMEmbeddedWebViewController* imEmbedWebViewController;
	int adOrientation;
	int skStoreState;
	BOOL isInterstitial;
	BOOL _disableHotspot;
	IMMediaManager* mediaManager;
	int lastOrientationAngle;
	NSTimer* micTimer;
	AVAudioRecorder* recorder;
	IMAsyncPingRequest* storePictureRequest;
	IMSKStoreProductViewController* skStoreController;
	IMVideoCacheManager* _videoCacheMgr;
	IMActivityViewController* _activityController;
	NSString* _clickUrl;
	double _skStoreStartTimeInterval;
	double _skStoreEndTimeInterval;
}
@property(readonly, copy) NSString* debugDescription;
@property(readonly, copy) NSString* description;
@property(readonly, assign) Class superclass;
@property(readonly, assign) unsigned hash;
@property(assign) BOOL disableHotspot;	// @synthesize=_disableHotspot
@property(assign) double skStoreEndTimeInterval;	// @synthesize=_skStoreEndTimeInterval
@property(assign) double skStoreStartTimeInterval;	// @synthesize=_skStoreStartTimeInterval
@property(retain, nonatomic) NSString* clickUrl;	// @synthesize=_clickUrl
@property(retain, nonatomic) IMActivityViewController* activityController;	// @synthesize=_activityController
@property(retain, nonatomic) IMSKStoreProductViewController* skStoreController;	// @synthesize
@property(retain, nonatomic) IMVideoCacheManager* videoCacheMgr;	// @synthesize=_videoCacheMgr
@property(retain, nonatomic) IMAsyncPingRequest* storePictureRequest;	// @synthesize
@property(retain, nonatomic) AVAudioRecorder* recorder;	// @synthesize
@property(retain, nonatomic) NSTimer* micTimer;	// @synthesize
@property(retain, nonatomic) IMEmbeddedWebViewController* imEmbedWebViewController;	// @synthesize
@property(assign, nonatomic) int lastOrientationAngle;	// @synthesize
@property(retain, nonatomic) UIImage* tempImage;	// @synthesize
@property(assign, nonatomic) float initialVolume;	// @synthesize
@property(assign, nonatomic) int skStoreState;	// @synthesize
@property(assign, nonatomic) int currentState;	// @synthesize
@property(assign, nonatomic) int previousState;	// @synthesize
@property(retain, nonatomic) IMOrientationProperties* orientationProps;	// @synthesize
@property(retain, nonatomic) IMResizeProperties* resizeProps;	// @synthesize
@property(retain, nonatomic) IMExpandProperties* expProperties;	// @synthesize
@property(retain, nonatomic) IMAdCloseButton* customAdCloseButton;	// @synthesize
@property(retain, nonatomic) IMAdCloseButton* graphicsAdCloseButton;	// @synthesize
@property(retain, nonatomic) IMMediaManager* mediaManager;	// @synthesize
@property(assign, nonatomic) BOOL isInterstitial;	// @synthesize
@property(retain, nonatomic) id<IMAdManagerDelegate> adManagerDelegate;	// @synthesize
@property(assign, nonatomic) __weak UIView* adView;	// @synthesize
 -(void)setDisableHotspot:(BOOL)hotspot;
 -(BOOL)disableHotspot;
 -(void)setSkStoreEndTimeInterval:(double)interval;
 -(double)skStoreEndTimeInterval;
 -(void)setSkStoreStartTimeInterval:(double)interval;
 -(double)skStoreStartTimeInterval;
 -(void)setClickUrl:(id)url;
 -(id)clickUrl;
 -(void)setActivityController:(id)controller;
 -(id)activityController;
 -(void)setVideoCacheMgr:(id)mgr;
 -(id)videoCacheMgr;
 -(void)setSkStoreController:(id)controller;
 -(id)skStoreController;
 -(void)setStorePictureRequest:(id)request;
 -(id)storePictureRequest;
 -(void)setRecorder:(id)recorder;
 -(id)recorder;
 -(void)setMicTimer:(id)timer;
 -(id)micTimer;
 -(void)setTempImage:(id)image;
 -(id)tempImage;
 -(void)setImEmbedWebViewController:(id)controller;
 -(id)imEmbedWebViewController;
 -(void)setSkStoreState:(int)state;
 -(int)skStoreState;
 -(void)setLastOrientationAngle:(int)angle;
 -(int)lastOrientationAngle;
 -(void)setInitialVolume:(float)volume;
 -(float)initialVolume;
 -(void)setCurrentState:(int)state;
 -(int)currentState;
 -(void)setPreviousState:(int)state;
 -(int)previousState;
 -(void)setOrientationProps:(id)props;
 -(id)orientationProps;
 -(void)setResizeProps:(id)props;
 -(id)resizeProps;
 -(void)setExpProperties:(id)properties;
 -(id)expProperties;
 -(void)setGraphicsAdCloseButton:(id)button;
 -(id)graphicsAdCloseButton;
 -(void)setCustomAdCloseButton:(id)button;
 -(id)customAdCloseButton;
 -(void)setMediaManager:(id)manager;
 -(id)mediaManager;
 -(void)setIsInterstitial:(BOOL)interstitial;
 -(BOOL)isInterstitial;
 -(void)setAdManagerDelegate:(id)delegate;
 -(id)adManagerDelegate;
 -(void)setAdView:(id)view;
 -(id)adView;
-(void).cxx_destruct;
-(CGRect)webFrame;
-(void)dismissViewController:(id)controller animated:(BOOL)animated;
-(void)dismissViewController:(id)controller animated:(BOOL)animated completionBlock:(id)block;
-(void)presentViewController:(id)controller from:(id)from;
-(void)presentViewController:(id)controller from:(id)from animated:(BOOL)animated;
-(void)pauseAllMedia;
-(void)resetAllMedia;
-(void)reset;
-(void)adScreenDidDismiss:(id)adScreen;
-(void)adScreenWillDismiss:(id)adScreen;
-(void)adWillLeaveApplication:(id)ad;
-(void)controller:(id)controller didInteractWithParams:(id)params;
-(void)adWillLeaveApplication;
-(int)statusBarSize:(CGSize)size accordingToOrientation:(int)orientation;
-(void)sendBeaconPingToUrl:(id)url;
-(void)openMap:(id)map withUrlString:(id)urlString andFullScreen:(BOOL)screen;
-(void)messageComposeViewController:(id)controller didFinishWithResult:(int)result;
-(void)mailComposeController:(id)controller didFinishWithResult:(int)result error:(id)error;
-(void)eventEditViewController:(id)controller didCompleteWithAction:(int)action;
-(void)collectSKStoreEvent;
-(void)productViewControllerDidFinish:(id)productViewController;
-(void)activityScreenDidDismiss:(id)activityScreen;
-(void)presentSKStore:(id)store;
-(void)addToSKStore:(id)skstore;
-(void)updateToPassbook:(id)passbook;
-(void)sendPassErrorMsg;
-(void)checkMessageURLAndProcessExternal:(id)external;
-(BOOL)openExternal:(id)external;
-(void)openBrowser:(id)browser withUrlString:(id)urlString;
-(void)addCloseButtonToWebView;
-(void)setUseCustomClose:(BOOL)close;
-(void)placeCallTo:(id)to;
-(BOOL)sendSMSTo:(id)to withBody:(id)body;
-(BOOL)sendEMailTo:(id)to withSubject:(id)subject withBody:(id)body isHTML:(BOOL)html;
-(void)createCalendarEvent:(id)event;
-(id)alarmWithParams:(id)params;
-(BOOL)addReminderTo:(id)to fromParams:(id)params withEvent:(id)event withStore:(id)store;
-(void)launchIMEventEditController:(id)controller;
-(id)dateFromW3CString:(id)w3CString;
-(id)arrayWithNumArray:(id)numArray withMin:(int)min withMax:(int)max;
-(id)daysOfTheMonthFromArray:(id)array;
-(id)weeksOfTheYear:(id)theYear;
-(id)daysOfTheYear:(id)theYear;
-(id)monthsOfTheYear:(id)theYear;
-(id)daysOfTheWeekFromArray:(id)array;
-(id)reminderWithParams:(id)params withStore:(id)store andStartDate:(id)date;
-(id)dateComponentsFromDate:(id)date;
-(id)recurrenceRuleWithParams:(id)params;
-(id)calendarEventWithParams:(id)params store:(id)store;
-(void)setExpandProperties;
-(void)setOrientatinProperties;
-(int)interfaceOrientationForAdProperties;
-(void)adjustCustomCloseForExpandedAdInOrientation:(int)orientation superview:(id)superview;
-(void)addEventToCalenderForDate:(id)date withTitle:(id)title withBody:(id)body;
-(void)rotateWebView:(id)view toOrientation:(int)orientation;
-(BOOL)shouldRotateToInterfaceOrientation:(int)interfaceOrientation;
-(void)rotateExpandedWindowsToCurrentOrientation;
-(CGPoint)adjustFrame:(CGRect)frame toFitInto:(CGRect)fitInto;
-(void)resizeWebView:(id)view forParent:(id)parent;
-(void)resizeInWebView:(id)webView;
-(void)expandWithURL:(id)url inWebView:(id)webView;
-(void)closeAd;
-(void)closeAd:(id)ad;
-(void)openEmbeddedBrowser:(id)browser;
-(void)showAlertWithMsg:(id)msg;
-(void)loadSKStoreContent:(id)content;
-(void)postToSocialType:(int)socialType text:(id)text url:(id)url imageURL:(id)url4 inWebView:(id)webView;
-(void)imagePickerControllerDidCancel:(id)imagePickerController;
-(void)imagePickerController:(id)controller didFinishPickingMediaWithInfo:(id)info;
-(id)writeData:(id)data toCacheByFileName:(id)cacheByFileName;
-(void)alertView:(id)view didDismissWithButtonIndex:(int)buttonIndex;
-(void)image:(id)image didFinishSavingWithError:(id)error contextInfo:(void*)info;
-(void)listenToMicIntensity:(BOOL)micIntensity inWebView:(id)webView;
-(void)micIntensityChange;
-(void)asyncPingRequest:(id)request didFailWithError:(id)error;
-(void)asyncPingRequestDidFinish:(id)asyncPingRequest;
-(BOOL)canProcessIMAICommand:(id)command params:(id)params forWebView:(id)webView;
-(void)sendSMSWithParams:(id)params;
-(void)sendEMailWithParams:(id)params;
-(BOOL)canProcessIMIMraidCommand:(id)command params:(id)params forWebView:(id)webView;
-(void)fireSKStoreOpened;
-(BOOL)canProcessIMMraidCommand:(id)command params:(id)params forWebView:(id)webView;
-(id)checkForTelAndReturnTelprompt:(id)telAndReturnTelprompt;
-(void)reportCommand:(int)command;
-(void)fireMraidLoad:(id)load andShow:(BOOL)show;
-(void)fireMraidLoad:(id)load forState:(int)state andShow:(BOOL)show;
-(void)fireMraidShow:(id)show;
-(void)fireMraidHide:(id)hide;
-(void)fireCurrentPositionChangeEvent:(id)event;
-(void)fireDefaultPositionChangeEvent:(id)event;
-(CGRect)getCurrentPositionOfUIView:(id)uiview;
-(void)deviceOrientationChanged:(id)changed;
-(void)interfaceOrientationChanged:(id)changed;
-(CGRect)getMaxAdFrameInCurrentStatusBarOrientation;
-(CGRect)getAvailableWindowFrame;
-(CGRect)getAvailableWindowFrame:(int)frame;
-(void)removeFrameObserverForWebView:(id)webView;
-(void)addFrameObserverForWebView:(id)webView;
-(id)webView;
-(id)mediaViewController;
-(id)mediaWebView;
-(id)topmostView;
-(id)imRootViewController;
-(id)imWebView;
-(void)appActiveNotificationReceived:(id)received;
-(void)appInActiveNotificationReceived:(id)activeNotificationReceived;
-(void)dealloc;
-(id)init;
@end

