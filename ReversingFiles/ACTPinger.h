/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>
#import "NSURLConnectionDelegate.h"

@class NSMutableData, NSURLResponse, NSURLRequest, NSURLConnection;
@protocol ACTPingerDelegate;


@interface ACTPinger : NSObject <NSURLConnectionDelegate> {
	id<ACTPingerDelegate> _delegate;
	NSURLRequest* _request;
	NSURLConnection* _connection;
	NSURLResponse* _response;
	NSMutableData* _receivedData;
}
@property(retain, nonatomic) NSMutableData* receivedData;	// @synthesize=_receivedData
@property(retain, nonatomic) NSURLResponse* response;	// @synthesize=_response
@property(retain, nonatomic) NSURLConnection* connection;	// @synthesize=_connection
@property(copy, nonatomic) NSURLRequest* request;	// @synthesize=_request
@property(retain, nonatomic) id<ACTPingerDelegate> delegate;	// @synthesize=_delegate
 -(void)setReceivedData:(id)data;
 -(id)receivedData;
 -(void)setResponse:(id)response;
 -(id)response;
 -(id)connection;
 -(void)setRequest:(id)request;
 -(id)request;
 -(void)setDelegate:(id)delegate;
 -(id)delegate;
-(void).cxx_destruct;
-(void)connection:(id)connection didFailWithError:(id)error;
-(void)connectionDidFinishLoading:(id)connection;
-(void)connection:(id)connection didReceiveData:(id)data;
-(void)connection:(id)connection didReceiveResponse:(id)response;
-(void)dealloc;
-(void)ping;
 -(void)setConnection:(id)connection;
-(id)initWithRequest:(id)request;
@end

