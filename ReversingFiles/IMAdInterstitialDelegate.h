/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "NSObject.h"


@protocol IMAdInterstitialDelegate <NSObject>
@optional
-(void)interstitial:(id)interstitial didPerformUserInteractionWithParams:(id)params;
-(void)interstitialWillLeaveApplication:(id)interstitial;
-(void)interstitialDidDismissScreen:(id)interstitial;
-(void)interstitialWillDismissScreen:(id)interstitial;
-(void)interstitial:(id)interstitial didFailToPresentScreenWithError:(id)error;
-(void)interstitialWillPresentScreen:(id)interstitial;
-(void)interstitial:(id)interstitial didFailToReceiveAdWithError:(id)error;
-(void)interstitialDidFinishRequest:(id)interstitial;
@end

