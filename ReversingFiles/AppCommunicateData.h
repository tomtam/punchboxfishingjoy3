/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>

@class NSString, NSMutableDictionary, NSData;

@interface AppCommunicateData : NSObject {
	unsigned _command;
	NSMutableDictionary* _dictionaryData;
	NSData* _fileData;
	BOOL _returnFromApp;
	NSString* _conversationAccount;
	int _result;
	int _scene;
	NSString* _openID;
	NSString* _sdkVer;
	NSString* _lang;
	NSString* _country;
}
@property(retain, nonatomic) NSString* country;	// @synthesize=_country
@property(retain, nonatomic) NSString* lang;	// @synthesize=_lang
@property(retain, nonatomic) NSString* sdkVer;	// @synthesize=_sdkVer
@property(retain, nonatomic) NSString* openID;	// @synthesize=_openID
@property(assign, nonatomic) int scene;	// @synthesize=_scene
@property(retain, nonatomic) NSData* fileData;	// @synthesize=_fileData
@property(retain, nonatomic) NSString* conversationAccount;	// @synthesize=_conversationAccount
@property(assign, nonatomic) BOOL returnFromApp;	// @synthesize=_returnFromApp
@property(assign, nonatomic) int result;	// @synthesize=_result
 -(void)setCountry:(id)country;
 -(void)setLang:(id)lang;
 -(void)setSdkVer:(id)ver;
 -(void)setOpenID:(id)anId;
 -(void)setScene:(int)scene;
 -(int)scene;
 -(void)setResult:(int)result;
 -(int)result;
 -(void)setReturnFromApp:(BOOL)app;
 -(BOOL)returnFromApp;
 -(void)setConversationAccount:(id)account;
 -(id)conversationAccount;
 -(void)setFileData:(id)data;
 -(id)fileData;
-(BOOL)RespToData:(id)data;
-(BOOL)ReqToData:(id)data withMediaInternalMessage:(id)mediaInternalMessage;
-(BOOL)ReqToData:(id)data;
-(id)DataToResp;
-(id)DataToReq;
-(BOOL)MakeMediaInternalMessage:(id)message;
-(id)mediaInternalMessage;
-(BOOL)MakeMediaMessage:(id)message;
-(BOOL)MakeLinkObject:(id)object;
-(id)mediaMessage;
-(BOOL)MakeTextMessage:(id)message;
-(id)textMessage;
-(BOOL)MakeAuthResp:(id)resp;
-(id)authResp;
-(BOOL)MakeAuthRequest:(id)request;
-(id)authRequest;
-(BOOL)MakeCommand:(unsigned)command;
-(void)initCommonField:(unsigned)field;
 -(id)country;
 -(id)lang;
 -(id)sdkVer;
 -(id)openID;
-(unsigned)command;
-(id)propertList;
-(id)initWithPropertList:(id)propertList;
-(void)dealloc;
-(id)init;
@end

