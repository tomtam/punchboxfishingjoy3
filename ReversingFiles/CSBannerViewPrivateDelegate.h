/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "NSObject.h"


@protocol CSBannerViewPrivateDelegate <NSObject>
-(void)csBannerView:(id)view clickAd:(id)ad;
-(void)csBannerView:(id)view sendLogForClickWithParams:(id)params with:(unsigned)with;
-(void)csBannerView:(id)view sendLogForShowSuccessWith:(unsigned)with;
-(void)csBannerView:(id)view sendLogForLoadError:(id)loadError;
-(void)csBannerViewRequestBanner:(id)banner;
@end

