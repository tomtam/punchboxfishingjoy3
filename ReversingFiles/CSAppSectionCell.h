/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "捕鱼达人3-Structs.h"
#import "CSSingleAppViewDelegate.h"
#import <UIKit/UITableViewCell.h>
#import "UIScrollViewDelegate.h"

@class NSString, UIScrollView, NSMutableArray, UIImageView, CSSectionItem, UILabel, UIButton;
@protocol CSAppSectionCellDelegate;

@interface CSAppSectionCell : UITableViewCell <CSSingleAppViewDelegate, UIScrollViewDelegate> {
	UILabel* _labelName;
	UIButton* _btnMore;
	UIScrollView* _scrollView;
	UIImageView* _imageViewMore;
	NSMutableArray* _marrAppView;
	BOOL _isiPad;
	float _yOffset;
	CSSectionItem* _sectionItem;
	id<CSAppSectionCellDelegate> _delegate;
}
@property(readonly, copy) NSString* debugDescription;
@property(readonly, copy) NSString* description;
@property(readonly, assign) Class superclass;
@property(readonly, assign) unsigned hash;
@property(readonly, assign, nonatomic) BOOL isiPad;	// @synthesize=_isiPad
@property(assign, nonatomic) id<CSAppSectionCellDelegate> delegate;	// @synthesize=_delegate
@property(retain, nonatomic) CSSectionItem* sectionItem;	// @synthesize=_sectionItem
@property(assign, nonatomic) float yOffset;	// @synthesize=_yOffset
 -(BOOL)isiPad;
 -(void)setDelegate:(id)delegate;
 -(id)delegate;
 -(void)setSectionItem:(id)item;
 -(id)sectionItem;
 -(void)setYOffset:(float)offset;
 -(float)yOffset;
-(id)createAppViewWithIndex:(unsigned)index withAppInfoItem:(id)appInfoItem;
-(void)scrollViewDidEndDecelerating:(id)scrollView;
-(void)scrollViewDidEndDragging:(id)scrollView willDecelerate:(BOOL)decelerate;
-(void)scrollViewDidScroll:(id)scrollView;
-(void)csSingleAppView:(id)view clickPriceButton:(id)button;
-(void)csSingleAppView:(id)view clickIcon:(id)icon;
-(void)clickMoreButton;
-(void)observeValueForKeyPath:(id)keyPath ofObject:(id)object change:(id)change context:(void*)context;
-(void)dealloc;
-(void)layoutSubviews;
-(void)setSelected:(BOOL)selected animated:(BOOL)animated;
-(id)initWithStyle:(int)style reuseIdentifier:(id)identifier;
@end

