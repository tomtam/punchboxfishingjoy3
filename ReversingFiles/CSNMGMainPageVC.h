/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import <UIKit/UIViewController.h>
#import "UIScrollViewDelegate.h"
#import "UITableViewDelegate.h"
#import "UITableViewDataSource.h"
#import "CSAppSectionCellDelegate.h"
#import "CSFocusViewDelegate.h"
#import "CSMainTabViewDelegate.h"
#import "CSAppItemCellDelegate.h"
#import "捕鱼达人3-Structs.h"

@class UIScrollView, NSMutableArray, NSArray, CSFocusView, NSString, CSLoadingView, CSMainTabView;
@protocol CSNMGMainPageVCDataSource, CSNMGMainPageVCDelegate;

@interface CSNMGMainPageVC : UIViewController <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, CSAppSectionCellDelegate, CSFocusViewDelegate, CSMainTabViewDelegate, CSAppItemCellDelegate> {
	CSMainTabView* _tabMainView;
	UIScrollView* _scrollView;
	CSFocusView* _focusView;
	CSLoadingView* _loadingView;
	NSMutableArray* _marrTableItem;
	BOOL _switchMenuAnimation;
	NSArray* _menuItems;
	id<CSNMGMainPageVCDataSource> _dataSource;
	id<CSNMGMainPageVCDelegate> _delegate;
}
@property(readonly, copy) NSString* debugDescription;
@property(readonly, copy) NSString* description;
@property(readonly, assign) Class superclass;
@property(readonly, assign) unsigned hash;
@property(assign, nonatomic) BOOL switchMenuAnimation;	// @synthesize=_switchMenuAnimation
@property(assign, nonatomic) id<CSNMGMainPageVCDelegate> delegate;	// @synthesize=_delegate
@property(assign, nonatomic) id<CSNMGMainPageVCDataSource> dataSource;	// @synthesize=_dataSource
@property(retain, nonatomic) NSArray* menuItems;	// @synthesize=_menuItems
 -(void)setSwitchMenuAnimation:(BOOL)animation;
 -(BOOL)switchMenuAnimation;
 -(void)setDelegate:(id)delegate;
 -(id)delegate;
 -(void)setDataSource:(id)source;
 -(id)dataSource;
 -(void)setMenuItems:(id)items;
 -(id)menuItems;
-(void)layoutViews:(CGSize)views;
-(void)csAppItemCellClickDownloadApp:(id)app;
-(void)csFocusView:(id)view clickImage:(id)image;
-(void)csAppSectionCell:(id)cell clickPriceButton:(id)button;
-(void)csAppSectionCell:(id)cell clickIcon:(id)icon;
-(void)csAppSectionCell:(id)cell clickMore:(id)more;
-(void)tableView:(id)view didSelectRowAtIndexPath:(id)indexPath;
-(float)tableView:(id)view heightForRowAtIndexPath:(id)indexPath;
-(id)tableView:(id)view cellForRowAtIndexPath:(id)indexPath;
-(int)tableView:(id)view numberOfRowsInSection:(int)section;
-(void)scrollViewDidEndDecelerating:(id)scrollView;
-(void)scrollViewDidEndDragging:(id)scrollView willDecelerate:(BOOL)decelerate;
-(void)csMainTabView:(id)view selectMenuItem:(id)item;
-(void)clickReload;
-(void)clickClose;
-(void)loadNewDataError:(id)error ofMenu:(id)menu isEnd:(BOOL)end;
-(void)newItems:(id)items ofMenu:(id)menu isFirstPage:(BOOL)page;
-(void)requestMainPageDataError:(id)error;
-(void)showAnimationForRequestMainPageData;
-(void)observeValueForKeyPath:(id)keyPath ofObject:(id)object change:(id)change context:(void*)context;
-(void)dealloc;
-(void)didReceiveMemoryWarning;
-(void)willAnimateRotationToInterfaceOrientation:(int)interfaceOrientation duration:(double)duration;
-(void)viewWillAppear:(BOOL)view;
-(void)viewDidLoad;
-(id)initWithNibName:(id)nibName bundle:(id)bundle;
@end

