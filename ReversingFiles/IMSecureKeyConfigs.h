/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "IMBaseConfigs.h"

@class NSString;

@interface IMSecureKeyConfigs : IMBaseConfigs {
	NSString* _keyVersion;
	NSString* _algorithm;
	NSString* _modulus;
	NSString* _exponent;
	NSString* _certificate;
}
@property(retain, nonatomic) NSString* certificate;	// @synthesize=_certificate
@property(retain, nonatomic) NSString* exponent;	// @synthesize=_exponent
@property(retain, nonatomic) NSString* modulus;	// @synthesize=_modulus
@property(retain, nonatomic) NSString* algorithm;	// @synthesize=_algorithm
@property(retain, nonatomic) NSString* keyVersion;	// @synthesize=_keyVersion
+(id)sharedConfigs;
 -(void)setCertificate:(id)certificate;
 -(id)certificate;
 -(void)setExponent:(id)exponent;
 -(id)exponent;
 -(void)setModulus:(id)modulus;
 -(id)modulus;
 -(void)setAlgorithm:(id)algorithm;
 -(id)algorithm;
 -(void)setKeyVersion:(id)version;
 -(id)keyVersion;
-(void).cxx_destruct;
-(BOOL)updateFromDictionary:(id)dictionary needValidation:(BOOL)validation;
-(id)product;
-(id)init;
@end

