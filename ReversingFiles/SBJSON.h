/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "SBJsonParserOptions.h"
#import "SBJsonWriterOptions.h"
#import "SBJsonBase.h"

@class SBJsonParser, SBJsonWriter;


@interface SBJSON : SBJsonBase <SBJsonParserOptions, SBJsonWriterOptions> {
	SBJsonParser* jsonParser;
	SBJsonWriter* jsonWriter;
}
@property(assign) BOOL sortKeys;
@property(assign) BOOL humanReadable;
@property(assign) unsigned maxDepth;
 -(void)setSortKeys:(BOOL)keys;
 -(BOOL)sortKeys;
 -(void)setHumanReadable:(BOOL)readable;
 -(BOOL)humanReadable;
 -(void)setMaxDepth:(unsigned)depth;
 -(unsigned)maxDepth;
-(id)objectWithString:(id)string error:(id*)error;
-(id)fragmentWithString:(id)string error:(id*)error;
-(id)objectWithString:(id)string allowScalar:(BOOL)scalar error:(id*)error;
-(id)objectWithString:(id)string;
-(id)stringWithObject:(id)object error:(id*)error;
-(id)stringWithFragment:(id)fragment error:(id*)error;
-(id)stringWithObject:(id)object allowScalar:(BOOL)scalar error:(id*)error;
-(void)dealloc;
-(id)init;
@end

