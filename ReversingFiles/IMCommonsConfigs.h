/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "IMBaseConfigs.h"

@class NSString;

@interface IMCommonsConfigs : IMBaseConfigs {
	int _configDefaultLogLevel;
	NSString* _rootConfigURL;
	int _apiMetricSampleFactor;
}
@property(assign, nonatomic) int apiMetricSampleFactor;	// @synthesize=_apiMetricSampleFactor
@property(retain, nonatomic) NSString* rootConfigURL;	// @synthesize=_rootConfigURL
@property(assign, nonatomic) int configDefaultLogLevel;	// @synthesize=_configDefaultLogLevel
+(id)sharedConfigs;
 -(void)setApiMetricSampleFactor:(int)factor;
 -(int)apiMetricSampleFactor;
 -(void)setRootConfigURL:(id)url;
 -(id)rootConfigURL;
 -(void)setConfigDefaultLogLevel:(int)level;
 -(int)configDefaultLogLevel;
-(void).cxx_destruct;
-(BOOL)updateFromDictionary:(id)dictionary needValidation:(BOOL)validation;
-(id)product;
-(id)init;
@end

