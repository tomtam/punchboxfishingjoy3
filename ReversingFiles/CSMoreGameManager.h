/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "CSMoreGameNetDelegate.h"
#import "CSMoreGameViewDelegate.h"
#import "CSMoreGamePrivateDataSource.h"
#import </libobjc.A.h>
#import "CSMoreGamePrivateDelegate.h"

@class NSMutableSet, CSMoreGameView, NSMutableDictionary, NSString, CSMoreGameNet, NSMutableArray;

@interface CSMoreGameManager : NSObject <CSMoreGameViewDelegate, CSMoreGamePrivateDataSource, CSMoreGamePrivateDelegate, CSMoreGameNetDelegate> {
	CSMoreGameNet* _moreGameNet;
	NSMutableSet* _msetMoreGame;
	NSMutableArray* _marrRequestFailureLog;
	NSMutableDictionary* _mdicNoNetRequestFailureLog;
	NSMutableArray* _marrLoadFailureLog;
	NSMutableArray* _marrLoadSuccessLog;
	NSMutableArray* _marrShowSuccessLog;
	NSMutableArray* _marrShowFailureLog;
	NSMutableArray* _marrLoadingWhenShowLog;
	NSMutableArray* _marrClickLog;
	BOOL _batchLogSending;
	NSString* _baseURLLog;
	NSString* _adOtherParams;
	CSMoreGameView* _moreGameView;
	int _requestInterval;
	NSString* _placementID;
	unsigned _errorCode;
	double _timeStartRequest;
	double _timeShowMoreGame;
	double _timeLoadSuccess;
}
@property(readonly, copy) NSString* debugDescription;
@property(readonly, copy) NSString* description;
@property(readonly, assign) Class superclass;
@property(readonly, assign) unsigned hash;
@property(assign, nonatomic) unsigned errorCode;	// @synthesize=_errorCode
@property(copy, nonatomic) NSString* placementID;	// @synthesize=_placementID
@property(assign, nonatomic) int requestInterval;	// @synthesize=_requestInterval
@property(assign, nonatomic) BOOL batchLogSending;	// @synthesize=_batchLogSending
@property(assign, nonatomic) double timeLoadSuccess;	// @synthesize=_timeLoadSuccess
@property(assign, nonatomic) double timeShowMoreGame;	// @synthesize=_timeShowMoreGame
@property(assign, nonatomic) double timeStartRequest;	// @synthesize=_timeStartRequest
@property(retain, nonatomic) CSMoreGameView* moreGameView;	// @synthesize=_moreGameView
@property(copy, nonatomic) NSString* adOtherParams;	// @synthesize=_adOtherParams
@property(copy, nonatomic) NSString* baseURLLog;	// @synthesize=_baseURLLog
+(id)sharedMoreGameManager;
 -(void)setErrorCode:(unsigned)code;
 -(unsigned)errorCode;
 -(void)setPlacementID:(id)anId;
 -(id)placementID;
 -(void)setRequestInterval:(int)interval;
 -(int)requestInterval;
 -(void)setBatchLogSending:(BOOL)sending;
 -(BOOL)batchLogSending;
 -(void)setTimeLoadSuccess:(double)success;
 -(double)timeLoadSuccess;
 -(void)setTimeShowMoreGame:(double)game;
 -(double)timeShowMoreGame;
 -(void)setTimeStartRequest:(double)request;
 -(double)timeStartRequest;
 -(void)setMoreGameView:(id)view;
 -(id)moreGameView;
 -(void)setAdOtherParams:(id)params;
 -(id)adOtherParams;
 -(void)setBaseURLLog:(id)log;
 -(id)baseURLLog;
-(void)sendLogForShowFailure:(id)showFailure;
-(void)sendLogForShowSuccess:(id)showSuccess;
-(BOOL)isMoreGameSuccess:(id)success;
-(BOOL)requestMoreGameData:(id)data;
-(void)csMoreGameNet:(id)net batchSendMoreGameFailureLogSuccess:(id)success and:(id)anAnd;
-(void)csMoreGameNet:(id)net batchSendMoreGameFailureLogFailure:(id)failure and:(id)anAnd;
-(void)csMoreGameNet:(id)net sendMoreGameShowErrorLogSuccess:(id)success with:(id)with error:(id)error;
-(void)csMoreGameNet:(id)net sendMoreGameShowErrorLogFailureWith:(id)with and:(id)anAnd;
-(void)csMoreGameNet:(id)net sendMoreGameLoadSuccessLogSuccess:(id)success with:(id)with andDuration:(unsigned)duration;
-(void)csMoreGameNet:(id)net sendMoreGameLoadSuccessLogFailureWith:(id)with andDuration:(unsigned)duration;
-(void)csMoreGameNet:(id)net sendMoreGameShowSuccessLogSuccess:(id)success with:(id)with;
-(void)csMoreGameNet:(id)net sendMoreGameShowSuccessLogFailureWith:(id)with;
-(void)csMoreGameNet:(id)net sendMoreGameClickLogSuccess:(id)success with:(id)with duration:(unsigned)duration;
-(void)csMoreGameNet:(id)net sendMoreGameClickLogFailureWith:(id)with;
-(void)csMoreGameNet:(id)net sendRequestMoreGameDataFailureLogSuccess:(id)success error:(id)error with:(id)with;
-(void)csMoreGameNet:(id)net sendRequestMoreGameDataFailureLogFailure:(id)failure with:(id)with;
-(void)csMoreGameNet:(id)net requestMoreGameMoreDataSuccess:(id)success;
-(void)csMoreGameNet:(id)net requestMoreGameMoreDataError:(id)error;
-(void)csMoreGameNet:(id)net requestMoreGameDataSuccess:(id)success;
-(void)csMoreGameNet:(id)net requestMoreGameDataError:(id)error;
-(void)csMoreGameCloseMoreGame:(id)game;
-(void)csMoreGame:(id)game fillMoreGameInto:(id)into;
-(void)csMoreGame:(id)game showMoreGameOnRootView:(id)view withScale:(float)scale;
-(void)csMoreGameLoadMoreGameData:(id)data;
-(unsigned)csMoreGameGetStatus:(id)status;
-(void)csMoreGameViewCloseAnimationEnd:(id)end;
-(void)csMoreGameViewWillClose:(id)csMoreGameView;
-(void)csMoreGameViewClickCloseButton:(id)button;
-(void)csMoreGameViewShowAnimationEnd:(id)end;
-(void)csMoreGameViewReload:(id)reload;
-(void)csMoreGameView:(id)view clickAd:(id)ad;
-(void)csMoreGameView:(id)view sendLogForClickWithParams:(id)params;
-(void)csMoreGameView:(id)view loadMoreWithParams:(id)params;
-(void)csMoreGameViewLoadFinished:(id)finished;
-(void)csMoreGameView:(id)view loadError:(id)error;
-(void)deleteClickLog:(id)log;
-(void)saveClickLog:(id)log;
-(void)deleteShowSuccessLog:(id)log;
-(void)saveShowSuccessLog:(id)log;
-(void)deleteRequestFailureLog:(id)log;
-(void)saveRequestFailureLog:(id)log;
-(void)unregisterMoreGame:(id)game;
-(void)registerMoreGame:(id)game;
-(void)notifOrientationDidChangeForPerform;
-(void)notifOrientationDidChange:(id)notifOrientation;
-(void)notifDidBecomeActive:(id)notif;
-(void)dealloc;
-(id)init;
@end

