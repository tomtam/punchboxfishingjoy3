/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import <Foundation/NSOperation.h>
#import "捕鱼达人3-Structs.h"

@class NSDictionary, NSString;
@protocol IMAnalyticsEventOperationDelegate;

@interface IMAnalyticsEvent : NSOperation {
	id<IMAnalyticsEventOperationDelegate> delegate;
	int eventType;
	double timestamp;
	double startSessionTS;
	NSDictionary* additionalParams;
	int eventId;
	NSString* sessionId;
	NSString* tag;
}
@property(copy, nonatomic) NSString* sessionId;	// @synthesize
@property(copy, nonatomic) NSString* tag;	// @synthesize
@property(assign, nonatomic) int eventId;	// @synthesize
@property(retain, nonatomic) NSDictionary* additionalParams;	// @synthesize
@property(assign, nonatomic) double startSessionTS;	// @synthesize
@property(assign, nonatomic) double timestamp;	// @synthesize
@property(assign, nonatomic) __weak id<IMAnalyticsEventOperationDelegate> delegate;	// @synthesize
@property(assign, nonatomic) int eventType;	// @synthesize
+(void)initialize;
+(void)updateConstants;
 -(void)setStartSessionTS:(double)ts;
 -(double)startSessionTS;
 -(void)setTag:(id)tag;
 -(id)tag;
 -(void)setEventId:(int)anId;
 -(int)eventId;
 -(void)setSessionId:(id)anId;
 -(id)sessionId;
 -(void)setTimestamp:(double)timestamp;
 -(double)timestamp;
 -(void)setAdditionalParams:(id)params;
 -(id)additionalParams;
 -(void)setEventType:(int)type;
 -(int)eventType;
 -(void)setDelegate:(id)delegate;
 -(id)delegate;
-(void).cxx_destruct;
-(void)dealloc;
-(id)dictionaryRepresentation;
-(void)convertJSONToAddnParams:(id)addnParams;
-(id)convertAddnParamstoJSON;
-(BOOL)isAdditionalParamsDictionaryValid:(id)valid;
-(void)main;
-(void)closeDatabase:(sqlite3*)database;
-(BOOL)insertIntoEventList:(sqlite3*)list withEventId:(int)eventId;
-(id)initWithEventType:(int)eventType;
-(id)init;
-(void)setDefaults;
@end

