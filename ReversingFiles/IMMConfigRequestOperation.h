/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import <Foundation/NSOperation.h>

@protocol IMMRuleRequestDelegate;

@interface IMMConfigRequestOperation : NSOperation {
	id<IMMRuleRequestDelegate> delegate;
}
@property(assign, nonatomic) __weak id<IMMRuleRequestDelegate> delegate;	// @synthesize
 -(void)setDelegate:(id)delegate;
 -(id)delegate;
-(void).cxx_destruct;
-(void)main;
@end

