/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "IMInterstitialAdManagerDelegate.h"
#import "IMWebViewDelegate.h"
#import "捕鱼达人3-Structs.h"
#import "UIAlertViewDelegate.h"
#import <UIKit/UIViewController.h>
#import "UIActionSheetDelegate.h"

@class UIBarButtonItem, UIActionSheet, IMInterstitialAdManager, NSString, IMWebView;
@protocol IMAdScreenDismissDelegate;

@interface IMEmbeddedWebViewController : UIViewController <IMWebViewDelegate, IMInterstitialAdManagerDelegate, UIActionSheetDelegate, UIAlertViewDelegate> {
	IMWebView* imWebView;
	UIBarButtonItem* safari;
	UIBarButtonItem* close;
	UIBarButtonItem* back;
	UIBarButtonItem* fwd;
	UIBarButtonItem* reload;
	id<IMAdScreenDismissDelegate> delegate;
	NSString* urlString;
	BOOL dismissFlag;
	BOOL isStatusBarVisible;
	BOOL isFirstLoad;
	UIActionSheet* actionSheet;
	IMInterstitialAdManager* interstitialManager;
}
@property(readonly, copy) NSString* debugDescription;
@property(readonly, copy) NSString* description;
@property(readonly, assign) Class superclass;
@property(readonly, assign) unsigned hash;
@property(retain, nonatomic) IMInterstitialAdManager* interstitialManager;	// @synthesize
@property(retain, nonatomic) IMWebView* imWebView;	// @synthesize
@property(copy, nonatomic) NSString* urlString;	// @synthesize
@property(assign) __weak id<IMAdScreenDismissDelegate> delegate;	// @synthesize
@property(retain, nonatomic) UIBarButtonItem* reload;	// @synthesize
@property(retain, nonatomic) UIBarButtonItem* fwd;	// @synthesize
@property(retain, nonatomic) UIBarButtonItem* back;	// @synthesize
@property(retain, nonatomic) UIBarButtonItem* close;	// @synthesize
@property(retain, nonatomic) UIBarButtonItem* safari;	// @synthesize
 -(void)setInterstitialManager:(id)manager;
 -(id)interstitialManager;
 -(void)setImWebView:(id)view;
 -(void)setUrlString:(id)string;
 -(id)urlString;
 -(void)setDelegate:(id)delegate;
 -(id)delegate;
 -(void)setReload:(id)reload;
 -(id)reload;
 -(void)setFwd:(id)fwd;
 -(id)fwd;
 -(void)setBack:(id)back;
 -(id)back;
 -(void)setClose:(id)close;
 -(id)close;
 -(void)setSafari:(id)safari;
 -(id)safari;
-(void).cxx_destruct;
-(void)adDidPerformUserInteraction:(id)ad;
-(void)adScreenDidClose:(id)adScreen;
-(void)adWillLeaveApplication:(id)ad;
 -(id)imWebView;
-(void)alertView:(id)view clickedButtonAtIndex:(int)index;
-(void)imWebView:(id)view linkWasClicked:(id)clicked;
-(void)imWebViewFormSubmissionStarted:(id)started;
-(void)imWebView:(id)view failedToLoad:(id)load;
-(void)imWebViewFinishedLoading:(id)loading;
-(void)imWebViewStartedLoading:(id)loading;
-(void)setNetworkActivityVisible:(BOOL)visible;
-(unsigned)supportedInterfaceOrientations;
-(BOOL)shouldAutorotate;
-(BOOL)shouldAutorotateToInterfaceOrientation:(int)interfaceOrientation;
-(void)viewDidDisappear:(BOOL)view;
-(void)viewDidAppear:(BOOL)view;
-(void)viewWillAppear:(BOOL)view;
-(id)flexiSpaceButton;
-(id)safariButton;
-(id)fwdButton;
-(id)reloadButton;
-(id)backButton;
-(id)closeButton;
-(CGImageRef)newFwdArrowImageRef;
-(CGImageRef)newBackArrowImageRef;
-(CGImageRef)newCloseImage;
-(CGContextRef)newContext;
-(void)actionSheet:(id)sheet clickedButtonAtIndex:(int)index;
-(void)openSafari;
-(void)refresh;
-(void)goFwd;
-(void)goBack;
-(void)closeMVC;
-(void)stopAllMedia;
-(void)checkButtonAction;
-(void)dealloc;
-(id)initWithUrlString:(id)urlString;
@end

