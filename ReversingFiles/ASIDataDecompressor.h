/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>
#import "捕鱼达人3-Structs.h"



@interface ASIDataDecompressor : NSObject {
	BOOL streamReady;
	z_stream_s zStream;
}
@property(readonly, assign) BOOL streamReady;	// @synthesize
+(id)inflateErrorWithCode:(int)code;
+(BOOL)uncompressDataFromFile:(id)file toFile:(id)file2 error:(id*)error;
+(id)uncompressData:(id)data error:(id*)error;
+(id)decompressor;
 -(BOOL)streamReady;
-(id)uncompressBytes:(char*)bytes length:(unsigned)length error:(id*)error;
-(id)closeStream;
-(id)setupStream;
-(void)dealloc;
@end

