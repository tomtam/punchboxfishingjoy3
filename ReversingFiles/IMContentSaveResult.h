/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>

@class NSMutableDictionary;

@interface IMContentSaveResult : NSObject {
	BOOL _saveContentStatus;
	int _errorCode;
	NSMutableDictionary* _resultDictionary;
}
@property(retain, nonatomic) NSMutableDictionary* resultDictionary;	// @synthesize=_resultDictionary
@property(assign, nonatomic) BOOL saveContentStatus;	// @synthesize=_saveContentStatus
@property(assign, nonatomic) int errorCode;	// @synthesize=_errorCode
 -(void)setResultDictionary:(id)dictionary;
 -(id)resultDictionary;
 -(void)setSaveContentStatus:(BOOL)status;
 -(BOOL)saveContentStatus;
 -(void)setErrorCode:(int)code;
 -(int)errorCode;
-(void).cxx_destruct;
@end

