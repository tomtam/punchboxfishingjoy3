/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "ISSPlatformShareInfo.h"
#import </libobjc.A.h>
#import "NSCoding.h"

@class NSDictionary, NSArray, NSString;

@interface SSWeChatShareInfo : NSObject <ISSPlatformShareInfo, NSCoding> {
	NSString* _text;
	NSArray* _urls;
	NSArray* _imgs;
	NSDictionary* _extInfo;
}
@property(retain, nonatomic) NSDictionary* extInfo;	// @synthesize=_extInfo
@property(retain, nonatomic) NSArray* imgs;	// @synthesize=_imgs
@property(retain, nonatomic) NSArray* urls;	// @synthesize=_urls
@property(copy, nonatomic) NSString* text;	// @synthesize=_text
 -(void)setExtInfo:(id)info;
 -(id)extInfo;
 -(void)setImgs:(id)imgs;
 -(id)imgs;
 -(void)setUrls:(id)urls;
 -(id)urls;
 -(void)setText:(id)text;
 -(id)text;
-(id)initWithCoder:(id)coder;
-(void)encodeWithCoder:(id)coder;
-(id)sid;
-(id)sourceData;
-(void)dealloc;
@end

