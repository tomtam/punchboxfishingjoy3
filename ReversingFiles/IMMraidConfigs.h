/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "IMBaseConfigs.h"

@class NSString;

@interface IMMraidConfigs : IMBaseConfigs {
	NSString* _mraidJSString;
}
@property(retain, nonatomic) NSString* mraidJSString;	// @synthesize=_mraidJSString
+(id)sharedConfigs;
 -(void)setMraidJSString:(id)string;
 -(id)mraidJSString;
-(void).cxx_destruct;
-(BOOL)updateFromDictionary:(id)dictionary needValidation:(BOOL)validation;
-(id)product;
-(id)init;
@end

