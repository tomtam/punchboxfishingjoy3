/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "NSObject.h"


@protocol IMAdDelegate <NSObject>
@optional
-(void)adView:(id)view didPerformUserInteractionWithParams:(id)params;
-(void)adViewWillLeaveApplication:(id)adView;
-(void)adViewDidDismissScreen:(id)adView;
-(void)adViewWillDismissScreen:(id)adView;
-(void)adViewWillPresentScreen:(id)adView;
-(void)adView:(id)view didFailRequestWithError:(id)error;
-(void)adViewDidFinishRequest:(id)adView;
@end

