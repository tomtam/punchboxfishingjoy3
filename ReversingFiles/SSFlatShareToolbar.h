/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "捕鱼达人3-Structs.h"
#import <UIKit/UIView.h>
#import "SSFlatShareToolbarItemViewDelegate.h"
#import "CMHTableViewDataSource.h"
#import "CMHTableViewDelegate.h"

@class CMHTableView, NSArray, UILabel;
@protocol ISSAuthOptions;

@interface SSFlatShareToolbar : UIView <CMHTableViewDataSource, CMHTableViewDelegate, SSFlatShareToolbarItemViewDelegate> {
	CMHTableView* _platTableView;
	UILabel* _textLabel;
	int _platType;
	NSArray* _platformArray;
	id<ISSAuthOptions> _authOptions;
	NSArray* selectedPlatforms;
}
@property(readonly, assign, nonatomic) NSArray* selectedPlatforms;	// @synthesize
-(void)itemOnClick:(id)click;
-(id)tableView:(id)view itemForIndexPath:(id)indexPath;
-(int)itemNumberOfTableView:(id)tableView;
-(void)updateWithType:(int)type platforms:(id)platforms authOptions:(id)options;
 -(id)selectedPlatforms;
-(void)dealloc;
-(id)initWithFrame:(CGRect)frame;
@end

