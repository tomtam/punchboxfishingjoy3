/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "IMAdManagerDelegate.h"


@protocol IMInterstitialAdManagerDelegate <IMAdManagerDelegate>
-(void)orientationPropertiesDidChanged:(id)orientationProperties;
-(void)adScreenDidClose:(id)adScreen;
@optional
-(BOOL)didInterstitialPresent;
-(void)presentedScreenDidDismiss:(id)presentedScreen;
@end

