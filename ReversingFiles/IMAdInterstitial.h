/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "IMInterstitialAdManagerDelegate.h"
#import "IMWebViewDelegate.h"
#import "IMHttpAdRequestDelegate.h"
#import </libobjc.A.h>

@class IMInterstitialAdWebViewController, NSDictionary, NSDate, IMInterstitialAdManager, NSTimer, NSString, IMHttpAdRequest, IMWebView;
@protocol IMAdInterstitialDelegate, IMAdIncentivisedDelegate;

@interface IMAdInterstitial : NSObject <IMHttpAdRequestDelegate, IMWebViewDelegate, IMInterstitialAdManagerDelegate> {
	BOOL isStatusBarHidden;
	BOOL isAnimated;
	BOOL needMetricCollection;
	BOOL isExpandedWithViewController;
	BOOL wasClosed;
	BOOL canRunPlayable;
	BOOL playablePrefetchSucceded;
	BOOL isPlayableAvailable;
	BOOL _playablePresented;
	id<IMAdInterstitialDelegate> delegate;
	int state;
	IMHttpAdRequest* httpAdRequest;
	NSTimer* adRenderingFailedTimer;
	IMWebView* imWebView;
	IMInterstitialAdManager* intAdManager;
	NSDate* startTime;
	NSString* adImpID;
	NSString* adServerUrl;
	id playableManager;
	id<IMAdIncentivisedDelegate> _incentivisedDelegate;
	IMInterstitialAdWebViewController* _interstitialAdWebViewController;
	NSDictionary* _playableSettings;
	unsigned _campaignId;
	double lastAdTimeStamp;
	double adFetchStartTimeInterval;
}
@property(readonly, copy) NSString* debugDescription;
@property(readonly, copy) NSString* description;
@property(readonly, assign) Class superclass;
@property(readonly, assign) unsigned hash;
@property(assign, nonatomic) BOOL playablePresented;	// @synthesize=_playablePresented
@property(assign, nonatomic) unsigned campaignId;	// @synthesize=_campaignId
@property(retain, nonatomic) NSDictionary* playableSettings;	// @synthesize=_playableSettings
@property(assign, nonatomic) BOOL playablePrefetchSucceded;	// @synthesize
@property(assign, nonatomic) BOOL canRunPlayable;	// @synthesize
@property(assign, nonatomic) BOOL isPlayableAvailable;	// @synthesize
@property(retain, nonatomic) id playableManager;	// @synthesize
@property(assign, nonatomic) BOOL wasClosed;	// @synthesize
@property(assign, nonatomic) BOOL isExpandedWithViewController;	// @synthesize
@property(retain, nonatomic) IMInterstitialAdWebViewController* interstitialAdWebViewController;	// @synthesize=_interstitialAdWebViewController
@property(assign, nonatomic) double adFetchStartTimeInterval;	// @synthesize
@property(copy, nonatomic) NSString* adImpID;	// @synthesize
@property(retain, nonatomic) NSDate* startTime;	// @synthesize
@property(assign, nonatomic) BOOL needMetricCollection;	// @synthesize
@property(retain, nonatomic) IMInterstitialAdManager* intAdManager;	// @synthesize
@property(retain, nonatomic) IMWebView* imWebView;	// @synthesize
@property(assign, nonatomic) double lastAdTimeStamp;	// @synthesize
@property(assign, nonatomic) __weak NSTimer* adRenderingFailedTimer;	// @synthesize
@property(retain, nonatomic) IMHttpAdRequest* httpAdRequest;	// @synthesize
@property(assign, nonatomic) BOOL isAnimated;	// @synthesize
@property(assign, nonatomic) BOOL isStatusBarHidden;	// @synthesize
@property(readonly, assign, nonatomic) int state;	// @synthesize
@property(copy, nonatomic) NSString* adServerUrl;	// @synthesize
@property(assign, nonatomic) __weak id<IMAdIncentivisedDelegate> incentivisedDelegate;	// @synthesize=_incentivisedDelegate
@property(assign, nonatomic) __weak id<IMAdInterstitialDelegate> delegate;	// @synthesize
 -(void)setPlayablePresented:(BOOL)presented;
 -(BOOL)playablePresented;
 -(void)setCampaignId:(unsigned)anId;
 -(unsigned)campaignId;
 -(void)setPlayableSettings:(id)settings;
 -(id)playableSettings;
 -(void)setInterstitialAdWebViewController:(id)controller;
 -(id)interstitialAdWebViewController;
 -(void)setIncentivisedDelegate:(id)delegate;
 -(id)incentivisedDelegate;
 -(void)setIsPlayableAvailable:(BOOL)available;
 -(BOOL)isPlayableAvailable;
 -(void)setPlayablePrefetchSucceded:(BOOL)succeded;
 -(BOOL)playablePrefetchSucceded;
 -(void)setCanRunPlayable:(BOOL)playable;
 -(BOOL)canRunPlayable;
 -(void)setPlayableManager:(id)manager;
 -(id)playableManager;
 -(void)setWasClosed:(BOOL)closed;
 -(BOOL)wasClosed;
 -(void)setIsExpandedWithViewController:(BOOL)viewController;
 -(BOOL)isExpandedWithViewController;
 -(void)setAdFetchStartTimeInterval:(double)interval;
 -(double)adFetchStartTimeInterval;
 -(void)setAdServerUrl:(id)url;
 -(id)adServerUrl;
 -(void)setAdImpID:(id)anId;
 -(id)adImpID;
 -(void)setStartTime:(id)time;
 -(id)startTime;
 -(void)setNeedMetricCollection:(BOOL)collection;
 -(BOOL)needMetricCollection;
 -(void)setIsAnimated:(BOOL)animated;
 -(BOOL)isAnimated;
 -(void)setIntAdManager:(id)manager;
 -(id)intAdManager;
 -(void)setImWebView:(id)view;
 -(void)setLastAdTimeStamp:(double)stamp;
 -(double)lastAdTimeStamp;
 -(void)setAdRenderingFailedTimer:(id)timer;
 -(id)adRenderingFailedTimer;
 -(void)setHttpAdRequest:(id)request;
 -(id)httpAdRequest;
 -(void)setIsStatusBarHidden:(BOOL)hidden;
 -(BOOL)isStatusBarHidden;
 -(int)state;
 -(id)delegate;
-(void).cxx_destruct;
-(void)registerPlayable;
-(void)loadPlayableSettings:(id)settings;
-(void)addPlayable;
-(void)displayBackupInterstitial:(BOOL)interstitial;
-(void)adManager:(id)manager fetchedPlayableSettings:(id)settings;
-(void)adManagerFiredFailed:(id)failed;
-(void)adManagerFiredReady:(id)ready;
-(void)doIncentCompleteCallback:(id)callback;
-(void)doUserInteractionCallback:(id)callback;
-(void)doAdWillLeaveCallback;
-(void)doAdDidDismissScreenCallback;
-(void)doAdWillDismissScreenCallback;
-(void)doAdWillPresentScreenCallback;
-(void)doAdPresentFailedCallback:(id)callback;
-(void)doAdFailedCallback:(id)callback;
-(void)doAdSuccessCallback;
-(void)incentActionComplete:(id)complete;
-(void)adDidPerformUserInteraction:(id)ad;
-(void)performAdWillLeaveCallback;
-(void)performAdDidDismissScreenCallback;
-(void)performAdWillDismissScreenCallback;
-(void)performAdWillPresentScreenCallback;
-(void)performAdPresentFailedCallback:(id)callback;
-(void)performAdFailedCallback:(id)callback errorCode:(int)code;
-(void)performAdFailedCallback:(id)callback;
-(void)performAdSuccessCallback;
-(BOOL)isModal;
-(void)checkAdUnitLoadComplete;
-(void)interstitialWillLeaveApp;
-(void)interstitialDidDismiss;
-(void)interstitialWillDismiss;
-(void)interstitialFailedToLoadWithError:(id)error;
-(void)interstitialFinishedLoading;
-(BOOL)didInterstitialPresent;
-(void)presentedScreenDidDismiss:(id)presentedScreen;
-(void)orientationPropertiesDidChanged:(id)orientationProperties;
-(void)adScreenDidClose:(id)adScreen;
-(void)adWillLeaveApplication:(id)ad;
 -(id)imWebView;
-(BOOL)canPerformDismissScreenCallback;
-(BOOL)canDismissInterstitialViewController;
-(void)imWebView:(id)view linkWasClicked:(id)clicked;
-(void)imWebViewFormSubmissionStarted:(id)started;
-(void)imWebView:(id)view failedToLoad:(id)load;
-(void)imWebViewFinishedLoading:(id)loading;
-(void)imWebViewStartedLoading:(id)loading;
-(void)imHttpAdRequest:(id)request didFinishWithResponse:(id)response andData:(id)data;
-(void)imHttpAdRequest:(id)request didFailWithBadResponse:(id)badResponse error:(id)error;
-(void)imHttpAdRequest:(id)request didFailToConnectWithError:(id)error;
-(void)presentFromRootViewController:(id)rootViewController animated:(BOOL)animated;
-(void)presentInterstitialAnimated:(BOOL)animated;
-(void)displayInterstitialAnimated:(BOOL)animated;
-(void)presentPlayableAd;
-(void)beginInterstitialPresentAnimationWithViewController;
-(void)beginInterstitialPresentAnimation;
-(void)showAnimationCompleted;
-(void)closeAnimationFinished;
-(void)stopLoading;
-(void)loadRequest:(id)request;
-(void)doAdRequestOnMainThread:(id)thread;
-(void)doAdRequest:(id)request;
-(void)startAdRenderingFailedTimer;
-(void)invalidateAdRenderingFailedTimer;
 -(void)setDelegate:(id)delegate;
-(void)releaseViews;
-(void)createViews;
-(void)createViewsOnMainThread;
-(void)dealloc;
-(id)init;
@end

