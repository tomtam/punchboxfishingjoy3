/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "SSCLogInfo.h"

@class NSString;

@interface SSCApiLogInfo : SSCLogInfo {
	int _shareType;
	NSString* _api;
}
+(id)apiLogWithType:(int)type api:(id)api account:(id)account;
-(id)stringValue;
-(void)dealloc;
-(id)initWithType:(int)type api:(id)api account:(id)account;
@end

