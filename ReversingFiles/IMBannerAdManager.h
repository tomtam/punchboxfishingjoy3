/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "IMWebViewDelegate.h"
#import "IMAdManager.h"
#import "捕鱼达人3-Structs.h"

@class IMExpandAdWebViewController, IMWebView, UIActivityIndicatorView, NSString;
@protocol IMBannerAdManagerDelegate;

@interface IMBannerAdManager : IMAdManager <IMWebViewDelegate> {
	UIActivityIndicatorView* spinningWheel;
	IMWebView* expandedWebView;
	BOOL isExpandedWithViewController;
	BOOL wasClosed;
	id<IMBannerAdManagerDelegate> delegate;
	IMExpandAdWebViewController* _imExpandWebViewController;
}
@property(readonly, copy) NSString* debugDescription;
@property(readonly, copy) NSString* description;
@property(readonly, assign) Class superclass;
@property(readonly, assign) unsigned hash;
@property(assign, nonatomic) BOOL wasClosed;	// @synthesize
@property(assign, nonatomic) BOOL isExpandedWithViewController;	// @synthesize
@property(retain, nonatomic) IMExpandAdWebViewController* imExpandWebViewController;	// @synthesize=_imExpandWebViewController
@property(retain, nonatomic) IMWebView* expandedWebView;	// @synthesize
@property(assign, nonatomic) id<IMBannerAdManagerDelegate> delegate;	// @synthesize
 -(void)setImExpandWebViewController:(id)controller;
 -(id)imExpandWebViewController;
 -(void)setWasClosed:(BOOL)closed;
 -(BOOL)wasClosed;
 -(void)setIsExpandedWithViewController:(BOOL)viewController;
 -(BOOL)isExpandedWithViewController;
 -(void)setExpandedWebView:(id)view;
 -(id)expandedWebView;
 -(id)delegate;
-(void).cxx_destruct;
-(void)activityScreenDidDismiss:(id)activityScreen;
-(void)appActiveNotificationReceived:(id)received;
-(void)appInActiveNotificationReceived:(id)activeNotificationReceived;
-(void)adScreenDidDismiss:(id)adScreen;
-(void)adScreenWillDismiss:(id)adScreen;
-(void)adScreenWillDisplay:(id)adScreen;
-(void)adScreenWillDisplay;
-(void)imWebView:(id)view linkWasClicked:(id)clicked;
-(void)imWebViewFormSubmissionStarted:(id)started;
-(void)imWebView:(id)view failedToLoad:(id)load;
-(void)imWebViewFinishedLoading:(id)loading;
-(void)imWebViewStartedLoading:(id)loading;
-(void)openMap:(id)map withUrlString:(id)urlString andFullScreen:(BOOL)screen;
-(CGRect)webFrame;
-(void)addToSKStore:(id)skstore;
-(void)resizeInWebView:(id)webView;
-(CGRect)getResizeCloseFrameFor:(int)aFor bounds:(CGSize)bounds;
-(void)setExpandProperties;
-(void)setOrientatinProperties;
-(void)expandWebViewWithViewController:(id)viewController;
-(void)expandWebViewWithView:(id)view;
-(void)expandWithURL:(id)url inWebView:(id)webView;
-(void)rotateExpandedWindowsToCurrentOrientation;
-(void)closeAd;
-(void)closeAd:(id)ad;
-(void)removeSpinningWheel;
-(BOOL)shouldRotateToInterfaceOrientation:(int)interfaceOrientation;
-(id)imWebView;
 -(void)setDelegate:(id)delegate;
-(void)dealloc;
-(id)init;
@end

