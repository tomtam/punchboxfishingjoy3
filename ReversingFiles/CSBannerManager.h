/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "CSBannerViewPrivateDelegate.h"
#import "CSBannerNetDelegate.h"
#import "CSWebSiteViewDelegate.h"
#import </libobjc.A.h>

@class CSBannerView, NSMutableSet, NSMutableDictionary, NSString, CSWebSiteView, NSMutableArray, UIViewController, CSBannerNet;

@interface CSBannerManager : NSObject <CSBannerViewPrivateDelegate, CSBannerNetDelegate, CSWebSiteViewDelegate> {
	CSBannerNet* _bannerNet;
	NSMutableSet* _msetBannerView;
	NSMutableArray* _marrRequestFailureLog;
	NSMutableDictionary* _mdicNoNetRequestFailureLog;
	NSMutableArray* _marrLoadFailureLog;
	NSMutableArray* _marrShowSuccessLog;
	NSMutableArray* _marrClickLog;
	BOOL _batchLogSending;
	int _indexBanner;
	CSBannerView* _bannerViewClicked;
	CSWebSiteView* _webSiteView;
	UIViewController* _topVC;
	NSString* _placementID;
}
@property(readonly, copy) NSString* debugDescription;
@property(readonly, copy) NSString* description;
@property(readonly, assign) Class superclass;
@property(readonly, assign) unsigned hash;
@property(copy, nonatomic) NSString* placementID;	// @synthesize=_placementID
@property(assign, nonatomic) BOOL batchLogSending;	// @synthesize=_batchLogSending
@property(retain, nonatomic) UIViewController* topVC;	// @synthesize=_topVC
@property(retain, nonatomic) CSWebSiteView* webSiteView;	// @synthesize=_webSiteView
@property(retain, nonatomic) CSBannerView* bannerViewClicked;	// @synthesize=_bannerViewClicked
@property(assign, nonatomic) int indexBanner;	// @synthesize=_indexBanner
+(id)sharedBannerManager;
 -(void)setPlacementID:(id)anId;
 -(id)placementID;
 -(void)setBatchLogSending:(BOOL)sending;
 -(BOOL)batchLogSending;
 -(void)setTopVC:(id)vc;
 -(id)topVC;
 -(void)setWebSiteView:(id)view;
 -(id)webSiteView;
 -(void)setBannerViewClicked:(id)clicked;
 -(id)bannerViewClicked;
 -(void)setIndexBanner:(int)banner;
 -(int)indexBanner;
-(void)animationDidStop:(id)animation finished:(id)finished context:(void*)context;
-(void)csWebSiteViewClickClose:(id)close;
-(void)csBannerNet:(id)net batchSendBannerFailureLogSuccess:(id)success with:(id)with;
-(void)csBannerNet:(id)net batchSendBannerFailureLogFailure:(id)failure and:(id)anAnd;
-(void)csBannerNet:(id)net sendBannerShowErrorLogSuccess:(id)success with:(id)with error:(id)error;
-(void)csBannerNet:(id)net sendBannerShowErrorLogWith:(id)with and:(id)anAnd;
-(void)csBannerNet:(id)net sendBannerShowSuccessLogSuccess:(id)success with:(id)with;
-(void)csBannerNet:(id)net sendBannerShowSuccessLogFailureWith:(id)with;
-(void)csBannerNet:(id)net sendBannerClickLogSuccess:(id)success with:(id)with;
-(void)csBannerNet:(id)net sendBannerClickLogFailureWith:(id)with;
-(void)csBannerNet:(id)net sendRequestBannerDataFailureLogSuccess:(id)success with:(id)with requestUrl:(id)url;
-(void)csBannerNet:(id)net sendRequestBannerDataFailureLogFailure:(id)failure with:(id)with;
-(void)csBannerNet:(id)net requestBannerDataSuccess:(id)success withBannerIdentifier:(id)bannerIdentifier;
-(void)csBannerNet:(id)net requestBannerDataError:(id)error withBannerIdentifier:(id)bannerIdentifier;
-(void)csBannerView:(id)view clickAd:(id)ad;
-(void)csBannerView:(id)view sendLogForClickWithParams:(id)params with:(unsigned)with;
-(void)csBannerView:(id)view sendLogForShowSuccessWith:(unsigned)with;
-(void)csBannerView:(id)view sendLogForLoadError:(id)loadError;
-(void)csBannerViewRequestBanner:(id)banner;
-(void)restoreRequestBanner;
-(void)registerBannerView:(id)view;
-(void)notifOrientationDidChangeForPerform;
-(void)notifOrientationDidChange:(id)notifOrientation;
-(void)notifDidBecomeActive:(id)notif;
-(void)dealloc;
-(id)init;
@end

