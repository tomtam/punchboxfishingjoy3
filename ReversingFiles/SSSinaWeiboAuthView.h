/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "UIWebViewDelegate.h"
#import <UIKit/UIView.h>

@class SSSinaWeiboApp, UIWebView, CMLoadingView;
@protocol ISSCRequest;

@interface SSSinaWeiboAuthView : UIView <UIWebViewDelegate> {
	UIWebView* _webView;
	CMLoadingView* _loadingView;
	SSSinaWeiboApp* _app;
	id<ISSCRequest> _request;
	id _result;
}
-(void)webView:(id)view didFailLoadWithError:(id)error;
-(void)webViewDidFinishLoad:(id)webView;
-(void)webViewDidStartLoad:(id)webView;
-(BOOL)webView:(id)view shouldStartLoadWithRequest:(id)request navigationType:(int)type;
-(void)cancel;
-(void)setLoading:(BOOL)loading;
-(void)loadRequest:(id)request result:(id)result;
-(void)dealloc;
-(id)initWithApp:(id)app;
@end

