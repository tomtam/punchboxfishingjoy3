/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "NSObject.h"


@protocol HSHTTPConnectionDelegate <NSObject>
@optional
-(void)httpConnect:(id)connect finish:(id)finish with:(id)with;
-(void)httpConnect:(id)connect receivePartData:(id)data with:(id)with;
-(void)httpConnect:(id)connect receiveResponseWithStatusCode:(int)statusCode andAllHeaderFields:(id)fields with:(id)with;
-(void)httpConnect:(id)connect error:(id)error with:(id)with;
@end

