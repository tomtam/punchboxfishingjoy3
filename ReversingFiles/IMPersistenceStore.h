/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>
#import "捕鱼达人3-Structs.h"


@interface IMPersistenceStore : NSObject {
}
+(BOOL)executeDeleteQuery:(id)query inDB:(id)db;
+(id)fetchDataFromDB:(id)db withQuery:(id)query;
+(BOOL)updateAndInsertData:(id)data andKey:(id)key usingQuery:(id)query updateQuery:(id)query4 dbPath:(id)path;
+(BOOL)createTableWithQuery:(id)query dbPath:(id)path;
+(id)initDatabase:(id)database withQuery:(id)query;
+(BOOL)openDb:(sqlite3**)db withPath:(id)path;
+(id)databasePathWithName:(id)name forProduct:(id)product;
@end

