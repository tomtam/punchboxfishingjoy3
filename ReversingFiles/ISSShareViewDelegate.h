/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "ISSViewDelegate.h"


@protocol ISSShareViewDelegate <ISSViewDelegate>
@optional
-(void)viewOnCancelPublish:(id)publish;
-(id)view:(id)view willPublishContent:(id)content shareList:(id)list;
-(id)view:(id)view willPublishContent:(id)content;
@end

