/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "捕鱼达人3-Structs.h"
#import </libobjc.A.h>
#import "NSFastEnumeration.h"

@class NSSet;

@interface FlurryPLCrashMachExceptionPortSet : NSObject <NSFastEnumeration> {
	NSSet* _state_set;
	plcrash_mach_exception_port_set _asyncSafeRepresentation;
}
@property(readonly, assign, nonatomic) plcrash_mach_exception_port_set asyncSafeRepresentation;	// @synthesize=_asyncSafeRepresentation
@property(readonly, assign, nonatomic) NSSet* set;	// @synthesize=_state_set
 -(id)set;
 -(plcrash_mach_exception_port_set)asyncSafeRepresentation;
-(unsigned)countByEnumeratingWithState:(XXStruct_kFm5bA*)state objects:(id*)objects count:(unsigned)count;
-(void)dealloc;
-(id)initWithAsyncSafeRepresentation:(plcrash_mach_exception_port_set)asyncSafeRepresentation;
-(id)initWithSet:(id)set;
@end

