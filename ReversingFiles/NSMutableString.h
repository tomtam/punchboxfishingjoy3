/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import <Foundation/NSMutableString.h>
#import "捕鱼达人3-Structs.h"


@interface NSMutableString (RegexKitLiteAdditions)
-(int)replaceOccurrencesOfRegex:(id)regex options:(unsigned)options inRange:(NSRange)range error:(id*)error enumerationOptions:(unsigned)options5 usingBlock:(id)block;
-(int)replaceOccurrencesOfRegex:(id)regex usingBlock:(id)block;
-(int)replaceOccurrencesOfRegex:(id)regex withString:(id)string options:(unsigned)options range:(NSRange)range error:(id*)error;
-(int)replaceOccurrencesOfRegex:(id)regex withString:(id)string range:(NSRange)range;
-(int)replaceOccurrencesOfRegex:(id)regex withString:(id)string;
@end

