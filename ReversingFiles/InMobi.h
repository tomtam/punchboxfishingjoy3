/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>


@interface InMobi : NSObject {
}
+(void)removeUserID:(int)anId;
+(void)addUserID:(int)anId withValue:(id)value;
+(void)setLocationWithCity:(id)city state:(id)state country:(id)country;
+(void)setLocationWithLatitude:(float)latitude longitude:(float)longitude accuracy:(float)accuracy;
+(void)setInterests:(id)interests;
+(void)setAreaCode:(id)code;
+(void)setPostalCode:(id)code;
+(void)setLanguage:(id)language;
+(void)setHasChildren:(int)children;
+(void)setSexualOrientation:(int)orientation;
+(void)setMaritalStatus:(int)status;
+(void)setAge:(int)age;
+(void)setIncome:(int)income;
+(void)setDateOfBirthWithMonth:(unsigned)month day:(unsigned)day year:(unsigned)year;
+(void)setDateOfBirth:(id)birth;
+(void)setEthnicity:(int)ethnicity;
+(void)setEducation:(int)education;
+(void)setGender:(int)gender;
+(id)getVersion;
+(void)setDeviceIdMask:(int)mask;
+(void)setLogLevel:(int)level;
+(void)initialize:(id)initialize;
@end

