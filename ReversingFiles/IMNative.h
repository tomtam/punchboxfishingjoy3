/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import </libobjc.A.h>
#import "IMWebViewControllerDelegate.h"
#import "IMNativeAdManagerDelegate.h"

@class NSDictionary, NSMutableArray, NSString, IMNativeAdManager, IMWebViewController;
@protocol IMNativeDelegate;

@interface IMNative : NSObject <IMWebViewControllerDelegate, IMNativeAdManagerDelegate> {
	BOOL _isAttached;
	BOOL _isWebViewLoaded;
	BOOL _shouldFireImpression;
	BOOL _isNativeLoaded;
	IMNativeAdManager* adManager;
	NSString* content;
	NSString* contextCode;
	NSString* namespace;
	IMWebViewController* hiddenWebViewController;
	NSMutableArray* eventList;
	NSString* appId;
	int currentState;
	NSDictionary* additionaParameters;
	NSString* refTagKey;
	NSString* refTagValue;
	NSString* keywords;
	id<IMNativeDelegate> _delegate;
	long long _slotId;
}
@property(readonly, copy) NSString* debugDescription;
@property(readonly, copy) NSString* description;
@property(readonly, assign) Class superclass;
@property(readonly, assign) unsigned hash;
@property(assign) BOOL isNativeLoaded;	// @synthesize=_isNativeLoaded
@property(retain, nonatomic) IMNativeAdManager* adManager;	// @synthesize
@property(assign) int currentState;	// @synthesize
@property(assign) long long slotId;	// @synthesize=_slotId
@property(retain, nonatomic) NSString* appId;	// @synthesize
@property(assign) BOOL shouldFireImpression;	// @synthesize=_shouldFireImpression
@property(assign) BOOL isWebViewLoaded;	// @synthesize=_isWebViewLoaded
@property(assign) BOOL isAttached;	// @synthesize=_isAttached
@property(retain) NSMutableArray* eventList;	// @synthesize
@property(retain, nonatomic) IMWebViewController* hiddenWebViewController;	// @synthesize
@property(retain, nonatomic) NSString* namespace;	// @synthesize
@property(retain, nonatomic) NSString* contextCode;	// @synthesize
@property(copy, nonatomic) NSString* refTagValue;	// @synthesize
@property(copy, nonatomic) NSString* refTagKey;	// @synthesize
@property(copy, nonatomic) NSString* keywords;	// @synthesize
@property(retain, nonatomic) NSDictionary* additionaParameters;	// @synthesize
@property(assign, nonatomic) __weak id<IMNativeDelegate> delegate;	// @synthesize=_delegate
@property(retain) NSString* content;	// @synthesize
 -(void)setIsNativeLoaded:(BOOL)loaded;
 -(BOOL)isNativeLoaded;
 -(void)setSlotId:(long long)anId;
 -(long long)slotId;
 -(void)setShouldFireImpression:(BOOL)fireImpression;
 -(BOOL)shouldFireImpression;
 -(void)setIsWebViewLoaded:(BOOL)loaded;
 -(BOOL)isWebViewLoaded;
 -(void)setIsAttached:(BOOL)attached;
 -(BOOL)isAttached;
 -(void)setDelegate:(id)delegate;
 -(id)delegate;
 -(void)setKeywords:(id)keywords;
 -(id)keywords;
 -(void)setRefTagValue:(id)value;
 -(id)refTagValue;
 -(void)setRefTagKey:(id)key;
 -(id)refTagKey;
 -(void)setAdditionaParameters:(id)parameters;
 -(id)additionaParameters;
 -(void)setCurrentState:(int)state;
 -(int)currentState;
 -(void)setAppId:(id)anId;
 -(id)appId;
 -(void)setEventList:(id)list;
 -(id)eventList;
 -(void)setHiddenWebViewController:(id)controller;
 -(id)hiddenWebViewController;
 -(void)setNamespace:(id)aNamespace;
 -(id)namespace;
 -(void)setContextCode:(id)code;
 -(id)contextCode;
 -(void)setContent:(id)content;
 -(id)content;
 -(void)setAdManager:(id)manager;
 -(id)adManager;
-(void).cxx_destruct;
-(void)dealloc;
-(void)nativeAdManager:(id)manager failedFetchingNativeAdWithError:(id)error;
-(void)nativeAdManager:(id)manager fetchedNativeAdWithContent:(id)content;
-(void)webViewController:(id)controller failedLoadingWebViewWithError:(id)error;
-(void)webViewControllerDidAppear:(id)webViewController;
-(void)webViewControllerDidFinishLoadingWebView:(id)webViewController;
-(void)recordEvent:(unsigned)event withParams:(id)params;
-(void)handleImpression:(id)impression;
-(void)handleClick:(id)click;
-(void)detachFromView;
-(void)forceDetachNativeAdFromParentView;
-(void)attachToView:(id)view;
-(void)loadContextCodeInWVController;
-(void)loadAd;
-(void)sendSuccessCallbackInBackground;
-(void)sendErrorInBackground:(id)background;
-(id)initWithAppId:(id)appId;
-(id)init;
-(void)tearDownNativeAd;
-(void)setupNativeAd;
@end

