/**
 * This header is generated by class-dump-z 0.2-0.
 * class-dump-z is Copyright (C) 2009 by KennyTM~, licensed under GPLv3.
 *
 * Source: (null)
 */

#import "NSObject.h"


@protocol ISSShareView <NSObject>
-(void)dismiss;
-(void)showInContainer:(id)container content:(id)content shareType:(int)type statusBarTips:(BOOL)tips authOptions:(id)options shareOptions:(id)options6 result:(id)result;
@end

